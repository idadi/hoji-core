package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.response.helper.HojiLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gitahi
 */
public class LocationResponse extends Response<HojiLocation, String> {

    /**
     * @should set the actual value to the date represented by the long value if actual is false.
     */
    protected LocationResponse(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        super(hojiContext, liveField, value, actual);
    }

    @Override
    protected HojiLocation toActualValue(String value) {
        HojiLocation hojiLocation = null;
        List<String> tokens = Utils.tokenizeString(value, "\\|", true);
        if (tokens.size() == 5) {
            hojiLocation = new HojiLocation(
                Utils.parseDouble(tokens.get(0)),
                Utils.parseDouble(tokens.get(1)),
                Utils.parseDouble(tokens.get(2)),
                Utils.parseDouble(tokens.get(3)),
                tokens.get(4)
            );
        }
        return hojiLocation;
    }

    /**
     * {@inheritDoc}
     *
     * @should return the actual date value as a long.
     */
    @Override
    public String toStorageValue() {
        return isAnswered() ? createValueString() : null;
    }

    @Override
    public Comparable comparableFromActualValue() {
        Double accuracy = null;
        if (getActualValue() != null) {
            accuracy = getActualValue().getAccuracy();
        }
        return accuracy;
    }

    /**
     * {@inheritDoc}
     *
     * @should return the actual date formatted to the current locale.
     */
    @Override
    public String displayString(boolean missingText) {
        if (getActualValue() != null) {
            if (getActualValue().getAddress() != null) {
                return getActualValue().getAddress();
            } else {
                return getActualValue().getLatitude() + ", " + getActualValue().getLongitude();
            }
        } else {
            return "";
        }
    }

    @Override
    public List<DataElement> getValue(String renderingType) {
        String type = liveField.getField().getDataElementType(renderingType);
        List<DataElement> dataElements = new ArrayList<>();
        if (getActualValue() != null) {
            dataElements.add(new DataElement(getLabel("Latitude"), getActualValue().getLatitude() != null
                ? getActualValue().getLatitude().toString() : "", type, getFormat()));
            dataElements.add(new DataElement(getLabel("Longitude"), getActualValue().getLongitude() != null
                ? getActualValue().getLongitude().toString() : "", type, getFormat()));
            dataElements.add(new DataElement(getLabel("Altitude"), getActualValue().getAltitude() != null
                ? getActualValue().getAltitude().toString() : "", type, getFormat()));
            dataElements.add(new DataElement(getLabel("Accuracy"), getActualValue().getAccuracy() != null
                ? getActualValue().getAccuracy().toString() : "", type, getFormat()));
            dataElements.add(new DataElement(getLabel("Address"), getActualValue().getAddress() != null
                ? getActualValue().getAddress() : "", DataElement.Type.STRING, getFormat()));
        } else {
            dataElements.add(new DataElement(getLabel("Latitude"), "", type, getFormat()));
            dataElements.add(new DataElement(getLabel("Longitude"), "", type, getFormat()));
            dataElements.add(new DataElement(getLabel("Altitude"), "", type, getFormat()));
            dataElements.add(new DataElement(getLabel("Accuracy"), "", type, getFormat()));
            dataElements.add(new DataElement(getLabel("Address"), "", DataElement.Type.STRING, getFormat()));
        }
        return dataElements;
    }

    private String createValueString() {
        List<String> tokens = new ArrayList<>();
        tokens.add(String.valueOf(getActualValue().getLatitude()));
        tokens.add(String.valueOf(getActualValue().getLongitude()));
        tokens.add(String.valueOf(getActualValue().getAltitude()));
        tokens.add(String.valueOf(getActualValue().getAccuracy()));
        tokens.add(getActualValue().getAddress());
        return Utils.string(tokens, "|", null, false);
    }
}
