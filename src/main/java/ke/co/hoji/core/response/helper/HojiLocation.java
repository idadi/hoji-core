package ke.co.hoji.core.response.helper;

public class HojiLocation {

    private final Double latitude;
    private final Double longitude;
    private final Double altitude;
    private final Double accuracy;
    private final String address;

    public HojiLocation(Double latitude, Double longitude, Double altitude, Double accuracy, String address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.accuracy = accuracy;
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getAltitude() {
        return altitude;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public String getAddress() {
        return address;
    }
}
