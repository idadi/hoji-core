package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Reference;
import ke.co.hoji.core.data.ReferenceLevel;
import ke.co.hoji.core.service.model.ReferenceService;

/**
 * @author gitahi
 */
public class ReferenceResponse extends Response<Reference, Long> {

    private Reference cachedActualValue;

    /**
     * @should set the actual value to the {@link Reference} represented by the Long value if actual is false.
     */
    protected ReferenceResponse(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        super(hojiContext, liveField, value, actual);
    }

    @Override
    protected Reference toActualValue(Long value) {
        if (cachedActualValue == null) {
            ReferenceService referenceService = hojiContext.getModelServiceManager().getReferenceService();
            Reference search = new Reference(value.intValue(), referenceService.getReferenceLevels());
            cachedActualValue = referenceService.search(search);
        }
        return cachedActualValue;
    }

    /**
     * {@inheritDoc}
     *
     * @should return the Long of the actual Record.
     */
    @Override
    public Long toStorageValue() {
        return getActualValue() != null ? getActualValue().getId().longValue() : null;
    }

    /**
     * {@inheritDoc}
     *
     * @should return the value of the first rulable level, if any.
     *
     * @should return empty string if no rulable level is found.
     */
    @Override
    public String ruleString() {
        if (getActualValue() != null) {
            String ruleString = "";
            for (ReferenceLevel referenceLevel : getActualValue().getReferenceLevels()) {
                if (referenceLevel.isRulable() && getActualValue().getValues() != null) {
                    String value = getActualValue().getValues().get(referenceLevel.getLevel());
                    if (value != null) {
                        ruleString = value;
                    }
                }
                break;
            }
            return ruleString;
        }
        return super.ruleString();
    }
}
