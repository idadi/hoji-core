package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.HojiContextAware;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form.OutputStrategy;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Rule;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * An answer to a question i.e. a Response to a {@link Field}. All types of Responses should extend this class. A
 * Field-Response combination is represented by the {@link LiveField} class.
 *
 * @author gitahi
 */
public class Response<A, S> implements Serializable, HojiContextAware {

    protected final LiveField liveField;

    protected final Boolean actual;

    protected final Object storedValue;

    protected transient HojiContext hojiContext;

    /**
     * Initializes a new Response for a given LiveField with the given value. If the value can readily be cast to the
     * actual type of the specific Response implementation, the parameter 'actual' should be set to true. Otherwise it
     * should be set to false to indicate to the specific implementation that the value cannot be readily cast and that
     * some custom processing needs to occur in order to convert the values to the appropriate actual type.
     * <p/>
     * The term actual type refers to the type used by a given Response implementation to store it's data. For example a
     * TextResponse might use a {@link String} to store the text response while a DateResponse might use a {@link Date}
     * to store the date response. There are no restrictions on what this actual type might be. It is entirely up to the
     * implementation to decide.
     * <p/>
     * Typically, when creating a Response inside a Widget, the value read from the Widget as input by the user will
     * usually be in the expected actual type for the particular Response implementation. For example a TextWidget will
     * likely produce the String value typed in by the user. A DateWidget will likely return the Date selected by the
     * user. However, when retrieving an existing response from the database, the value will rarely be amenable to
     * casting to the actual type as it is likely stored in the database as a primitive rather than the appropriate
     * object type. A Date might be saved as a String for example. More likely, a {@link Choice} might be saved as the
     * {@link Choice#code} for the actual Choice object.
     * <p/>
     * As such, the value actual = false tends to apply mostly during retrieval, and should automatically be set that
     * way in the DAO implementation. The value actual = true applies mostly within Widgets. Widget implementers should
     * be especially careful with this fact, otherwise Response implementations might be unable to correctly process
     * responses from those Widgets.
     * <p/>
     * Response constructors are not intended to be called directly and as such they should be declared protected.
     * Instead, the static method {@link Response#create(HojiContext, LiveField, Object, boolean)} should be used.
     *
     * @param hojiContext the current {@link HojiContext}.
     * @param liveField   the LiveField for which this is a Response.
     * @param value       the arbitrary value for this response.
     * @param actual      whether value is the actual type and thus amenable to casting as such.
     */
    protected Response(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        this.hojiContext = hojiContext;
        this.liveField = liveField;
        this.storedValue = value;
        this.actual = actual;
    }

    /**
     * Creates an instance of a specific implementation of a Response as specified by {@link
     * FieldType#getResponseClass()}.
     *
     * @param liveField the LiveField to which the response to be created belongs.
     * @param value     the value for the response.
     * @param actual    Whether the value is amenable to casting to the response implementation's actual type.
     * @return the Response instance.
     */
    public static Response create(HojiContext hojiContext, LiveField liveField, Object value, boolean actual) {
        Object object;
        try {
            Class clazz = Class.forName(liveField.getField().getType().getResponseClass());
            Constructor<Response> constructor = clazz.getDeclaredConstructor(HojiContext.class,
                LiveField.class, Object.class, Boolean.class);
            constructor.setAccessible(true);
            object = constructor.newInstance(hojiContext, liveField, value, actual);
        } catch (InstantiationException | IllegalAccessException |
            IllegalArgumentException | InvocationTargetException | NoSuchMethodException |
            SecurityException | ClassNotFoundException ex) {
            throw new RuntimeException("Could not instantiate Response.", ex);
        }
        return (Response) object;
    }

    /**
     * Gets the current {@link HojiContext} for this Response.
     *
     * @return the HojiContext.
     */
    @Override
    public HojiContext getHojiContext() {
        return hojiContext;
    }

    /**
     * Sets the current {@link HojiContext} for this Response.
     *
     * @param hojiContext the HojiContext.
     */
    public void setHojiContext(HojiContext hojiContext) {
        this.hojiContext = hojiContext;
    }

    /**
     * Gets the {@link LiveField} for which this is the Response.
     *
     * @return the LiveField.
     */
    public final LiveField getLiveField() {
        return liveField;
    }

    /**
     * Gets the actual value <A> of this Response. Simply calls {@link #getActualValue(boolean)} with a false argument.
     *
     * @return the actual value <A>.
     */
    public final A getActualValue() {
        return getActualValue(false);
    }

    /**
     * Gets the actual value <A> of this Response.
     *
     * @param missingAsNull if true and the actual value is merely a placeholder for a missing value, then null is
     *                      returned instead, otherwise the actual value is returned as is regardless.
     * @return the actual value <A>.
     */
    public A getActualValue(boolean missingAsNull) {
        if (missingAsNull) {
            if (isMissing(getMissingValues())) {
                return null;
            }
        }
        A actualValue;
        try {
            if (actual || storedValue == null) {
                actualValue = (A) storedValue;
            } else {
                actualValue = toActualValue((S) storedValue);
            }
        } catch (ClassCastException ex) {
            actualValue = null;
        }
        return actualValue;
    }

    /**
     * Convert the storage value of type <S> to the actual value of type <A>. By default, this method assumes that <A>
     * and <S> are the same, and thus simply casts value from S to A and returns that. You should definitely override
     * this method in your Response implementation if you know this assumption does not hold true for it.
     *
     * @param value the storage value of type <S>.
     * @return the actual value of type <A>.
     * @throws ClassCastException if <A> and <S> are not the same.
     * @should return S cast to A if S is the same type as A.
     * @should throw ClassCastException if A and S are not the same.
     */
    protected A toActualValue(S value) {
        return (A) value;
    }

    /**
     * Convert the actual value of type <A> to the storage value of type <S>. By default, this method assumes that <A>
     * and <S> are the same, and thus simply casts actual value to <S> and returns that.  You should definitely
     * override this method in your Response implementation if you know this assumption does not hold true for it.
     *
     * @return the storage value of type <S>.
     * @throws ClassCastException if <A> and <S> are not the same.
     * @should return A case to S if A is the same type as S.
     * @should throw ClassCastException if A and S are not the same.
     */
    public S toStorageValue() {
        return (S) getActualValue();
    }

    /**
     * Returns true to signify that this Response should be considered a satisfactory answer to a {@link LiveField}. The
     * default implementation returns true if the actual value is not null, and false otherwise. This suffices
     * for most Response implementations, but those for which it does not should override this method.
     *
     * @return the boolean value.
     * @should return true if actual value is not null.
     * @should return false if actual value is null.
     */
    public final boolean isAnswered() {
        return isAnswered(false);
    }

    /**
     * Returns true to signify that this Response should be considered a satisfactory answer to a {@link LiveField}. The
     * default implementation returns true if the actual value is not null, and false otherwise. This suffices
     * for most Response implementations, but those for which it does not should override this method.
     *
     * @param missingAsNull true if a missing value placeholder should be treated as null and false otherwise.
     * @return the boolean value.
     * @should return true if actual value is not null.
     * @should return false if actual value is null.
     */
    public boolean isAnswered(boolean missingAsNull) {
        return getActualValue(missingAsNull) != null;
    }

    /**
     * If actual value is not null, this method returns its string representation. Otherwise it returns an
     * empty String.
     *
     * @return the String to be displayed to the user as a representation of this response.
     * @should return string representation of actual value if possible and empty String otherwise.
     */
    public String displayString() {
        return displayString(false);
    }

    /**
     * If actual value is not null, this method returns its string representation. Otherwise it returns an
     * empty string or "Missing" as specified by the parameter missingText.
     *
     * @param missingText determines if null actual value is returned as empty string or "Missing"
     * @return string representation of actual value if possible and empty String or "Missing".
     */
    public String displayString(boolean missingText) {
        if (missingText && isMissing(getMissingValues())) {
            return "Missing";
        } else if (getActualValue() != null) {
            return getActualValue().toString();
        } else {
            return "";
        }
    }

    protected List<? extends Comparable> getMissingValues() {
        List<String> missing = new ArrayList<>();
        if (liveField.getField().getMissingValue() != null) {
            String[] values = liveField.getField().getMissingValue().split(",");
            for (String value : values) {
                missing.add(value.trim());
            }
        }
        return missing;
    }

    final boolean isMissing(List<? extends Comparable> missingValues) {
        if (liveField.getUuid() != null && getActualValue() == null) {
            return true;
        }
        return missingValues.contains(comparableFromActualValue());
    }

    /**
     * @return the String to be used when processing any {@link Rule} involving the {@link Field} for which this is a
     * response. The default implementation simply calls {@link #displayString()}.
     * @should return display string.
     */
    public String ruleString() {
        return displayString();
    }

    /**
     * A string representing the underlying quantity represented by this Response. This method is analogous to
     * {@link #displayString()}, and in fact by default merely delegates to it. The difference is that it represents
     * a scalar quantity, so it may be override-n to return something more interesting, such as the {@link Choice#scale}
     * of a {@link Choice}.
     * <p>
     * This method is primarily intended for piping choice scale values as defaults for integer fields, but it may
     * be used more flexibly in future.
     *
     * @return the quantity
     */
    public String quantityString() {
        return displayString(true);
    }

    /**
     * This method should return a Comparable value computed from the {@link Rule#value} passed. If no such value can
     * reasonably be computed as such, null should be returned. By default, this method returns null and implementations
     * that are not comparable should not bother implementing this method.
     *
     * @param ruleValue the rule value.
     * @return a comparable value.
     * @should return null.
     */
    public Comparable comparableFromRuleString(String ruleValue) {
        return null;
    }

    /**
     * This method should return a Comparable value based on the actual value. If no such value can reasonably
     * be computed, null should be returned. By default, this method returns the actual value if it is
     * {@link Comparable} and null otherwise. Implementations may whose actual value is not Comparable but that
     * are able to return a Comparable value based on it should do so.
     *
     * @return a comparable value.
     * @should return actual value if comparable and null otherwise.
     */
    public Comparable comparableFromActualValue() {
        if (getActualValue() != null && getActualValue() instanceof Comparable) {
            return (Comparable) getActualValue();
        }
        return null;
    }

    /**
     * Get a suitable label for this Response. These labels are typically used as column headers when rendering
     * responses in tabular form.
     *
     * @return the label for this Response.
     */
    protected String getLabel() {
        return Utils.removeHtmlTags(liveField.getField().getHeader());
    }

    /**
     * Exactly like {@link #getLabel()} but appends an additional String to the label in parentheses.
     *
     * @param append the String to append to the label. Ignored if null or blank.
     * @return the label for this Response.
     */
    protected String getLabel(String append) {
        if (!StringUtils.isBlank(append)) {
            return getLabel() + " (" + append + ")";
        }
        return getLabel();
    }

    /**
     * Gets the format associated with this Response for the purposes of rendering tabular output.
     *
     * @return the format.
     */
    protected String getFormat() {
        return "";
    }

    /**
     * Gets the list of {@link DataElement} representing a simplified version of this Response for the purposes
     * of rendering tabular output.
     *
     * @param renderingType a value representing one of the various {@link OutputStrategy} options and which tells
     *                      this method how to prepare the value.
     * @return the value as a list of {@link DataElement}s.
     */
    public List<DataElement> getValue(String renderingType) {
        String type = liveField.getField().getDataElementType(renderingType);
        boolean missingText = true;
        if (OutputStrategy.API.equals(renderingType)) {
            missingText = false;
        }
        return Collections.singletonList(new DataElement(getLabel(), displayString(missingText), type, getFormat()));
    }
}
