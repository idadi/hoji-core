package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author gitahi
 */
public class DateResponse extends Response<Date, Long> {

    /**
     * @should set the actual value to the date represented by the long value if actual is false.
     */
    protected DateResponse(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        super(hojiContext, liveField, value, actual);
    }

    @Override
    protected Date toActualValue(Long value) {
        return new Date(value);
    }

    /**
     * @should return the actual date value as a long.
     */
    @Override
    public Long toStorageValue() {
        return getActualValue() != null ? getActualValue().getTime() : null;
    }

    /**
     * {@inheritDoc}
     *
     * @should return the actual date formatted to the current locale.
     */
    @Override
    public String displayString(boolean missingText) {
        if (missingText && isMissing(getMissingValues())) {
            return "Missing";
        } else if (getActualValue() != null) {
            return Utils.formatDate(getActualValue());
        } else {
            return "";
        }
    }

    @Override
    public Comparable comparableFromRuleString(String ruleValue) {
        return Utils.parseLocalDate(ruleValue);
    }

    @Override
    public Comparable comparableFromActualValue() {
        LocalDate localDate = null;
        if (getActualValue() != null) {
            localDate = new LocalDate(getActualValue());
        }
        return localDate;
    }

    @Override
    public List<DataElement> getValue(String renderingType) {
        String type = liveField.getField().getDataElementType(renderingType);
        boolean missingText = true;
        if (Form.OutputStrategy.API.equals(renderingType)) {
            missingText = false;
        }
        String value = "";
        if (missingText && isMissing(getMissingValues())) {
            value = "Missing";
        } else if (getActualValue() != null) {
            value = Utils.formatIsoDate(getActualValue());
        }
        return Collections.singletonList(new DataElement(getLabel(), value, type, getFormat()));
    }

    @Override
    protected List<? extends Comparable> getMissingValues() {
        List<Comparable> missing = new ArrayList<>();
        if (liveField.getField().getMissingValue() != null) {
            String[] values = liveField.getField().getMissingValue().split(",");
            for (String value : values) {
                if (value.length() == 4) {
                    Integer year = Utils.parseInteger(value);
                    if (year != null) {
                        missing.add(new LocalDate(getActualValue()).withYear(year));
                    }
                }
                LocalDate parsedMissingDate = Utils.parseLocalDate(value);
                if (parsedMissingDate != null) {
                    missing.add(parsedMissingDate);
                }
            }
        }
        return missing;
    }

    static class DateComparator implements Comparable<Date> {

        private final String date;

        DateComparator(String date) {
            this.date = date;
        }

        @Override
        public int compareTo(Date anotherDate) {
            if (date.length() == 4) {
                Integer year = Utils.parseInteger(this.date);
                if (year == null) {
                    return -1;
                }
                return year.compareTo(new LocalDate(anotherDate).getYear());
            }
            LocalDate parsedDate = Utils.parseLocalDate(date);
            if (parsedDate == null) {
                return -1;
            }
            return parsedDate.toDate().compareTo(anotherDate);
        }
    }
}
