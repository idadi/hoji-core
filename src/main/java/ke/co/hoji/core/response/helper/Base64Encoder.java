package ke.co.hoji.core.response.helper;

/**
 * An abstraction for the means by which to encode binary data to a Base64 string for the purposes of storage and
 * transmission, and vice versa.
 */
public interface Base64Encoder {

    /**
     * Encode binary data to a Base64 string.
     *
     * @param byteArray the binary data.
     * @return the Base64 encode String.
     */
    String encodeToBase64String(byte[] byteArray);

    /**
     * Decode a Base64 String to binary data.
     *
     * @param base64String the Base64 String.
     * @return the binary data.
     */
    byte[] decodeToByteArray(String base64String);
}
