package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.model.LiveField;

/**
 * @author gitahi
 */
public class LabelResponse extends Response {

    protected LabelResponse(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        super(hojiContext, liveField, value, actual);
    }

    /**
     * {@inheritDoc}
     *
     * @should always return true.
     */
    @Override
    public boolean isAnswered(boolean missingAsNullc) {
        return true;
    }
}
