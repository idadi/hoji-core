package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.Choice.TranslatableComponent;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.service.model.TranslationService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author gitahi
 */
public class MultipleChoiceResponse extends Response<LinkedHashMap<Choice, Boolean>, String> {

    private final static Logger logger = LoggerFactory.getLogger(MultipleChoiceResponse.class);

    /**
     * @should set the actual value to the {@link <Map<Choice, Boolean>} represented by the String value if actual is
     * false.
     */
    protected MultipleChoiceResponse(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        super(hojiContext, liveField, value, actual);
    }

    @Override
    protected LinkedHashMap<Choice, Boolean> toActualValue(String value) {
        LinkedHashMap<Choice, Boolean> choiceMap = null;
        if (value.matches("((\\d+#[01])\\|?)+")) {
            LinkedHashMap<Integer, Boolean> valueMap = createValueMap(value);
            choiceMap = new LinkedHashMap<>();
            List<Choice> choices = this.liveField.getField().getChoiceGroup().getChoices();
            for (Choice choice : choices) {
                choiceMap.put(choice, valueMap.get(choice.getId()));
            }
        }
        return choiceMap;
    }

    /**
     * {@inheritDoc}
     *
     * @should return the pipe separated string of choice ids and boolean values of the actual {@link <Map<Choice,
     * Boolean>}.
     */
    @Override
    public String toStorageValue() {
        return isAnswered() ? createValueString() : null;
    }

    /**
     * {@inheritDoc}
     *
     * @should return the coma separated names of all the Choices selected.
     * @should return empty string if no Choices are selected.
     */
    @Override
    public String displayString(boolean missingText) {
        if (getActualValue() != null) {
            TranslationService translationService = hojiContext.getModelServiceManager().getTranslationService();
            Integer languageId = hojiContext.getLanguageId();
            List<String> tokens = new ArrayList<>();
            for (Choice choice : getActualValue().keySet()) {
                Boolean checked = getActualValue().get(choice);
                if (checked != null && checked) {
                    tokens.add(translationService.translate(
                            choice,
                            languageId,
                            TranslatableComponent.NAME,
                            choice.getName()
                    ));
                }
            }
            return Utils.string(tokens);
        }
        return super.displayString(missingText);
    }

    /**
     * {@inheritDoc}
     *
     * @should return the 0 or 1 boolean values of the actual {@link <Map<Choice,Boolean>} stringed together.
     */
    @Override
    public String ruleString() {
        if (getActualValue() != null) {
            return createRuleString();
        }
        return super.ruleString();
    }

    /**
     * {@inheritDoc}
     *
     * @should return the scale of the Choice, if choice is not null.
     */
    @Override
    public String quantityString() {
        logger.info("Calculating score for multiple choice field [id= {}]", liveField.getField().getId());
        logger.debug("Response for field [response= {}]", getActualValue());
        if (getActualValue() != null) {
            int quantity = 0;
            for (Choice choice : getActualValue().keySet()) {
                logger.debug("Checking if [choice= {}] was selected.", choice);
                Boolean checked = getActualValue().get(choice);
                if (checked != null && checked) {
                    logger.debug("Adding score of choice [id= {}, score= {}]", choice.getId(), choice.getScale());
                    quantity += choice.getScale();
                }
            }
            logger.info("Score for field [id= {}] is [score= {}]", liveField.getField().getId(), quantity);
            return String.valueOf(quantity);
        }
        logger.info("Score for field [id= {}] is [score= 0] because field has no value", liveField.getField().getId());
        return super.quantityString();
    }

    /**
     * {@inheritDoc}
     *
     * @should return ruleValue converted to Long.
     */
    public Comparable comparableFromRuleString(String ruleValue) {
        String binaryString = "";
        LinkedHashMap<Choice, Boolean> ruleChoiceMap = toActualValue(ruleValue);
        if (ruleChoiceMap == null) {
            return null;
        }
        for (Choice choice : getActualValue().keySet()) {
            Boolean value = ruleChoiceMap.get(choice);
            binaryString += value != null && value ? "1" : "0";
        }
        return Utils.parseBin(binaryString);
    }

    /**
     * {@inheritDoc}
     *
     * @should return {@link #ruleString()} converted to Long.
     */
    public Comparable comparableFromActualValue() {
        return Utils.parseBin(ruleString());
    }

    private LinkedHashMap<Integer, Boolean> createValueMap(String value) {
        LinkedHashMap<Integer, Boolean> valueMap = new LinkedHashMap<>();
        List<String> tokens = Utils.tokenizeString(value, "\\|", true);
        for (String token : tokens) {
            List<String> subTokens = Utils.tokenizeString(token, "#", true);
            valueMap.put(Integer.parseInt(subTokens.get(0)), Integer.parseInt(subTokens.get(1)) > 0);
        }
        return valueMap;
    }

    private String createValueString() {
        List<String> tokens = new ArrayList<>();
        for (Choice choice : getActualValue().keySet()) {
            tokens.add(choice.getId() + "#" + ((getActualValue().get(choice)) ? 1 : 0));
        }
        String value = Utils.string(tokens, "|", null, false);
        value = value.replace(" ", "");
        return value;
    }

    private String createRuleString() {
        String value = "";
        if (getActualValue() != null) {
            for (Choice choice : getActualValue().keySet()) {
                Boolean checked = getActualValue().get(choice);
                value += ((checked != null && checked) ? "1" : "0");
            }
        }
        return value;
    }

    /**
     * {@inheritDoc}
     *
     * @should return false if actual value is null.
     * @should return true if at least one choice is selected.
     * @should return false if no choice is selected.
     */
    @Override
    public boolean isAnswered(boolean missingAsNull) {
        if (getActualValue() != null) {
            for (Boolean value : getActualValue().values()) {
                if (value != null && value) {
                    return true;
                }
            }
            return false;
        }
        return super.isAnswered(missingAsNull);
    }

    @Override
    public List<DataElement> getValue(String renderingType) {
        List<DataElement> dataElements = new ArrayList<>();
        Map<Choice, Boolean> choiceMap = new LinkedHashMap<>();
        if (getActualValue() == null) {
            for (Choice choice : liveField.getField().getChoiceGroup().getChoices()) {
                choiceMap.put(choice, null);
            }
        } else {
            choiceMap = getActualValue();
        }
        String type = liveField.getField().getDataElementType(renderingType);
        if (liveField.getField().getForm().getOutputStrategy().contains(renderingType)
                && Integer.valueOf(Field.OutputType.NUMBER).equals(liveField.getField().getOutputType())) {
            dataElements.add(new DataElement(getLabel(), quantityString(), type, getFormat()));
        } else {
            for (Choice choice : choiceMap.keySet()) {
                Boolean checked = choiceMap.get(choice);
                String value = liveField.getUuid() != null ? "Missing" : "";
                if (checked != null) {
                    value = checked ? "1" : "0";
                }
                dataElements.add(new DataElement(getLabel(choice.getName()), value, type, getFormat()));
            }
        }
        return dataElements;
    }
}
