package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.Choice.TranslatableComponent;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.service.model.TranslationService;

import java.util.Collections;
import java.util.List;

/**
 * @author gitahi
 */
public class SingleChoiceResponse extends Response<Choice, Long> {

    /**
     * @should set the actual value to the {@link Choice} represented by the String (choice code) value if actual is
     * false.
     */
    protected SingleChoiceResponse(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        super(hojiContext, liveField, value, actual);
    }

    @Override
    protected Choice toActualValue(Long value) {
        return liveField.getField().getChoiceGroup().getChoiceById(value.intValue());
    }

    /**
     * {@inheritDoc}
     *
     * @should return the code of the actual Choice.
     */
    @Override
    public Long toStorageValue() {
        return (getActualValue() != null && getActualValue().getId() != null) ? getActualValue().getId().longValue() : null;
    }

    /**
     * {@inheritDoc}
     *
     * @should return the name of the Choice, if choice is not null.
     */
    @Override
    public String displayString(boolean missingText) {
        if (getActualValue() != null) {
            TranslationService translationService = hojiContext.getModelServiceManager().getTranslationService();
            Integer languageId = hojiContext.getLanguageId();
            return translationService.translate(
                    getActualValue(),
                    languageId,
                    TranslatableComponent.NAME,
                    getActualValue().getName()
            );
        }
        return super.displayString(missingText);
    }

    /**
     * {@inheritDoc}
     *
     * @should return the code of the Choice, if choice is not null.
     */
    @Override
    public String ruleString() {
        if (getActualValue() != null) {
            return String.valueOf(getActualValue().getId());
        }
        return super.ruleString();
    }

    /**
     * {@inheritDoc}
     *
     * @should return the scale of the Choice, if choice is not null.
     */
    @Override
    public String quantityString() {
        if (getActualValue() != null) {
            return String.valueOf(getActualValue().getScale());
        }
        return super.quantityString();
    }

    /**
     * {@inheritDoc}
     *
     * @should return the Choice whose code is equal to the rule value passed
     */
    @Override
    public Comparable comparableFromRuleString(String ruleValue) {
        return toActualValue(Utils.parseLong(ruleValue));
    }

    @Override
    public List<DataElement> getValue(String renderingType) {
        String type = liveField.getField().getDataElementType(renderingType);
        String choiceValue = liveField.getUuid() != null ? "Missing" : "";
        if (getActualValue() != null) {
            choiceValue = getActualValue().getName();
            if (liveField.getField().getForm().getOutputStrategy().contains(renderingType)
                    && Integer.valueOf(Field.OutputType.NUMBER).equals(liveField.getField().getOutputType())) {
                if (Integer.valueOf(Field.OutputType.NUMBER).equals(liveField.getField().getOutputType())) {
                    choiceValue = String.valueOf(getActualValue().getScale());
                }
            }
        }
        return Collections.singletonList(new DataElement(getLabel(), choiceValue, type, getFormat()));
    }
}
