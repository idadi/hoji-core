package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.Choice.TranslatableComponent;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.service.model.TranslationService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author gitahi
 */
public class RankingResponse extends Response<LinkedHashMap<Choice, Integer>, String> {

    /**
     * @should set the actual value to the {@link <Map<Choice, Integer>} represented by the String value if actual is
     * false.
     */
    protected RankingResponse(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        super(hojiContext, liveField, value, actual);
    }

    @Override
    protected LinkedHashMap<Choice, Integer> toActualValue(String value) {
        LinkedHashMap<Choice, Integer> choiceMap = null;
        if (value.matches("((\\d+#\\d)\\|?)+")) {
            LinkedHashMap<Integer, Integer> valueMap = createValueMap(value);
            choiceMap = new LinkedHashMap<>();
            List<Choice> choices = liveField.getField().getChoiceGroup().getChoices();
            for (Choice choice : choices) {
                choiceMap.put(choice, valueMap.get(choice.getId()));
            }
        }
        return choiceMap;
    }

    /**
     * {@inheritDoc}
     *
     * @should return the pipe separated string of choice ids and rank values of the actual {@link <Map<Choice,
     * Boolean>}.
     */
    @Override
    public String toStorageValue() {
        return isAnswered() ? createValueString() : null;
    }

    /**
     * {@inheritDoc}
     *
     * @should return the ordered coma separated names of all ranked Choices.
     * @should return empty string if no Choices are selected.
     */
    @Override
    public String displayString(boolean missingText) {
        if (getActualValue() != null) {
            TranslationService translationService = hojiContext.getModelServiceManager().getTranslationService();
            Integer languageId = hojiContext.getLanguageId();
            List<Choice> rankedChoices = new ArrayList<>();
            for (Choice choice : getActualValue().keySet()) {
                Integer rank = getActualValue().get(choice);
                if (rank != null && rank > 0) {
                    rankedChoices.add(choice);
                    choice.setRandomId(rank);
                }
            }
            Collections.sort(rankedChoices, new Comparator<Choice>() {
                @Override
                public int compare(Choice c1, Choice c2) {
                    return c1.getRandomId().compareTo(c2.getRandomId());
                }
            });
            List<String> tokens = new ArrayList<>();
            for (Choice rankedChoice : rankedChoices) {
                tokens.add(translationService.translate(
                        rankedChoice,
                        languageId,
                        TranslatableComponent.NAME,
                        rankedChoice.getName()
                ));
            }
            return Utils.string(tokens);
        }
        return super.displayString(missingText);
    }

    /**
     * {@inheritDoc}
     *
     * @should return the 0 or 1 boolean values of the actual {@link <Map<Choice,Integer>} stringed together with 1 when ranked and false otherwise.
     */
    @Override
    public String ruleString() {
        if (getActualValue() != null) {
            return createRuleString();
        }
        return super.ruleString();
    }

    /**
     * {@inheritDoc}
     *
     * @should return the scale of the Choice, if choice is not null.
     */
    @Override
    public String quantityString() {
        if (getActualValue() != null) {
            int quantity = 0;
            for (Choice choice : getActualValue().keySet()) {
                Integer rank = getActualValue().get(choice);
                if (rank > 0) {
                    quantity += choice.getScale();
                }
            }
            return String.valueOf(quantity);
        }
        return super.quantityString();
    }

    /**
     * {@inheritDoc}
     *
     * @should return ruleValue converted to Long.
     */
    public Comparable comparableFromRuleString(String ruleValue) {
        String binaryString = "";
        LinkedHashMap<Choice, Integer> ruleChoiceMap = toActualValue(ruleValue);
        if (ruleChoiceMap == null) {
            return null;
        }
        for (Choice choice : getActualValue().keySet()) {
            Integer value = ruleChoiceMap.get(choice);
            binaryString += value != null && value != 0 ? "1" : "0";
        }
        return Utils.parseBin(binaryString);
    }

    /**
     * {@inheritDoc}
     *
     * @should return {@link #ruleString()} converted to Long.
     */
    public Comparable comparableFromActualValue() {
        return Utils.parseBin(ruleString());
    }

    private LinkedHashMap<Integer, Integer> createValueMap(String value) {
        LinkedHashMap<Integer, Integer> valueMap = new LinkedHashMap<>();
        List<String> tokens = Utils.tokenizeString(value, "\\|", true);
        for (String token : tokens) {
            List<String> subTokens = Utils.tokenizeString(token, "#", true);
            valueMap.put(Integer.parseInt(subTokens.get(0)), Integer.parseInt(subTokens.get(1)));
        }
        return valueMap;
    }

    private String createValueString() {
        List<String> tokens = new ArrayList<>();
        for (Choice choice : getActualValue().keySet()) {
            tokens.add(choice.getId() + "#" + getActualValue().get(choice));
        }
        String value = Utils.string(tokens, "|", null, false);
        value = value.replace(" ", "");
        return value;
    }

    private String createRuleString() {
        String value = "";
        if (getActualValue() != null) {
            for (Choice choice : getActualValue().keySet()) {
                Integer rank = getActualValue().get(choice);
                value += ((rank != null && rank > 0) ? "1" : "0");
            }
        }
        return value;
    }

    /**
     * {@inheritDoc}
     *
     * @should return false if actual value is null.
     * @should return true if at least one choice is ranked.
     * @should return false if no choice is ranked.
     */
    @Override
    public boolean isAnswered(boolean missingAsNull) {
        if (getActualValue() != null) {
            for (Integer rank : getActualValue().values()) {
                if (rank != null && rank > 0) {
                    return true;
                }
            }
            return false;
        }
        return super.isAnswered(missingAsNull);
    }

    @Override
    public List<DataElement> getValue(String renderingType) {
        List<DataElement> dataElements = new ArrayList<>();
        Map<Choice, Integer> choiceMap = new LinkedHashMap<>();
        if (getActualValue() == null) {
            for (Choice choice : liveField.getField().getChoiceGroup().getChoices()) {
                choiceMap.put(choice, 0);
            }
        } else {
            choiceMap = getActualValue();
        }
        for (Choice choice : choiceMap.keySet()) {
            Integer rank = choiceMap.get(choice);
            String value = liveField.getUuid() != null ? String.valueOf(rank) : "Missing";
            dataElements.add(new DataElement(getLabel(choice.getName()), value, DataElement.Type.NUMBER, getFormat()));
        }
        return dataElements;
    }
}
