package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * A @{@link Response} for time.
 *
 * @author gitahi
 */
public class TimeResponse extends Response<Date, Long> {

    /**
     *
     * @should set the actual value to the time represented by the long value if actual is false.
     */
    protected TimeResponse(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        super(hojiContext, liveField, value, actual);
    }

    @Override
    protected Date toActualValue(Long value) {
        return new Date(value);
    }

    /**
     * {@inheritDoc}
     *
     * @should return the actual time value as a long.
     */
    @Override
    public Long toStorageValue() {
        return getActualValue() != null ? getActualValue().getTime() : null;
    }

    /**
     * {@inheritDoc}
     *
     * @should return the actual time formatted to the current locale.
     */
    @Override
    public String displayString(boolean missingText) {
        if (missingText && isMissing(getMissingValues())) {
            return "Missing";
        } else if (getActualValue() != null) {
            return Utils.formatTime(getActualValue());
        }
        return super.displayString(missingText);
    }

    @Override
    public Comparable comparableFromRuleString(String ruleValue) {
        return Utils.parseLocalTime(ruleValue);
    }

    @Override
    public Comparable comparableFromActualValue() {
        LocalTime localTime = null;
        if (getActualValue() != null) {
            localTime = new LocalTime(getActualValue());
        }
        return localTime;
    }

    @Override
    public List<DataElement> getValue(String renderingType) {
        String type = liveField.getField().getDataElementType(renderingType);
        boolean missingText = true;
        if (Form.OutputStrategy.API.equals(renderingType)) {
            missingText = false;
        }
        String value = "";
        if (missingText && isMissing(getMissingValues())) {
            value = "Missing";
        } else if (getActualValue() != null) {
            value = Utils.formatIsoTime(getActualValue());
        }
        return Collections.singletonList(new DataElement(getLabel(), value, type, getFormat()));
    }

    @Override
    protected List<? extends Comparable> getMissingValues() {
        List<LocalTime> missing = new ArrayList<>();
        if (liveField.getField().getMissingValue() != null) {
            String[] values = liveField.getField().getMissingValue().split(",");
            for (String value : values) {
                LocalTime parsed = Utils.parseLocalTime(value.trim());
                if (parsed != null) {
                    missing.add(parsed);
                }
            }
        }
        return missing;
    }
}
