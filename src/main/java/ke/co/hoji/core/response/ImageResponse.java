package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.response.helper.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ke.co.hoji.core.data.model.Form.OutputStrategy.DOWNLOAD;
import static ke.co.hoji.core.data.model.Form.OutputStrategy.PIVOT;
import static ke.co.hoji.core.data.model.Form.OutputStrategy.TABLE;

/**
 * @author gitahi
 */
public class ImageResponse extends Response<Image, String> {

    protected static final Logger logger = LoggerFactory.getLogger(ImageResponse.class);

    /**
     * Placeholder text to represent an image. Usually displayed to the user to let them know there is an image.
     */
    public static final String PLACEHOLDER = "Image";

    protected ImageResponse(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        super(hojiContext, liveField, value, actual);
    }

    @Override
    public String displayString(boolean missingText) {
        if (storedValue != null) {
            return PLACEHOLDER;
        }
        return super.displayString(missingText);
    }

    public Response prune() {
        Image actualValue = getActualValue();
        Image pruned = null;
        if (actualValue != null) {
            pruned = new Image(null, actualValue.getFilePath());
        }
        return Response.create(hojiContext, liveField, pruned, true);
    }

    @Override
    protected Image toActualValue(String value) {
        if (PLACEHOLDER.equals(value)) {
            return new Image(null, null);
        }
        File imageFile = new File(value);
        byte[] imageData = null;
        try {
            imageData = readAllBytes(imageFile);
        } catch (IOException e) {
            logger.error("Unable to read image: " + imageFile, e);
        }
        return new Image(imageData, value);
    }

    @Override
    public String toStorageValue() {
        Image actualValue = getActualValue();
        if (actualValue != null) {
            return actualValue.getFilePath();
        }
        return null;
    }

    @Override
    public List<DataElement> getValue(String renderingType) {
        String type = liveField.getField().getDataElementType(renderingType);
        String value = "Missing";
        List<String> nonImageOutputs = Arrays.asList(PIVOT, TABLE, DOWNLOAD);
        if (nonImageOutputs.contains(renderingType)) {
            value = displayString(true);
        } else {
            if (storedValue != null) {
                value = hojiContext.getBase64Encoder().encodeToBase64String(toActualValue((String) storedValue).getData());
            }
        }
        return Collections.singletonList(new DataElement(getLabel(), value, type, getFormat()));
    }

    private byte[] readAllBytes(File file) throws IOException {
        boolean hasToPathMethod = true;
        try {
            file.getClass().getMethod("toPath");
        } catch (NoSuchMethodException e) {
            hasToPathMethod = false;
        }
        if (hasToPathMethod) {
            return Files.readAllBytes(file.toPath());
        } else {
            byte[] bytes = new byte[(int) file.length()];
            FileInputStream fis = new FileInputStream(file);
            fis.read(bytes);
            fis.close();
            return bytes;
        }
    }
}
