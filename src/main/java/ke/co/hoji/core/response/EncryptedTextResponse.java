package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.model.LiveField;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author gitahi
 */
public class EncryptedTextResponse extends Response<String, String> {

    private static final String IV = "AAAAAAAAAAAAAAAA";
    private static final String encryptionKey = "ec4ac4ba8c6211e5";

    protected EncryptedTextResponse(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        super(hojiContext, liveField, value, actual);
    }

    /**
     * {@inheritDoc}
     *
     * @should return the parameter passed.
     */
    @Override
    public Comparable comparableFromRuleString(String ruleValue) {
        return ruleValue;
    }

    @Override
    protected String toActualValue(String value) {
        if (HojiContext.ANDROID_CONTEXT.equals(hojiContext.getType()) && value != null) {
            return decrypt(hojiContext.getBase64Encoder().decodeToByteArray(value));
        } else {
            return value;
        }
    }

    @Override
    public String toStorageValue() {
        String actualValue = getActualValue();
        if (actualValue != null) {
            return hojiContext.getBase64Encoder().encodeToBase64String(encrypt(getActualValue()));
        }
        return null;
    }

    public byte[] encrypt(String plainText) {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
            cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
            return cipher.doFinal(plainText.getBytes("UTF-8"));
        } catch (Exception ex) {
            return null;
        }

    }

    public String decrypt(byte[] cipherText) {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
            return new String(cipher.doFinal(cipherText), "UTF-8");
        } catch (Exception ex) {
            return null;
        }
    }
}
