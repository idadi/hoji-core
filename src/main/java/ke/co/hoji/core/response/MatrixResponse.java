package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.MatrixRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gitahi
 */
public class MatrixResponse extends Response<List<MatrixRecord>, List<MatrixRecord>> {

    protected MatrixResponse(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        super(hojiContext, liveField, value, actual);
    }

    /**
     * {@inheritDoc}
     *
     * @should return a comma separated list of the string representation of the constituent matrix records.
     */
    @Override
    public String displayString(boolean missingText) {
        if (getActualValue() != null) {
            List<String> tokens = new ArrayList<>();
            for (MatrixRecord matrixRecord : getActualValue()) {
                tokens.add(matrixRecord.toString());
            }
            return Utils.string(tokens);
        }
        return super.displayString(missingText);
    }

    /**
     * {@inheritDoc}
     *
     * @should return true if actual value is not null and is not empty.
     * @should return false if actual value is null.
     * @should return false if actual value is not null and is empty.
     */
    @Override
    public boolean isAnswered(boolean missingAsNull) {
        if (getActualValue() != null) {
            return !getActualValue().isEmpty();
        }
        return super.isAnswered(missingAsNull);
    }

    @Override
    public List<MatrixRecord> toStorageValue() {
        return getActualValue() != null && !getActualValue().isEmpty() ? super.toStorageValue() : null;
    }
}
