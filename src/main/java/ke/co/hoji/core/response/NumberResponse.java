package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.LiveField;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author gitahi
 */
public class NumberResponse extends Response<Long, Long> {

    protected NumberResponse(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        super(hojiContext, liveField, value, actual);
    }

    /**
     * {@inheritDoc}
     *
     * @should return rule value converted to Long.
     */
    @Override
    public Comparable comparableFromRuleString(String ruleValue) {
        return Utils.parseLong(ruleValue);
    }

    @Override
    protected String getFormat() {
        if (getActualValue() != null && !isMissing(getMissingValues()) && liveField.getField().qualifiesAsNumber(null)) {
            return NumberFormat.getInstance().format(getActualValue());
        }
        return super.getFormat();
    }

    @Override
    protected List<? extends Comparable> getMissingValues() {
        List<Long> missing = new ArrayList<>();
        if (liveField.getField().getMissingValue() != null) {
            String[] values = liveField.getField().getMissingValue().split(",");
            for (String value : values) {
                Long parsed = Utils.parseLong(value.trim());
                if (parsed != null) {
                    missing.add(parsed);
                }
            }
        }
        return missing;
    }
}
