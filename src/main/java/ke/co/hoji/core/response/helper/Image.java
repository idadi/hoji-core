package ke.co.hoji.core.response.helper;

/**
 * An abstraction for an image.
 */
public class Image {

    /**
     * The image data in bytes.
     */
    private final byte[] data;

    /**
     * The path to the file where the image is stored.
     */
    private final String filePath;

    public Image(byte[] data, String filePath) {
        this.data = data;
        this.filePath = filePath;
    }

    public byte[] getData() {
        return data;
    }

    public String getFilePath() {
        return filePath;
    }
}
