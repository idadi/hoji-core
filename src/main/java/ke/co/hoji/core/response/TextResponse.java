package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.model.LiveField;

/**
 * @author gitahi
 */
public class TextResponse extends Response<String, String> {

    protected TextResponse(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        super(hojiContext, liveField, value, actual);
    }

    /**
     * {@inheritDoc}
     *
     * @should return the parameter passed.
     */
    @Override
    public Comparable comparableFromRuleString(String ruleValue) {
        return ruleValue;
    }
}
