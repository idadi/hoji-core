package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Form.OutputStrategy;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author gitahi
 */
public class RecordResponse extends Response<Record, String> {

    private Record cachedActualValue;

    /**
     * @should set the actual value to the {@link Record} represented by the uuid value if actual is false.
     */
    protected RecordResponse(HojiContext hojiContext, LiveField liveField, Object value, Boolean actual) {
        super(hojiContext, liveField, value, actual);
    }

    @Override
    protected Record toActualValue(String value) {
        if (cachedActualValue == null) {
            cachedActualValue = hojiContext.getModelServiceManager().getRecordService()
                    .findRecordByUuid(value, liveField.getField());
        }
        return cachedActualValue;
    }

    /**
     * {@inheritDoc}
     *
     * @should return the uuid of the actual Record.
     */
    @Override
    public String toStorageValue() {
        return getActualValue() != null ? getActualValue().getUuid() : null;
    }

    @Override
    public List<DataElement> getValue(String renderingType) {
        List<DataElement> dataElements = new ArrayList<>();
        Record record = getActualValue();
        Map<Integer, LiveField> liveFieldBucket = indexLiveFields(record);
        List<Form> linkedForms = liveField.getField().getRecordForms();
        if (OutputStrategy.DOWNLOAD.equals(renderingType)) {
            String value = record != null ? record.getUuid() : "";
            dataElements.add(new DataElement(liveField.getField().getHeader(), value, liveField.getField().getDataElementType(null), ""));
        } else if (liveField.getField().getMatrixRecordField() != null) {
            if (record != null) {
                dataElements.add(new DataElement(liveField.getField().getHeader(), record.getCaption(), liveField.getField().getDataElementType(null), "" ));
            } else {
                dataElements.add(generateMissingDataElement(liveField.getField()));
            }
        } else {
            for (Form form : linkedForms) {
                for (Field field : form.getFields()) {
                    if (isQualifiedLinkedField(field)) {
                        LiveField liveField = liveFieldBucket.get(field.getId());
                        if (liveField != null) {
                            dataElements.addAll(liveField.getResponse().getValue(renderingType));
                        } else {
                            dataElements.add(generateMissingDataElement(field));
                        }
                    }
                }
            }
        }
        return dataElements;
    }

    private boolean isQualifiedLinkedField(Field field) {
        return field.isSearchable() &&
                ((field.getType().isChoice() && !field.getType().isMultiChoice())
                        || field.getType().isNumber() || field.getType().isTextual()
                        || field.getType().isRecord());
    }

    private DataElement generateMissingDataElement(Field field) {
        return new DataElement(field.getHeader(), null, field.getDataElementType(null), "");
    }

    private Map<Integer, LiveField> indexLiveFields(Record record) {
    	if (record != null) {
	    	Map<Integer, LiveField> indexed = new HashMap<Integer, LiveField>();
	    	for (LiveField liveField : record.getLiveFields()) {
				indexed.put(liveField.getField().getId(), liveField);
			}
	    	return indexed;
    	}
		return Collections.emptyMap();
	}
}
