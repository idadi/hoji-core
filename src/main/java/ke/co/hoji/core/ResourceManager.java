package ke.co.hoji.core;

/**
 * This class is used for managing custom resources, particularly dynamic instances of classes or interfaces that are
 * configured based on context. It provides static storage for one instance each of {@link HojiContext} and {@link
 * ModelServiceManager}.
 * <p/>
 * Generally, any module that uses the core API should initialize the ServiceManager as it will likely need it for
 * business logic and access to the DAO layer. However, only client modules should need to initialize the HojiContext.
 * <p/>
 * Modules are responsible for initializing these things at startup, as other code, including code in core will expect
 * the resources to existing later on and will not do any null checking themselves.
 *
 * @author gitahi
 */
public class ResourceManager {

    /**
     * The {@link HojiContext} instance.
     */
    private static HojiContext hojiContext;

    /**
     * The {@link ModelServiceManager} instance.
     */
    private static ModelServiceManager modelServiceManager;

    /**
     * Gets the {@link HojiContext} instance.
     *
     * @return the {@link HojiContext} instance.
     */
    public static HojiContext getHojiContext() {
        return hojiContext;
    }

    /**
     * Sets the {@link HojiContext} instance.
     *
     * @param hojiContext the {@link HojiContext} instance.
     */
    public static void setHojiContext(HojiContext hojiContext) {
        ResourceManager.hojiContext = hojiContext;
    }

    /**
     * Gets the {@link ModelServiceManager} instance.
     * <p/>
     * In general, callers that operate only in a Hoji client environment should obtain references to the ServiceManager
     * instance via the {@link HojiContext} by calling {@link HojiContext#getModelServiceManager()}. This allows them to
     * take advantage of the additional methods provided by the HojiContext, if necessary.
     * <p/>
     * Other types of callers, either that operate outside a Hoji client environment or that can operate in addtional
     * environment e.g. server side, should call this method directly to obtain a reference to the running {@link
     * ModelServiceManager} instance.
     *
     * @return the {@link ModelServiceManager} instance.
     */
    public static ModelServiceManager getModelServiceManager() {
        return modelServiceManager;
    }

    /**
     * Sets the {@link ModelServiceManager} instance.
     *
     * @param modelServiceManager the {@link ModelServiceManager} instance.
     */
    public static void setModelServiceManager(ModelServiceManager modelServiceManager) {
        ResourceManager.modelServiceManager = modelServiceManager;
    }
}
