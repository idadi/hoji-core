package ke.co.hoji.core.widget;

import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.LiveField;

import java.util.Map;

/**
 * Loads the appropriate {@link Widget} for a given {@link LiveField} based on the {@link FieldType#widgetClass}.
 *
 * @author gitahi
 */
public interface WidgetLoader {

    /**
     * Loads a {@link Widget} for the first time. Widgets are subsequently cached, so this method is called only
     * once for every type of Widget.
     *
     * @param liveField the {@link LiveField} for which to load a Widget.
     * @param params    additional arbitrary parameters, if any.
     * @return the loaded {@link Widget}.
     */
    Widget loadWidget(LiveField liveField, Map<String, Object> params);

    /**
     * Refreshes a cached {@link Widget}. Implementations should use this method to do whatever makes sense in
     * terms of refreshing a Widget, including nothing.
     *
     * @param params additional arbitrary parameters, if any.
     */
    void refreshWidget(Widget widget, Map<String, Object> params);
}
