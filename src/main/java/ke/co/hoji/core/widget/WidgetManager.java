package ke.co.hoji.core.widget;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.calculation.CalculationEvaluator;
import ke.co.hoji.core.calculation.CalculationException;
import ke.co.hoji.core.calculation.CalculationResult;
import ke.co.hoji.core.calculation.CalculationUtils;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.response.Response;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Manages the loading, caching and rendering of {@link Widget}s. Widgets are loaded only once and cached for reuse.
 * Also manages @{@link Response} inheritance.
 *
 * @author gitahi
 */
public class WidgetManager {

    /**
     * The @{@link WidgetLoader} to use for loading @{@link Widget}s.
     */
    private final WidgetLoader loader;

    /**
     * The @{@link WidgetRenderer} to use for rendering @{@link Widget}s.
     */
    private final WidgetRenderer renderer;

    /**
     * A cache of all loaded @{@link Widget}s so that we can reuse them.
     */
    private final Map<String, Widget> widgetCache = new HashMap<>();

    /**
     * A cache of the @{@link Response}s inherited from the previous @{@link Record} for when we need to pass them on
     * to the current @{@link Record}.
     */
    private final Map<Field, Response> responseCache = new HashMap<>();

    /**
     * Initializes a WidgetManager with a {@link WidgetLoader} and a {@link WidgetRenderer}.
     *
     * @param loader   The {@link WidgetLoader} to use for loading {@link Widget}s.
     * @param renderer The {@link WidgetRenderer} to use for rendering {@link Widget}s.
     */
    public WidgetManager(WidgetLoader loader, WidgetRenderer renderer) {
        this.loader = loader;
        this.renderer = renderer;
    }

    /**
     * Gets the {@link WidgetLoader} used for loading {@link Widget}s.
     */
    public WidgetLoader getWidgetLoader() {
        return loader;
    }

    /**
     * Gets the {@link WidgetRenderer} used for rendering {@link Widget}s.
     */
    public WidgetRenderer getWidgetRenderer() {
        return renderer;
    }

    public Widget renderWidget(
        Navigable navigable,
        NavigableHelper.Direction direction,
        boolean transitioning,
        LiveField liveField
    ) {
        return renderWidget(navigable, direction, transitioning, liveField, null);
    }

    /**
     * Loads and renders the appropriate {@link Widget} for a given {@link LiveField} on a given {@link Navigable}.
     * <p/>
     * This method internally calls {@link WidgetLoader#loadWidget(LiveField, Map)}. if the loaded widget is an instance
     * of {@link Navigable}, it is assigned the navigable passed here as the parent. This method then sets the navigable
     * passed as both the {@link ResponseChangedListener} and the {@link ResponseChangingListener} for the widget.
     * Finally, {@link WidgetManager#renderer} is called to actually render the loaded Widget.
     *
     * @param navigable     the {@link Navigable} component on which to renderWidget the Widget.
     * @param direction     the {@link NavigableHelper.Direction} of navigation.
     * @param transitioning true if transitioning from another {@link LiveField} and false otherwise.
     * @param liveField     the LiveField for which to renderWidget a Widget.
     * @param record        the current {@link Record}, in case we need to lookup piped values.
     * @return the Widget that was rendered.
     */
    Widget renderWidget(
        Navigable navigable,
        NavigableHelper.Direction direction,
        boolean transitioning,
        LiveField liveField,
        Record record
    ) {
        Widget widget = loadWidget(liveField, navigable.getParameters());
        widget.setNavigable(navigable);
        widget.setEventCause(WidgetEvent.EventCause.PROGRAM);
        if (widget instanceof Navigable) {
            Navigable nested = (Navigable) widget;
            nested.setParentNavigable(navigable);
        }
        widget.setResponseChangingListener(navigable);
        widget.setResponseChangedListener(navigable);
        renderer.render(this, widget, navigable, direction, transitioning, record);
        return widget;
    }

    /**
     * Prime a widget for data collection. This method should be called by implementations of {@link WidgetRenderer}
     * once they are finished rendering. The reason is that such methods may take time to be ready, for instance
     * if they are animating the rendering. As such, the incoming widget cannot be primed immediately in this
     * class since it might mess with the outgoing widget, particularly because fields of the same type always reuse
     * the same widget. If the widget passed here is flash-navihatable i.e. it loads a value automatically and thus does
     * not require user input, this method should return true. Othwerwise it should return false.
     *
     * @param navigable the {@link Navigable} on which the @{@link Widget} was rendered.
     * @param widget    the @{@link Widget} that was rendered.
     * @return whether or not the widget passed is flash-navigatable.
     */
    public boolean primeWidget(Navigable navigable, Widget widget) {
        HojiContext hojiContext = widget.getHojiContext();
        LiveField liveField = widget.getLiveField();
        Field field = liveField.getField();
        CalculationResult calculationResult = new CalculationResult(null, false);
        String valueScript = widget.getLiveField().getField().getValueScript();
        widget.setEditable(true);
        if (navigable instanceof Widget) {
            widget.setParentWidget((Widget) navigable);
        } else {
            widget.setParentWidget(null);
        }
        boolean calculationErrored = false;
        navigable.setResetText(widget.getResetText());
        if (field.hasScriptFunction(Field.ScriptFunctions.CALCULATE)) {
            Survey currentSurvey = hojiContext.getSurvey();
            Integer surveyId = currentSurvey != null ? currentSurvey.getId() : null;
            ModelServiceManager modelServiceManager = hojiContext.getModelServiceManager();
            Record record = navigable.getNavigator().getRecord();
            CalculationUtils calculationUtils = new CalculationUtils(
                surveyId,
                modelServiceManager,
                record.getLiveFields(),
                field,
                new Date(record.getDateCreated())
            );
            CalculationEvaluator calculationEvaluator = hojiContext.getCalculationEvaluator();
            try {
                Object calculatedValue = calculationEvaluator.evaluateScript(
                    valueScript,
                    Field.ScriptFunctions.CALCULATE,
                    calculationUtils,
                    field
                );
                if (calculatedValue instanceof CalculationResult) {
                    calculationResult = (CalculationResult) calculatedValue;
                } else {
                    calculationResult = new CalculationResult(calculatedValue, false);
                }
            } catch (CalculationException ex) {
                navigable.handleCalculationException(ex);
                calculationErrored = true;
            }
            widget.setEditable(calculationResult.getValue() == null && calculationResult.isEnableIfNull());
        }
        boolean hasPipedDefaultValue = false;
        boolean hasCalculatedValue = calculationResult.getValue() != null;
        if (!widget.isResuming()) {
            widget.prepare();
            widget.load();
            hasPipedDefaultValue = widget.reset();
            if (hasPipedDefaultValue) {
                widget.setEditable(false);
            }
            if (!hasPipedDefaultValue && !hasCalculatedValue) {
                widget.writeResponse();
            }
        }
        widget.ready();
        boolean hasInheritedResponse = false;
        if (field.isResponseInheriting()) {
            Response inheritedResponse = responseCache.get(field);
            if (inheritedResponse != null) {
                liveField.setResponse(inheritedResponse);
                widget.writeResponse();
                hasInheritedResponse = true;
            }
        }
        if (hasCalculatedValue) {
            liveField.setResponse(Response.create(hojiContext, liveField, calculationResult.getValue(), true));
            widget.writeResponse();
            navigable.setDirty(true);
        }
        widget.focus();
        navigable.setActiveWidget(widget);
        widget.setEventCause(WidgetEvent.EventCause.USER);
        boolean flashNavigable = false;
        if (!calculationErrored) {
            flashNavigable =
                hasInheritedResponse
                    || hasPipedDefaultValue
                    || (field.hasScriptFunction(Field.ScriptFunctions.CALCULATE)
                    && (calculationResult.getValue() != null || !calculationResult.isEnableIfNull()));
        }
        return flashNavigable;
    }

    /**
     * Clears cached {@link Widget}s so that future calls to
     * {@link #renderWidget(Navigable, NavigableHelper.Direction, boolean, LiveField, Record)} reload Widgets
     * afresh. This is useful when the UI theme is modified, for example, and dynamically loaded widgets need to
     * "get the memo".
     */
    public void clearWidgetCache() {
        widgetCache.clear();
    }

    /**
     * Cache a @{@link Response} from the current @{@link Record} as inheritance to be passed on to the  next
     * {@link Record}.
     *
     * @param field    the @{@link Field} by which to index the cached @{@link Response}.
     * @param response the @{@link Response} to cache.
     */
    void cacheResponse(Field field, Response response) {
        responseCache.put(field, response);
    }

    /**
     * Clears the {@link Response}s cached for the purposes of inheritance. Depending on the parameter passed, not
     * all {@link Response}s will be cleared. This method is never called in core. It is up to client implementations
     * to call it whenever they see fit.
     *
     * @param field if null, the entire cache is cleared, otherwise only the {@link Field}s after the
     *              {@link Response}s {@link Field} passed are cleared.
     */
    public void clearResponseCache(Field field) {
        if (field == null) {
            responseCache.clear();
        } else {
            for (Field f : responseCache.keySet()) {
                if (f.compareTo(field) > 0) {
                    responseCache.put(f, null);
                }
            }
        }
    }

    private Widget loadWidget(LiveField liveField, Map<String, Object> params) {
        Field field = liveField.getField();
        FieldType fieldType = field.getType();
        String className = fieldType.getWidgetClassByFieldParent(field.getParent());
        Widget widget = widgetCache.get(className);
        if (widget == null) {
            widget = loader.loadWidget(liveField, params);
            widgetCache.put(className, widget);
        } else {
            loader.refreshWidget(widget, params);
            widget.setLiveField(liveField);
        }
        return widget;
    }
}
