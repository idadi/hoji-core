package ke.co.hoji.core.widget;

/**
 * Objects that want to be notified whenever a {@link Widget} response is changing should implement this interface.
 *
 * @author gitahi
 */
public interface ResponseChangingListener {

    /**
     * The method called whenever a Widget's response is in the process of changing.
     *
     * @param event the {@link WidgetEvent}.
     */
    void responseChanging(WidgetEvent event);
}
