package ke.co.hoji.core.widget;

import ke.co.hoji.core.calculation.CalculationException;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.RecordDetail;
import ke.co.hoji.core.data.model.ValidationResult;
import ke.co.hoji.core.response.Response;

import java.util.Map;

/**
 * A UI component used as a container of {@link Widget}s and that a user may navigate through in a wizard fashion.
 * Navigation inside a Navigable component is provided by a {@link Navigator}.
 * <p/>
 * Implementations must delegate applicable functionality to an instance of the {@link NavigableHelper} class or its
 * subclasses for consistency. They cannot directly call {@link Navigator} navigation methods as those are package
 * private.
 *
 * @param <C> the type of the underlying widget container for this Navigable.
 * @param <L> the type of the underlying widget label for this Navigable.
 * @author gitahi
 */
public interface Navigable<C, L> extends ResponseChangingListener, ResponseChangedListener {

    /**
     * This method is called by the {@link Navigator} before moving next to determine if the {@link Response} to the
     * outgoing field changed and needs to be saved..
     *
     * @return true if the {@link Response} to the outgoing field changed and false otherwise.
     */
    boolean isDirty();

    /**
     * This method is called by the {@link Navigator} when a field's response needs to be marked as dirty or clean.
     *
     * @param dirty true for dirty and false for clean.
     */
    void setDirty(boolean dirty);

    /**
     * Called whenever a {@link Widget} has just been rendered. It is {@link WidgetRenderer} implementations that
     * "know for sure" when rendering is complete, as they may choose to do fancy things such as animation, which may
     * occur in a background thread. As such, they are responsible for calling this method.
     *
     * @param widget the Widget rendered.
     * @param record the Record associated with the LiveField rendered.
     */
    void rendered(Widget widget, Record record);

    /**
     * Called by the {@link Navigator} whenever a {@link LiveField} has just been saved.
     *
     * @param liveField the LiveField saved.
     * @param record    the Record associated with the LiveField saved.
     */
    void saved(LiveField liveField, Record record);

    /**
     * This method is called by the {@link Navigator} when navigating from the first field of the record for the first
     * time if the {@link Form#gpsCapture} setting requires an attempt to capture GPS data to be made. Navigable
     * implementations should pop up a progressbar while attempting to capture satisfactory GPS data. Thereafter they
     * should recall {@link Navigator#next(Navigable, int, int, int)} with the appropriate {@link Navigator.Interruption}
     * flags.
     *
     * @param form the current {@link Form}.
     */
    void processGps(Form form);

    /**
     * This method is called by the {@link Navigator} if the next {@link Field} about to be rendered would violate skip
     * logic. Navigable implementations should ask the user to allow or disallow the violation and then recall {@link
     * Navigator#next(Navigable, int, int, int)} with the appropriate {@link Navigator.Interruption} flags.
     */
    void processSkipViolation();

    /**
     * This method is called by the {@link Navigator} if the {@link Widget} returned by
     * {@linkNavigable#getActiveWidget() } fails to validate with a {@link ValidationResult#WARN} or {@link
     * ValidationResult#STOP} signal. Implementations should alert the user of the violation and allow them to make the
     * applicable choices. They should then recall {@link Navigator#next(Navigable, int, int, int)} with the appropriate
     * {@link Navigator.Interruption} flags.
     */
    void processValidation();

    /**
     * This method is called by the {@link Navigator} once skip violated fields have been cleared. Implementations may
     * use this method to report the number of live fields cleared.
     *
     * @param cleared the number of live fields cleared.
     */
    void clearedViolatedFields(int cleared);

    /**
     * This method is called by the {@link Navigator} when there are no more unprocessed fields in the form, meaning the
     * end of the record has been reached. Implementations may use this to:
     * <p>
     * a) Notify the user that they have gotten to the end of the current Record.
     * <p>
     * b) Perform anly last-minute validation and prevent the user from completing the record.
     * <p>
     * c) Automatically navigate the user away from the data entry screen to a home page or some other logical place,
     * perhaps in readiness to start a new record.
     * <p>
     * To fully commit to COMPLETE a MainRecord, implementations should call {@link Navigator#finalizeRecord(Record)},
     * otherwise the current Record is left in DRAFT status. This does not apply for MatrixRecords.
     *
     * @param record    the Record that just ended.
     * @param navigator the {@link Navigator} to call to finalize the record. See
     *                  {@link Navigator#finalizeRecord(Record)}.
     */
    void processRecordEnd(Record record, Navigator navigator);

    /**
     * A Navigable component is main if it is the top level navigable component - as opposed to a nested Navigable
     * component such as a given implementation of a matrix widget. There should only be one main navigable component.
     * All other navigable components should return false for this method.
     *
     * @return true if main.
     */
    boolean isMain();

    /**
     * @return the {@link FieldParent} to provide {@link Field}s for this Navigable.
     */
    FieldParent getFieldParent();

    /**
     * @return the {@link Navigator} for this Navigable.
     */
    Navigator getNavigator();

    /**
     * @param navigator the {@link Navigator} for this Navigable.
     */
    void setNavigator(Navigator navigator);

    /**
     * Gets the parent({@link Navigable}) of this Navigable if this navigable is nested. Otherwise returns null. The
     * parent navigable, if set, is will be used to save the {@link FieldParent} of this Navigable.
     *
     * @return the parent({@link Navigable}).
     */
    Navigable getParentNavigable();

    /**
     * @param parent the parent({@link Navigable}) in which this Navigable is nested.
     */
    void setParentNavigable(Navigable parent);

    /**
     * @return the container that holds {@link Widget}s for this Navigable.
     */
    C getWidgetContainer();

    /**
     * @return the label used to render {@link Widget} captions for this Navigable.
     */
    L getFieldLabel();

    /**
     * @return the {@link Widget} currently rendered for this Navigable.
     */
    Widget getActiveWidget();

    /**
     * @param widget the {@link Widget} currently rendered for this Navigable.
     */
    void setActiveWidget(Widget widget);

    /**
     * Set the reset text that prompts a user to reset a {@link Widget}.
     *
     * @param resetText the reset text.
     */
    void setResetText(String resetText);

    /**
     * Navigate to the first {@link Widget}.
     */
    void first();

    /**
     * Navigate to the previous {@link Widget}.
     */
    void previous();

    /**
     * Navigate to the next {@link Widget}.
     */
    void next();

    /**
     * Reset the active {@link Widget}.
     */
    void resetWidget();

    /**
     * Navigate to the last {@link Widget}.
     */
    void last();

    /**
     * Gets any parameters that this Navigable wishes to pass to {@link WidgetLoader} and/or {@link WidgetRenderer} e.g
     * any context specific information.
     *
     * @return the parameters
     */
    Map<String, Object> getParameters();

    /**
     * Gets the {@link RecordDetail} represented by this Navigable.
     *
     * @return the RecordDetail.
     */
    RecordDetail getDetail();

    /**
     * Reads the {@link Response} of the currently active {@link Widget}.
     *
     * @return the response.
     */
    Response getResponse();

    /**
     * Validates the currently active {@link Widget}.
     *
     * @return the {@link ValidationResult}.
     */
    ValidationResult validateWidget();

    /**
     * Returns true if this Navigable should be auto-navigated and false otherwise. This value should typically be
     * obtained via a user setting.
     *
     * @return whether this Navigable should be auto-navigated.
     */
    boolean autoNavigate();

    /**
     * Returns true if this Navigable should be flash-navigated and false otherwise. This value should typically be
     * obtained via a user setting. Flash navigation means that if a field has a pre-filled value, either via piping
     * or calculation, the app should quickly and automatically navigate to the next field.
     *
     * @return whether this Navigable should be auto-navigated.
     */
    boolean flashNavigate();

    /**
     * Sets a flag saying whether this Navigable is already in the process of navigating. While in the process of
     * navigating, the Navigable should ignore all new requests for navigation in order to maintain the integrity of
     * the navigation sequence.
     * <p>
     * Navigating should be set to true whenever the process of rendering a new {@link LiveField} is initiated.
     * Similarly, it should be set to false once the rendering process is complete i.e. when
     * {@link Navigable#rendered(Widget, Record)} is called.
     *
     * @param navigating whether or not this Navigable is navigating.
     */
    void setNavigating(boolean navigating);

    /**
     * Gets the flag that says whether or not this Navigable is in the process of navigating. Whule in the process of
     * navigating, the Navigable should ignore all new requests for navigation in order to maintain the integrity of
     * the navigation sequence.
     *
     * @return true if this Navigable is in the process of navigating and false otherwise.
     */
    boolean isNavigating();

    /**
     * Handle a {@link CalculationException}, presumably be showing a message to the user but potentially
     * however else.
     *
     * @param ex the {@link CalculationException}.
     */
    void handleCalculationException(CalculationException ex);
}
