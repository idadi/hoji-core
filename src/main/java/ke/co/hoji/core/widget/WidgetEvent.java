package ke.co.hoji.core.widget;

/**
 * An object representing an interesting event occurring inside a {@link Widget}.
 *
 * @author gitahi
 */
public final class WidgetEvent {

    /**
     * Possible causes of a WidgetEvent.
     */
    public enum EventCause {

        /**
         * This event was caused by the user by interacting directly with interface. The user interface should respond
         * accordingly.
         */
        USER,

        /**
         * This event was raised by the program. The user interface should not respond to it.
         */
        PROGRAM,

        /**
         * This event was raised by the program but to simulate an event raised by the user. The user interface should
         * respond accordingly. This is used to support flash navigation, where a @{@link ke.co.hoji.core.response.Response}
         * is set on a @{@link Widget} by the program but it should be treated as if the user himself set it.
         */
        SIMULATION
    }

    private final Widget source;
    private final EventCause eventCause;
    private boolean directionAgnostic = false;

    /**
     * Initiates a new WidgetEvent.
     *
     * @param source     the source {@link Widget} of the event.
     * @param eventCause the {@link EventCause} of the event.
     */
    public WidgetEvent(Widget source, EventCause eventCause) {
        this.source = source;
        this.eventCause = eventCause;
    }

    /**
     * Initiates a new WidgetEvent.
     *
     * @param source            the source {@link Widget} of the event.
     * @param eventCause        the {@link EventCause} of the event.
     * @param directionAgnostic the direction agnosticism of the event. @see {@link #isDirectionAgnostic()}.
     */
    public WidgetEvent(Widget source, EventCause eventCause, boolean directionAgnostic) {
        this(source, eventCause);
        this.directionAgnostic = directionAgnostic;
    }

    /**
     * Gets the {@link Widget} that originated the event.
     *
     * @return the source {@link Widget} of the event.
     */
    public Widget getSource() {
        return source;
    }

    /**
     * Gets the {@link EventCause} of the event.
     *
     * @return the {@link EventCause}.
     */
    public EventCause getEventCause() {
        return eventCause;
    }

    /**
     * Gets the direction agnosticism of the event. A value o true means that navigation decisions affecting the
     * event should not take into account the {@link NavigableHelper.Direction} of navigation. By default, this
     * method return false.
     *
     * @return the source {@link Widget} of the event.
     */
    public boolean isDirectionAgnostic() {
        return directionAgnostic;
    }
}
