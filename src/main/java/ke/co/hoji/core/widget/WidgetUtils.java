package ke.co.hoji.core.widget;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.MultipleChoiceResponse;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.response.SingleChoiceResponse;
import ke.co.hoji.core.service.model.RecordService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import static ke.co.hoji.core.Utils.sortChoicesAlphabetically;
import static ke.co.hoji.core.Utils.sortChoicesRandomly;

/**
 * Utility methods for the widget sub-system.
 * <p>
 * Created by gitahi on 1/29/17.
 */
public class WidgetUtils {

    /**
     * Get the {@link Choice}s to be rendered as options for any field that offers choices. The choices may be
     * filtered by their parent and /or randomized as specified by the relevant configuration.
     *
     * @param field  the @{@link Field} to which the choices belong
     * @param record the {@link Record} currently associated with the {@link Widget} for the requesting for choices.
     * @return a list of Choices
     */
    public static List<Choice> getChoices(
        Field field,
        Record record,
        Record mainRecord,
        RecordService recordService
    ) {
        List<Choice> choices = new ArrayList<>();
        if (field.getChoiceGroup() != null) {
            if (field.getChoiceFilterField() != null) {
                LiveField filterLiveField = null;
                if (record != null) {
                    filterLiveField = record.find(field.getChoiceFilterField());
                }
                if (filterLiveField != null) {
                    Response response = filterLiveField.getResponse();
                    if (response != null && response.getActualValue() != null) {
                        if (response instanceof SingleChoiceResponse) {
                            Choice parent = ((SingleChoiceResponse) response).getActualValue();
                            choices.addAll(getChildChoices(field, parent));
                        } else if (response instanceof MultipleChoiceResponse) {
                            LinkedHashMap<Choice, Boolean> choiceMap = ((MultipleChoiceResponse) response).getActualValue();
                            for (Choice parent : choiceMap.keySet()) {
                                Boolean checked = choiceMap.get(parent);
                                if (checked != null && checked) {
                                    choices.addAll(getChildChoices(field, parent));
                                }
                            }
                        }
                    }
                }
            } else {
                choices.addAll(getChildChoices(field, null));
            }
            if (field.getType().isSingleChoice() && field.getUniqueness() == Field.Uniqueness.SIMPLE.getValue()) {
                markAlreadyEnteredChoices(choices, field, record, mainRecord, recordService);
            }
            int orderingStrategy = field.getChoiceGroup().getOrderingStrategy();
            if (orderingStrategy != ChoiceGroup.OrderingStrategy.AS_SPECIFIED) {
                orderChoices(choices, orderingStrategy, field.getChoiceGroup().getExcludeLast());
            }
        }
        return choices;
    }

    private static List<Choice> getChildChoices(Field field, Choice parent) {
        List<Choice> allChoices = new ArrayList<>(field.getChoiceGroup().getChoices());
        if (parent == null) {
            return allChoices;
        }
        List<Choice> children = new ArrayList<>();
        for (Choice choice : allChoices) {
            if (parent.equals(choice.getParent())) {
                children.add(choice);
            }
        }
        return children;
    }

    private static void orderChoices(List<Choice> choices, int orderingStrategy, Integer excludeLast) {
        List<Choice> exclude = new ArrayList<>();
        int n = choices.size() - 1;
        if (choices != null && !choices.isEmpty() && excludeLast < n) {
            if (excludeLast != null && excludeLast != 0) {
                int c = n - excludeLast;
                for (int i = n; i > c; i--) {
                    exclude.add(choices.get(i));
                }
            }
            choices.removeAll(exclude);
            if (orderingStrategy == ChoiceGroup.OrderingStrategy.RANDOM) {
                sortChoicesRandomly(choices);
            } else if (orderingStrategy == ChoiceGroup.OrderingStrategy.ALPHABETICAL) {
                sortChoicesAlphabetically(choices);
            }
            Collections.sort(exclude);
            choices.addAll(exclude);
        }
    }

    private static void markAlreadyEnteredChoices(
        List<Choice> choices,
        Field field,
        Record record,
        Record mainRecord,
        RecordService recordService
    ) {
        Set<Integer> usedChoiceIds = recordService.getUsedChoiceIds(field, record, mainRecord);
        for (Choice choice : choices) {
            if (usedChoiceIds.contains(choice.getId())) {
                choice.setAlreadyEntered(true);
            } else {
                choice.setAlreadyEntered(false);
            }
        }
    }
}
