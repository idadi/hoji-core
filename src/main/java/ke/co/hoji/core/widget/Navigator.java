package ke.co.hoji.core.widget;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.NavigableList;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.MainRecord;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.data.model.ValidationResult;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.rule.RuleEvaluator;
import ke.co.hoji.core.service.model.RecordService;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;

/**
 * Provides methods for navigating through a {@link Record} in a {@link Navigable} UI component.
 *
 * @author gitahi
 */
public final class Navigator {

    /**
     * Provides constants for processing navigation interruptions.
     * <p>
     * When navigating forwards from one {@link Field} to the next, the {@link Navigator} may detect interesting
     * events that must be handled outside the Navigator e.g. capturing GPS data or arriving at the end of a
     * {@link Record}.
     * <p>
     * When such events are detected, they are passed on to the associated {@link Navigable} for processing. Such
     * processing may include offering the user some feedback e.g. a progressbar showing the progress of GPS data
     * acquisition or indeed collecting user input.
     * <p>
     * Regardless of the processing performed by the Navigable, control must be returned to the Navigator by
     * recalling the {@link #next(Navigable, int, int, int)} method with the appropriate {@link Interruption} flags.
     * These flags advise the Navigator on how to proceed.
     */
    public static class Interruption {

        /**
         * This is the initial setting for all supported types of interruptions. It says to the Navigator to pass on
         * any applicable interruptions to the {@link Navigable} and ask it to deal with it as necessary.
         */
        public static final int CHECK = 0;

        /**
         * This is setting is used when the {@link Navigable} is passing back control to the {@link Navigator} by
         * recalling the {@link #next(Navigable, int, int, int)} method. It says "I am done processing the
         * interruption and you may proceed normally".
         */
        public static final int EXECUTE = 1;

        /**
         * This is setting is used when the {@link Navigable} is passing back control to the {@link Navigator} by
         * recalling the {@link #next(Navigable, int, int, int)} method. It says "I am done processing the
         * interruption but please abort navigation because there's something the user needs to deal with".
         */
        public static final int ABORT = 2;
    }

    /**
     * Describes the next {@link Field} in the context of forward navigation without taking into consideration any
     * Fields in the {@link #backwardStack}.
     * <p>
     * The sequential next field may be a) the field that immediately follows the current one, entirely unaffected
     * by any forward {@link Rule} or affected by a useless forward rule that doesn't go anywhere else or b) a field
     * ahead of (a) above arrived at by a useful forward rule or c) a field ahead of (a) above arrived at because all
     * other fields between it and (a) were passed by their own backward rules.
     * <p>
     * In all but the case (a) above, the SequentialNext object must indicate that the Field being rendered is because
     * of {@link Rule} evaluation and is not the one that would naturally be next if no rules applied.
     */
    private static class SequentialNext {

        /**
         * The sequential next {@link Field}.
         */
        private final Field field;

        /**
         * Whether or not the sequential next {@link #field} above was arrived at as a result of the execution of one
         * or more {@link Rule}s.
         */
        private final boolean becauseOfRule;

        private SequentialNext(Field field, boolean becauseOfRule) {
            this.field = field;
            this.becauseOfRule = becauseOfRule;
        }

        private Field getField() {
            return field;
        }

        private boolean isBecauseOfRule() {
            return becauseOfRule;
        }
    }

    /**
     * Describes the next {@link Field} in the context of forward navigation while taking into consideration any
     * Fields in the {@link #backwardStack}.
     * <p>
     * The true next field may be a) the {@link SequentialNext#field} or b) the field peeked from the
     * {@link #backwardStack}. If the (a) and (b) are not the same and (a) is {@link SequentialNext#becauseOfRule},
     * then (a) is designated as {@link TrueNextField#violator} and {@link TrueNextField#violent} is set to
     * true. Otherwise (a) is designated as {@link TrueNextField#abider} and {@link TrueNextField#violent} is set to
     * false. The value of {@link TrueNextField#violent} advises the {@link Navigable} as to whether navigating forward
     * would cause all subsequent fields to be deleted or not, so that the user may be warned accordingly.
     *
     * @author gitahi
     */
    private static class TrueNextField {

        /**
         * Whether or not the next indicated field would violate any {@link Rule}s in relation to already existing
         * subsequent {@link Field}s as recorded in the {@link #backwardStack}. This typically happens when a user
         * navigate backwards and then changes a response to an earlier field in a way that cause relevant rules to
         * navigate in a different direction. This means all fields in the abandoned direction must be cleared of their
         * responses before navigating in the new direction. A value of true means branching rule(s) have been violated.
         */
        private final boolean violent;

        /**
         * The next field to navigate to given that branching {@link Rule}(s) have been violated. This attribute is
         * not set if no Rules are violated i.e. if violent = false.
         */
        private final Field violator;

        /**
         * The next field to navigate to given when branching {@link Rule}(s) been violated. This field is always set, even
         * if Rule(s) were violated. The reason for this is that the user may, upon being notified of the violation, decide
         * to NOT go ahead in the new direction.
         */
        private final Field abider;

        public TrueNextField(boolean violent, Field violator, Field abider) {
            this.violent = violent;
            this.violator = violator;
            this.abider = abider;
        }

        public boolean isViolent() {
            return violent;
        }

        public Field getViolator() {
            return violator;
        }

        public Field getAbider() {
            return abider;
        }
    }

    private final WidgetManager widgetManager;
    private final RuleEvaluator ruleEvaluator;
    private final RecordService recordService;
    private final Record record;
    private LiveField liveField;

    private final Deque<Field> forwardStack = new ArrayDeque<>();
    private final Deque<Field> backwardStack = new ArrayDeque<>();

    /**
     * Initializes a new Navigator.
     *
     * @param ruleEvaluator the {@link RuleEvaluator} for processing conditional navigation rules. aka forward and
     *                      backward skips.
     * @param recordService the {@link RecordService} to be used for persisting records.
     * @param record        {@link Record} to be navigated.
     */
    public Navigator(WidgetManager widgetManager, RuleEvaluator ruleEvaluator, RecordService recordService,
                     Record record) {
        this.widgetManager = widgetManager;
        this.ruleEvaluator = ruleEvaluator;
        this.recordService = recordService;
        this.record = record;
    }

    /**
     * @return the {@link Record} navigated by this Navigator.
     */
    public Record getRecord() {
        return record;
    }

    /**
     * @return the current {@link LiveField} in this Navigator.
     */
    protected LiveField getLiveField() {
        return liveField;
    }

    /**
     * Sets the current LiveField in this Navigator.
     *
     * @param liveField the LiveField to set.
     */
    protected void setLiveField(LiveField liveField) {
        this.liveField = liveField;
    }

    /**
     * Tests whether the {@link Record} in this Navigator is complete. A Record is complete if it is not legally
     * possible to navigate forward to another @{@link Field} from the current last Field. This method is
     * intended for use in validating that all records in a matrix are complete.
     *
     * @param hojiContext the {@link HojiContext} within which this method is being executed.
     *
     * @return true if complete and false otherwise.
     */
    public boolean isRecordComplete(HojiContext hojiContext) {
        //Set responses on the fields for the purposes of rule evaluation.
        for (LiveField lf : record.getLiveFields()) {
            Response response = lf.getResponse();
            if (response != null) {
                response.setHojiContext(hojiContext);
                lf.getField().setResponse(response);
            }
        }
        liveField = new NavigableList<>(record.getLiveFields()).last();
        if (liveField != null) {
            last();
            TrueNextField trueNextField = calculateNext(record);
            Field next = trueNextField.getAbider();
            if (next == null) {
                return true;
            }
        }
        return false;
    }

    /**
     * Reorganizes the navigator internal structure and returns the first {@link LiveField} to be navigated to. This
     * should always be the first navigation method called on a new Navigator.
     *
     * @return the first {@link Field} of this {@link Navigator#record}'s {@link FieldParent}.
     */
    protected LiveField first() {
        LiveField first = record.first();
        if (first == null) {
            FieldParent parent = record.getFieldParent();
            if (parent.getChildren() == null || parent.getChildren().isEmpty()) {
                return null;
            }
            Field firstField = new NavigableList<>(parent.getChildren()).first();
            first = new LiveField(firstField);
        }
        while (!forwardStack.isEmpty()) {
            Field popped = forwardStack.pop();
            Field current = liveField.getField();
            if (!popped.equals(first.getField())) {
                if (current.compareTo(popped) > 0) {
                    pushBackward(current);
                }
                pushBackward(popped);
            } else {
                pushBackward(current);
            }
        }
        return first;
    }

    /**
     * Reorganizes the navigator internal structure and returns the previous {@link LiveField} to be navigated to.
     *
     * @return the previous {@link Field} of this {@link Navigator#record}'s {@link FieldParent} relative to the current
     * {@link Navigator#liveField}.
     */
    protected LiveField previous() {
        if (liveField == null) {
            return null;
        }
        Field previousField = calculatePrevious(liveField.getField());
        if (previousField != null) {
            if (!forwardStack.isEmpty()) {
                if (forwardStack.peek().equals(previousField)) {
                    forwardStack.pop();
                }
            }
            if (backwardStack.isEmpty()) {
                pushBackward(liveField.getField());
            } else {
                if (!backwardStack.peek().equals(liveField.getField())) {
                    pushBackward(liveField.getField());
                }
            }
            LiveField previous = record.find(previousField);
            if (previous == null) {
                previous = record.last();
            }
            return previous;
        }
        return null;
    }

    /**
     * Reorganizes the navigator internal structure, saves the current {@link #liveField} and returns the next {@link
     * LiveField} to be navigated to.
     *
     * @param navigable              the {@link Navigable} that provides UI navigation.
     * @param gpsInterruption        the {@link Interruption} flag for {@link Form.GpsCapture} processing.
     * @param violationInterruption  the {@link Interruption} flag for skip pattern violation.
     * @param validationInterruption the {@link Interruption} flag for validation.
     *
     * @return the next {@link Field} of this {@link Navigator#record}'s {@link FieldParent} relative to the current
     * {@link Navigator#liveField}. The next Field may or may not be the sequentially next Field based on backward ot
     * forward skips. @see {@link Field#getForwardSkips()} and {@link Field#getBackwardSkips()}.
     */
    protected LiveField next(Navigable navigable, int gpsInterruption, int violationInterruption, int validationInterruption) {
        if (gpsInterruption != Interruption.ABORT
            && violationInterruption != Interruption.ABORT
            && validationInterruption != Interruption.ABORT) {
            if (validationInterruption == Interruption.CHECK) {
                ValidationResult validationResult = navigable.validateWidget();
                if (validationResult.getSignal() != ValidationResult.OKAY) {
                    if (validationResult.getSignal() != ValidationResult.ERROR) {
                        navigable.processValidation();
                    }
                    return null;
                }
            }
            if (record.isMain() && record.getUuid() == null) {
                Form form = (Form) record.getFieldParent();
                if (form.getGpsCapture() != Form.GpsCapture.NEVER) {
                    if (gpsInterruption == Interruption.CHECK) {
                        navigable.processGps(form);
                        return null;
                    }
                }
            }
            Response response = navigable.getResponse();
            liveField.getField().setResponse(response);
            TrueNextField trueNextField = calculateNext(record);
            liveField.getField().setResponse(null);
            Field next = trueNextField.getAbider();
            if (trueNextField.isViolent()) {
                if (violationInterruption == Interruption.CHECK) {
                    navigable.processSkipViolation();
                    return null;
                } else if (violationInterruption == Interruption.EXECUTE) {
                    int cleared = clearViolated();
                    if (cleared > -1) {
                        navigable.clearedViolatedFields(cleared);
                        next = trueNextField.getViolator();
                    } else {
                        return null;
                    }
                }
            }
            save(navigable, response);
            if (liveField.getField().isResponseInheriting()) {
                widgetManager.cacheResponse(liveField.getField(), response);
            }
            if (!backwardStack.isEmpty()) {
                if (backwardStack.peek().equals(next)) {
                    backwardStack.pop();
                }
            }
            pushForward(liveField.getField());
            if (next != null) {
                LiveField nextLiveField = record.find(next);
                if (nextLiveField == null) {
                    nextLiveField = new LiveField(next);
                }
                return nextLiveField;
            } else {
                navigable.processRecordEnd(record, this);
            }
        }
        return null;
    }


    /**
     * Change the status of a MainRecord from DRAFT to COMPLETE so that it becomes eligible for submission to the
     * server.
     *
     * @param record the Record to finalize.
     */
    public void finalizeRecord(Record record) {
        if (record.isMain() && record.getDetail() != null) {
            MainRecord mainRecord = (MainRecord) record.getDetail();
            if (mainRecord.getStage() == MainRecord.Stage.DRAFT) {
                mainRecord.setStage(MainRecord.Stage.COMPLETE);
                recordService.updateStage(mainRecord, true);
            }
        }
    }

    /**
     * Reorganizes the navigator internal structure and returns the last {@link LiveField} to be navigated to.
     *
     * @return the last {@link Field} of this {@link Navigator#record}'s list of {@link LiveField}s. Note that this
     * method does NOT navigate to the last {@link Field} of this {@link Navigator#record}'s {@link FieldParent}.
     */
    protected LiveField last() {
        if (liveField == null) {
            return null;
        }
        Field field = liveField.getField();
        LiveField lastLiveField = record.last();
        if (lastLiveField != null) {
            Field last = lastLiveField.getField();
            while (!backwardStack.isEmpty()) {
                Field popped = backwardStack.pop();
                if (!popped.equals(last)) {
                    if (field.compareTo(popped) < 0) {
                        pushForward(field);
                    }
                    pushForward(popped);
                } else {
                    pushForward(field);
                }
            }
            return record.find(last);
        }
        return null;
    }

    /**
     * Reorganizes the navigator internal structure and returns the {@link LiveField} to be navigated to.
     *
     * @param field the {@link Field} to navigate to.
     *
     * @return the specified {@link Field}.
     */
    protected final LiveField navigateTo(Field field) {
        forwardStack.clear();
        backwardStack.clear();
        LiveField toGoTo = record.find(field);
        if (toGoTo == null) {
            toGoTo = new LiveField(field);
        }
        List<LiveField> all = new ArrayList<>(record.getLiveFields());
        List<LiveField> forward = new ArrayList<>();
        List<LiveField> backward = new ArrayList<>(all);
        for (LiveField lf : all) {
            if (!lf.getField().equals(toGoTo.getField())) {
                pushForward(lf.getField());
                forward.add(lf);
            } else {
                backward.removeAll(forward);
                Collections.reverse(backward);
                for (LiveField l : backward) {
                    pushBackward(l.getField());
                }
                break;
            }
        }
        if (!backwardStack.isEmpty()) {
            backwardStack.pop();
        }
        return toGoTo;
    }

    protected void deleteRecord(Record record) {
        recordService.delete(record);
    }

    private void pushForward(Field field) {
        if (!forwardStack.contains(field)) {
            forwardStack.push(field);
        }
    }

    private void pushBackward(Field field) {
        if (record.contains(new LiveField(field))) {
            if (!backwardStack.contains(field)) {
                backwardStack.push(field);
            }
        }
    }

    private Field calculatePrevious(Field current) {
        Field previous = null;
        if (!forwardStack.isEmpty()) {
            if (forwardStack.peek().equals(current)) {
                Field popped = forwardStack.pop();
                pushBackward(popped);
                if (!forwardStack.isEmpty()) {
                    previous = forwardStack.peek();
                }
            } else {
                previous = forwardStack.peek();
            }
        }
        return previous;
    }

    private TrueNextField calculateNext(Record record) {
        boolean violent = false;
        Field violator = null;
        Field abider;
        SequentialNext sequentialNext = sequentialNext(
            record,
            liveField.getField(),
            liveField.getField(),
            false,
            false);
        Field sequentialNextField = sequentialNext != null ? sequentialNext.getField() : null;
        boolean sequentialNextIsBecauseOfRule = sequentialNext != null ? sequentialNext.isBecauseOfRule() : false;
        if (backwardStack.isEmpty()) {
            abider = sequentialNextField;
        } else {
            Field backwardStackNext = null;
            if (backwardStack.peek().equals(liveField.getField())) {
                Field popped = backwardStack.pop();
                pushForward(popped);
                if (!backwardStack.isEmpty()) {
                    backwardStackNext = backwardStack.peek();
                }
            } else {
                backwardStackNext = backwardStack.peek();
            }
            if (backwardStackNext == null) {
                abider = sequentialNextField;
            } else {
                if (!sequentialNextIsBecauseOfRule) {
                    abider = sequentialNextField;
                } else {
                    abider = backwardStackNext;
                    if (!backwardStackNext.equals(sequentialNextField)) {
                        violent = true;
                        violator = sequentialNextField;
                    }
                }
            }
        }
        return new TrueNextField(violent, violator, abider);
    }

    private SequentialNext sequentialNext(
        Record record,
        Field current,
        Field naturalFrom,
        boolean pass,
        boolean becauseOfRule
    ) {
        Field nextByNature = naturalNext(record, naturalFrom);
        Field nextByForwardRule = null;
        Field nextByWinning = null;
        if (!pass) {
            nextByForwardRule = forwardRuleNext(record, current);
        }
        if (nextByForwardRule != null) {
            nextByWinning = nextByForwardRule;
            if (!nextByForwardRule.equals(nextByNature)) {
                becauseOfRule = true;
            }
        } else {
            if (nextByNature != null) {
                nextByWinning = nextByNature;
            }
        }
        if (nextByWinning != null) {
            if (showByBackRule(record, nextByWinning, current)) {
                return new SequentialNext(nextByWinning, becauseOfRule);
            } else {
                if (backwardStack.contains(nextByWinning)) {
                    becauseOfRule = true;
                }
                return sequentialNext(record, current, nextByWinning, true, becauseOfRule);
            }
        }
        return null;
    }

    private Field naturalNext(Record record, Field from) {
        return new NavigableList<>(record.getFields()).next(from);
    }

    private Field forwardRuleNext(Record record, Field from) {
        if (from.hasForwardSkips()) {
            for (Rule rule : from.getForwardSkips()) {
                if (ruleEvaluator.evaluateRule(rule, record, from)) {
                    return rule.getTarget();
                }
            }
        }
        return null;
    }

    private boolean showByBackRule(Record record, Field field, Field current) {
        if (field.hasBackwardSkips()) {
            return ruleEvaluator.evaluateRules(field.getBackwardSkips(), record,
                current);
        }
        return true;
    }

    /**
     * Saves the current live field i.e. {@link Navigator#liveField}, with the given response.
     *
     * @param response the {@link Response} to be assigned to the current live field.
     */
    private void save(Navigable navigable, Response response) {
        activateLiveField(navigable, response);
        if (record.getDetail() == null) {
            record.setDetail(navigable.getDetail());
        }
        LiveField active = record.getActiveLiveField();
        if (active.getUuid() == null || navigable.isDirty()) {
            if (active.getField().isCaptioning()) {
                LiveField captioningLiveField = active;
                int n = record.getLiveFields().size() - 1;
                for (int i = n; i > 0; i--) {
                    LiveField lf = record.getLiveFields().get(i);
                    if (lf.compareTo(active) > 0 && lf.getField().isCaptioning()) {
                        captioningLiveField = lf;
                        break;
                    }
                }
                Response captionResponse = captioningLiveField.getResponse();
                if (captionResponse != null) {
                    record.setCaption(captionResponse.displayString());
                }
            }
            String uniqueValue = record.getUniqueValue();
            if (active.getField().getUniqueness() == Field.Uniqueness.COMPOUND.getValue()) {
                uniqueValue = getCompoundKey();
            }
            record.setUniqueValue(uniqueValue);
            record.setSearchTerms(getSearchTerms(record));
            recordService.save(record);
            record.addLiveField(active);
        } else {
            record.addLiveField(active);
        }
        {
            // If the field we just saved is a child field, activate the parent live field so that it updates its own
            // response with the newly saved live field.
            if (!navigable.isMain()) {
                Navigable parentNavigable = navigable.getParentNavigable();
                Navigator parentNavigator = parentNavigable.getNavigator();
                parentNavigator.activateLiveField(parentNavigable, parentNavigable.getResponse());
            }
        }
        navigable.saved(liveField, record);
        navigable.setDirty(false);
    }

    private String getSearchTerms(Record record) {
        String recordUuid = record.getUuid();
        if (recordUuid == null) {
            recordUuid = "id";
        }
        String searchTerms = "|" + recordUuid + "|";
        List<Field> searchableFields = record.getFieldParent().getSearchableFields();
        for (Field field : searchableFields) {
            LiveField lf = record.getLiveField(field);
            if (lf != null) {
                Response response = lf.getResponse();
                if (response != null) {
                    List<String> tokens = Utils.tokenizeString(response.displayString(), " ", true, true);
                    for (String token : tokens) {
                        searchTerms += "|" + token + "|";
                    }
                }
            }
        }
        return searchTerms.replace("||", "|");
    }

    private String getCompoundKey() {
        String compoundKey = "";
        List<LiveField> liveFields = new ArrayList<>(record.getLiveFields());
        liveFields.add(liveField);
        for (LiveField lf : liveFields) {
            if (lf.getField().getUniqueness() == Field.Uniqueness.COMPOUND.getValue()) {
                Object compoundKeyToken = lf.getResponse().toStorageValue();
                if (compoundKeyToken != null) {
                    compoundKey += compoundKeyToken.toString();
                }
            }
        }
        return compoundKey;
    }

    private void activateLiveField(Navigable navigable, Response response) {
        liveField.setResponse(response);
        record.setActiveLiveField(liveField);
        {
            // If this is a child field, activate the parent live field so that it may be associated with the child
            // ahead of saving the child to the database.
            if (!navigable.isMain()) {
                Navigable parentNavigable = navigable.getParentNavigable();
                Navigator parentNavigator = parentNavigable.getNavigator();
                parentNavigator.activateLiveField(parentNavigable, parentNavigable.getResponse());
            }
        }
    }


    private int clearViolated() {
        List<LiveField> toClear = new ArrayList<>();
        for (LiveField lf : record.getLiveFields()) {
            if (backwardStack.contains(lf.getField())) {
                toClear.add(lf);
            }
        }
        int cleared = recordService.clear(toClear, record);
        if (cleared != -1) {
            record.getLiveFields().removeAll(toClear);
            backwardStack.clear();
            if (record.isMain() && record.getDetail() != null) {
                MainRecord mainRecord = (MainRecord) record.getDetail();
                mainRecord.setStage(MainRecord.Stage.DRAFT);
                recordService.updateStage(mainRecord, false);
            }
        }
        return cleared;
    }
}
