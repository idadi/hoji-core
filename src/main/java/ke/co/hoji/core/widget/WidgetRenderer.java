package ke.co.hoji.core.widget;

import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;

/**
 * Renders a {@link Widget} on a {@link Navigable} component.
 *
 * @author gitahi
 */
public interface WidgetRenderer {

    /**
     * Render a {@link Widget} on a {@link Navigable} object. When finished rendering, implementations should call
     * {@link WidgetManager#primeWidget(Navigable, Widget)}.
     *
     * @param widgetManager the {@link WidgetManager} to be used to prime the widget once fully rendered.
     * @param widget        the {@link Widget} to render.
     * @param navigable     the {@link Navigable} on which to render the {@link Widget}.
     * @param direction     the {@link NavigableHelper.Direction} of navigation.
     * @param transitioning true if transitioning from another {@link LiveField} and false otherwise.
     * @param record        the current {@link Record} in case we need to lookup piped values.
     */
    void render
    (
        WidgetManager widgetManager,
        Widget widget,
        Navigable navigable,
        NavigableHelper.Direction direction,
        boolean transitioning,
        Record record
    );
}
