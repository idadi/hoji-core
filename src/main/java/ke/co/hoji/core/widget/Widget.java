package ke.co.hoji.core.widget;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.HojiContextAware;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Field.ValidationValue;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.ValidationResult;
import ke.co.hoji.core.response.Response;

/**
 * Conceptually a single user interface element used to collect data for a particular {@link Field}.
 * <p/>
 * Different technologies e.g. Android or JavaFx should implement this interface an abstract class, extending the
 * appropriate container. Specific widgets should then extends the abstract class, instead of this interface directly.
 * <p/>
 * IMPORTANT: Widgets are responsible for raising events to signal that their response has changed or is changing. See
 * {@link WidgetEvent}, {@link ResponseChangedListener} and {@link ResponseChangingListener}. Every widget SHOULD RAISE
 * AT LEAST ONE of these events because the navigation system depends on them to decide whether or not to save the
 * response for this widget. Also @see {@link Navigable#isDirty()}.
 *
 * @author gitahi
 */
public interface Widget extends HojiContextAware {

    /**
     * Gets the {@link LiveField} represented by this Widget
     *
     * @return the {@link LiveField}.
     */
    LiveField getLiveField();

    /**
     * Sets the {@link LiveField} represented by this Widget
     *
     * @param liveField the {@link LiveField}.
     */
    void setLiveField(LiveField liveField);

    /**
     * This method is called once the Widget has been rendered and it should load any data needed by the Widget. For
     * example it might load the items to be offered for selection in a drop-down widget. The {@link #writeResponse()}
     * method is called immediately thereafter.
     */
    void load();

    /**
     * This method is called once the Widget response has been written via {@link #writeResponse()}. It should perform
     * any tasks necessary to get the Widget "ready for action". E.g. a MatrixWidget might use it to automatically
     * initialize a new record, ready for data collection.
     */
    void ready();

    /**
     * This method is called just before this widget is (re)used by a new {@link LiveField}. Logic to reset a Widget for
     * fresh use should be placed here.
     */
    void prepare();

    /**
     * Reads the {@link Response} currently represented by this widget.
     *
     * @return the {@link Response}.
     */
    Response readResponse();

    /**
     * Renders on the widget the {@link Response} associated with the {@link LiveField} currently using this widget.
     */
    void writeResponse();

    /**
     * @return the value that constitutes a response for this widget.
     */
    Object getValue();

    /**
     * @return true if this Widget can be {@link #reset() } and false otherwise.
     */
    boolean isResettable();

    /**
     * @return the text to be indicated in the UI for the reset action of this widget. See {@link Widget#reset()}
     */
    String getResetText();

    /**
     * Resets this Widget to it's initial state before it is was used to collect any data. For instance, a text widget
     * might have its test cleared. Implementations should use this method to call {@link #setDefaultValue(String)} if
     * the associated {@link Field} has a {@link Field#defaultValue} and {@link #wipe()} if it doesn't. If a piped
     * default value is set, this method should return true, and false otherwise. Implementations should ideally
     * implement this method as final.
     *
     * @return true if a piped default value was set as part of this process and false otherwise.
     */
    boolean reset();

    /**
     * Set the default value on the Widget.
     *
     * @param defaultValue the default value to set.
     *
     * @return true if the value was set successfully i.e. it was the correct type e.t.c. and false otherwise.
     */
    boolean setDefaultValue(String defaultValue);

    /**
     * Clear the Widget ready for input. Could have been called clear but that tends to be a method name already used by
     * UI components.
     */
    void wipe();

    /**
     * Sets focus on the Widget so that it is ready for data entry. A TextWidget for instance might set focus on it's
     * underlying TextBox.
     */
    void focus();

    /**
     * @return true if the current status if this widget represents a missing value and false otherwise.
     */
    boolean isEmpty();

    /**
     * @return a {@link Comparable} value that can be used to compare against the minimum and maximum values returned by
     * {@link #getMin(Record)} and {@link #getMax(Record)} respectively - for the purposes of validation. The
     * value returned by these 3 methods should be of the same type. Future API may use generics to enforce this.
     */
    Comparable getComparable();

    /**
     * @return a {@link Comparable} value that can be used to compare against the missing value returned by
     * {@link #getMissingComparable(String)} for the purposes of determining whether to skip validation.
     */
    Comparable getMissingComparable();

    /**
     * @return a @{@link Comparable} value representing the missing value to use for this Widget.
     * @see @{@link Field#getMissingValue()}. Widgets that do not wish to support missing values should return
     * null here.
     */
    Comparable getMissingComparable(String stringValue);

    /**
     * Gets the min {@link ValidationValue} for validation.
     *
     * @param record the current {@link Record} in case we need to lookup a piped min value.
     *
     * @return the minimum {@link ValidationValue} for validation.
     */
    Field.ValidationValue getMin(Record record);

    /**
     * Gets the max {@link ValidationValue} for validation.
     *
     * @param record the current {@link Record} in case we need to lookup a piped max value.
     *
     * @return the maximum {@link ValidationValue} for validation.
     */
    Field.ValidationValue getMax(Record record);

    /**
     * Gets the regex {@link ValidationValue} for validation.
     *
     * @param record the current {@link Record} in case we need to lookup a piped max value.
     *
     * @return the regex {@link ValidationValue} for validation.
     */
    Field.ValidationValue getRegex(Record record);

    /**
     * Set the {@link ResponseChangedListener} for this Widget. Typically this will be the {@link Navigable} containing
     * the {@link Widget}
     *
     * @param responseChangedListener the {@link ResponseChangedListener}.
     */
    void setResponseChangedListener(ResponseChangedListener responseChangedListener);

    /**
     * Set the {@link ResponseChangingListener} for this Widget. Typically this will be the {@link Navigable} containing
     * the {@link Widget}
     *
     * @param responseChangingListener the {@link ResponseChangingListener}.
     */
    void setResponseChangingListener(ResponseChangingListener responseChangingListener);

    /**
     * @return the {@link ResponseChangedListener} for this Widget. Typically this will be the {@link Navigable}
     * containing the {@link Widget}
     */
    ResponseChangedListener getResponseChangedListener();

    /**
     * @return the {@link ResponseChangingListener} for this Widget. Typically this will be the {@link Navigable}
     * containing the {@link Widget}
     */
    ResponseChangingListener getResponseChangingListener();

    /**
     * Gets current {@link WidgetEvent.EventCause} which is the one that will be reported for the next event raised.
     *
     * @return the {@link WidgetEvent.EventCause}
     */
    WidgetEvent.EventCause getEventCause();

    /**
     * Sets the current {@link WidgetEvent.EventCause} which is the one that will be reported for the next event
     * raised.
     *
     * @param eventCause the {@link WidgetEvent.EventCause}.
     */
    void setEventCause(WidgetEvent.EventCause eventCause);

    /**
     * Sets this Widget's parent Widget, if any.
     *
     * @param parent the parent Widget.
     */
    void setParentWidget(Widget parent);

    /**
     * Gets this Widget's parent Widget, if any.
     *
     * @return the parent Widget.
     */
    Widget getParentWidget();

    /**
     * Validates this Widget's response.
     *
     * @param record the current {@link Record} in case we need to lookup piped min and max values.
     *
     * @return the {@link ValidationResult} of the validation.
     */
    ValidationResult validate(Record record);

    /**
     * @return true if this Widget needs the keyboard and false otherwise. {@link WidgetRenderer} implementations may
     * use this hint to hide they keyboard when it is not needed.
     */
    boolean needsKeyboard();

    /**
     * Called by the the {@link WidgetManager} before it calls some Widget lifecycle methods. Only Widgets that delegate
     * to other components as part of their functionality should substantively implement this method. All other Widgets
     * should simply return false always.
     * <p/>
     * For example an image widget might delegate part of its function to the device's in-build camera application. Once
     * the image is captured and control returned to the Widget, the WidgetManager reloads the Widget, but should know
     * that the Widget is merely resuming and some of the "destructive" lifecycle methods such as reset() should not be
     * called again as they might upset the state of the Widget.
     * <p/>
     * This method works because the {@link WidgetManager} loads widgets only once and then caches them for reuse.
     *
     * @return true if this Widget is resuming and false otherwise.
     */
    boolean isResuming();

    /**
     * {@link WidgetRenderer} implementations should call this method once they are done rendering a Widget. It is up
     * to the rendered Widget to decide what to do, usually nothing. But, Widgets that invoke external apps such as
     * the camera may use this notification to manage their interaction with that external app, including, for
     * example, triggering their (the Widget's) {@link ResponseChangedListener#responseChanged(WidgetEvent)}.
     */
    void rendered();

    /**
     * Gets a Widget's ability to be edited.
     *
     * @return whether or not this Widget is editable..
     */
    boolean isEditable();

    /**
     * Sets a Widget's ability to be edited.
     *
     * @param editable whether or not this Widget is editable.
     */
    void setEditable(boolean editable);

    /**
     * Sets the {@link Navigable} currently containing this Widget.
     */
    void setNavigable(Navigable navigable);

    /**
     * Gets the {@link Navigable} that currently contains this Widget.
     *
     * @return the {@link Navigable}.
     */
    Navigable getNavigable();

    /**
     * Gets the current {@link HojiContext}.
     *
     * @return the current {@link HojiContext}.
     */
    @Override
    HojiContext getHojiContext();
}
