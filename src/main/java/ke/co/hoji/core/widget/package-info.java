/**
 * Contains the core interfaces and classes that support UI component rendering and navigation.
 */
package ke.co.hoji.core.widget;