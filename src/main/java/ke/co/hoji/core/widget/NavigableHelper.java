package ke.co.hoji.core.widget;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.ValidationResult;
import ke.co.hoji.core.response.Response;

/**
 * Provides useful default implementations for some of the methods defined in the {@link Navigable} interface. This
 * allows for more consistent and easier-to-create Navigable implementations.
 * <p/>
 * Created by gitahi on 27/07/15.
 */
public class NavigableHelper {

    /**
     * The direction of navigation.
     */
    public enum Direction {

        FORWARD,
        BACKWARD
    }

    protected Widget activeWidget;
    protected final Navigable navigable;
    protected final WidgetManager widgetManager;
    protected Direction direction = Direction.FORWARD;
    protected boolean dirty;

    /**
     * Creates a new NavigationHelper.
     *
     * @param navigable     the associated {@link Navigable}.
     * @param widgetManager the {@link WidgetManager} to be used for managing {@link Widget}s.
     */
    public NavigableHelper(Navigable navigable, WidgetManager widgetManager) {
        this.navigable = navigable;
        this.widgetManager = widgetManager;
    }

    /**
     * Navigate to the first {@link Field} in the {@link Navigable}, if any.
     *
     * @should set the current field in the navigator to the first field in the record, if any.
     * @should set direction to backward.
     */
    public void first() {
        if (navigable.isNavigating()) {
            return;
        }
        render(navigable.getNavigator().first(), Direction.BACKWARD);
    }

    /**
     * Navigate to the previous {@link Field} in the {@link Navigable}, if any.
     *
     * @should set the current field in the navigator to the previous field in the record, if any.
     * @should set direction to backward.
     */
    public void previous() {
        if (navigable.isNavigating()) {
            return;
        }
        render(navigable.getNavigator().previous(), Direction.BACKWARD);
    }

    /**
     * A convenience version of {@link #next(int, int, int)} that simply calls it with all flags set to {@link
     * Navigator.Interruption#CHECK}.
     */
    public void next() {
        if (navigable.isNavigating()) {
            return;
        }
        next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.CHECK);
    }

    /**
     * Navigate to the next {@link Field} in the {@link Navigable}, if any.
     *
     * @param gpsInterruption        the {@link Navigator.Interruption} flag for {@link Form.GpsCapture} violation.
     * @param violationInterruption  the {@link Navigator.Interruption} flag for skip pattern violation.
     * @param validationInterruption the {@link Navigator.Interruption} flag for widget validation result.
     * @should do nothing if any interruption flag is set to abort.
     * @should set the current field in the navigator to the next field if any interruption flag is set to execute.
     * @should save the record if any interruption flag is set to execute.
     * @should set the current field in the navigator to the next field all interruption flags are check, widget
     * validates okay, response is not record end and there is no violation.
     * @should save the record interruption flags are check, widget validates okay, response is not record end and there
     * is no violation.
     * @should save the record if any interruption flag is set to execute.
     * @should process validation if widget does not validate okay and validation interruption is check.
     * @should not process validation if widget does not validate okay but validation interruption is not check.
     * @should process record end if current response is EndResponse.
     * @should not process record end if if current response is EndResponse but validation interruption is not check.
     * @should process skip violation next field violates skip.
     * @should not process skip violation next field violates skip but validation interruption is not check.
     * @should notify end if there is no next field.
     */
    public void next(int gpsInterruption, int violationInterruption, int validationInterruption) {
        if (navigable.isNavigating()) {
            return;
        }
        render(
            navigable.getNavigator().next(navigable, gpsInterruption, violationInterruption, validationInterruption),
            Direction.FORWARD
        );
    }

    /**
     * Navigate to the last previously visited and saved {@link Field} in the {@link Navigable}, if any.
     *
     * @should set the current field in the navigator to the last previously visited and saved field in the record, if
     * any.
     * @should set direction to backward.
     */
    public void last() {
        if (navigable.isNavigating()) {
            return;
        }
        render(navigable.getNavigator().last(), Direction.FORWARD);
    }

    /**
     * Navigate to the given {@link Field} in the {@link Navigable}.
     *
     * @param field the {@link Field} to navigate to.
     * @throws RuntimeException if field has not been visited and saved before.
     * @should set the current field in the navigator to the field passed.
     */
    public void navigateTo(Field field) {
        if (navigable.isNavigating()) {
            return;
        }
        render(navigable.getNavigator().navigateTo(field), Direction.FORWARD, true);
    }

    /**
     * Calls {@link Widget#reset()} on the {@link #activeWidget}, ensuring that the Widget does not raise a {@link
     * WidgetEvent}.
     *
     * @should reset the active widget. @see {@link Widget#reset();}
     * @should set {@link WidgetEvent.EventCause} to user on the current widget.
     * @should set dirty to true.
     */
    public void resetWidget() {
        activeWidget.setEventCause(WidgetEvent.EventCause.PROGRAM);
        activeWidget.reset();
        setDirty(true);
        activeWidget.setEventCause(WidgetEvent.EventCause.USER);
    }

    /**
     * Processes a {@link Navigable}'s responseChanged event.
     *
     * @param widgetEvent the event.
     * @should set navigable dirty to true if widget event cause is {@link WidgetEvent.EventCause#USER}
     * @should not set navigable dirty to true if widget event cause is {@link WidgetEvent.EventCause#PROGRAM}
     * @should call {@link NavigableHelper#next();} if widget event is direction agnostic
     * @should call {@link NavigableHelper#next();} if direction is forward
     * @should call {@link NavigableHelper#next();} if direction is backward and widget is for first field
     * @should not call {@link NavigableHelper#next();} if direction is backward and widget event is not direction
     * agnostic and widget is not for first field
     */
    public void responseChanged(WidgetEvent widgetEvent) {
        if (widgetEvent.getEventCause() == WidgetEvent.EventCause.USER
            || widgetEvent.getEventCause() == WidgetEvent.EventCause.SIMULATION) {
            setDirty(true);
            if (widgetEvent.isDirectionAgnostic() || direction == Direction.FORWARD) {
                if (navigable.autoNavigate()) {
                    next();
                }
            } else {
                if (direction == Direction.BACKWARD) {
                    FieldParent fieldParent = navigable.getNavigator().getRecord().getFieldParent();
                    if (widgetEvent.getSource().getLiveField().getField().equals(fieldParent.getChildren().get(0))) {
                        if (widgetEvent.getEventCause() == WidgetEvent.EventCause.USER) {
                            next();
                        }
                    }
                }
            }
        }
    }

    /**
     * Processes a {@link Navigable}'s responseChanging event.
     *
     * @param widgetEvent the event.
     * @should set navigable dirty to true if widget event cause is {@link WidgetEvent.EventCause#USER}
     * @should not set navigable dirty to true if widget event cause is {@link WidgetEvent.EventCause#PROGRAM}
     */
    public void responseChanging(WidgetEvent widgetEvent) {
        if (widgetEvent.getEventCause() == WidgetEvent.EventCause.USER) {
            setDirty(true);
        }
    }

    /**
     * Gets the currently active {@link Widget} for the {@link Navigable}.
     *
     * @return the active Widget.
     */
    public Widget getActiveWidget() {
        return activeWidget;
    }

    /**
     * Sets the currently active {@link Widget} for the {@link Navigable}.
     *
     * @param activeWidget the active Widget.
     */
    public void setActiveWidget(Widget activeWidget) {
        this.activeWidget = activeWidget;
    }

    /**
     * Gets the dirty state of the {@link Navigable}. @see {@link Navigable#isDirty()}.
     *
     * @return the dirty state.
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     * Sets the dirty state of the {@link Navigable}. @see {@link Navigable#setDirty(boolean)}. If {@link #navigable}
     * has a parent, the parent's {@link Navigable#setDirty(boolean)} method is also called with the same parameter.
     *
     * @param dirty the dirty state.
     * @should if navigable has a parent navigable, set dirty should be called on it too.
     */
    public void setDirty(boolean dirty) {
        this.dirty = dirty;
        if (navigable.getParentNavigable() != null) {
            navigable.getParentNavigable().setDirty(dirty);
        }
    }

    /**
     * Read the {@link Response} of the currently active {@link Widget}.
     *
     * @return the response.
     * @should read the response of the active widget.
     */
    public Response readResponse() {
        return activeWidget.readResponse();
    }

    /**
     * Validate the currently active {@link Widget}.
     *
     * @return the {@link ValidationResult}.
     * @should validate the active widget.
     */
    public ValidationResult validate() {
        return activeWidget.validate(navigable.getNavigator().getRecord());
    }

    private void render(LiveField liveField, NavigableHelper.Direction direction) {
        render(liveField, direction, false);
    }

    private void render(LiveField liveField, NavigableHelper.Direction direction, boolean inSitu) {
        this.direction = direction;
        boolean transitioning = true;
        if (liveField != null) {
            if (inSitu
                || navigable.getNavigator().getLiveField() == null
                || liveField.equals(navigable.getNavigator().getLiveField())) {
                transitioning = false;
            }
            Record record = navigable.getNavigator().getRecord();
            navigable.setNavigating(true);
            widgetManager.renderWidget
                (
                    navigable,
                    direction,
                    transitioning,
                    liveField,
                    record
                );
            navigable.getNavigator().setLiveField(liveField);
        }
    }

    public void deleteRecord(Record record) {
        navigable.getNavigator().deleteRecord(record);
    }
}