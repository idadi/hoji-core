package ke.co.hoji.core.widget;

/**
 * Objects that want to be notified whenever a {@link Widget} response has changed should implement this interface.
 *
 * @author gitahi
 */
public interface ResponseChangedListener {

    /**
     * The method called whenever a Widget's response is set and ready.
     *
     * @param event the {@link WidgetEvent}.
     */
    void responseChanged(WidgetEvent event);
}
