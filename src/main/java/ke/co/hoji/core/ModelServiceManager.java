package ke.co.hoji.core;

import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.core.service.model.LanguageService;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.core.service.model.RecordService;
import ke.co.hoji.core.service.model.ReferenceService;
import ke.co.hoji.core.service.model.TranslationService;

/**
 * A convenience access point for some of the Model services provided in the service layer. We put here the services
 * that we need to be accessible universally across the system through the @{@link HojiContext}.
 *
 * @author gitahi
 */
public interface ModelServiceManager extends ServiceManager {

    /**
     * @return an instance of {@link ConfigService}.
     */
    ConfigService getConfigService();

    /**
     * @return an instance of {@link RecordService}.
     */
    RecordService getRecordService();

    /**
     * @return an instance of {@link ReferenceService}.
     */
    ReferenceService getReferenceService();

    /**
     * @return an instance of {@link PropertyService}.
     */
    PropertyService getPropertyService();

    /**
     * @return an instance of @{@link LanguageService}.
     */
    LanguageService getLanguageService();

    /**
     * @return an instance of @{@link TranslationService}.
     */
    TranslationService getTranslationService();
}
