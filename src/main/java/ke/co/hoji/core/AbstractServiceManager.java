package ke.co.hoji.core;

import java.util.HashMap;
import java.util.Map;

/**
 * A useful abstract implementation of {@link ModelServiceManager}.
 *
 * @author gitahi
 */
public abstract class AbstractServiceManager implements ServiceManager {

    private Map<String, Object> parameters;

    @Override
    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    /**
     * @return the parameters set via {@link #setParameters(Map)} if any and an empty map otherwise.
     */
    @Override
    public Map<String, Object> getParameters() {
        if (parameters == null) {
            parameters = new HashMap<>();
        }
        return parameters;
    }
}
