package ke.co.hoji.core;

import ke.co.hoji.core.calculation.CalculationEvaluator;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Language;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.response.helper.Base64Encoder;
import ke.co.hoji.core.rule.RuleEvaluator;
import ke.co.hoji.core.widget.Widget;
import ke.co.hoji.core.widget.WidgetManager;

/**
 * This interface represents the context of a running Hoji client application. For example, it contains references to
 * the {@link WidgetManager} used to manage widgets, the current {@link Form}, the current {@link Record} e.t.c.
 *
 * @author gitahi
 */
public interface HojiContext {

    /**
     * Designates the Server Hoji Context i.e. The running application is the Server Web Application.
     */
    public static final String SERVER_CONTEXT = "Server";

    /**
     * Designates the Android Hoji Context i.e. The running application is the Android App.
     */
    public static final String ANDROID_CONTEXT = "Android";

    /**
     * @return the {@link ModelServiceManager} used for Model accessing services.
     */
    ModelServiceManager getModelServiceManager();

    /**
     * @return the {@link DtoServiceManager} used for DTO accessing services.
     */
    DtoServiceManager getDtoServiceManager();

    /**
     * @return the {@link WidgetManager} used for managing {@link Widget}s.
     */
    WidgetManager getWidgetManager();

    /**
     * @return the {@link RuleEvaluator} used to process {@link Rule}s.
     */
    RuleEvaluator getRuleEvaluator();

    /**
     * Get the {@link CalculationEvaluator} used to process calculations.
     *
     * @return the {@link CalculationEvaluator}.
     */
    CalculationEvaluator getCalculationEvaluator();

    /**
     * Get the {@link Base64Encoder} used to encode binary data to Base 64 string representation and vice versa.
     *
     * @return the {@link Base64Encoder}.
     */
    Base64Encoder getBase64Encoder();

    /**
     * Implement only for client contexts where there is can only be at most one Survey in the context. Otherwise throw
     * an {@link UnsupportedOperationException}.
     *
     * @return the current {@link Survey}.
     */
    Survey getSurvey();

    /**
     * Sets the current {@link Survey}.
     *
     * @param survey the current {@link Survey}.
     */
    void setSurvey(Survey survey);

    /**
     * @return the current {@link Form}.
     */
    Form getForm();

    /**
     * Sets the current {@link Form}.
     *
     * @param form the current {@link Form}.
     */
    void setForm(Form form);

    /**
     * @return the current {@link Record}.
     */
    Record getRecord();

    /**
     * Sets the current {@link Record}. This method should be implemented so that it implicitly calls {@link
     * #setForm(Form)} with the {@link Form} associated with the Record.
     *
     * @param record the current {@link Record}.
     */
    void setRecord(Record record);

    /**
     * Gets the unique identifier of the device. Exactly what identifier is returned is up to the implementation.
     * However, the chosen id should be both as unique as possible and stable i.e. not likely to change frequently, if
     * at all.
     *
     * @return the unique device id.
     */
    String getDeviceIdentifier();

    /**
     * Gets the username of the currently logged in user.
     *
     * @return the username.
     */
    String getUsername();

    /**
     * Gets the user id of the currently logged in user.
     *
     * @return the id.
     */
    Integer getUserId();

    /**
     * Gets the code of the currently logged in user.
     *
     * @return the code.
     */
    String getUserCode();

    /**
     * Gets the name of the currently logged in user.
     *
     * @return the name.
     */
    String getNameOfUser();

    /**
     * Gets the id of the owner of the {@link Survey}.
     *
     * @return the id of the owner.
     */
    Integer getOwnerId();

    /**
     * Gets the name of the owner of the {@link Survey}.
     *
     * @return the name of the owner.
     */
    String getOwnerName();

    /**
     * Gets the verifircation status of the owner of the {@link Survey} i.e. Does Hoji Ltd know
     * who this survey owner is?
     *
     * @return whether or not the owner is verified by Hoji Ltd.
     */
    boolean getOwnerVerified();

    /**
     * Gets the name of the tenant under whom the current user is operating.
     *
     * @return the name of the tenant.
     */
    String getTenantName();

    /**
     * Gets the code of the tenant under whom the current user is operating.
     *
     * @return the code of the tenant.
     */
    String getTenantCode();

    /**
     * Gets the id of the current @{@link Language}. This affects Hoji user data i.e. configuration data, and not the
     * Hoji app itself.
     *
     * @return the id of the current Language.
     */
    Integer getLanguageId();

    /**
     * The type of context this is. One of either {@link #SERVER_CONTEXT} or {@link #ANDROID_CONTEXT}.
     */
    String getType();

    /**
     * The version of the app (web, mobile or otherwise) for which this is the context.
     */
    String appVersion();
}
