package ke.co.hoji.core;

import java.util.Map;

/**
 * Created by gitahi on 28/08/15.
 */
public interface ServiceManager {

    /**
     * Sets any necessary parameters for a specific ServiceManager implementation.
     *
     * @param parameters the parameters.
     */
    void setParameters(Map<String, Object> parameters);

    /**
     * Returns any parameters set for a specific ServiceManager implementation.
     *
     * @return the parameters.
     */
    Map<String, Object> getParameters();

    /**
     * Clears any cached data objects.
     */
    void clearCache();
}
