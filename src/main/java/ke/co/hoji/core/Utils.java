package ke.co.hoji.core;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.DescribableOrderable;
import ke.co.hoji.core.data.NavigableList;
import ke.co.hoji.core.data.Orderable;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

/**
 * Provides general purpose static methods for doing sundry things.
 * <p/>
 * Created by gitahi on 15/05/15.
 */
public class Utils {

    /**
     * The separator for separating keys and values when converting a map to string.
     */
    public static final String KEY_VALUE_SEPARATOR = "#";

    /**
     * The separator for separating map entries when converting a map to string.
     */
    public static final String ENTRY_SEPARATOR = "|";

    /**
     * The {@link ResourceBundle} for handling internationalization.
     */
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("HojiCoreBundle");

    /**
     * Formats a Date object to a time String to hours, minutes and am/pm.
     *
     * @param date the Date object to format.
     * @return the time String.
     */
    public static String formatSimpleTime(Date date) {
        return format(date, new SimpleDateFormat("hh:mm a"));
    }

    /**
     * Formats a Date object to a time String according to current locale date format.
     *
     * @param date the Date object to format.
     * @return the time String.
     */
    public static String formatTime(Date date) {
        return format(date, DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.getDefault()));
    }

    /**
     * Formats a Date object to a time String according to ISO time format.
     *
     * @param date the Date object to format.
     * @return the ISO formatted time String.
     */
    public static String formatIsoTime(Date date) {
        return format(date, new SimpleDateFormat("HH:mm:ss"));
    }

    /**
     * Formats a Date object to a date String according to current locale date format.
     *
     * @param date the Date object to format.
     * @return the date String.
     */
    public static String formatDate(Date date) {
        return format(date, DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault()));
    }

    /**
     * Formats a Date object to a datetime String according to current locate datetime format.
     *
     * @param date the Date object to format.
     * @return the datetime String.
     */
    public static String formatDateTime(Date date) {
        return format(date, DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.getDefault()));
    }

    /**
     * Formats a Date object to a date String to ISO date format.
     *
     * @param date the Date object to format.
     * @return the ISO formatted date String.
     */
    public static String formatIsoDate(Date date) {
        return format(date, new SimpleDateFormat("yyyy-MM-dd"));
    }

    /**
     * Formats a Date object to a datetime String according to ISO datetime format.
     *
     * @param date the Date object to format.
     * @return the ISO formatted datetime String.
     */
    public static String formatIsoDateTime(Date date) {
        return format(date, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
    }

    /**
     * Parses a String to a Date object according to current locate time format.
     *
     * @param dateString the String to parse.
     * @return the resulting Date object.
     */
    public static Date parseTime(String dateString) {
        return parse(dateString, DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.getDefault()));
    }

    /**
     * Parses a String to a Date object according to current locale date format.
     *
     * @param dateString the String to parse.
     * @return the resulting Date object.
     */
    public static Date parseDate(String dateString) {
        return parse(dateString, DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault()));
    }

    /**
     * Parse the given date string to LocalDate.
     *
     * @param dateString the date String to parse.
     * @return the LocalDate.
     */
    public static LocalDate parseLocalDate(String dateString) {
        LocalDate localDate = null;
        Date date = parse(dateString, new SimpleDateFormat("yyyy-MM-dd"));
        if (date != null) {
            localDate = new LocalDate(date);
        }
        return localDate;
    }

    /**
     * Parse the given time string to LocalTime.
     *
     * @param timeString the date String to parse.
     * @return the LocalTime.
     */
    public static LocalTime parseLocalTime(String timeString) {
        LocalTime localTime = null;
        Date date = parse(timeString, new SimpleDateFormat("HH:mm:ss"));
        if (date != null) {
            localTime = new LocalTime(date);
        }
        return localTime;
    }

    /**
     * Parses a String to a Date object according to current Locale date format.
     *
     * @param dateString the String to parse.
     * @return the resulting Date object.
     */
    public static Date parseDateTime(String dateString) {
        if (Constants.Command.NOW.equals(dateString)) {
            return new Date();
        }
        return parse(dateString, DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.getDefault()));
    }

    /**
     * Parses a String to a Date object (with time) according to ISO datetime format.
     *
     * @param dateTimeString the String to parse.
     * @return the resulting Date object.
     */
    public static Date parseIsoDateTime(String dateTimeString) {
        if (Constants.Command.TODAY.equals(dateTimeString)) {
            return new Date();
        }
        return parse(dateTimeString, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
    }

    /**
     * Parses a String to a Date object (without time) according to ISO datetime format.
     *
     * @param dateString the String to parse.
     * @return the resulting Date object.
     */
    public static Date parseIsoDate(String dateString) {
        if (Constants.Command.TODAY.equals(dateString)) {
            return new Date();
        }
        return parse(dateString, new SimpleDateFormat("yyyy-MM-dd"));
    }

    /**
     * The regex pattern for removing HTML tags from text.
     */
    private static Pattern htmlPattern = Pattern.compile("</?\\w+((\\s+\\w+(\\s*=\\s*(?:\".*?\"|'.*?'|[\\^'\">\\s]+))?)" +
        "+\\s*|\\s*)/?>");

    public static Integer parseInteger(String intString) {
        try {
            return Integer.parseInt(intString);
        } catch (Exception ex) {
            return null;
        }
    }

    public static Long parseLong(String longString) {
        try {
            return Long.parseLong(longString);
        } catch (Exception ex) {
            return null;
        }
    }

    public static Double parseDouble(String doubleString) {
        try {
            return Double.parseDouble(doubleString);
        } catch (Exception ex) {
            return null;
        }
    }

    public static Long parseBin(String longBin) {
        try {
            return Long.parseLong(longBin, 2);
        } catch (Exception ex) {
            return null;
        }
    }

    private static String format(Date date, DateFormat format) {
        if (date == null) {
            return "";
        }
        return format.format(date);
    }

    private static Date parse(String dateString, DateFormat format) {
        Date date = null;
        if (dateString != null && !dateString.equals("")) {
            try {
                date = format.parse(dateString);
            } catch (Exception ex) {
                return null;
            }
        }
        return date;
    }

    /**
     * Splits a String into tokens based on a give delimiter and then returns the tokens as a list of Strings.
     *
     * @param string     the String to tokenize.
     * @param delimiter  the delimiter by which to tokenize.
     * @param trimTokens true if tokens should be trimmed and false otherwise.
     * @return the list of tokens.
     */
    public static List<String> tokenizeString(String string, String delimiter, boolean trimTokens) {
        return tokenizeString(string, delimiter, trimTokens, false);
    }

    /**
     * Splits a String into tokens based on a give delimiter and then returns the tokens as a list of Strings.
     *
     * @param string        the String to tokenize.
     * @param delimiter     the delimiter by which to tokenize.
     * @param trimTokens    true if tokens should be trimmed and false otherwise.
     * @param excludeBlanks If true, blank (whitespace) tokens are excluded from list.
     * @return the list of tokens.
     */
    public static List<String> tokenizeString(String string, String delimiter, boolean trimTokens,
                                              boolean excludeBlanks) {
        List<String> list = new ArrayList<>();
        if (string != null) {
            String[] tokens = string.split(delimiter);
            for (String token : tokens) {
                if (excludeBlanks && token.trim().length() == 0) {
                    continue;
                }
                if (trimTokens) {
                    list.add(token.trim());
                } else {
                    list.add(token);
                }
            }
        }
        return list;
    }

    /**
     * Strings a Collection of Strings into a single String with each member separated by the specified String.
     * Optionally appends a custom string either to each Collection member or to the resultant String. Useful for
     * generating Strings for use in SQL statements e.t.c.
     *
     * @param strings        the collections of strings to be converted.
     * @param separator      the separator to use.
     * @param delimit        whether or not delimiters should be placed around separated strings.
     * @param leftDelimiter  the delimiter to apply to the left.
     * @param rightDelimiter the delimiter to apply to the right.
     * @param append         the String to be appended.
     * @param toEach         if true, the append String is appended to each member of the collection. Otherwise it is
     *                       appended at the very end.
     * @return the single String.
     */
    public static String string(Collection<String> strings, String separator, boolean delimit, String leftDelimiter,
                                String rightDelimiter, String append, boolean toEach) {
        String string = "";
        if (separator == null) {
            separator = ",";
        }
        int i = 0;
        int n = strings.size() - 1;
        for (String column : strings) {
            if (append != null && toEach) {
                string += ((delimit ? delimit(column, leftDelimiter, rightDelimiter) : column) + append);
            } else {
                if (delimit) {
                    string += delimit(column, leftDelimiter, rightDelimiter);
                } else {
                    string += column;
                }
            }
            if (i < n) {
                string += separator + " ";
                i++;
            }
        }
        if (append != null) {
            if (!toEach) {
                string += append;
            }
        }
        return string;
    }

    public static String string(Collection<String> strings, String separator, boolean delimit, String append,
                                boolean toEach) {
        return string(strings, separator, delimit, "", "", append, toEach);
    }

    /**
     * See {@link Utils#string(java.util.Collection, java.lang.String, boolean, java.lang.String, boolean) }
     *
     * @param strings   the collections of strings to be converted.
     * @param separator the separator to use
     * @param append    the String to be appended
     * @param toEach    if true, the append String is appended to each member of the collection. Otherwise it is
     *                  appended at the very end
     * @return the single String
     */
    public static String string(Collection<String> strings, String separator,
                                String append, boolean toEach) {
        return string(strings, separator, false, append, toEach);
    }

    /**
     * See {@link Utils#string(java.util.Collection, java.lang.String, boolean, java.lang.String, boolean) }
     *
     * @param strings the collections of strings to be converted.
     * @param append  the String to be appended
     * @return the single String
     */
    public static String string(Collection<String> strings, String append) {
        return string(strings, null, append, true);
    }

    /**
     * See {@link Utils#string(java.util.Collection, java.lang.String, boolean, java.lang.String, boolean) }
     *
     * @param strings the collections of strings to be converted.
     * @param delimit whether or not delimiters should be placed around separated strings
     * @return the single String
     */
    public static String string(Collection<String> strings, boolean delimit) {
        return string(strings, null, delimit, null, false);
    }

    public static String string(String string, boolean delimit) {
        return string(Arrays.asList(string), delimit);
    }

    public static String string(String string) {
        return string(Arrays.asList(string), true);
    }

    /**
     * See {@link Utils#string(java.util.Collection, java.lang.String, boolean, java.lang.String, boolean) }
     *
     * @param strings the collections of strings to be converted.
     * @return the single String
     */
    public static String string(Collection<String> strings) {
        return string(strings, null);
    }

    /**
     * Converts a map of strings and objects into a String.
     *
     * @param map the map to convert
     * @return a String representation of the map
     */
    public static String mapToString(LinkedHashMap<String, Object> map) {
        String string = "";
        int i = 0;
        int n = map.size() - 1;
        for (String key : map.keySet()) {
            string += (key + KEY_VALUE_SEPARATOR + String.valueOf(map.get(key)));
            if (i < n) {
                string += ENTRY_SEPARATOR;
            }
            i++;
        }
        return string;
    }

    /**
     * Delimits a value with the specified delimiters.
     *
     * @param value          the value to delimit
     * @param leftDelimiter  the delimiter to apply to the left.
     * @param rightDelimiter the delimiter to apply to the right.
     * @return the delimited value.
     */
    public static String delimit(String value, String leftDelimiter, String rightDelimiter) {
        return leftDelimiter + value + rightDelimiter;
    }

    /**
     * Gets the internationalized String represented by a given key with the given arguments from the configured {@link
     * ResourceBundle}. If no ResourceBundle is configured, the key is itself returned.
     *
     * @param key  the key.
     * @param args the arguments.
     * @return the internationalized String.
     */
    public static String getI18nString(String key, Object args) {
        if (resourceBundle != null) {
            return MessageFormat.format(resourceBundle.getString(key), args);
        }
        return key;
    }

    /**
     * Calls {@link #ueid(int, int)} with the values 36,5 to return a UEID that does not include lowercase letters.
     *
     * @See ueid
     */
    public static String ueid() {
        return ueid(36, 5);
    }

    /**
     * Generates a random human readable UEID (Unique Enough ID) based on Base62 encoding. The member characters of each
     * unique ID are the digits 0 - 9, the uppercase alphabetical characters A-Z and the lowercase alphabetical
     * characters a-z to make a total of 62.
     * <p/>
     * If desired, the lowercase characters may be omitted by using base 36 instead of 62. You my also use any base
     * between 36 and 62. Any attempt to use a base outside this range will result in an {@link
     * IllegalArgumentException}.
     * <p/>
     * You may specify a character length of 4 or greater. Anything less will result in an {@link
     * IllegalArgumentException}.
     * <p/>
     * The theoretical total number of unique IDs generated by this method is equal to base raised to the power of
     * length. For instance, a 5 character base 62 ID implies a possibility of 62^5 = 916,132,832.
     * <p/>
     * This code is adapted from the accepted answer to this question on SO: http://stackoverflow.com/questions/9543715/generating-human-readable-usable-short-but-unique-ids
     *
     * @param base   the base for encoding
     * @param length the length of the desired ID
     * @return a UEID String.
     * @throws IllegalArgumentException if any of the arguments are outside the specified range.
     */
    public static String ueid(int base, int length) {
        if (base < 36 | base > 62) {
            throw new IllegalArgumentException("Base must be between 36 and 62");
        }
        if (length < 4) {
            throw new IllegalArgumentException("Length must be 4 or greater");
        }
        char[] base62Set = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder builder = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            builder.append(base62Set[new Random().nextInt(base)]);
        }
        return builder.toString();
    }

    /**
     * Processes a String into a clean identifier. A clean identifier is a String comprising only of alphanumeric
     * characters and underscores. This is useful for database object names such as table names, column names and such,
     * but also for any other situation necessitating such identifiers.
     * <p/>
     * This particular implementation discards all non-alphanumeric characters and replaces all spaces with the
     * underscore character. It also strips any \r or \n characters. Upper case letters are replaced with lowercase.
     *
     * @param input the input String.
     * @return a String representing the clean identifier or an empty String if input is null.
     */
    public static String toCleanIdentifier(String input) {
        String output = "";
        if (input != null) {
            output = input
                .replace("\r", "")
                .replace("\n", "")
                .replace("\t", "")
                .toLowerCase();
            output = removeHtmlTags(output);
            output = output
                .replaceAll("[^A-Za-z0-9\\s]", "")
                .replace(" ", "_");
        }
        return output;
    }

    public static String removeHtmlTags(String input) {
        return htmlPattern.matcher(input).replaceAll("");
    }

    /**
     * Generates the next ordinal value for an {@link DescribableOrderable} object about to join a list. If the list is
     * null or empty, the value 1.0is returned. Otherwise, the value returned is the ordinal value of the last
     * DescribableOrderable in the list plus one rounded down to the nearest integer.
     *
     * @param orderables the list of orderable objects that the next one is about to join.
     * @return the next ordinal value.
     */
    public static BigDecimal nextOrdinal(List<Orderable> orderables) {
        BigDecimal next = BigDecimal.ONE;
        if (orderables != null && !orderables.isEmpty()) {
            Orderable last = new NavigableList<>(orderables).last();
            BigDecimal current = last.getOrdinal();
            next = current.add(BigDecimal.ONE);
            boolean isInteger = next.setScale(0, RoundingMode.HALF_UP).compareTo(next) == 0;
            if (!isInteger) {
                next = next.setScale(0, RoundingMode.FLOOR);
            }
        }
        return next;
    }

    /**
     * Calculate distance between two points in latitude and longitude taking into account height difference. If you are
     * not interested in height difference pass 0.0. Uses Haversine method as its base.
     * <p/>
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters el2 End altitude in meters
     * <p/>
     * Adapted from: http://stackoverflow.com/a/16794680/1528225
     *
     * @returns Distance in Meters
     */
    public static double distance(double lat1, double lat2, double lon1, double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        Double latDistance = Math.toRadians(lat2 - lat1);
        Double lonDistance = Math.toRadians(lon2 - lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
            + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }

    /**
     * See {@link #distance(double, double, double, double, double, double)}
     */
    public static double distance(double lat1, double lat2, double lon1, double lon2) {
        return distance(lat1, lat2, lon1, lon2, 0.0, 0.0);
    }

    /**
     * Rounds off a double to the given number of decimal places.
     *
     * @param value  the double values.
     * @param places the number of decimal places.
     */
    public static double roundDouble(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }
        if (value == Double.NaN) {
            return value;
        }
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    /**
     * Calls {@link #roundDouble(double, int)} with 2 for number of decimal places.
     */
    public static double roundDouble(double value) {
        return roundDouble(value, 2);
    }

    /**
     * Sorts a given list of choices randomly
     *
     * @param choices
     */
    public static void sortChoicesRandomly(List<Choice> choices) {
        Random random = new Random();
        for (Choice choice : choices) {
            choice.setRandomId(random.nextInt());
        }
        Collections.sort(choices, new Comparator<Choice>() {
            @Override
            public int compare(Choice thisOne, Choice thatOne) {
                return thisOne.getRandomId().compareTo(thatOne.getRandomId());
            }
        });
    }

    /**
     * Sorts choices alphabetically
     *
     * @param choices
     */
    public static void sortChoicesAlphabetically(List<Choice> choices) {
        Collections.sort(choices, new Comparator<Choice>() {
            @Override
            public int compare(Choice thisOne, Choice thatOne) {
                return thisOne.getName().compareToIgnoreCase(thatOne.getName());
            }
        });
    }

    /**
     * Remove leading, trailing and multiple spaces between words
     */
    public static String removeSpaces(String value) {
        return value.trim().replaceAll("\\s{2,}", " ");
    }

    /**
     * Checks if a field validation value (usually min or max) is optional.
     *
     * @param validationValue the validation value to check.
     * @return true if it is optional and false otherwise.
     */
    public static boolean isOptionalValidationValue(String validationValue) {
        return validationValue.startsWith("[") && validationValue.endsWith("]");
    }

    /**
     * Returns the substantive field validation value after removing any characters used to designate how the
     * substantive value itself should be validate e.g. strictly enforced or merely to raise a warning.
     *
     * @param validationValue the raw validation value from which to extract the substantive value.
     * @return the substantive validation value.
     */
    public static String extractSubstantiveValidationValue(String validationValue) {
        return validationValue.substring(1, validationValue.length() - 1);
    }
}
