package ke.co.hoji.core;

import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.Response;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Interpolates a string based on the ${fieldId} notation. Useful for piping values into text during data entry, or
 * into min and max values during validation.
 */
public class StringInterpolator {

    /**
     * The type of interpolation being requested.
     */
    public static class InterpolationType {

        /**
         * Interpolate for label value. Replaces the place holder values with the @{@link Response#displayString()} of
         * the response associated with the field specified in the placeholder using the ${fieldId} notation.
         */
        public static final int LABEL_VALUE = 1;

        /**
         * Interpolate for validation. Replaces the place holder values with the
         *
         * @{@link Response#comparableFromActualValue()} value of the response associated with the field specified in
         * the placeholder using the ${fieldId} notation.
         */
        public static final int VALIDATION_VALUE = 2;

        /**
         * Interpolate for default value. Replaces the place holder values with the @{@link Response#quantityString()}
         * of the response associated with the field specified in the placeholder using the ${fieldId} notation.
         */
        public static final int DEFAULT_VALUE = 3;

        /**
         * Interpolate for rule value. Replaces the place holder values with the @{@link Response#getActualValue(boolean)}
         * of the response associated with the field specified in the placeholder using the ${fieldId} notation.
         */
        public static final int RULE_VALUE = 4;
    }

    /**
     * The value used by the interpolator to represent missingness i.e. a replacement value that could not be
     * successfully replaced.
     */
    public static final String MISSING_VALUE = "_________";

    /**
     * Matches the ${id} notation used as a templating placeholder for interpolated values.
     */
    private final String regex = "\\$([^}]+)\\}";
    private final Pattern pattern = Pattern.compile(regex);

    /**
     * Interpolate a raw string with the intention of using the result for display to the user.
     *
     * @param rawString the string with the ${fieldId} notation placehoders.
     * @param record    the record from which piped values are to be looked up.
     * @return interpolated string
     */
    public String interpolate(String rawString, Record record, int interpolationType) {
        String toFormat = rawString.replace("%", "%%");
        toFormat = toFormat.replaceAll(regex, "%s");
        String formatted = String.format(
            toFormat,
            getValueMap(
                getPlaceHolders(rawString),
                record,
                interpolationType
            ).values().toArray());
        return formatted;
    }

    private LinkedHashMap getValueMap(List<String> placeHolders, Record record, int interpolationType) {
        LinkedHashMap<Integer, Object> valueMap = new LinkedHashMap<>();
        int index = 1;
        for (String placeHolder : placeHolders) {
            Integer fieldId = getFieldId(placeHolder);
            Object value = null;
            if (fieldId != null) {
                LiveField liveField = record.find(fieldId);
                if (liveField != null) {
                    Response response = liveField.getResponse();
                    if (response != null) {
                        if (interpolationType == InterpolationType.LABEL_VALUE) {
                            value = response.displayString();
                        } else if (interpolationType == InterpolationType.VALIDATION_VALUE) {
                            value = response.comparableFromActualValue();
                        } else if (interpolationType == InterpolationType.DEFAULT_VALUE) {
                            value = response.quantityString();
                        } else if (interpolationType == InterpolationType.RULE_VALUE) {
                            value = response.ruleString();
                        } else {
                            throw new IllegalArgumentException("Unrecognized interpolation type!");
                        }
                    }
                }
            } else {
                value = placeHolder;
            }
            if (value == null) {
                value = MISSING_VALUE;
            }
            valueMap.put(index, value);
            index++;
        }
        return valueMap;
    }

    private List<String> getPlaceHolders(String rawString) {
        List<String> placeHolders = new ArrayList<>();
        Matcher matcher = pattern.matcher(rawString);
        while (matcher.find()) {
            placeHolders.add(matcher.group());
        }
        return placeHolders;
    }

    private Integer getFieldId(String placeHolder) {
        String fieldIdString = placeHolder.substring(2, placeHolder.length() - 1);
        return Utils.parseInteger(fieldIdString);
    }
}
