package ke.co.hoji.core.rule;

import ke.co.hoji.core.response.Response;

import java.util.List;

/**
 * The negation of the {@link Contains} @{@link RuleOperator}.
 * <p/>
 * Created by gitahi on 11/04/18.
 */
public class NotContains extends RuleOperator {

    private final Contains contains = new Contains();

    @Override
    public boolean evaluate(Response response, List<String> values) {
        return !contains.evaluate(response, values);
    }
}
