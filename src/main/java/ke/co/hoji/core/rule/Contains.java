package ke.co.hoji.core.rule;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.response.Response;

import java.util.List;

/**
 * Check if the {@link Response} to a {@link Field} contains any of the {@link Rule} values.
 *
 * @author gitahi
 */
public class Contains extends RuleOperator {

    private final ComparableHelper comparableHelper = new ComparableHelper();

    /**
     * {@inheritDoc}
     *
     * @should return true if actual value of response equals any of the values.
     * @should return false if actual value of response does not equal any of the values.
     */

    @Override
    public boolean evaluate(Response response, List<String> values) {
        for (String value : values) {
            if (comparableHelper.evaluate(response, value, ComparableHelper.Sign.EQUALS)) {
                return true;
            }
        }
        return false;
    }
}
