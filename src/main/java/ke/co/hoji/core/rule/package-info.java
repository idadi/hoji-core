/**
 * Contains the core interfaces and classes that support {@link ke.co.hoji.core.data.model.Rule} processing.
 */
package ke.co.hoji.core.rule;