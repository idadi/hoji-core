package ke.co.hoji.core.rule;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.response.Response;

import java.util.List;

/**
 * Perform a logical AND on the {@link Response} to a {@link Field} against the {@link Rule} value.
 * <p/>
 * Created by gitahi on 01/12/15.
 */
public class BitwiseAnd extends RuleOperator {

    /**
     * {@inheritDoc}
     *
     * @should return false if any of the operands are null.
     * @should return false if any of the operands are not integers.
     * @should return true if the logical and result is equal to the rule value.
     */

    @Override
    protected boolean evaluate(Response response, List<String> values) {
        boolean result = false;
        String against = Utils.string(values, "|", null, false).replace(" ", "");
        Comparable thisOne = null;
        if (response != null) {
            thisOne = response.comparableFromActualValue();
        }
        if (thisOne != null && against != null) {
            if (thisOne instanceof Long) {
                Comparable thatOne = response.comparableFromRuleString(against);
                if (thatOne != null && thatOne instanceof Long) {
                    long thatLong = (Long) thatOne;
                    long thisLong = (Long) thisOne;
                    long logicalAndResult = thisLong & thatLong;
                    result = logicalAndResult == thatLong;
                }
            }
        }
        return result;
    }
}
