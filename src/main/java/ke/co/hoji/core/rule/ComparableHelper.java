package ke.co.hoji.core.rule;

import ke.co.hoji.core.response.Response;

import java.util.List;

/**
 * A helper class that makes it easier to implement {@link Comparable}-type comparisons. It supports the following
 * comparisons for {@link Response}s that return Comparable {@link Response#actualValue}: Equals, Not equals, Greater
 * than, Less than, Greater than or equals and Less than or equals.
 *
 * @author gitahi
 */
class ComparableHelper {

    /**
     * A String flag indicating which of the six supported comparison types to use.
     */
    static class Sign {

        public static final String EQUALS = "==";
        public static final String NOT_EQUALS = "!=";
        public static final String GREATER_THAN = ">";
        public static final String LESS_THAN = "<";
        public static final String GREATER_THAN_OR_EQUALS = ">=";
        public static final String LESS_THAN_OR_EQUALS = "<=";
    }

    /**
     * Evaluates a {@link Response} against the supplied value. @see {@link RuleOperator#evaluate(Response, List)}.
     *
     * @param response the {@link Response} to evaluate.
     * @param against  the value against which to evaluate the response.
     * @param sign     the {@link ComparableHelper.Sign} flag.
     *
     * @return the result of the evaluation as a boolean.
     *
     * @should return true if both the response and the against value are null.
     * @should return true if both the actual value of response and the against value are null.
     * @should return false if response is null and the against value is not.
     * @should return false if the actual value of response is null and the against value is not.
     * @should return false if the actual value of response is not null and the against value is.
     * @should compute the comparable value from the against value if both response actual value and against value are
     * not null
     * @should compare actual response to computed against value if response actual value and against value are not
     * null
     */
    boolean evaluate(Response response, String against, String sign) {
        boolean result = false;
        Comparable thisOne = null;
        if (response != null) {
            thisOne = response.comparableFromActualValue();
        }
        if (thisOne == null && against == null) {
            if (sign.equals(Sign.EQUALS)
                    || sign.equals(Sign.GREATER_THAN_OR_EQUALS)
                    || sign.equals(Sign.LESS_THAN_OR_EQUALS)) {
                result = true;
            } else {
                result = false;
            }
        } else if (thisOne != null && against != null) {
            Comparable thatOne = response.comparableFromRuleString(against);
            if (thatOne != null) {
                int compareTo = thisOne.compareTo(thatOne);
                switch (sign) {
                    case Sign.EQUALS:
                        result = (compareTo == 0);
                        break;
                    case Sign.NOT_EQUALS:
                        result = (compareTo != 0);
                        break;
                    case Sign.GREATER_THAN:
                        result = (compareTo > 0);
                        break;
                    case Sign.LESS_THAN:
                        result = (compareTo < 0);
                        break;
                    case Sign.GREATER_THAN_OR_EQUALS:
                        result = ((compareTo > 0) || (compareTo == 0));
                        break;
                    case Sign.LESS_THAN_OR_EQUALS:
                        result = ((compareTo < 0) || (compareTo == 0));
                        break;
                }
            }
        }
        return result;
    }
}
