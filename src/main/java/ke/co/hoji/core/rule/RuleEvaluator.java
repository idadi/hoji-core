package ke.co.hoji.core.rule;

import ke.co.hoji.core.data.NavigableList;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.OperatorDescriptor;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.data.model.RuleGroup;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class processes {@link Rule}s associated with fields and returns a boolean result.
 *
 * @author gitahi
 */
public class RuleEvaluator {

    /**
     * A cache of {@link ke.co.hoji.core.rule.RuleOperator}s indexed by their fully qualified class names so that they
     * only need to be loaded once.
     */
    private final Map<String, RuleOperator> ruleOperatorCache = new HashMap<>();

    /**
     * The current @{@link Record}.
     */
    private Record currentRecord;

    /**
     * Sets the current @{@link Record}.
     */
    public void setCurrentRecord(Record currentRecord) {
        this.currentRecord = currentRecord;
    }

    /**
     * Evaluates a single {@link Rule}. A Rule is evaluated according to it's {@link RuleOperator}.
     *
     * @param rule    the Rule.
     * @param record  the {@link Record} within which the Rule is to be processed.
     * @param current the current {@link Field} in the Record.
     * @return the result of evaluating this Rule.
     */
    public boolean evaluateRule(Rule rule, Record record, Field current) {
        RuleOperator operator = loadRuleOperator(rule);
        return operator.evaluate(rule, record, current);
    }

    /**
     * Evaluates a list of {@link Rule}s. This method is similar to {@link #evaluateRule(Rule, Record, Field)} except
     * that it evaluates a list of rules all together. The results of the different Rules are combined logically based
     * on the Rules' {@link Rule#combiner}s.
     *
     * @param rules   the list of Rules.
     * @param record  the {@link Record} within which the Rule is to be processed.
     * @param current the current {@link Field} in the Record.
     * @return the result of evaluating these Rules together.
     */
    public boolean evaluateRules(List<Rule> rules, Record record, Field current) {
        if (rules == null || rules.isEmpty()) {
            return true;
        }
        Map ruleGroups = groupRules(rules);
        if (ruleGroups.isEmpty()) {
            return true;
        }
        NavigableList<RuleGroup> ruleGroupList
            = new NavigableList<>(listRules(ruleGroups.values()));
        RuleGroup first = ruleGroupList.first();
        boolean result = evaluateRuleGroup(first, record, current);
        if (ruleGroupList.size() == 1) {
            return result;
        }
        Integer combiner = first.getCombiner();
        int n = ruleGroupList.indexOfLast();
        for (int i = 1; i <= n; i++) {
            RuleGroup next = ruleGroupList.get(i);
            if (combiner.equals(Rule.Combiner.AND)) {
                result = result && evaluateRuleGroup(next, record, current);
            } else if (combiner.equals(Rule.Combiner.OR)) {
                result = result || evaluateRuleGroup(next, record, current);
            }
            combiner = next.getCombiner();
        }
        return result;
    }

    private boolean evaluateRuleGroup(RuleGroup ruleGroup, Record record, Field current) {
        NavigableList<Rule> rules = new NavigableList<>(ruleGroup.getRules());
        Rule first = rules.first();
        boolean result = evaluateRule(first, record, current);
        if (rules.size() == 1) {
            return result;
        }
        Integer combiner = first.getCombiner();
        int n = rules.indexOfLast();
        for (int i = 1; i <= n; i++) {
            Rule next = rules.get(i);
            if (combiner.equals(Rule.Combiner.AND)) {
                result = result && evaluateRule(next, record, current);
            } else if (combiner.equals(Rule.Combiner.OR)) {
                result = result || evaluateRule(next, record, current);
            }
            combiner = next.getCombiner();
        }
        return result;
    }

    private Map<Integer, RuleGroup> groupRules(List<Rule> rules) {
        Map<Integer, RuleGroup> ruleGroups = new HashMap<>();
        for (Rule rule : rules) {
            if (!ruleGroups.containsKey(rule.getGrouper())) {
                ruleGroups.put(rule.getGrouper(),
                    new RuleGroup(new ArrayList<Rule>()));
            }
            RuleGroup ruleGroup = ruleGroups.get(rule.getGrouper());
            ruleGroup.setCombiner(rule.getCombiner());
            ruleGroup.getRules().add(rule);
        }
        return ruleGroups;
    }

    private List<RuleGroup> listRules(Collection<RuleGroup> ruleGroups) {
        List<RuleGroup> list = new ArrayList<>();
        for (RuleGroup cg : ruleGroups) {
            list.add(cg);
        }
        return list;
    }

    /*
     * This method loads the appropriate {@link RuleOperator} for a given {@link Rule}. RuleOperators are defined in
     * the database by specifying their class name via an {@link OperatorDescriptor}. The original idea was for
     * RuleOperators to be configurable through 3rd party libraries, but this use case has not arisen in practice, and
     * likely won't.
     *
     * This method has an important quirk to note. When loading a RuleOperator for a rule associated with a field of type
     * "multiple choice", this method implicitly converts the {@link Contains} operator to the {@link BitwiseAnd}
     * operator. The reason is that the "Bitwise And" operator only applies to "multi choice" fields, but in the eyes of
     * the user, it is easy to think about it as the classical Contains operator. As such, users configure BitwiseAnd
     * as Contains. This method implicitly performs the conversion to cover this fact.
     *
     * Another thing to note is that RuleOperators are loaded once and then cached for reuse.
     */
    private RuleOperator loadRuleOperator(Rule rule) {
        String clazz = rule.getOperatorDescriptor().getClazz();
        if (clazz.equals(OperatorDescriptor.CONTAINS)) {
            if ((rule.isForwardSkip() && isMultipleChoice(rule.getOwner().getType()))
                || (rule.isBackwardSkip() && isMultipleChoice(rule.getTarget().getType()))) {
                clazz = OperatorDescriptor.BITWISE_AND;
            }
        }
        if (clazz.equals(OperatorDescriptor.NOT_CONTAINS)) {
            if ((rule.isForwardSkip() && isMultipleChoice(rule.getOwner().getType()))
                || (rule.isBackwardSkip() && isMultipleChoice(rule.getTarget().getType()))) {
                clazz = OperatorDescriptor.NOT_BITWISE_AND;
            }
        }
        RuleOperator operator = ruleOperatorCache.get(clazz);
        if (operator == null) {
            try {
                Class cls = Class.forName(clazz);
                Constructor<RuleOperator> constructor = cls.getDeclaredConstructor();
                constructor.setAccessible(true);
                operator = constructor.newInstance();
                ruleOperatorCache.put(clazz, operator);
            } catch (InstantiationException | IllegalAccessException |
                IllegalArgumentException | InvocationTargetException | NoSuchMethodException |
                ClassNotFoundException | SecurityException ex) {
                throw new RuntimeException("Could not instantiate RuleOperator.", ex);
            }
        }
        operator.setCurrentRecord(currentRecord);
        return operator;
    }

    private boolean isMultipleChoice(FieldType fieldType) {
        return fieldType.isMultiChoice() || fieldType.isRanking();
    }
}
