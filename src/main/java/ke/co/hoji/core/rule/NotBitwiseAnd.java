package ke.co.hoji.core.rule;

import ke.co.hoji.core.response.Response;

import java.util.List;

/**
 * The negation of the {@link BitwiseAnd} @{@link RuleOperator}.
 * <p/>
 * Created by gitahi on 11/04/18.
 */
public class NotBitwiseAnd extends RuleOperator {

    private final BitwiseAnd bitwiseAnd = new BitwiseAnd();

    @Override
    protected boolean evaluate(Response response, List<String> values) {
        return !bitwiseAnd.evaluate(response, values);
    }
}
