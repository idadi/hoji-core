package ke.co.hoji.core.rule;

import ke.co.hoji.core.StringInterpolator;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.response.Response;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * An abstract class that is the base class of all operators for {@link Rule}s. A RuleOperator evaluates a given Rule
 * within the context of a given material {@link Record} and current {@link Field} and returns a boolean value. The
 * return value determines the action to be taken taken according to the semantics of the {@link Rule.Type} of the
 * Rule.
 * <p/>
 * Particularly, if a Rule of type {@link Rule.Type#FORWARD_SKIP} evaluates to true, then the navigation system must
 * skip to the Rule's {@link Rule#target}. Otherwise it must navigate ordinarily to the next field in line. If a Rule of
 * type {@link Rule.Type#BACKWARD_SKIP} evaluates to true, then the navigation system must render the Rule's {@link
 * Rule#owner}. Otherwise it must skip the owner and navigate ordinarily yo the next field in line. If a Rule of type
 * {@link Rule.Type#FILTER_RULE} evaluates to true, the entire record must be preserved in the material list. Otherwise
 * if it evaluates to false, the entire record must be filtered out.
 * <p/>
 * This class defines a single abstract method {@link RuleOperator#evaluate(Response, List)}, that subclasses must
 * implement. In addition, it defines a few other convenience methods for retrieving data from the parameters supplied
 * to the <code>evaluate()<code/> method.
 *
 * @author gitahi
 */
public abstract class RuleOperator {

    private static final StringInterpolator STRING_INTERPOLATOR = new StringInterpolator();

    /**
     * The current @{@link Record}.
     */
    private Record currentRecord;

    /**
     * Sets the current @{@link Record}.
     */
    public void setCurrentRecord(Record currentRecord) {
        this.currentRecord = currentRecord;
    }

    /**
     * Initializes a RuleOperator with no arguments.
     */
    protected RuleOperator() {
    }

    /**
     * Evaluates a {@link Rule} in the context of the {@link Record} and current {@link Field} supplied.
     *
     * @param rule   the Rule to evaluate.
     * @param record the material Record.
     * @param field  the current Field.
     * @return the result of the evaluation as a boolean.
     * @should call evaluate with null response if material field is not already a live field in record
     */
    protected boolean evaluate(Rule rule, Record record, Field field) {
        return evaluate(materialResponse(rule, record, field), evaluationValues(rule));
    }

    /**
     * Evaluates a {@link Response} against the supplied values. Result expecting a single value should only use the
     * first value in the list.
     *
     * @param response the {@link Response} to evaluate.
     * @param values   the list of values against which to evaluate the response.
     * @return the result of the evaluation as a boolean.
     */
    protected abstract boolean evaluate(Response response, List<String> values);

    /**
     * Gets the ith evaluation value from a given list of values in a null-tolerant way.
     *
     * @param values the list of values.
     * @param i      the value of i.
     * @return the ith evaluation value.
     * @see {@link Rule#value}.
     */
    protected final String value(List<String> values, int i) {
        int index = i - 1;
        if (values != null && values.size() > index) {
            return values.get(index);
        }
        return null;
    }

    private Response materialResponse(Rule rule, Record record, Field field) {
        Response materialResponse = null;
        Field materialField;
        if (rule.getType() == Rule.Type.FORWARD_SKIP) {
            materialField = rule.getOwner();
        } else {
            materialField = rule.getTarget();
        }
        if (materialField.equals(field)) {
            materialResponse = field.getResponse();
        } else {
            LiveField materialLiveField = record.find(materialField);
            if (materialLiveField != null) {
                return materialLiveField.getResponse();
            }
        }
        return materialResponse;
    }

    private List<String> evaluationValues(Rule rule) {
        List<String> rawValues = Utils.tokenizeString(rule.getValue(), "\\|", true);
        List<String> processedValues = new ArrayList<>();
        for (String rawValue : rawValues) {
            processedValues.add(processRawValue(rawValue, rule.getType()));
        }
        return processedValues;
    }

    private String processRawValue(String rawValue, int ruleType) {
        if (rawValue != null) {
            if (rawValue.equals(Constants.Command.EMPTY)) {
                return null;
            } else if (rawValue.equals(Constants.Command.TODAY)) {
                return Utils.formatDate(new Date());
            } else if (rawValue.equals(Constants.Command.NOW)) {
                return Utils.formatTime(new Date());
            }
        }
        if (ruleType == Rule.Type.FILTER_RULE) {
            return STRING_INTERPOLATOR.interpolate(rawValue, currentRecord, StringInterpolator.InterpolationType.RULE_VALUE);
        } else {
            return rawValue;
        }
    }
}
