package ke.co.hoji.core.rule;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.response.Response;

import java.util.List;

/**
 * Check if the {@link Response} to a {@link Field} is not equal to the {@link Rule} value.
 *
 * @author gitahi
 */
public class NotEquals extends RuleOperator {

    private final ComparableHelper comparableHelper = new ComparableHelper();

    /**
     * {@inheritDoc}
     *
     * @should return true if actual value of response compares to comparable value computed from first value with
     * result equal to zero.
     * @should return false if actual value of response compares to comparable value computed from first value with
     * result not equal to zero.
     */

    @Override
    public boolean evaluate(Response response, List<String> values) {
        return comparableHelper.evaluate(response, value(values, 1), ComparableHelper.Sign.NOT_EQUALS);
    }
}
