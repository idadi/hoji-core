package ke.co.hoji.core;

import ke.co.hoji.core.service.dto.FieldService;
import ke.co.hoji.core.service.dto.FormService;
import ke.co.hoji.core.service.dto.LanguageService;
import ke.co.hoji.core.service.dto.RecordService;
import ke.co.hoji.core.service.dto.ReferenceService;
import ke.co.hoji.core.service.dto.SurveyService;
import ke.co.hoji.core.service.dto.TranslationService;

/**
 * A convenience access point for DTO services provided in the service layer.
 *
 * @author gitahi
 */
public interface DtoServiceManager extends ServiceManager {

    SurveyService getSurveyService();

    FormService getFormService();

    FieldService getFieldService();

    ReferenceService getReferenceService();

    RecordService getRecordService();

    LanguageService getLanguageService();
    
    TranslationService getTranslationService();
}
