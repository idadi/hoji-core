package ke.co.hoji.core.calculation.expression;

public abstract class Expression<L,R> {

    protected final L leftOperand;
    protected final R rightOperand;

    private int fieldId;

    public Expression(L leftOperand, R rightOperand) {
        this.leftOperand = leftOperand;
        this.rightOperand = rightOperand;
    }

    public  void setFieldId(int fieldId) {
        this.fieldId = fieldId;
    }

    public Integer getFieldId() {
        return this.fieldId;
    }

    public abstract <T> T toQuery();
}
