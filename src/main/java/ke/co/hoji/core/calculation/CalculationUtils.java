package ke.co.hoji.core.calculation;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.calculation.expression.CrossFormValueRetriever;
import ke.co.hoji.core.calculation.expression.Expression;
import ke.co.hoji.core.calculation.expression.ExpressionParser;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.ValidationResult;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.response.SingleChoiceResponse;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.Weeks;
import org.joda.time.Years;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * The public API that gets exposed to calculation scripts. Any functionality that a calculation script might
 * need must be exposed through this class.
 */
public class CalculationUtils {

    private static final Logger logger = LoggerFactory.getLogger(CalculationUtils.class);

    /**
     * Attributes of a {@link Field} by which we might query for data.
     */
    public class FieldAttributes {

        /**
         * The id of the Field. See {@link Field#id}.
         */
        public static final int ID = 0;

        /**
         * The tag of the Field. See {@link Field#tag}.
         */
        public static final int TAG = 1;
    }

    /**
     * Different units for measuring a period of time.
     */
    public class TimePeriodUnits {

        public static final String DAYS = "DAYS";
        public static final String WEEKS = "WEEKS";
        public static final String MONTHS = "MONTHS";
        public static final String YEARS = "YEARS";
    }

    /**
     * Constants for date and time.
     */
    public class DateTimeConstants {

        /**
         * A place holder for the current date. Allow's JavaScript callers to easily request the current date via
         * {@link #createDate(String)}.
         */
        public static final String TODAY = "TODAY";
    }

    /**
     * Parses json expressions for cross form value extraction
     */
    private ExpressionParser expressionParser;

    /**
     * Allows calculations to obtain values from other forms
     */
    private CrossFormValueRetriever crossFormValueRetriever;

    /**
     * Collection of {@link LiveField liveFields} in a {@link Record} that can be used in the calculation script.
     */
    private final List<LiveField> liveFields;

    /**
     * The current {@link Field}.
     */
    private final Field currentField;

    /**
     * The {@link Date} when a {@link Record} was created for use in the calculation script.
     */
    private final Date dateCreated;

    /**
     * The uuid of the {@link ke.co.hoji.core.data.model.MainRecord main record} under which a calculation is executing.
     */
    private String recordUuid;

    /**
     * The id of the survey under which the execution is taking place
     */
    private final Integer surveyId;

    /**
     * Whether or not properties have been loaded.
     */
    private boolean propertiesLoaded = false;

    /**
     * Used by {@link #countLeafChoices(String, String, Choice)} to keep track of {@link Choice} hierarchy.
     */
    private SortedSet<Field> fieldTree = Collections.synchronizedSortedSet(new TreeSet<Field>());

    /**
     * Helper for accessing model service instances
     */
    private final ModelServiceManager modelServiceManager;

    /**
     * <p>
     * The type of {@link HojiContext} under which to execute calculations. The value should be either
     * {@link HojiContext#ANDROID_CONTEXT} or {@link HojiContext#SERVER_CONTEXT}. This has implications
     * on how calculations are processed. Specifically, calculations executed in the server context
     * return the current value when {@link #createCalculationResult(Object, boolean)} is invoked with
     * enableIfNull = true.
     * </p>
     * <p>
     * When not explicitly set in the constructor, the value defaults to {@link HojiContext#ANDROID_CONTEXT}.
     * </p>
     */
    private final String hojiContextType;

    /**
     * Creates a new instance of CalculationUtils.
     *
     * @param surveyId            id of survey where script execution is taking place.
     * @param modelServiceManager helper for accessing services.
     * @param liveFields          the list of {@link LiveField} in the current record.
     * @param dateCreated         the date when the current record was created.
     * @param hojiContextType
     */
    public CalculationUtils(
        Integer surveyId,
        ModelServiceManager modelServiceManager,
        List<LiveField> liveFields,
        Field currentField,
        Date dateCreated,
        String hojiContextType
    ) {
        this.surveyId = surveyId;
        this.modelServiceManager = modelServiceManager;
        this.liveFields = liveFields;
        this.currentField = currentField;
        this.dateCreated = dateCreated;
        this.hojiContextType = hojiContextType;
    }

    /**
     * Creates a new instance of CalculationUtils with {@link #hojiContextType} set
     * to {@link HojiContext#ANDROID_CONTEXT}.
     *
     * @param surveyId            id of survey where script execution is taking place.
     * @param modelServiceManager helper for accessing services.
     * @param liveFields          the list of {@link LiveField} in the current record.
     * @param dateCreated         the date when the current record was created.
     */
    public CalculationUtils(
        Integer surveyId,
        ModelServiceManager modelServiceManager,
        List<LiveField> liveFields,
        Field currentField,
        Date dateCreated
    ) {
        this(surveyId, modelServiceManager, liveFields, currentField, dateCreated, HojiContext.ANDROID_CONTEXT);
    }

    public void setExpressionParser(ExpressionParser expressionParser) {
        this.expressionParser = expressionParser;
    }

    public void setCrossFormValueRetriever(CrossFormValueRetriever crossFormValueRetriever) {
        this.crossFormValueRetriever = crossFormValueRetriever;
    }

    public void setRecordUuid(String recordUuid) {
        this.recordUuid = recordUuid;
    }

    public String getHojiContextType() {
        return hojiContextType;
    }

    public Integer getCurrentFieldId() {
        return currentField.getId();
    }

    public String getCurrentFieldNo() {
        return currentField.getName();
    }

    public String getCurrentFieldText() {
        return currentField.getDescription();
    }

    public String getCurrentFieldInstructions() {
        return currentField.getInstructions();
    }

    public String getCurrentFieldTag() {
        return currentField.getTag();
    }

    public Object getCurrentValue() {
        return currentField.getResponse() != null ? currentField.getResponse().getActualValue() : null;
    }

    /**
     * Get the {@link Date} when the current @{@link Record} was created.
     *
     * @return the Date when the Record was created.
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    public String getRecordUuid() {
        return recordUuid;
    }

    /**
     * Retrieves a list of values matched by the supplied expression. This method allows obtaining values from other forms.
     *
     * @param expression, json expression for filtering returned live fields
     * @return a collection of matched values.
     */
    public List<Object> getForeignValues(String expression) {
        if (expressionParser == null && crossFormValueRetriever == null) {
            return Collections.emptyList();
        }
        try {
            Expression parsed = expressionParser.parse(expression);
            return crossFormValueRetriever.retrieve(parsed);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new CalculationException(e, expression);
        }
    }

    /**
     * Get {@link Response} value for {@link Field} matching the given key.
     *
     * @param fieldKey the identifier of the {@link Field field}
     * @return the value of the response
     */
    public Object getValue(String fieldKey) {
        Response response = getResponse(fieldKey);
        if (response != null) {
            return response.getActualValue(true);
        }
        return null;
    }

    /**
     * Get the quantity of the {@link Response} for the {@link Field} matching the given fieldKey. This method
     * calls {@link Response#quantityString()} and tries to cast the return value to a {@link Double}. If the value
     * cannot be cast, null is returned instead.
     *
     * @param fieldKey the ID or tag of the {@link Field field} whose quantity is to be obtained.
     * @return the quantity associated with the response for the requested {@link Field}.
     */
    public Double getQuantity(String fieldKey) {
        Response response = getResponse(fieldKey);
        if (response != null) {
            return Utils.parseDouble(response.quantityString());
        }
        return null;
    }

    /**
     * Gets the {@link Choice} associated with the given choiceId. This method is useful for setting calculated
     * {@link SingleChoiceResponse}s.
     *
     * @param field    the Field from which the {@link Choice} is to be obtained.
     * @param choiceId the Choice Id.
     * @return the {@link Choice}.
     */
    @Deprecated
    public Choice getChoice(Field field, Integer choiceId) {
        if (field != null) {
            if (field.getType().getCode().equals(FieldType.Code.SINGLE_CHOICE)) {
                Choice match = null;
                for (Choice choice : field.getChoiceGroup().getChoices()) {
                    if (choice.getId().equals(choiceId)) {
                        match = choice;
                        break;
                    }
                }
                return match;
            }
        }
        return null;
    }

    /**
     * Gets the {@link Choice} associated with the given name. This method is useful for setting calculated
     * {@link SingleChoiceResponse}s.
     *
     * @param field      the Field from which the {@link Choice} is to be obtained.
     * @param choiceName the Choice name.
     * @return the {@link Choice}.
     */
    @Deprecated
    public Choice getChoice(Field field, String choiceName) {
        if (field != null) {
            if (field.getType().getCode().equals(FieldType.Code.SINGLE_CHOICE)) {
                Choice match = null;
                for (Choice choice : field.getChoiceGroup().getChoices()) {
                    if (choice.getName().equals(choiceName)) {
                        match = choice;
                        break;
                    }
                }
                return match;
            }
        }
        return null;
    }

    /**
     * Gets the {@link Choice} associated with the given choiceId. This method is useful for setting calculated
     * {@link SingleChoiceResponse}s.
     *
     * @param fieldId  the id of the Field from which the {@link Choice} is to be obtained.
     * @param choiceId the Choice Id.
     * @return the {@link Choice}.
     */
    public Choice getChoice(Integer fieldId, Integer choiceId) {
        Field field = null;
        if (fieldId.equals(currentField.getId())) {
            field = currentField;
        } else {
            LiveField liveField = findLiveField(String.valueOf(fieldId), FieldAttributes.ID);
            if (liveField != null) {
                field = liveField.getField();
            }
        }
        if (field != null) {
            Choice match = null;
            if (field.getChoiceGroup() != null && field.getChoiceGroup().getChoices() != null) {
                for (Choice choice : field.getChoiceGroup().getChoices()) {
                    if (choice.getId().equals(choiceId)) {
                        match = choice;
                        break;
                    }
                }
            }
            return match;
        }
        return null;
    }

    /**
     * Gets the {@link Choice} associated with the given name. This method is useful for setting calculated
     * {@link SingleChoiceResponse}s.
     *
     * @param fieldId    the id of the Field from which the {@link Choice} is to be obtained.
     * @param choiceName the Choice name.
     * @return the {@link Choice}.
     */
    public Choice getChoice(Integer fieldId, String choiceName) {
        Field field = null;
        if (fieldId.equals(currentField.getId())) {
            field = currentField;
        } else {
            LiveField liveField = findLiveField(String.valueOf(fieldId), FieldAttributes.ID);
            if (liveField != null) {
                field = liveField.getField();
            }
        }
        if (field != null) {
            Choice match = null;
            if (field.getChoiceGroup() != null && field.getChoiceGroup().getChoices() != null) {
                for (Choice choice : field.getChoiceGroup().getChoices()) {
                    if (choice.getName().equals(choiceName)) {
                        match = choice;
                        break;
                    }
                }
            }
            return match;
        }
        return null;
    }

    /**
     * Get a text property from the database.
     *
     * @param key the key by which to get the property.
     * @return the property.
     */
    public String getTextProperty(String key) {
        return getProperty(key);
    }

    /**
     * Get a numeric property from the database.
     *
     * @param key the key by which to get the property.
     * @return the property.
     */
    public Long getNumberProperty(String key) {
        String numberProperty = getProperty(key);
        if (numberProperty != null) {
            return Long.valueOf(numberProperty);
        }
        return null;
    }

    /**
     * Get a decimal property from the database.
     *
     * @param key the key by which to get the property.
     * @return the property.
     */
    public Double getDecimalProperty(String key) {
        String decimalProperty = getProperty(key);
        if (decimalProperty != null) {
            return Double.valueOf(decimalProperty);
        }
        return null;
    }

    /**
     * Get a {@link Date} property from the database.
     *
     * @param key the key by which to get the property.
     * @return the property.
     */
    public Date getDateProperty(String key) {
        String dateProperty = getProperty(key);
        if (dateProperty != null) {
            return Utils.parseIsoDate(dateProperty);
        }
        return null;
    }

    /**
     * Sums up an array of fields. Each field contributes one addend as defined by {@link #getQuantity(String)}.
     *
     * @param strict    if true, an {@link IllegalArgumentException} is thrown if any of the fields returns a null
     *                  addend.
     * @param fieldKeys the Ids (or tags) of the {@link Field}s to be summed up.
     * @return the sum.
     */
    public Double sum(boolean strict, String... fieldKeys) {
        Double sum = null;
        for (String fieldKey : fieldKeys) {
            Double addend = getQuantity(fieldKey);
            if (addend != null) {
                if (sum == null) {
                    sum = 0D;
                }
                sum += addend;
            } else {
                if (strict) {
                    throw new IllegalArgumentException("Null operands not allowed!");
                }
            }
        }
        return sum;
    }

    /**
     * Sums up the number or decimal {@link Response}s of the {@link Field}s identified by the Field Ids passed. If
     * any of the operand values are null they are simply ignored.
     *
     * @param fieldKeys the Ids of the {@link Field}s to be summed up.
     * @return the sum.
     */
    public Double sum(String... fieldKeys) {
        return sum(false, fieldKeys);
    }

    /**
     * Converts a Double to a Long. This is useful because JavaScript treats all numbers as decimal numbers.
     *
     * @param value the Double value.
     * @return the Long value.
     */
    public Long doubleToLong(Double value) {
        if (value != null) {
            return value.longValue();
        }
        return null;
    }

    /**
     * Creates a {@link Date} object from an ISO string representation of a date i.e. yyyy-MM-dd
     */
    public Date createDate(String isoDateString) {
        if (DateTimeConstants.TODAY.equals(isoDateString)) {
            return new Date();
        }
        return Utils.parseIsoDate(isoDateString);
    }

    /**
     * Gets the difference between two dates in the specified units e.g. days, weeks, months e.t.c. The difference is
     * calculated in terms of date2 - date1;
     *
     * @param date1 the first date.
     * @param date2 the second date.
     * @param units the {@link TimePeriodUnits} in which the date should be returned in.
     * @return the difference in the specified units.
     */
    public Integer dateDifference(Date date1, Date date2, String units) {
        if (date1 == null) {
            throw new IllegalArgumentException("date1 cannot be null.");
        } else if (date2 == null) {
            throw new IllegalArgumentException("date2 cannot be null.");
        } else if (units == null) {
            throw new IllegalArgumentException("units cannot be null.");
        }
        return dateDifference(new LocalDate(date1), new LocalDate(date2), units);
    }

    /**
     * Compares two dates, ignoring the time component.
     *
     * @param date1 the first {@link Date}.
     * @param date2 the second {@link Date}.
     * @return a negative integer, zero, or a positive integer as the date1 is less than, equal to, or greater than
     * the date2.
     */
    public Integer compareDatesWithoutTime(Date date1, Date date2) {
        LocalDate localDate1 = new DateTime(date1).toLocalDate();
        LocalDate localDate2 = new DateTime(date2).toLocalDate();
        return localDate1.compareTo(localDate2);
    }

    /**
     * Adds a period of time to a date and returns the resulting date.
     *
     * @param date   the date to add to.
     * @param period the period to add.
     * @param units  the {@link TimePeriodUnits} of the period
     * @return the new date.
     */
    public Date addPeriod(Date date, Long period, String units) {
        if (date == null) {
            throw new IllegalArgumentException("date cannot be null.");
        } else if (period == null) {
            throw new IllegalArgumentException("period cannot be null.");
        } else if (units == null) {
            throw new IllegalArgumentException("units cannot be null.");
        }
        return addPeriod(new LocalDate(date), period.intValue(), units);
    }

    /**
     * Create a {@link CalculationResult} with the given value and enableIfNull setting.
     * <p>
     *
     * @param value
     * @param enableIfNull
     * @return the {@link CalculationResult}.
     */
    public CalculationResult createCalculationResult(Object value, boolean enableIfNull) {
        if (enableIfNull && HojiContext.SERVER_CONTEXT.equals(hojiContextType)) {
            // Use whatever value the user originally entered since there is no opportunity to
            // enter a value server-side.
            return new CalculationResult(getCurrentValue());
        }
        return new CalculationResult(value, enableIfNull);
    }

    /**
     * Create a {@link ValidationResult} with the given type and message. Valid ValidationResult types are:
     * <p>
     * 1. {@link ValidationResult#OKAY}
     * <p>
     * 2. {@link ValidationResult#WARN}
     * <p>
     * 3. {@link ValidationResult#STOP}
     *
     * @param type    the type of validation.
     * @param message the message for this validation.
     * @return the {@link ValidationResult}
     */
    public ValidationResult createValidationResult(int type, String message) {
        return new ValidationResult(type, message);
    }

    /**
     * Formats {@link Date} to ISO date format i.e. yyyy-MM-dd.
     *
     * @param date the {@link Date} to format.
     * @return the formated date string.
     */
    public String formatIsoDate(Date date) {
        return Utils.formatIsoDate(date);
    }

    /**
     * Formats {@link Date} to ISO time format i.e. HH:mm:ss.
     *
     * @param date the {@link Date} to format.
     * @return the formated date string.
     */
    public String formatIsoTime(Date date) {
        return Utils.formatIsoTime(date);
    }

    /**
     * In general, this method counts and returns the number of leaf {@link Choice}s under the given root
     * {@link Choice}.
     * <p>
     * The use case here bears some explaining. Often, we have a survey targeting to reach a specific number of
     * records, based on a known sample. For example, if the unit of surveying is a school, the entire sample is
     * often provided and listed in Hoji as choices.
     * <p>
     * Usually, these schools are listed under some sort of hierarchy, such as Region - County - Sub-county -
     * School. Clients often want to know, of the targeted schools in say, Region 1, how many have been covered? The
     * same question may be asked at the level of the County or Sub-county instead of the Region.
     * <p>
     * More generically, we need to count the number of leaf choices i.e. the schools in the above example, under a
     * given root choice i.e. Region, County or Sub-county in the above example.
     *
     * @param leafFieldKey the key identifying the leaf {@link Field}.
     * @param rootFieldKey the key identifying the root {@link Field}.
     * @param rootChoice   the actual {@link Choice} under which to count leaf {@link Choice}s.
     * @return the total number of leaf {@link Choice}s under the root choice.
     */
    public Integer countLeafChoices(String leafFieldKey, String rootFieldKey, Choice rootChoice) {
        LiveField leafLiveField = getLiveField(leafFieldKey);
        LiveField rootLiveField = getLiveField(rootFieldKey);
        if (leafLiveField == null || rootLiveField == null) {
            throw new RuntimeException("Missing leaf or root field.");
        }
        Field leafField = leafLiveField.getField();
        Field rootField = rootLiveField.getField();
        if (!leafIsDescendantOfRoot(leafField, rootField)) {
            throw new RuntimeException("Leaf is not descendant of root.");
        }
        List<Choice> filterBy = null;
        for (Field field : fieldTree) {
            if (field.equals(rootField)) {
                filterBy = Arrays.asList(rootChoice);
            } else {
                List<Choice> qualifyingList = new ArrayList<>();
                for (Choice choice : field.getChoiceGroup().getChoices()) {
                    if (filterBy.contains(choice.getParent())) {
                        qualifyingList.add(choice);
                    }
                }
                if (field.equals(leafField)) {
                    return qualifyingList.size();
                }
                filterBy = qualifyingList;
            }
        }
        return null;
    }

    private boolean leafIsDescendantOfRoot(Field leafField, Field rootField) {
        Field choiceFilterField = leafField.getChoiceFilterField();
        if (choiceFilterField != null) {
            fieldTree.add(leafField);
            fieldTree.add(choiceFilterField);
            if (rootField.equals(choiceFilterField)) {
                return true;
            } else {
                return leafIsDescendantOfRoot(choiceFilterField, rootField);
            }
        }
        return false;
    }

    private Response getResponse(String fieldKey) {
        LiveField liveField = getLiveField(fieldKey);
        if (liveField != null) {
            return liveField.getResponse();
        }
        return null;
    }

    private LiveField getLiveField(String fieldKey) {
        LiveField liveField = findLiveField(fieldKey, FieldAttributes.ID);
        if (liveField == null) {
            liveField = findLiveField(fieldKey, FieldAttributes.TAG);
        }
        if (liveField != null) {
            return liveField;
        }
        return null;
    }

    private LiveField findLiveField(String fieldKey, int fieldAttribute) {
        for (LiveField liveField : liveFields) {
            String attributeValue = null;
            switch (fieldAttribute) {
                case FieldAttributes.ID:
                    attributeValue = liveField.getField().getId().toString();
                    break;
                case FieldAttributes.TAG:
                    attributeValue = liveField.getField().getTag();
                    break;
            }
            if (fieldKey.equals(attributeValue) && liveField.getField().isEnabled()) {
                return liveField;
            }
        }
        return null;
    }

    private String getProperty(String key) {
        if (!propertiesLoaded) {
            modelServiceManager.getPropertyService().getProperties(surveyId);
            propertiesLoaded = true;
        }
        return modelServiceManager.getPropertyService().getValue(key, surveyId);
    }

    private Integer dateDifference(LocalDate date1, LocalDate date2, String units) {
        switch (units) {
            case TimePeriodUnits.DAYS:
                return Days.daysBetween(date1, date2).getDays();
            case TimePeriodUnits.WEEKS:
                return Weeks.weeksBetween(date1, date2).getWeeks();
            case TimePeriodUnits.MONTHS:
                return Months.monthsBetween(date1, date2).getMonths();
            case TimePeriodUnits.YEARS:
                return Years.yearsBetween(date1, date2).getYears();
            default:
                throw new IllegalArgumentException(units + " is not a recognized time unit.");
        }
    }

    private Date addPeriod(LocalDate date, Integer period, String units) {
        switch (units) {
            case TimePeriodUnits.DAYS:
                return date.plusDays(period).toDate();
            case TimePeriodUnits.WEEKS:
                return date.plusWeeks(period).toDate();
            case TimePeriodUnits.MONTHS:
                return date.plusMonths(period).toDate();
            case TimePeriodUnits.YEARS:
                return date.plusYears(period).toDate();
            default:
                throw new IllegalArgumentException(units + " is not a recognized time unit.");
        }
    }
}
