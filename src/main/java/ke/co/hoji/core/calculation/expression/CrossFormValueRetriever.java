package ke.co.hoji.core.calculation.expression;

import java.util.List;

public interface CrossFormValueRetriever {

    List<Object> retrieve(Expression expression);

}
