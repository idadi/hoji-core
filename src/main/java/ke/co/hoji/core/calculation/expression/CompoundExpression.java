package ke.co.hoji.core.calculation.expression;

import java.util.List;

public abstract class CompoundExpression extends Expression<String, List<Expression>> {

    public CompoundExpression(String leftOperand, List<Expression> rightOperand) {
        super(leftOperand, rightOperand);
    }
}
