package ke.co.hoji.core.calculation.expression;

public abstract class EqualsExpression extends Expression<String, Object> {

    protected final boolean negated;

    public EqualsExpression(String leftOperand, Object rightOperand, boolean negated) {
        super(leftOperand, rightOperand);
        this.negated = negated;
    }
}
