package ke.co.hoji.core.calculation.expression;

import java.io.IOException;

public interface ExpressionParser {

    Expression parse(String expression) throws IOException;

}
