package ke.co.hoji.core.calculation;

/**
 * A CalculationResult is the result of executing a calculation script using a {@link CalculationEvaluator}.
 * Calculation scripts comprise of any code encapsulated in a calculate() function inside a script.
 * <p>
 * Calculation scripts can either construct and return a {@link CalculationResult}, or, they can simply return some
 * other object. In the latter case, a {@link CalculationResult} is constructed with the object as the {@link #value}
 * and {@link #enableIfNull} set to false. Use the former for convenience and the latter for more fine-grained control.
 */
public final class CalculationResult {

    /**
     * The value of the result.
     */
    private final Object value;

    /**
     * If true and {@link #value} is null, then the program enables data entry for the field.
     */
    private final boolean enableIfNull;

    public CalculationResult(Object value) {
        this(value, false);
    }

    public CalculationResult(Object value, boolean enableIfNull) {
        this.value = value;
        this.enableIfNull = enableIfNull;
    }

    public Object getValue() {
        return value;
    }

    public boolean isEnableIfNull() {
        return enableIfNull;
    }
}
