package ke.co.hoji.core.calculation;

import ke.co.hoji.core.data.model.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import java.io.StringWriter;

/**
 * Evaluates externally defined scripts and returns the result to the program. These scripts may be defined in any
 * language, as long as the appropriate {@link ScriptEngine} is provided.
 * <p>
 * Scripts must be written in functions, as opposed to free-flowing. Hoji currently supports the  functions calculate()
 * and validate(). The following functions may be supported in future: label(), ifShow(), showIf().
 */
public class CalculationEvaluator {

    private static final Logger logger = LoggerFactory.getLogger(CalculationEvaluator.class);

    /**
     * The key of the {@link CalculationUtils} accessible from script
     */
    public static final String UTILS_SCRIPT_KEY = "calculationUtils";

    /**
     * The key of the current {@link ke.co.hoji.core.data.model.Field field} whose value is being calculated
     */
    public static final String CURRENT_FIELD_KEY = "currentField";

    /**
     * The shorthand key of the {@link CalculationUtils} accessible from script
     */
    public static final String UTILS_SCRIPT_KEY_SHORT = "cu";

    /**
     * The branded key of the {@link CalculationUtils} accessible from script
     */
    public static final String UTILS_SCRIPT_KEY_BRANDED = "Hoji";

    /**
     * The shorthand key of the current {@link ke.co.hoji.core.data.model.Field field} whose value is being calculated
     */
    public static final String CURRENT_FIELD_KEY_SHORT = "cf";

    /**
     * The {@link ScriptEngine} for evaluating scripts.
     */
    private final ScriptEngine scriptEngine;

    /**
     * Initializes a new {@link CalculationEvaluator}.
     *
     * @param scriptEngine the {@link ScriptEngine} to use in this {@link CalculationEvaluator}.
     */
    public CalculationEvaluator(ScriptEngine scriptEngine) {
        this.scriptEngine = scriptEngine;
    }

    /**
     * Evaluate a calculation script.
     *
     * @param calculationScript the script.
     * @param function          the function to be invoked
     * @param calculationUtils  an instance of {@link CalculationUtils} to provide access to utility methods.
     * @param currentField      the current {@link Field}.
     * @return the result of the evaluation. May return null.
     */
    public Object evaluateScript(
        String calculationScript,
        String function,
        CalculationUtils calculationUtils,
        Field currentField
    ) throws CalculationException {
        StringWriter logWriter = new StringWriter();
        scriptEngine.getContext().setWriter(logWriter);

        scriptEngine.put(UTILS_SCRIPT_KEY, calculationUtils);
        scriptEngine.put(UTILS_SCRIPT_KEY_SHORT, calculationUtils);
        scriptEngine.put(UTILS_SCRIPT_KEY_BRANDED, calculationUtils);
        scriptEngine.put(CURRENT_FIELD_KEY, currentField);
        scriptEngine.put(CURRENT_FIELD_KEY_SHORT, currentField);
        try {
            Object value;
            scriptEngine.eval(calculationScript);
            if (calculationScript.contains("function " + function + "()")) {
                Invocable invocable = (Invocable) scriptEngine;
                value = invocable.invokeFunction(function);
            } else {
                throw new CalculationException("Script does not contain a function named '" + function + "'.",
                    calculationScript);
            }
            if (logWriter.toString().length() > 0) {
                logger.debug(logWriter.toString());
            }
            return value;
        } catch (Exception ex) {
            if (logWriter.toString().length() > 0) {
                logger.debug(logWriter.toString());
            }
            logger.debug("JS script in error: {}", calculationScript);
            logger.debug(ex.getMessage(), ex);
            throw new CalculationException(ex, calculationScript);
        }
    }
}
