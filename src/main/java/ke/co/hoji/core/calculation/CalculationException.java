package ke.co.hoji.core.calculation;

/**
 * An exception that may happen when when evaluating a calculation in a {@link CalculationEvaluator}.
 */
public class CalculationException extends RuntimeException {

    /**
     * The offending calculation script.
     */
    private final String calculationScript;

    public CalculationException(Throwable cause, String calculationScript) {
        this(cause.getMessage(), cause, calculationScript);
    }

    public CalculationException(String message, String calculationScript) {
        super(message);
        this.calculationScript = calculationScript;
    }

    public CalculationException(String message, Throwable cause, String calculationScript) {
        super(message, cause);
        this.calculationScript = calculationScript;
    }

    public String getCalculationScript() {
        return calculationScript;
    }
}
