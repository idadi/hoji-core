package ke.co.hoji.core;

/**
 * Objects that implement this interface should obtain a reference to the {@link HojiContext}, normally in the
 * constructor. Implement this interface if your object(s) need access to the running hoji application context.
 * <p/>
 * In reality, not all HojiContext aware objects actually use the {@link HojiContext}, so verifying whether or not they
 * do is useful when writing unit tests - so you know whether or not a null HojiContext is acceptable.
 * <p/>
 * Created by gitahi on 04/08/15.
 */
public interface HojiContextAware {

    /**
     * Gets the {@link HojiContext} associated with this Object.
     *
     * @return the {@link HojiContext}.
     */
    HojiContext getHojiContext();
}
