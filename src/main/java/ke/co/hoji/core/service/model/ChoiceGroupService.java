package ke.co.hoji.core.service.model;

import ke.co.hoji.core.data.ChoiceGroup;

import java.util.List;

public interface ChoiceGroupService {

    List<ChoiceGroup> getChoiceGroupsByTenant(Integer tenantUserId);

    ChoiceGroup getChoiceGroupById(Integer id);

    void deleteChoiceGroup(Integer id);

    ChoiceGroup saveChoiceGroup(ChoiceGroup choiceGroup);
}
