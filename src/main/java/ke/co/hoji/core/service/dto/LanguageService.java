package ke.co.hoji.core.service.dto;

import ke.co.hoji.core.data.dto.Language;

import java.util.List;

public interface LanguageService {

    void saveLanguages(List<Language> languages);
}
