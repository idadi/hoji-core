package ke.co.hoji.core.service.model;

import ke.co.hoji.core.data.model.Language;

import java.util.List;

public interface LanguageService {

    List<Language> findAllLanguages();
}
