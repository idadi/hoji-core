package ke.co.hoji.core.service.model;

import ke.co.hoji.core.data.model.Property;

import java.util.Collection;
import java.util.List;

public interface PropertyService {

    Property getProperty(String propertyKey, Integer surveyId);

    List<Property> getProperties(Integer surveyId);

    void setProperty(Property property);

    void setProperties(Property... propertyArray);

    void setProperties(Collection<Property> props);

    boolean deleteProperty(String propertyKey, Integer surveyId);

    String getValue(String propertyKey, Integer surveyId);

    String getValue(String propertyKey);

    boolean hasValue(String propertyKey, Integer surveyId);

    boolean hasValue(String propertyKey);

    void clearCache();
}
