package ke.co.hoji.core.service.dto;

import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.dto.LiteRecord;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.http.UploadResult;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface RecordService {

    List<MainRecord> findMainRecords(RecordSearch recordSearch, int limit) throws IOException;

    void saveMainRecords(List<MainRecord> mainRecords);

    void flagNonNewLiteRecords(List<LiteRecord> liteRecords);

    int updateStatuses(Map<String, UploadResult> results);

    void undoUpdateStatuses();
}
