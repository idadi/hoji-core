package ke.co.hoji.core.service.dto;

import ke.co.hoji.core.data.dto.Translation;

import java.util.List;

public interface TranslationService {

    void saveTranslations(List<Translation> translations);
}
