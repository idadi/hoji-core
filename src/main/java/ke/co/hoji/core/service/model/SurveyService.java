package ke.co.hoji.core.service.model;

import ke.co.hoji.core.data.model.Survey;

import java.util.List;

public interface SurveyService {

    Survey getSurveyById(Integer surveyId);

    List<Survey> getAllSurveys();

    List<Survey> getSurveysByTenantUserId(Integer tenantUserId, int publicSurveyAccess);

    Survey saveSurvey(Survey survey);

    void deleteSurvey(Integer surveyId);

    void enableSurvey(Survey survey, boolean enabled);

    boolean surveyExists(String surveyCode);

    void setPublishedStatus(Survey survey, Integer status);
}
