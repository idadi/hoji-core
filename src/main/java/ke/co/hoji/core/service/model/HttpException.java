package ke.co.hoji.core.service.model;

/**
 * Any exception arising from a HTTP interaction.
 * <p/>
 * Created by gitahi on 24/08/15.
 */
public class HttpException extends Exception {

    /**
     * Codes signaling various problems.
     */
    public static class Code {

        public static final int NO_CONNECTION = -1;
        public static final int ERROR = -2;
        public static final int BAD_REQUEST = 400;
        public static final int UNAUTHORIZED = 401;
        public static final int FORBIDDEN = 403;
        public static final int NOT_FOUND = 404;
        public static final int METHOD_NOT_ALLOWED = 405;
        public static final int ACCOUNT_SUSPENDED = 451;
        public static final int INTERNAL_SERVER_ERROR = 500;
        public static final int SERVICE_UNAVAILABLE = 503;
        public static final int GATEWAY_TIMEOUT = 504;
    }

    private final int code;

    public HttpException(Throwable cause, int code) {
        super(cause);
        this.code = code;
    }

    public HttpException(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
