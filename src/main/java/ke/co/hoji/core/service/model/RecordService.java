package ke.co.hoji.core.service.model;

import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.MainRecord;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.Response;

import java.util.List;
import java.util.Set;

/**
 * Created by geoffreywasilwa on 31/05/2017.
 */
public interface RecordService {

    /**
     * Check if the {@link Response} for a particular {@link Field} is unique across all the {@link Record}s in the
     * database.
     *
     * @param record      the {@link Record} checking whether a compound key is unique.
     * @param mainRecord  the current main {@link Record}. Same as record for non-matrix records.
     * @param field       the Field whose response is supposed to be unique.
     * @param uniqueValue the response value of the Field that is allegedly to be unique.
     * @param testMode    the current test mode setting.
     * @return true if the alleged uniqueValue is unique and false otherwise.
     */
    boolean isSimpleKeyUnique(Record record, Record mainRecord, Field field, Object uniqueValue, boolean testMode);

    /**
     * Check if the combined {@link Response}s for a collection of {@link Field}s (compound key) is unique across all
     * the {@link Record}s in the database.
     *
     * @param record      the {@link Record} checking whether a compound key is unique.
     * @param mainRecord  the current main {@link Record}. Same as record for non-matrix records.
     * @param uniqueValue the compound key that is to be checked for uniqueness.
     * @param testMode    the current test mode setting.
     * @return true if the compound key is unique and false otherwise.
     */
    boolean isCompoundKeyUnique(Record record, Record mainRecord, String uniqueValue, boolean testMode);

    List<Record> findRecords(LiveField liveField, int testMode);

    /**
     * Retrieves a single main {@link Record} from the database if one exists with the given uuid.
     *
     * @param uuid the uuid by which to find the {@link Record}
     * @return the {@link Record} retrieved, if any, and null otherwise.
     */
    Record findRecordByUuid(String uuid, Field field);

    List<Record> search(RecordSearch recordSearch, int limit);

    void save(Record record);

    void updateStage(MainRecord mainRecord, boolean updateDateCompleted);

    int count(RecordSearch recordSearch);

    void setLiveFields(Record record);

    int clear(List<LiveField> liveFields, Record record);

    void delete(Record record);

    String getImageData(String uuid);

    /**
     * Gives the system an opportunity to port any existing data from one format to another. This
     * may be necessary if the underlying data storage mechanism has changed, and legacy data needs
     * to be ported accordingly.
     * <p>
     * This method came into existence to support porting choice codes to choice ids (Issue #938).
     * In future this method may be used to perform other similar tasks.
     */
    void portDataIfNecessary();

    Set<Integer> getUsedChoiceIds(Field field, Record record, Record mainRecord);
}
