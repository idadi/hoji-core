package ke.co.hoji.core.service.model;

import ke.co.hoji.core.data.ConfigObjectCache;
import ke.co.hoji.core.data.NavigableList;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Loads and wires together configuration data objects from the database.
 *
 * @author gitahi
 */
public class ConfigService {

    private final SurveyService surveyService;
    private final FormService formService;
    private final FieldService fieldService;

    private final Map<Integer, Survey> surveyCache = new HashMap<>();
    private final Map<Integer, Form> formCache = new HashMap<>();
    private final Map<Integer, Field> fieldCache = new HashMap<>();

    private final ConfigObjectCache<Field> fieldConfigObjectCache = new ConfigObjectCache();

    /**
     * Initializes an instance of ConfigService.
     *
     * @param surveyService the {@link SurveyService} to use for loading {@link Survey}s.
     * @param formService   the {@link FormService} to use for loading {@link Form}s.
     * @param fieldService  the {@link FieldService} to use for loading {@link Field}s.
     */
    public ConfigService(SurveyService surveyService, FormService formService,
                         FieldService fieldService) {
        this.surveyService = surveyService;
        this.formService = formService;
        this.fieldService = fieldService;
    }

    /**
     * Calls {@link ConfigService#getSurveys() } and returns the first item in the list if available and null if not.
     *
     * @return the first survey configured in the database with all constituent objects properly wired up.
     */
    public Survey getSurvey() {
        List<Survey> surveys = getSurveys();
        if (!surveys.isEmpty()) {
            Survey survey = new NavigableList<>(surveys).first();
            List<Form> formsToRemove = new ArrayList<>();
            for (Form form : survey.getForms()) {
                if (!form.isEnabled()) {
                    formsToRemove.add(form);
                } else {
                    List<Field> fieldsToRemove = new ArrayList<>();
                    for (Field field : form.getFields()) {
                        if (!field.isEnabled()
                            || field.getCalculated().equals(Field.Calculated.SERVER)) {
                            fieldsToRemove.add(field);
                        }
                    }
                    form.getFields().removeAll(fieldsToRemove);
                }
            }
            survey.getForms().removeAll(formsToRemove);
            return survey;
        }
        return null;
    }

    /**
     * Gets the {@link Survey} with the given Id, either by reading it freshly from the database or by retrieving a
     * previously read value from the cache.
     *
     * @param surveyId           the Survey Id
     * @param cachedIsAcceptable whether or not a cached value, if available, is acceptable.
     * @return the {@link Survey} with the given id as configured in the database with all constituent data objects
     * properly wired up.
     */
    public Survey getSurvey(Integer surveyId, boolean cachedIsAcceptable) {
        Survey survey = null;
        if (cachedIsAcceptable) {
            survey = surveyCache.get(surveyId);
        }
        if (survey == null) {
            survey = surveyService.getSurveyById(surveyId);
            if (survey != null) {
                setUpSurvey(survey);
                surveyCache.put(survey.getId(), survey);
            }
        }
        return survey;
    }

    /**
     * Get all {@link Survey}s with all constituent data objects properly wired up.
     *
     * @return the surveys.
     */
    public List<Survey> getSurveys() {
        List<Survey> surveys = surveyService.getAllSurveys();
        for (Survey survey : surveys) {
            setUpSurvey(survey);
        }
        return surveys;
    }

    /**
     * Get the {@link Form} with the given id with all constituent data objects properly wired up.
     *
     * @param formId the id of the Form.
     * @return the {@link Form}.
     */
    public Form getForm(Integer formId) {
        Form form = formCache.get(formId);
        if (form != null) {
            return form;
        }
        form = formService.getFormById(formId);
        if (form != null) {
            setUpFields(form);
        }
        formCache.put(form.getId(), form);
        return form;
    }

    /**
     * Get the {@link Field} with the given id with all constituent data objects properly wired up.
     *
     * @param fieldId the id of the Field.
     * @return the {@link Field}.
     */
    public Field getField(Integer fieldId) {
        Field field = fieldCache.get(fieldId);
        if (field != null) {
            return field;
        }
        field = fieldService.getFieldById(fieldId);
        if (field != null) {
            Form form = getForm(field.getForm().getId());
            setUpField(field, form);
        }
        fieldCache.put(field.getId(), field);
        return field;
    }

    /**
     * Deletes a Survey.
     *
     * @param survey the Survey to delete.
     */
    public void deleteSurvey(Survey survey) {
        surveyService.deleteSurvey(survey.getId());
    }

    private void setUpSurvey(Survey survey) {
        survey.setForms(formService.getFormsBySurvey(survey));
        for (Form form : survey.getForms()) {
            form.setSurvey(survey);
            setUpFields(form);
            formCache.put(form.getId(), form);
        }
    }

    private void setUpFields(Form form) {
        form.setFields(fieldService.getFields(form));
        fieldConfigObjectCache.putAll(form.getFields());
        for (Field field : form.getFields()) {
            setUpField(field, form);
            fieldCache.put(field.getId(), field);
        }
        captionWithFirst(form);
    }

    private void setUpField(Field field, Form form) {
        field.setForm(form);
        if (field.getMatrixRecordField() != null) {
            setUpField(field.getMatrixRecordField(), field.getMatrixRecordField().getForm());
        }
        if (field.getRecordForms() != null) {
            for (Form recordForm : field.getRecordForms()) {
                setUpFields(recordForm);
            }
        }
        if (field.isCaptioning()) {
            form.addCaptioningField(field);
            form.addSearchableField(field);
        }
        if (field.isSearchable()) {
            form.addSearchableField(field);
        }
        if (field.getUniqueness() == Field.Uniqueness.COMPOUND.getValue()) {
            form.addCompoundUniqueField(field);
        }
        field.setForwardSkips(fieldService.getForwardSkips(field, fieldConfigObjectCache));
        field.setBackwardSkips(fieldService.getBackwardSkips(field, fieldConfigObjectCache));
        field.setFilterRules(fieldService.getFilterRules(field, fieldConfigObjectCache));
        if (field.isMatrix()) {
            field.setChildren(fieldService.getChildren(field));
            fieldConfigObjectCache.putAll(field.getChildren());
            for (Field child : field.getChildren()) {
                child.setParent(field);
                child.setForm(field.getForm());
                if (child.isCaptioning()) {
                    field.addCaptioningField(child);
                    form.addSearchableField(field);
                }
                if (child.isSearchable()) {
                    field.addSearchableField(child);
                }
                if (child.getUniqueness() == Field.Uniqueness.COMPOUND.getValue()) {
                    field.addCompoundUniqueField(child);
                }
                child.setForwardSkips(fieldService.getForwardSkips(child, fieldConfigObjectCache));
                child.setBackwardSkips(fieldService.getBackwardSkips(child, fieldConfigObjectCache));
                child.setFilterRules(fieldService.getFilterRules(child, fieldConfigObjectCache));
            }
            captionWithFirst(field);
        }
    }

    /*
     * Set first field (that is not a label, image or matrix) as the captioning field if no other field is set as such.
     */
    private void captionWithFirst(FieldParent parent) {
        if (parent.getCaptioningFields() != null && parent.getCaptioningFields().isEmpty()) {
            if (parent.getChildren() != null && !parent.getChildren().isEmpty()) {
                for (Field field : parent.getChildren()) {
                    if (!field.getType().isLabel() && !field.getType().isImage() && !field.getType().isParent()) {
                        parent.addCaptioningField(field);
                        field.setCaptioning(true);
                        return;
                    }
                }
            }
        }
    }
}
