package ke.co.hoji.core.service.dto;

import ke.co.hoji.core.data.dto.Field;

import java.util.List;

public interface FieldService {

    List<Field> saveFields(List<Field> fields);
}
