package ke.co.hoji.core.service.dto;

import ke.co.hoji.core.data.dto.Reference;

import java.util.List;

public interface ReferenceService {

    void saveReferences(List<Reference> references);
}
