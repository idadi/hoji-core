package ke.co.hoji.core.service.model;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;

import java.util.List;

public interface FormService {

    Form getFormById(Integer formId);

    List<Form> getFormsBySurvey(Survey survey);

    List<Form> getFormsBySurvey(Survey survey, boolean enabled);

    void saveForm(Form form);

    List<Form> modifyForms(List<Form> forms);

    void deleteForm(Integer formId);

    void autoNumber(List<Field> fields);

    void enableForm(Form form, boolean enabled);

    void updateOrdinal(List<Form> forms);

    int computeCredits(Form form);
}
