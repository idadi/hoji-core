package ke.co.hoji.core.service.model;

import ke.co.hoji.core.data.ConfigObjectCache;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.OperatorDescriptor;
import ke.co.hoji.core.data.model.Rule;

import java.util.List;

public interface FieldService {

    List<Field> getFields(Form form);

    List<Field> getAllFields(Form form);

    List<Field> getParentFields(Form form);

    List<Field> getReferenceFields(Form form);

    List<Rule> getForwardSkips(Field source, ConfigObjectCache<Field> fieldCache);

    List<Rule> getBackwardSkips(Field source, ConfigObjectCache<Field> fieldCache);

    List<Rule> getFilterRules(Field source, ConfigObjectCache<Field> fieldCache);

    List<Field> getChildren(Field parent);

    Field getFieldById(Integer id);

    void deleteField(Integer id);

    Field createField(Field field);

    Field modifyField(Field field);

    List<Field> modifyFields(List<Field> fields);

    void enableField(Field field, boolean enabled);

    List<FieldType> getAllFieldTypes();

    List<OperatorDescriptor> getAllOperatorDescriptors();

    FieldType getFieldTypeById(Integer fieldTypeId);

    OperatorDescriptor getOperatorDescriptorById(Integer operatorDescriptorId);

    void deleteRule(Integer id);

    void deleteRule(Rule rule);

    Rule createRule(Rule rule);

    Rule modifyRule(Rule rule);

    List<Rule> getRules(Field owner, Integer type);

    Rule getRuleById(Field owner, Integer ruleId);

    void setQuestionNumber(Field field, String questionNo);

    void updateOrdinal(List<Field> fields);

    void updateRuleOrdinal(List<Rule> rules);

    Field getFieldFromDto(ke.co.hoji.core.data.dto.Field fieldDto);
}
