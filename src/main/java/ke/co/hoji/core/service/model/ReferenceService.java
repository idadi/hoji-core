package ke.co.hoji.core.service.model;

import ke.co.hoji.core.data.ReferenceLevel;
import ke.co.hoji.core.data.model.Reference;

import java.util.List;

public interface ReferenceService {

    Reference search(Reference reference);

    List<ReferenceLevel> getReferenceLevels();
}
