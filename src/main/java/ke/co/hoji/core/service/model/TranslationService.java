package ke.co.hoji.core.service.model;

import ke.co.hoji.core.data.Translatable;

public interface TranslationService {

    String translate(Translatable translatable, Integer languageId, String component, String untranslated);

    void clearCache();
}
