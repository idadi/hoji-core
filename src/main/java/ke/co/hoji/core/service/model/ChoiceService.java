package ke.co.hoji.core.service.model;

import ke.co.hoji.core.data.Choice;

import java.util.List;

public interface ChoiceService {

    List<Choice> getChoicesByTenant(Integer tenantUserId);

    Choice getChoiceById(Integer id);

    void deleteChoice(Integer id);

    Choice saveChoice(Choice choice);

    Object getNewChoiceValue(Integer fieldId, Object value);
}
