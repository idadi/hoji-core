package ke.co.hoji.core.service.dto;

import ke.co.hoji.core.data.dto.Form;

import java.util.List;

public interface FormService {

    List<Form> saveForms(List<Form> forms);

    List<Form> getAllForms();

    Form getFormById(Integer formId);
}
