package ke.co.hoji.core.service.dto;

import ke.co.hoji.core.data.dto.Survey;

public interface SurveyService {

    void saveSurvey(Survey survey);

    Survey getSurveyByCode(String surveyCode);

    void deleteSurvey(Integer id);
}
