package ke.co.hoji.core.data;

import java.util.List;

/**
 * A wrapper for the List class. Provides convenient methods for getting elements from strategic positions in the list.
 *
 * @param <T>
 *
 * @author gitahi
 */
public class NavigableList<T extends Object> {

    private final List<T> list;

    /**
     * Construct a NavigableList.
     *
     * @param list the List to wrap
     */
    public NavigableList(List<T> list) {
        if (list == null) {
            throw new RuntimeException("NavigableList cannot be initialized with "
                    + "null list.");
        }
        this.list = list;
    }

    public List<T> getList() {
        return list;
    }

    /**
     * @return the first element in the list and null if the list is empty
     */
    public T first() {
        if (!list.isEmpty()) {
            return list.get(indexOfFirst());
        }
        return null;
    }

    /**
     * @param from the element from which to get the previous element
     *
     * @return the previous element in the list and null if from is the first element in the list
     */
    public T previous(T from) {
        if (!list.isEmpty()) {
            int indexOfFrom = list.indexOf(from);
            if (indexOfFrom != -1) {
                int indexOfPrevious = indexOfFrom - 1;
                if (indexOfPrevious >= indexOfFirst()) {
                    return list.get(indexOfPrevious);
                }
            }
        }
        return null;
    }

    /**
     * @param from the element from which to get the next element
     *
     * @return the next element in the list and null if from is the last element in the list
     */
    public T next(T from) {
        if (!list.isEmpty()) {
            int indexOfFrom = list.indexOf(from);
            if (indexOfFrom != -1) {
                int indexOfNext = indexOfFrom + 1;
                if (indexOfNext <= indexOfLast()) {
                    return list.get(indexOfNext);
                }
            }
        }
        return null;
    }

    /**
     * @return the last element in the list and null if the list is empty
     */
    public T last() {
        if (!list.isEmpty()) {
            return list.get(indexOfLast());
        }
        return null;
    }

    public int indexOfFirst() {
        return 0;
    }

    public int indexOfLast() {
        return list.size() - 1;
    }

    public int size() {
        return list.size();
    }

    public T get(int index) {
        return list.get(index);
    }
}
