package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.dto.Survey;

/**
 * Created by gitahi on 27/08/15.
 */
public class SurveyResponse {

    /**
     * An integer code saying the nature of the response.
     */
    public static class Code {

        /**
         * No Survey was found with the code supplied.
         */
        public static final int SURVEY_NOT_FOUND = 0;

        /**
         * A Survey was found but it is not enabled.
         */
        public static final int SURVEY_NOT_ENABLED = 1;

        /**
         * A Survey was found but it is not published.
         */
        public static final int SURVEY_NOT_PUBLISHED = 2;

        /**
         * The Survey was found.
         */
        public static final int SURVEY_FOUND = 3;

        /**
         * The request is from an older version
         */
        public static final int APP_VERSION_TOO_OLD = 4;
    }

    /**
     * The {@link SurveyResponse.Code} of this response.
     */
    private int code;

    /**
     * The {@link Survey} with which this response is associated.
     */
    private Survey survey;

    /**
     * The id of the ownerName of this {@link Survey} i.e of the tenant who owns it.
     */
    private Integer ownerId;

    /**
     * The name of the ownerName of this {@link Survey} i.e of the tenant who owns it.
     */
    private String ownerName;

    /**
     * Whether or not Hoji has explicitly verified the identity of the ownerName of this {@link Survey}.
     */
    private boolean ownerVerified;

    public SurveyResponse() {
    }

    public SurveyResponse(int code, Survey survey, Integer ownerId, String ownerName, boolean ownerVerified) {
        this.code = code;
        this.survey = survey;
        this.ownerId = ownerId;
        this.ownerName = ownerName;
        this.ownerVerified = ownerVerified;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public boolean isOwnerVerified() {
        return ownerVerified;
    }

    public void setOwnerVerified(boolean ownerVerified) {
        this.ownerVerified = ownerVerified;
    }
}
