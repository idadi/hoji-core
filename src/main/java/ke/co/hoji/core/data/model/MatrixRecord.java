package ke.co.hoji.core.data.model;

/**
 * @author gitahi
 */
public class MatrixRecord extends RecordDetail {

    private final Record mainRecord;

    public MatrixRecord(Record record, Record mainRecord) {
        super(record);
        this.mainRecord = mainRecord;
    }

    public Record getMainRecord() {
        return mainRecord;
    }

    @Override
    public String toString() {
        return getRecord().toString();
    }
}
