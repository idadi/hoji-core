package ke.co.hoji.core.data;

import ke.co.hoji.core.data.model.Language;

import java.util.List;

/**
 * The interface for all classes that can be translated to different @{@link Language}s.
 */
public interface Translatable {

    /**
     * @return the unique id of this Translatable.
     */
    Integer getId();

    /**
     * @return the name used by this Translatable for the purposes of translation. Should be unique per Translatable.
     */
    String getTranslatableName();

    /**
     * @return the list of all components in this Translatable that can be Translated.
     */
    List<String> getTranslatableComponents();
}
