package ke.co.hoji.core.data.dto;

import ke.co.hoji.core.data.model.EventSubject;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author gitahi
 */
public class MainRecord extends Record implements EventSubject {

    private String id;
    private Integer formId;
    private Integer stage;
    private Integer referenceId;
    private String baseRecordUuid;
    private Long dateCompleted;
    private Long dateUploaded;
    private Integer version;
    private Integer userId;
    private String deviceId;
    private Double startLatitude;
    private Double startLongitude;
    private Double startAccuracy;
    private Long startAge;
    private String startAddress;
    private Double endLatitude;
    private Double endLongitude;
    private Double endAccuracy;
    private Long endAge;
    private String endAddress;
    private String formVersion;
    private String appVersion;
    private List<MatrixRecord> matrixRecords;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    public Integer getStage() {
        return stage;
    }

    public void setStage(Integer stage) {
        this.stage = stage;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public String getBaseRecordUuid() {
        return baseRecordUuid;
    }

    public void setBaseRecordUuid(String baseRecordUuid) {
        this.baseRecordUuid = baseRecordUuid;
    }

    public Long getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(Long dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public Long getDateUploaded() {
        return dateUploaded;
    }

    public void setDateUploaded(Long dateUploaded) {
        this.dateUploaded = dateUploaded;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Double getStartLatitude() {
        return startLatitude;
    }

    public void setStartLatitude(Double startLatitude) {
        this.startLatitude = startLatitude;
    }

    public Double getStartLongitude() {
        return startLongitude;
    }

    public void setStartLongitude(Double startLongitude) {
        this.startLongitude = startLongitude;
    }

    public Double getStartAccuracy() {
        return startAccuracy;
    }

    public void setStartAccuracy(Double startAccuracy) {
        this.startAccuracy = startAccuracy;
    }

    public Long getStartAge() {
        return startAge;
    }

    public void setStartAge(Long startAge) {
        this.startAge = startAge;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public Double getEndLatitude() {
        return endLatitude;
    }

    public void setEndLatitude(Double endLatitude) {
        this.endLatitude = endLatitude;
    }

    public Double getEndLongitude() {
        return endLongitude;
    }

    public void setEndLongitude(Double endLongitude) {
        this.endLongitude = endLongitude;
    }

    public Double getEndAccuracy() {
        return endAccuracy;
    }

    public void setEndAccuracy(Double endAccuracy) {
        this.endAccuracy = endAccuracy;
    }

    public Long getEndAge() {
        return endAge;
    }

    public void setEndAge(Long endAge) {
        this.endAge = endAge;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public String getFormVersion() {
        return formVersion;
    }

    public void setFormVersion(String formVersion) {
        this.formVersion = formVersion;
    }

    public String getAppVersion() {
        if (appVersion == null) {
            return "Unknown";
        }
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public List<MatrixRecord> getMatrixRecords() {
        return matrixRecords;
    }

    public void setMatrixRecords(List<MatrixRecord> matrixRecords) {
        this.matrixRecords = matrixRecords;
    }

    @Override
    public LinkedHashMap<String, Object> descriptiveProperties() {
        LinkedHashMap descriptiveProperties = new LinkedHashMap();
        descriptiveProperties.put("UUID", getUuid());
        if (StringUtils.isNotBlank(caption)) {
            descriptiveProperties.put("Caption", caption);
        }
        return descriptiveProperties;
    }
}
