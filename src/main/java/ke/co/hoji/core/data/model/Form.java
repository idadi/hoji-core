package ke.co.hoji.core.data.model;

import ke.co.hoji.core.data.Enablable;
import ke.co.hoji.core.data.Nameable;
import ke.co.hoji.core.data.Orderable;
import ke.co.hoji.core.data.Translatable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * A form for a {@link Survey}. This basically corresponds to a questionnaire. One survey typically has one or more
 * forms.
 *
 * @author gitahi
 */
public class Form extends Nameable implements Orderable<Form>, FieldParent, Translatable, EventSubject, Enablable {

    /**
     * True if this Form is enabled and false otherwise.
     */
    protected boolean enabled;

    /**
     * The order position of this Form relative to other Forms.
     */
    private BigDecimal ordinal;

    /**
     * The minor version number of this @{@link Form}.
     */
    protected Integer minorVersion;
    /**
     * The major version number of this @{@link Form}.
     */
    protected Integer majorVersion;

    /**
     * The rule for GPS data capture. See {@link GpsCapture}.
     */
    private int gpsCapture;

    public int getGpsCapture() {
        return gpsCapture;
    }

    public void setGpsCapture(int gpsCapture) {
        this.gpsCapture = gpsCapture;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public BigDecimal getOrdinal() {
        return ordinal;
    }

    @Override
    public void setOrdinal(BigDecimal ordinal) {
        this.ordinal = ordinal;
    }

    public Integer getMinorVersion() {
        return minorVersion;
    }

    public void setMinorVersion(Integer minorVersion) {
        this.minorVersion = minorVersion;
    }

    public Integer getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(Integer majorVersion) {
        this.majorVersion = majorVersion;
    }

    /**
     * @return the full version of this @{@link Form} i.e. the combination of bothe the {@link #majorVersion}
     * and the {@link #minorVersion}.
     */
    public String getFullVersion() {
        return majorVersion + "." + minorVersion;
    }

    /**
     * The components of this Form that are to be translated.
     */
    public final static class TranslatableComponent {

        public final static String NAME = "NAME";
        public final static String DESCRIPTION = "DESCRIPTION";
    }

    /**
     * The {@link Survey} to which this Form belongs.
     */
    private Survey survey;

    /**
     * The {@link Field}s under this Form.
     */
    private List<Field> fields;

    /**
     * The list of {@link Field#captioning} Fields.
     */
    private final List<Field> captioningFields = new ArrayList<>();

    /**
     * The list of {@link Field#searchable} Fields.
     */
    private final List<Field> searchableFields = new ArrayList<>();

    /**
     * The list of compound unique Fields.
     */
    private final List<Field> compoundUniqueFields = new ArrayList<>();

    /**
     * True if this @{@link Form} has been modified in a minor way and false otherwise. Minor modification is
     * everything apart from adding, deleting or disabling a field.
     */
    protected boolean minorModified;

    /**
     * True if this @{@link Form} has been modified in a major way and false otherwise. Major modification is
     * adding, deleting or disabling a field.
     */
    protected boolean majorModified;

    /**
     * The number of credits it costs to submit this form.
     */
    private transient int credits;

    /**
     * A list of outputs to which score/scale values will be displayed instead of default values such as
     * choice name or code
     */
    protected List<String> outputStrategy = new ArrayList<>();

    /**
     * True if the pivot table output for this @{@link Form} should be transposed. Transposition is useful when
     * individual variables in the raw dataset need to be converted into repeating categories in the rows. In order
     * for this to work, the transposable columns must have as their value labels a pipe separated list of categories
     * e.g. Number Tested for HIV|Male|15 - 19.
     */
    protected boolean transposable;

    /**
     * A comma separated list of category names to be used when columns are transposed. E.g. if the value label for a column is
     * Number Tested for HIV|Male|15 - 19, then Number Tested for HIV is the "Indicator", Male is the "Sex" and 15 - 19
     * is the "Age Group", so this variable would contain "Indicator, Sex, Age Group" in the list.
     */
    protected String transpositionCategories;

    /**
     * The label to use for the value column resulting from transposition.
     */
    protected String transpositionValueLabel;

    /**
     * A collection of {@link Form forms} that depend on this form.
     */
    private List<Form> dependentForms = new ArrayList<>();

    public Form() {
        super();
    }

    public Form(Integer id) {
        super(id);
    }

    public Form(Integer id, String name, boolean enabled, BigDecimal ordinal, Integer minorVersion, Integer majorVersion) {
        super(id, name);
        this.enabled = enabled;
        this.ordinal = ordinal;
        this.minorVersion = minorVersion;
        this.majorVersion = majorVersion;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    @Override
    public List<Field> getCaptioningFields() {
        return captioningFields;
    }

    @Override
    public List<Field> getSearchableFields() {
        return searchableFields;
    }

    @Override
    public List<Field> getCompoundUniqueFields() {
        return compoundUniqueFields;
    }

    public boolean isMinorModified() {
        return minorModified;
    }

    public void setMinorModified(boolean minorModified) {
        this.minorModified = minorModified;
    }

    public boolean isMajorModified() {
        return majorModified;
    }

    public void setMajorModified(boolean majorModified) {
        this.majorModified = majorModified;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public List<String> getOutputStrategy() {
        if (outputStrategy.size() == 0) {
            outputStrategy.add(OutputStrategy.PIVOT);
            outputStrategy.add(OutputStrategy.DOWNLOAD);
        }
        return outputStrategy;
    }

    public void setOutputStrategy(List<String> outputStrategy) {
        this.outputStrategy = outputStrategy;
    }

    public boolean isTransposable() {
        return transposable;
    }

    public void setTransposable(boolean transposable) {
        this.transposable = transposable;
    }

    public String getTranspositionCategories() {
        return transpositionCategories;
    }

    public void setTranspositionCategories(String transpositionCategories) {
        this.transpositionCategories = transpositionCategories;
    }

    public String getTranspositionValueLabel() {
        return transpositionValueLabel;
    }

    public void setTranspositionValueLabel(String transpositionValueLabel) {
        this.transpositionValueLabel = transpositionValueLabel;
    }

    public void addDependentForm(Form dependant) {
        this.dependentForms.add(dependant);
    }

    public List<Form> getDependentForms() {
        return Collections.unmodifiableList(dependentForms);
    }

    @Override
    public boolean addCaptioningField(Field captioningField) {
        if (captioningField.getType().isNull()) {
            return false;
        }
        return captioningFields.add(captioningField);
    }

    @Override
    public boolean addSearchableField(Field searchableField) {
        if (searchableField.getType().isNull()) {
            return false;
        }
        return searchableFields.add(searchableField);
    }

    @Override
    public boolean addCompoundUniqueField(Field compoundUniqueField) {
        if (compoundUniqueField.getType().isNull()) {
            return false;
        }
        return compoundUniqueFields.add(compoundUniqueField);
    }

    @Override
    public List<Field> getChildren() {
        return getFields();
    }

    @Override
    public LinkedHashMap<String, Object> descriptiveProperties() {
        LinkedHashMap descriptiveProperties = super.descriptiveProperties();
        descriptiveProperties.put("Version", getFullVersion());
        return descriptiveProperties;
    }

    @Override
    public String getTranslatableName() {
        return getClass().getSimpleName().toUpperCase();
    }

    @Override
    public List<String> getTranslatableComponents() {
        return new ArrayList<>(Arrays.asList(TranslatableComponent.NAME, TranslatableComponent.DESCRIPTION));
    }

    @Override
    public int compareTo(Form form) {
        return this.ordinal.compareTo(form.ordinal);
    }

    /**
     * Settings for defining rules for GPS data capture.
     */
    public final static class GpsCapture {

        /**
         * Do not capture GPS data. Does not enforce GPS accuracy, obviously.
         */
        public static final int NEVER = 0;

        /**
         * Capture GPS data if possible. Does not enforce GPS accuracy. Any accuracy is accepted.
         */
        public static final int IF_POSSIBLE = 1;

        /**
         * Always capture GPS data. Enforces GPS accuracy, if set to a non-negative number.
         */
        public static final int ALWAYS = 2;
    }

    /**
     * Output types that will have number, choice and image responses processed based on
     * {@link Field#outputType} value
     */
    public static final class OutputStrategy {

        public static final String API = "API";
        public static final String ANALYSIS = "ANALYSIS";
        public static final String PIVOT = "PIVOT";
        public static final String TABLE = "TABLE";
        public static final String DOWNLOAD = "DOWNLOAD";
        public static final String OVERVIEW = "OVERVIEW";
    }
}
