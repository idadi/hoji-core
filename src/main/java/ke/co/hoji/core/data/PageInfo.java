package ke.co.hoji.core.data;

import java.io.Serializable;

/**
 * Stores an manages information for paging through a set of pages of items.
 * <p/>
 * Created by gitahi on 22/05/15.
 */
public class PageInfo implements Serializable {

    /**
     * The maximum number of items to be loaded per page.
     */
    public final static int PAGE_SIZE = 25;

    /**
     * The current page number.
     */
    private final int maxPageSize;

    /**
     * The current page number.
     */
    private int page = 0;

    /**
     * The total number of pages.
     */
    private int pageCount = 0;

    /**
     * The total number of items.
     */
    private final int totalItemCount;

    /**
     * The total number of items available to be loaded on the current page.
     */
    private int pageItemCount = 0;

    /**
     * The number of the first item on the current page.
     */
    private int firstItemNo = 0;

    /**
     * The number of the last item on the current page.
     */
    private int lastItemNo = 0;

    public PageInfo(int totalItemCount, int maxPageSize) {
        this.totalItemCount = totalItemCount;
        this.maxPageSize = maxPageSize;
        this.pageCount = (this.totalItemCount + this.maxPageSize - 1) / this.maxPageSize;
    }

    public int getMaxPageSize() {
        return maxPageSize;
    }

    public int getPage() {
        return page;
    }

    public int getPageCount() {
        return pageCount;
    }

    public int getTotalItemCount() {
        return totalItemCount;
    }

    public int getPageItemCount() {
        return pageItemCount;
    }

    public int getFirstItemNo() {
        return firstItemNo;
    }

    public int getLastItemNo() {
        return lastItemNo;
    }

    private void setPageItemCount(int pageItemCount) {
        int expected;
        if (hasNext() || (pageItemCount == maxPageSize)) {
            expected = maxPageSize;
        } else {
            expected = totalItemCount % maxPageSize;
        }
        if (pageItemCount != expected) {
            throw new IllegalArgumentException("Expected " + expected + " page items but received " + pageItemCount);
        }
        this.pageItemCount = pageItemCount;
    }

    /**
     * @return true if the there is one or more pages after the current page.
     */
    public boolean hasNext() {
        return this.page < pageCount;
    }

    /**
     * @return true if the there is one or more pages before the current page.
     */
    public boolean hasPrevious() {
        return this.page > 1;
    }

    public void next(int pageItemCount) {
        page++;
        setPageItemCount(pageItemCount);
        updateState();
    }

    public void previous(int pageItemCount) {
        page--;
        setPageItemCount(pageItemCount);
        updateState();
    }

    private void updateState() {
        firstItemNo = (maxPageSize * (page - 1) + 1);
        int offset;
        if (pageItemCount == maxPageSize) {
            offset = maxPageSize;
        } else {
            offset = pageItemCount;
        }
        lastItemNo = firstItemNo + offset - 1;
    }
}
