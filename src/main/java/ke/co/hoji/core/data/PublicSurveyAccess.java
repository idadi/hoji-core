package ke.co.hoji.core.data;

import ke.co.hoji.core.data.model.Survey;

/**
 * Constants for controlling user access to public {@link Survey}s.
 * <p>
 * Created by gitahi on 3/18/17.
 */
public class PublicSurveyAccess {

    /**
     * Do not show any public surveys to this user and users under their account.
     */
    public static final int NONE = 0;

    /**
     * Only show to this user and users under their account public surveys owned by verified users .
     */
    public static final int VERIFIED = 1;

    /**
     * Show to this user and users under their account all public surveys.
     */
    public static final int ALL = 2;
}
