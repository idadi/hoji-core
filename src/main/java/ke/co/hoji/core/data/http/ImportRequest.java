package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.model.Record;

import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.data.model.Form;

import java.util.List;

/**
 * A (HTTP) request from the client asking the server to send the @{@link Record}s specified. The server should send
 * the full, rather than the lite form of the records. If a record has related records, the server should send those
 * too.
 * <p>
 * Created by gitahi on 11/16/16.
 */
public class ImportRequest {

    /**
     * The id of the @{@link Survey} whose record is to be imported.
     */
    private Integer surveyId;

    /**
     * The id of the @{@link Form} whose record is to be imported.
     */
    private Integer formId;

    /**
     * The {@link Form#majorVersion} of the @{@link Form} whose record is to be imported.
     */
    private Integer formVersion;

    /**
     * The UUID of the @{@link Record} being requested.
     */
    private List<String> uuids;

    public ImportRequest() {
    }

    public ImportRequest(Integer surveyId, Integer formId) {
        this.surveyId = surveyId;
        this.formId = formId;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    public Integer getFormVersion() {
        return formVersion;
    }

    public void setFormVersion(Integer formVersion) {
        this.formVersion = formVersion;
    }

    public List<String> getUuids() {
        return uuids;
    }

    public void setUuids(List<String> uuids) {
        this.uuids = uuids;
    }
}
