package ke.co.hoji.core.data.dto;

import ke.co.hoji.core.data.DataObject;

import java.util.List;

/**
 * @author gitahi
 */
public abstract class Record extends DataObject {

    /**
     * A String that a human may use to identify this record.
     */
    protected String caption;
    /**
     * A String that may be used to find this record.
     */
    protected String searchTerms;
    /**
     * A compound key based on the compound unique {@link ke.co.hoji.core.data.model.Field}s designated in this Record's
     * {@link ke.co.hoji.core.data.model.FieldParent}.
     */
    protected String uniqueValue;
    /**
     * A record is main if it is the top level record, as opposed to a nested record such as a matrix record
     */
    protected boolean main;
    /**
     * A record is a test record if it was submitted from a device in test mode
     */
    protected boolean test;
    /**
     * The date when this record was created in Unix time.
     */
    protected Long dateCreated;
    /**
     * The date when this record was last updated in Unix time.
     */
    protected Long dateUpdated;
    private List<LiveField> liveFields;

    public List<LiveField> getLiveFields() {
        return liveFields;
    }

    public void setLiveFields(List<LiveField> liveFields) {
        this.liveFields = liveFields;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getSearchTerms() {
        return searchTerms;
    }

    public void setSearchTerms(String searchTerms) {
        this.searchTerms = searchTerms;
    }

    public String getUniqueValue() {
        return uniqueValue;
    }

    public void setUniqueValue(String uniqueValue) {
        this.uniqueValue = uniqueValue;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Long dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
}
