package ke.co.hoji.core.data.http;

public class PropertiesRequest {

    private Integer surveyId;

    public PropertiesRequest() {
    }

    public PropertiesRequest(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }
}
