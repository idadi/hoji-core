package ke.co.hoji.core.data.model;

import java.io.Serializable;
import java.util.List;

/**
 * An object that supplies the fields associated with a {@link Record}. It also specifies the database objects (schemas,
 * tables and keys) associated with saving data for it's children.
 *
 * @author gitahi
 */
public interface FieldParent extends Serializable {

    /**
     * @return the name of this parent.
     */
    String getName();

    /**
     * @return the list of {@link Field}s owned by this FieldParent.
     */
    List<Field> getChildren();

    /**
     * @return the ID of the concrete FieldParent.
     */
    Integer getId();

    /**
     * Get's the list of all {@link Field#captioning} Fields of this FieldParent.
     */
    List<Field> getCaptioningFields();

    /**
     * Get's the list of all {@link Field#searchable} Fields of this FieldParent.
     */
    List<Field> getSearchableFields();

    /**
     * Get's the list of all compound unique Fields of this FieldParent.
     */
    List<Field> getCompoundUniqueFields();

    /**
     * Adds a {@link Field#captioning} Field to this FieldParent.
     */
    boolean addCaptioningField(Field captioningField);

    /**
     * Adds a {@link Field#searchable} Field to this FieldParent.
     */
    boolean addSearchableField(Field searchableField);

    /**
     * Adds a compound unique Field to this FieldParent.
     */
    boolean addCompoundUniqueField(Field compoundUniqueField);
}
