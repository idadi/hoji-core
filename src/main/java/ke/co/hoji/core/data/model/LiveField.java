package ke.co.hoji.core.data.model;

import ke.co.hoji.core.data.DataObject;
import ke.co.hoji.core.response.Response;

/**
 * A field combined with it's answer and comments.
 *
 * @author gitahi
 */
public class LiveField extends DataObject implements Comparable<LiveField> {

    private final Field field;
    private Response response;
    private Long dateCreated;
    private Long dateUpdated;

    public LiveField(Field field) {
        this.field = field;
    }

    public Field getField() {
        return field;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Long dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @Override
    public int compareTo(LiveField liveField) {
        return field.compareTo(liveField.field);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.field == null ? 0 : this.field.hashCode());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LiveField other = (LiveField) obj;
        return other.field.equals(this.field);
    }

    @Override
    public String toString() {
        return field.toString() + ": " + response;
    }

    public boolean isAnswered(boolean missingAsNull) {
        boolean ret = false;
        if (response != null) {
            ret = response.isAnswered(missingAsNull);
        }
        return ret;
    }
}
