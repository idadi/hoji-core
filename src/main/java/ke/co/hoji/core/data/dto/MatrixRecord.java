package ke.co.hoji.core.data.dto;

/**
 * @author gitahi
 */
public class MatrixRecord extends Record {

    private int fieldId;
    private String mainRecordUuid;

    public int getFieldId() {
        return fieldId;
    }

    public void setFieldId(int fieldId) {
        this.fieldId = fieldId;
    }

    public String getMainRecordUuid() {
        return mainRecordUuid;
    }

    public void setMainRecordUuid(String mainRecordUuid) {
        this.mainRecordUuid = mainRecordUuid;
    }
}
