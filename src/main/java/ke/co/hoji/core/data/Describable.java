package ke.co.hoji.core.data;

/**
 * A {@link Nameable} with a description.
 *
 * @author gitahi
 */
public abstract class Describable extends Nameable {

    /**
     * The description of this object.
     */
    protected String description;

    public Describable() {
        super();
    }

    public Describable(Integer id) {
        super(id);
    }

    public Describable(Integer id, String name) {
        super(id, name);
    }

    public Describable(Integer id, String name, String description) {
        super(id, name);
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
