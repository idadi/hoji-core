package ke.co.hoji.core.data.dto;

import ke.co.hoji.core.data.Enablable;
import ke.co.hoji.core.data.Nameable;
import ke.co.hoji.core.data.Orderable;

import java.math.BigDecimal;

/**
 * Created by gitahi on 26/08/15.
 */
public class Form extends Nameable implements Orderable<Form>, Enablable {

    private boolean enabled;

    private BigDecimal ordinal;

    private Integer minorVersion;

    private Integer majorVersion;

    private Integer surveyId;

    private int gpsCapture;

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public int getGpsCapture() {
        return gpsCapture;
    }

    public void setGpsCapture(int gpsCapture) {
        this.gpsCapture = gpsCapture;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public BigDecimal getOrdinal() {
        return ordinal;
    }

    @Override
    public void setOrdinal(BigDecimal ordinal) {
        this.ordinal = ordinal;
    }

    public Integer getMinorVersion() {
        return minorVersion;
    }

    public void setMinorVersion(Integer minorVersion) {
        this.minorVersion = minorVersion;
    }

    public Integer getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(Integer majorVersion) {
        this.majorVersion = majorVersion;
    }

    @Override
    public int compareTo(Form form) {
        return this.ordinal.compareTo(form.ordinal);
    }
}
