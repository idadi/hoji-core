package ke.co.hoji.core.data.model;

/**
 * @author gitahi
 */
public class ValidationResult {

    public static final int OKAY = 0;
    public static final int WARN = 1;
    public static final int STOP = 2;
    public static final int ERROR = 3;

    private final int signal;
    private String message;

    public ValidationResult(int signal) {
        this.signal = signal;
    }

    public ValidationResult(int signal, String message) {
        this(signal);
        this.message = message;
    }

    public int getSignal() {
        return signal;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static ValidationResult okay() {
        ValidationResult okay = new ValidationResult(OKAY);
        return okay;
    }

    public static ValidationResult warn(String message) {
        ValidationResult warn = new ValidationResult(WARN,
            message);
        return warn;
    }

    public static ValidationResult stop(String message) {
        ValidationResult stop = new ValidationResult(STOP,
            message);
        return stop;
    }

    public static ValidationResult error(String message) {
        ValidationResult error = new ValidationResult(ERROR, message);
        return error;
    }
}
