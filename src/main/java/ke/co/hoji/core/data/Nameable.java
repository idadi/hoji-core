package ke.co.hoji.core.data;

/**
 * A {@link ConfigObject} with a name.
 *
 * @author gitahi
 */
public abstract class Nameable extends ConfigObject {

    protected static final int SHORT_NAME_LENGTH = 25;

    /**
     * The name of this object.
     */
    protected String name;

    public Nameable() {
    }

    public Nameable(Integer id) {
        super(id);
    }

    public Nameable(Integer id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        if (name != null && name.length() > SHORT_NAME_LENGTH) {
            return name.substring(0, SHORT_NAME_LENGTH) + " ...";
        }
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
