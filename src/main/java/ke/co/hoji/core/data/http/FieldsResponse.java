package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.dto.Field;

import java.util.List;

/**
 * Created by gitahi on 27/08/15.
 */
public class FieldsResponse {

    private List<Field> fields;

    public FieldsResponse() {
    }
    public FieldsResponse(List<Field> fields) {
        this.fields = fields;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }
}
