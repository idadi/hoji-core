package ke.co.hoji.core.data.model;

import ke.co.hoji.core.data.ConfigObject;
import ke.co.hoji.core.data.Orderable;

import java.math.BigDecimal;
import java.util.LinkedHashMap;

/**
 * An object describing the rule on an {@link Field}.
 *
 * @author gitahi
 */
public class Rule extends ConfigObject implements Orderable<Rule>, EventSubject {

    /**
     * The order position of this Rule relative to other Rules.
     */
    protected BigDecimal ordinal;
    /**
     * The value against which evaluate this Rule.
     */
    protected String value;
    /**
     * The type of this Rule. It may either be FORWARD, BACKWARD OR CRITERIA. Determines the semantics of processing the
     * Rule.
     */
    protected Integer type;
    /**
     * An AND or OR operatorClass that is used to combine two or more grouped Rules. See {@link Rule#grouper}.
     */
    protected Integer combiner;
    /**
     * Rules belonging to th same owner {@link Field} are evaluated in a combined format (rather than each separately)
     * if they have the same grouper. See {@link #combiner}.
     */
    protected int grouper = 0;

    public BigDecimal getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(BigDecimal ordinal) {
        this.ordinal = ordinal;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCombiner() {
        return combiner;
    }

    public void setCombiner(Integer combiner) {
        this.combiner = combiner;
    }

    public int getGrouper() {
        return grouper;
    }

    public void setGrouper(int grouper) {
        this.grouper = grouper;
    }

    /**
     * @param ruleType the type of Rule. See: {@link Type}
     * @return a String description of the given type of Rule.
     */
    protected String typeString(Integer ruleType) {
        String ruleString = "UNKNOWN";
        if (ruleType.equals(Type.FORWARD_SKIP)) {
            ruleString = "IF SHOW";
        } else if (ruleType.equals(Type.BACKWARD_SKIP)) {
            ruleString = "SHOW IF";
        } else if (ruleType.equals(Type.FILTER_RULE)) {
            ruleString = "FILTER";
        }
        return ruleString;
    }

    public boolean isForwardSkip() {
        return type == Type.FORWARD_SKIP;
    }

    public boolean isBackwardSkip() {
        return type == Type.BACKWARD_SKIP;
    }

    public boolean isFilter() {
        return type == Type.FILTER_RULE;
    }

    public static class Type {

        public static final int FORWARD_SKIP = 1;
        public static final int BACKWARD_SKIP = 2;
        public static final int FILTER_RULE = 3;
    }

    public static class Combiner {

        public static final int AND = 1;
        public static final int OR = 2;
    }

    /**
     * The {@link OperatorDescriptor} describing the {@link ke.co.hoji.core.rule.RuleOperator} to be used to evaluate
     * this Rule.
     */
    private OperatorDescriptor operatorDescriptor;

    /**
     * The {@link Field} that owns this Rule.
     */
    private Field owner;

    /**
     * The field against which this rule is evaluated.
     */
    private Field target;

    public Rule() {
    }

    public Rule(Integer id) {
        super(id);
    }

    public Rule(String value, Integer type, Field owner) {
        this.value = value;
        this.type = type;
        this.owner = owner;
    }

    public Rule(Integer id, BigDecimal ordinal, String value, Integer type, int grouper) {
        super(id);
        this.ordinal = ordinal;
        this.value = value;
        this.type = type;
        this.grouper = grouper;
    }

    public OperatorDescriptor getOperatorDescriptor() {
        return operatorDescriptor;
    }

    public void setOperatorDescriptor(OperatorDescriptor operatorDescriptor) {
        this.operatorDescriptor = operatorDescriptor;
    }

    public Field getOwner() {
        return owner;
    }

    public void setOwner(Field owner) {
        this.owner = owner;
    }

    public Field getTarget() {
        return target;
    }

    public void setTarget(Field target) {
        this.target = target;
    }

    @Override
    public int compareTo(Rule rule) {
        return this.ordinal.compareTo(rule.ordinal);
    }

    @Override
    public LinkedHashMap<String, Object> descriptiveProperties() {
        LinkedHashMap descriptiveProperties = super.descriptiveProperties();
        descriptiveProperties.put("Type", typeString(type));
        return descriptiveProperties;
    }
}
