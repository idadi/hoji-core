package ke.co.hoji.core.data;

import java.util.Date;

/**
 * A singleton object representing the current location. Client implementation e.g. Android should keep the data here
 * as up-to-date as necessary.
 *
 * @author Gitahi: Create on 13/08/15.
 */
public class GpsLocation {

    /**
     * The latitude, in degrees.
     */
    private Double latitude;

    /**
     * The longitude, in degrees.
     */
    private Double longitude;

    /**
     * The accuracy of this location in meters.
     */
    private Double accuracy;

    /**
     * The time when this location was captured by the device.
     */
    private Long time;

    /**
     * The time between when this location was captured by the device and when it was reported to Hoji.
     */
    private Long age;

    /**
     * The human-readable location address e.g. street address
     */
    protected String address;

    private GpsLocation() {
    }

    private static GpsLocation instance;

    /**
     * Creates a returns an instance of this class. If there's already an existing instance, that instance is returned
     * instead.
     *
     * @return GpsLocation an instance of GpsLocation.
     */
    public static GpsLocation getInstance() {
        if (instance == null) {
            instance = new GpsLocation();
        }
        return instance;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
        setAge();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getAge() {
        return age;
    }

    private void setAge() {
        if (getTime() != null) {
            age = (new Date().getTime() - getTime()) / 3600;
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new UnsupportedOperationException();
    }
}
