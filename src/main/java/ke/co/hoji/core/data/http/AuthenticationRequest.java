package ke.co.hoji.core.data.http;

/**
 * Created by gitahi on 27/10/15.
 */
public class AuthenticationRequest {

    private String username;
    private String password;
    private Integer surveyId;

    public AuthenticationRequest() {
    }

    public AuthenticationRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public AuthenticationRequest(String username, String password, Integer surveyId) {
        this(username, password);
        this.surveyId = surveyId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }
}
