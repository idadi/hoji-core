package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.dto.LiteRecord;

import java.util.List;

/**
 * A (HTTP) response to a @{@link SearchRequest}.
 * <p>
 * Created by gitahi on 11/16/16.
 */
public class SearchResponse {

    /**
     * The {@link ResultCode} of this response.
     */
    private int resultCode;

    /**
     * The list of @{@link LiteRecord}s that match the @{@link SearchRequest} for which this is a response.
     */
    private List<LiteRecord> liteRecords;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public List<LiteRecord> getLiteRecords() {
        return liteRecords;
    }

    public void setLiteRecords(List<LiteRecord> liteRecords) {
        this.liteRecords = liteRecords;
    }
}
