package ke.co.hoji.core.data.model;

import ke.co.hoji.core.data.Nameable;
import ke.co.hoji.core.rule.BitwiseAnd;
import ke.co.hoji.core.rule.Contains;
import ke.co.hoji.core.rule.NotBitwiseAnd;
import ke.co.hoji.core.rule.NotContains;
import ke.co.hoji.core.rule.RuleOperator;

/**
 * Specifies the classname of an {@link RuleOperator}.
 *
 * @author gitahi
 */
public class OperatorDescriptor extends Nameable {

    /**
     * The class name for the {@link Contains} {@link RuleOperator}.
     */
    public static final String CONTAINS = "ke.co.hoji.core.rule.Contains";

    /**
     * The class name for the {@link BitwiseAnd} {@link RuleOperator}.
     */
    public static final String BITWISE_AND = "ke.co.hoji.core.rule.BitwiseAnd";

    /**
     * The class name for the {@link NotContains} {@link RuleOperator}.
     */
    public static final String NOT_CONTAINS = "ke.co.hoji.core.rule.NotContains";

    /**
     * The class name for the {@link NotBitwiseAnd} {@link RuleOperator}.
     */
    public static final String NOT_BITWISE_AND = "ke.co.hoji.core.rule.NotBitwiseAnd";

    private String clazz;

    public OperatorDescriptor() {
    }

    public OperatorDescriptor(Integer id) {
        super(id);
    }

    public OperatorDescriptor(Integer id, String name, String clazz) {
        super(id, name);
        this.clazz = clazz;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }
}
