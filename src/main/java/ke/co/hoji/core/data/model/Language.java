package ke.co.hoji.core.data.model;

import ke.co.hoji.core.data.Nameable;

/**
 * A human language for the purposes of translating project data i.e. config e.g. surveys, forms and fields.
 */
public final class Language extends Nameable implements Comparable<Language> {

    public Language() {
    }

    public Language(Integer id) {
        super(id);
    }

    public Language(Integer id, String name) {
        super(id, name);
    }

    /**
     * The @{@link Survey} translated into this Language.
     */
    private Survey survey;

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    @Override
    public int compareTo(Language language) {
        return this.name.compareTo(language.name);
    }
}
