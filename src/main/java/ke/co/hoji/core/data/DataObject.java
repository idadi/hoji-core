package ke.co.hoji.core.data;

/**
 * An object representing actual data or meta-data.
 *
 * @author gitahi
 */
public abstract class DataObject implements HojiObject {

    /**
     * The UUID primary key that identifies this object in the database.
     */
    private String uuid;

    public DataObject() {
    }

    public DataObject(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public Object getUniqueId() {
        return getUuid();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.uuid == null ? 0 : this.uuid.hashCode());
        return hash;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (getClass() != other.getClass()) {
            return false;
        }
        final DataObject that = (DataObject) other;
        if (this.uuid == null && that.uuid == null) {
            return true;
        }
        if (this.uuid != null && that.uuid != null) {
            return this.uuid.equals(that.uuid);
        }
        return false;
    }
}
