package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.model.Survey;

/**
 * Created by gitahi on 27/10/15.
 */
public class AuthenticationResponse {

    /**
     * Whether or not the user was authenticate successfully.
     */
    private boolean authenticated;

    /**
     * Whether or not the user is authorized to access the requested resource, typically a @{@link Survey}.
     */
    private boolean authorized;

    /**
     * The id of the user.
     */
    private Integer userId;

    /**
     * The name of the user.
     */
    private String name;

    /**
     * The code of the user.
     */
    private String userCode;

    /**
     * The code of the tenant under which this user is operating.
     */
    private String tenantCode;

    /**
     * The name of the tenant under which this user is operating.
     */
    private String tenantName;

    public AuthenticationResponse() {
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public boolean isAuthorized() {
        return authorized;
    }

    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }
}
