package ke.co.hoji.core.data.http;

import java.util.Map;

/**
 * A data upload response as sent from the server to the client, in response to an {@link UploadRequest}.
 * <p/>
 * Created by gitahi on 14/09/15.
 */
public class UploadResponse {

    /**
     * The results of the upload. Each entry in the map contains the record uuid of the affected record and the date of
     * upload. If the upload was unsuccessful for any reason, the date will be null.
     */
    private Map<String, UploadResult> results;

    /**
     * Any message accompanying these results e.g. insufficient credits
     */
    private String message;

    public UploadResponse() {
    }

    public UploadResponse(Map<String, UploadResult> results) {
        this.results = results;
    }

    public Map<String, UploadResult> getResults() {
        return results;
    }

    public void setResults(Map<String, UploadResult> results) {
        this.results = results;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
