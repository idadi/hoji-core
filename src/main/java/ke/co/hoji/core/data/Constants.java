package ke.co.hoji.core.data;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Rule;

/**
 * Various constants grouped by category.
 * <p/>
 * Created by gitahi on 21/08/15.
 */
public class Constants {

    /**
     * Constants for the Hoji Server.
     */
    public static final class Server {

        /**
         * The URL of the server.
         */
        public static final String URL = "https://server.hoji.co.ke/";

        /**
         * The name of the current user at the server.
         */
        public static final String NAME = "server.name";

        /**
         * The id of the current user at the server.
         */
        public static final String USER_ID = "server.userId";

        /**
         * The code of the current user at the server.
         */
        public static final String USER_CODE = "server.userCode";

        /**
         * The username of the current user at the server.
         */
        public static final String USERNAME = "server.username";

        /**
         * The password of the current user at the server.
         */
        public static final String PASSWORD = "server.password";

        /**
         * The id of the "owner" of the survey at the server i.e. the tenant who owns the survey
         */
        public static final String OWNER_ID = "server.ownerId";

        /**
         * The name of the "owner" of the survey at the server i.e. the tenant who owns the survey
         */
        public static final String OWNER_NAME = "server.ownerName";

        /**
         * Whether or not the owner of a survey has been explicitly verified by Hoji Ltd.
         */
        public static final String OWNER_VERIFIED = "server.ownerVerified";

        /**
         * The code of the current tenant at the server.
         */
        public static final String TENANT_CODE = "server.tenatCode";

        /**
         * The name of the current tenant at the server.
         */
        public static final String TENANT_NAME = "server.tenantName";

        /**
         * Set to true if you want users to be forced to fix blank fields detected at the end of the form.
         */
        public static final String FIX_BLANKS = "blanks.fix";

        /**
         * The number of hours after which draft records are locked for a given form.
         */
        public static final String DRAFT_LOCK = "lock.draft";

        /**
         * The number of hours after which outbox records are locked for a given form.
         */
        public static final String OUTOBOX_LOCK = "lock.outbox";

        /**
         * The number of hours after which sent records are locked for agiven form.
         */
        public static final String SENT_LOCK = "lock.sent";

        /**
         * The minimum acceptable GPS accuracy for a given form.
         */
        public static final String GPS_ACCURACY = "gps.accuracy";

        /**
         * The maximum acceptable GPS shelf life. I.e. how old a fix has to be before it is considered stale/unusable.
         */
        public static final String GPS_SHELF_LIFE = "gps.shelfLife";

        /**
         * Minimum password length.
         */
        public static final int MIN_PASSWORD_LENGTH = 8;

        /**
         * Verification period expiry time in hours.
         */
        public static final int VERIFICATION_EMAIL_EXPIRY = 48;
    }

    /**
     * Constants for the Device.
     */
    public static final class Device {

        /**
         * The ID of the device.
         */
        public static final String ID = "device.id";

        /**
         * Android app version when choice id was saved as the value instead of choice code
         */
        public static final int CODE_TO_ID_MILESTONE = 98;

        /**
         * Android app version when choice names were unified per user account
         */
        public static final int UNIQUE_NAME_MILESTONE = 150;
    }

    /**
     * Constants for Hoji Api endpoints.
     */
    public static final class Api {

        public static final String SIGN_UP = "api/user/signUp";
        public static final String AUTHENTICATE = "api/user/authenticate";
        public static final String REQUEST_ACCESS = "api/user/requestAccess";
        public static final String CHANGE_TENANT = "api/user/changeTenant";
        public static final String SURVEYS_DOWNLOAD = "api/download/projects";
        public static final String SURVEY_DOWNLOAD = "api/download/project";
        public static final String FORMS_DOWNLOAD = "api/download/forms";
        public static final String FIELDS_DOWNLOAD = "api/download/fields";
        public static final String REFERENCES_DOWNLOAD = "api/download/references";
        public static final String PROPERTIES_DOWNLOAD = "api/download/properties";
        public static final String LANGUAGES_DOWNLOAD = "api/download/languages";
        public static final String TRANSLATIONS_DOWNLOAD = "api/download/translations";
        public static final String DATA_UPLOAD = "api/upload/data";
        public static final String IMPORT_SEARCH = "api/import/search";
        public static final String IMPORT_RECORD = "api/import/record";
    }

    public static final class PropertyKeys {

        public static final String LAST_UPLOAD = "last.upload";
    }

    /**
     * Constants for ACRA crash reporter.
     */
    public static final class Acra {

        /**
         * The URL of the crash report server.
         */
        public static final String URL = "http://acralyzer.hoji.co.ke:5984/acra-hoji/_design/acra-storage/_update/report";

        /**
         * The username of the crash report server.
         */
        public static final String USERNAME = "reporter";

        /**
         * The password of the crash report server.
         */
        public static final String PASSWORD = "HojiCrashReporter9439;";
    }

    /**
     * HTTP constants
     */
    public static final class Http {

        /**
         * Connection timeout in milliseconds.
         */
        public static final int CONNECTION_TIMEOUT = 20000;

        /**
         * Socket timeout in milliseconds.
         */
        public static final int SOCKET_TIMEOUT = 40000;
    }

    /**
     * Processable command that means something other than its literal value.
     */
    public static final class Command {

        /**
         * Pipe the name of the logged in user.
         */
        public static final String USERNAME = "${user}";

        /**
         * Null rule value. See {@link Rule#value}.
         */
        public static final String EMPTY = "${empty}";

        /**
         * The date today.
         */
        public static final String TODAY = "${today}";

        /**
         * The time now.
         */
        public static final String NOW = "${now}";
    }

    /**
     * Constants related to billing for uploaded data.
     */
    public static final class Billing {

        /**
         * The cost, in credits, of uploading 1 regular(unremarkable) field.
         */
        public static final int COST_PER_REGULAR_FIELD = 1;

        /**
         * The cost, in credits, of uploading 1 image field.
         */
        public static final int COST_PER_IMAGE_FIELD = 10;
    }

    /**
     * Constants related to image management.
     */
    public static final class ImageManagement {

        /**
         * The minimum width or height allowable for images (in pixels).
         */
        public static final int MIN_DIMENSION = 64;

        /**
         * The maximum width or height allowable for images (in pixels).
         */
        public static final int MAX_DIMENSION = 1024;

        /**
         * The default image quality setting to use when compressing images to reduce the file size.
         */
        public static final int DEFAULT_QUALITY = 85;

        /**
         * The minimum image quality setting that's acceptable for compressing images to reduce the file size.
         */
        public static final int MIN_QUALITY = 25;

        /**
         * The maximum image quality setting that's acceptable for compressing images to reduce the file size.
         */
        public static final int MAX_QUALITY = 100;
    }
}
