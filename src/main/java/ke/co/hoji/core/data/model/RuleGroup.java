package ke.co.hoji.core.data.model;

import java.util.List;

/**
 * @author gitahi
 */
public class RuleGroup {

    private final List<Rule> rules;
    private Integer combiner;

    public RuleGroup(List<Rule> rules) {
        if (rules == null) {
            throw new RuntimeException("RuleGroup cannot be initialized "
                    + "with null rules.");
        }
        this.rules = rules;
    }

    public RuleGroup(List<Rule> rules, Integer combiner) {
        this(rules);
        this.combiner = combiner;
    }

    public List<Rule> getRules() {
        return rules;
    }

    public Integer getCombiner() {
        return combiner;
    }

    public void setCombiner(Integer combiner) {
        this.combiner = combiner;
    }
}
