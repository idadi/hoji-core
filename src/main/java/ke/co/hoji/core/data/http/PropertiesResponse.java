package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.model.Property;

import java.util.List;

public class PropertiesResponse {

    private List<Property> properties;

    public PropertiesResponse() {
    }

    public PropertiesResponse(List<Property> properties) {
        this.properties = properties;
    }

    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }
}
