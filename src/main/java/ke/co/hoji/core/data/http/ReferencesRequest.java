package ke.co.hoji.core.data.http;

/**
 * Created by gitahi on 27/08/15.
 */
public class ReferencesRequest {

    private Integer surveyId;

    public ReferencesRequest() {
    }

    public ReferencesRequest(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }
}
