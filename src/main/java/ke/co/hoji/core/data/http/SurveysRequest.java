package ke.co.hoji.core.data.http;

/**
 * A request for all surveys that are available for a user to download from the web app to the mobile app.
 */
public class SurveysRequest {

    /**
     * An optional publicSurveyAccess setting to be set for the user. Used to activate access to public projects.
     */
    private Integer publicSurveyAccess;

    public SurveysRequest() {
    }

    public SurveysRequest(Integer publicSurveyAccess) {
        this.publicSurveyAccess = publicSurveyAccess;
    }

    public Integer getPublicSurveyAccess() {
        return publicSurveyAccess;
    }

    public void setPublicSurveyAccess(Integer publicSurveyAccess) {
        this.publicSurveyAccess = publicSurveyAccess;
    }
}
