package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.dto.Language;
import ke.co.hoji.core.data.model.Survey;

/**
 * A (HTTP) request for getting the list of {@link Language}s associated with a @{@link Survey}.
 */
public class LanguagesRequest {

    /**
     * The id of the @{@link Survey} for which to request @{@link Language}s.
     */
    private Integer surveyId;

    public LanguagesRequest() {
    }

    public LanguagesRequest(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }
}
