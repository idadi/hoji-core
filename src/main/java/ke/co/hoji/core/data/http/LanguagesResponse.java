package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.dto.Language;
import ke.co.hoji.core.data.model.Survey;

import java.util.List;

/**
 * A (HTTP) response for returning the list of {@link Language}s associated with a @{@link Survey}.
 */
public class LanguagesResponse {

    private List<Language> languages;

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }
}
