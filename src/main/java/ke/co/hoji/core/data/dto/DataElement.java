package ke.co.hoji.core.data.dto;

/**
 * Representation of a column from the database that adds label and format to the attributes provided by
 * {@link DataValue}
 * <p/>
 * Created by geoffreywasilwa on 18/04/2017.
 */
public class DataElement extends DataValue {

    private final String label;
    private final String format;

    public DataElement(String label, String value, String type, String format) {
        super(value, type);
        this.label = label;
        this.format = format;
    }

    public String getLabel() {
        return label;
    }

    public String getFormat() {
        return format;
    }

    public class Type {

        public static final String NUMBER = "number";
        public static final String DATE = "date";
        public static final String STRING = "string";
        public static final String TIME = "time";
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (this.label == null ? 0 : this.label.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof DataElement)) {
            return false;
        }
        DataElement o = (DataElement) other;
        return super.equals(o) &&
                (this.label == null ? o.label == null : this.label.equals(o.label));
    }

    @Override
    public String toString() {
        return this.getLabel();
    }
}
