package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.dto.Reference;

import java.util.List;

/**
 * Created by gitahi on 27/08/15.
 */
public class ReferencesResponse {

    private List<Reference> references;

    public ReferencesResponse() {
    }

    public ReferencesResponse(List<Reference> references) {
        this.references = references;
    }

    public List<Reference> getReferences() {
        return references;
    }

    public void setReferences(List<Reference> references) {
        this.references = references;
    }
}
