package ke.co.hoji.core.data.dto;

import ke.co.hoji.core.data.Nameable;

/**
 * The DTO for @{@link ke.co.hoji.core.data.model.Language}.
 */
public final class Language extends Nameable {

    private Integer surveyId;

    public Language() {
    }

    public Language(Integer id, String name) {
        super(id, name);
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }
}
