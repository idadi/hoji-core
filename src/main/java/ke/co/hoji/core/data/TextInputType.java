package ke.co.hoji.core.data;

/**
 * Created by gitahi on 14/08/15.
 */
public final class TextInputType {

    public static final int NONE = 0;
    public static final int TEXT = 1;
    public static final int TEXT_ALL_CAPS = 2;
    public static final int TEXT_CAP_WORDS = 3;
    public static final int TEXT_SENTENCE = 4;
    public static final int TEXT_MULTILINE = 5;
    public static final int TEXT_EMAIL = 6;
    public static final int TEXT_PASSWORD = 7;
    public static final int NUMBER = 8;
    public static final int NUMBER_SIGNED = 9;
    public static final int NUMBER_PHONE = 10;
    public static final int NUMBER_PASSWORD = 11;
    public static final int DECIMAL = 12;
    public static final int DECIMAL_SIGNED = 13;
    public static final int DATE = 14;
    public static final int TIME = 15;
    public static final int DATE_TIME = 16;
}
