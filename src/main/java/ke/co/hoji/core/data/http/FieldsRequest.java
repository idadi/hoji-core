package ke.co.hoji.core.data.http;

/**
 * Created by gitahi on 27/08/15.
 */
public class FieldsRequest {

    private Integer formId;

    public FieldsRequest() {
    }

    public FieldsRequest(Integer formId) {
        this.formId = formId;
    }

    public Integer getFormId() {
        return formId;
    }

    public void setFormIds(Integer formId) {
        this.formId = formId;
    }
}
