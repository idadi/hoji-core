package ke.co.hoji.core.data;

import ke.co.hoji.core.data.model.EventSubject;

import java.util.LinkedHashMap;

/**
 * An {@link HojiObject} representing configuration data.
 *
 * @author gitahi
 */
public abstract class ConfigObject implements HojiObject, EventSubject {

    /**
     * The integer primary key that identifies this object in the database.
     */
    protected Integer id;

    public ConfigObject() {
    }

    public ConfigObject(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public Object getUniqueId() {
        return getId();
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (getClass() != other.getClass()) {
            return false;
        }
        ConfigObject that = (ConfigObject) other;
        if (this.id == null && that.id == null) {
            return true;
        }
        if (this.id != null && that.id != null) {
            return this.id.equals(that.id);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public LinkedHashMap<String, Object> descriptiveProperties() {
        LinkedHashMap descriptiveProperties = new LinkedHashMap();
        if (this instanceof Nameable) {
            descriptiveProperties.put("Name", ((Nameable) this).getName());
        } else {
            descriptiveProperties.put("ID", this.getId());
        }
        if (this instanceof Enablable) {
            descriptiveProperties.put("Enabled", ((Enablable) this).isEnabled());
        }
        if (this instanceof Orderable) {
            descriptiveProperties.put("Order", ((Orderable) this).getOrdinal());
        }
        return descriptiveProperties;
    }
}
