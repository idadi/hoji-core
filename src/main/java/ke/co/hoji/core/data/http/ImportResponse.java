package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.dto.MainRecord;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * A (HTTP) response to an @{@link ImportRequest}.
 * <p>
 * Created by gitahi on 11/16/16.
 */
public class ImportResponse {

    /**
     * The {@link ResultCode} of this response.
     */
    private int resultCode;

    /**
     * The map containing {@link MainRecord}s indexed by the UUID that retrieved them.
     */
    private LinkedHashMap<String, List<MainRecord>> mainRecordMap;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public LinkedHashMap<String, List<MainRecord>> getMainRecordMap() {
        return mainRecordMap;
    }

    public void setMainRecordMap(LinkedHashMap<String, List<MainRecord>> mainRecordMap) {
        this.mainRecordMap = mainRecordMap;
    }
}
