package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.dto.Language;
import ke.co.hoji.core.data.dto.Translation;

import java.util.List;

/**
 * A (HTTP) request for getting the list of {@link Translation}s associated with the given @{@link Language}s.
 */
public class TranslationsRequest {
    
    public TranslationsRequest() {
    }

    public TranslationsRequest(List<Language> languages) {
        this.languages = languages;
    }

    private List<Language> languages;

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }
}
