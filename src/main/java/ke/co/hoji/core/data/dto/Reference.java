package ke.co.hoji.core.data.dto;

import ke.co.hoji.core.data.*;

import java.util.List;
import java.util.Map;

/**
 * Created by gitahi on 28/08/15.
 */
public class Reference extends ConfigObject {

    private Map<Integer, String> values;

    private List<ReferenceLevel> referenceLevels;

    public Reference() {
    }

    public Reference(Integer id, List<ReferenceLevel> referenceLevels) {
        super(id);
        this.referenceLevels = referenceLevels;
    }

    public Map<Integer, String> getValues() {
        return values;
    }

    public void setValues(Map<Integer, String> values) {
        this.values = values;
    }

    public List<ReferenceLevel> getReferenceLevels() {
        return referenceLevels;
    }

    public void setReferenceLevels(List<ReferenceLevel> referenceLevels) {
        this.referenceLevels = referenceLevels;
    }
}
