package ke.co.hoji.core.data.http;

/**
 * A request for asking the server to change a user's operating acccount/tenancy.
 *
 * @author Gitahi (Apr 9, 2017)
 */
public class ChangeTenantRequest {

    /**
     * The id of the joining user.
     */
    private Integer userId;

    /**
     * The code of the user/tenant to be joined.
     */
    private String tenantCode;

    public ChangeTenantRequest() {
    }

    public ChangeTenantRequest(Integer userId, String tenantCode) {
        this.userId = userId;
        this.tenantCode = tenantCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }
}
