package ke.co.hoji.core.data.http;

/**
 * A response to {@link ChangeTenantRequest}.
 *
 * @author Gitahi (Apr 9, 2017)
 */
public class ChangeTenantResponse {

    /**
     * An integer code saying the nature of the response.
     */
    public static class Code {

        /**
         * A tenant was found and joined successfully.
         */
        public static final int TENANT_FOUND = 0;

        /**
         * No tenant was found with the code supplied.
         */
        public static final int TENANT_NOT_FOUND = 1;
    }

    public static ChangeTenantResponse tenantNotFound() {
        return new ChangeTenantResponse(Code.TENANT_NOT_FOUND, null, null);
    }

    public ChangeTenantResponse(int code, String tenantCode, String tenantName) {
        this.code = code;
        this.tenantCode = tenantCode;
        this.tenantName = tenantName;
    }

    /**
     * The {@link ChangeTenantResponse.Code} of this response.
     */
    private int code;

    /**
     * The code of the tenant, if one was found. This should be displayed somewhere client side (presumably on the
     * about page) so that users in the field can easily advise each other on which tenant they're operating under e.g.
     * if a fellow field worker also needs to join the same tenant.
     */
    private String tenantCode;

    /**
     * The name of the tenant, if one was found. This should be displayed somewhere client side (presumably on the
     * about page) so that users know which tenant they are operating under.
     */
    private String tenantName;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }
}
