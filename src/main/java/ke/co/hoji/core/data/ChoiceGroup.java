package ke.co.hoji.core.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gitahi on 03/11/15.
 */
public class ChoiceGroup extends Nameable implements UserOwned {

    /**
     * Defines strategies for ordering the constituent {@link Choice}s in this ChoiceGroup.
     */
    public static class OrderingStrategy {

        /**
         * Order based on the ordinal.
         */
        public static final int AS_SPECIFIED = 0;

        /**
         * Order randomly.
         */
        public static final int RANDOM = 1;

        /**
         * Order alphabetically.
         */
        public static final int ALPHABETICAL = 2;
    }

    private List<Choice> choices;

    /**
     * The {@link OrderingStrategy} by which to order consituent {@link Choice}s.
     */
    private int orderingStrategy;

    /**
     * The number of {@link Choice}s to be exclude from random or alphabetical ordering, counted from the bottom.
     */
    private int excludeLast;

    /**
     * The id of the user who owns this Survey.
     */
    protected Integer userId;

    /**
     * A map of all the {@link Choice}s in this ChoiceGroup, indexed by their ids. Used for retrieving Choices by
     * their ids more efficiently.
     */
    private Map<Integer, Choice> choiceMap = null;

    public ChoiceGroup() {
    }

    public ChoiceGroup(Integer id) {
        super(id);
    }

    public ChoiceGroup(Integer id, String name) {
        super(id, name);
    }

    public ChoiceGroup(List<Choice> choices) {
        this.choices = choices;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    public int getOrderingStrategy() {
        return orderingStrategy;
    }

    public void setOrderingStrategy(int orderingStrategy) {
        this.orderingStrategy = orderingStrategy;
    }

    public int getExcludeLast() {
        return excludeLast;
    }

    public void setExcludeLast(int excludeLast) {
        this.excludeLast = excludeLast;
    }

    @Override
    public Integer getUserId() {
        return userId;
    }

    @Override
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof ChoiceGroup)) {
            return false;
        }
        ChoiceGroup o = (ChoiceGroup) other;
        return (this.id == null ? o.id == null : this.id.equals(o.id)) &&
            (this.name == null ? o.name == null : this.name.equalsIgnoreCase(o.name));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (this.id == null ? 0 : this.id);
        result = 31 * result + (this.name == null ? 0 : this.name.hashCode());
        return result;
    }

    //For creating copies of choice groups
    public ChoiceGroup(ChoiceGroup choiceGroup) {
        this(choiceGroup.id, choiceGroup.name);
        this.setUserId(choiceGroup.getUserId());
        this.setOrderingStrategy(choiceGroup.getOrderingStrategy());
        this.setExcludeLast(choiceGroup.getExcludeLast());
        if (choiceGroup.getChoices() != null) {
            this.setChoices(new ArrayList<Choice>());
            for (Choice choice : choiceGroup.getChoices()) {
                Choice copy = new Choice(choice);
                this.getChoices().add(copy);
            }
        }
    }


    /**
     * An optimized method for efficiently retrieving a {@link Choice} from this ChoiceGroup by it's id. It's
     * intended to avoid looping through all choices every time you need to retrieve a single Choice by its id.
     *
     * @param choiceId the id of the Choice you want to retrieve.
     */
    public Choice getChoiceById(Integer choiceId) {
        Choice choice = null;
        if (choiceMap != null) {
            choice = choiceMap.get(choiceId);
        }
        if (choice == null) {
            createChoiceMap(); //re-create choiceMap if choice is null, just in case new choices have since been added.
            choice = choiceMap.get(choiceId);
        }
        return choice;
    }

    private void createChoiceMap() {
        choiceMap = new HashMap<>();
        for (Choice choice : getChoices()) {
            choiceMap.put(choice.getId(), choice);
        }
    }
}
