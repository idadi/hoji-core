package ke.co.hoji.core.data.model;

import ke.co.hoji.core.data.ConfigObject;
import ke.co.hoji.core.data.ReferenceLevel;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author gitahi
 */
public class Reference extends ConfigObject {

    protected Map<Integer, String> values;
    protected List<ReferenceLevel> referenceLevels;

    public Reference() {
    }

    public Reference(Integer id, List<ReferenceLevel> referenceLevels) {
        super(id);
        this.referenceLevels = referenceLevels;
    }

    public void addValue(Integer key, String value) {
        if (values == null) {
            values = new LinkedHashMap<>();
        }
        values.put(key, value);
    }

    @Override
    public String toString() {
        String displayString = "";
        for (ReferenceLevel referenceLevel : referenceLevels) {
            if (referenceLevel.isDisplayable() && values != null) {
                String value = values.get(referenceLevel.getLevel());
                if (value != null) {
                    displayString = value;
                }
                break;
            }
        }
        return displayString;
    }

    public Map<String, Object> translateToMap() {
        Map<String, Object> params = new LinkedHashMap<>();
        if (getId() != null) {
            params.put("id", getId());
        }
        for (ReferenceLevel referenceLevel : referenceLevels) {
            if (referenceLevel.isEditable() && values != null) {
                String value = values.get(referenceLevel.getLevel());
                if (value != null) {
                    params.put(referenceLevel.getLevelColumn(), value);
                }
            }
        }
        return params;
    }

    public Map<Integer, String> getValues() {
        return values;
    }

    public void setValues(Map<Integer, String> values) {
        this.values = values;
    }

    public List<ReferenceLevel> getReferenceLevels() {
        return referenceLevels;
    }

    public void setReferenceLevels(List<ReferenceLevel> referenceLevels) {
        this.referenceLevels = referenceLevels;
    }
}
