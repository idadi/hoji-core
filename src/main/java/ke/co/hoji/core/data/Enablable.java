package ke.co.hoji.core.data;

/**
 * Created by gitahi on 13/11/15.
 */
public interface Enablable {

    boolean isEnabled();

    void setEnabled(boolean enabled);
}
