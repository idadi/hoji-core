package ke.co.hoji.core.data;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Record;

import java.io.Serializable;
import java.util.Date;

/**
 * Parameters for performing a {@link Record} findMainRecords.
 * <p/>
 * Created by gitahi on 15/05/15.
 */
public class RecordSearch implements Serializable {

    private String uuid;
    private Form form;
    private Integer stage;
    private String caption;
    private Date from;
    private Date to;
    private Long timeStamp;
    private boolean reverse = false;
    private PageInfo pageInfo;
    private Boolean testMode;

    public RecordSearch() {
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Integer getStage() {
        return stage;
    }

    public void setStage(Integer stage) {
        this.stage = stage;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public boolean isReverse() {
        return reverse;
    }

    public void setReverse(boolean reverse) {
        this.reverse = reverse;
    }

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public Boolean isTestMode() {
        return testMode;
    }

    public void setTestMode(Boolean testMode) {
        this.testMode = testMode;
    }
}
