package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Survey;

import java.util.List;

/**
 * A data upload request as sent from the client to the server.
 * <p/>
 * Created by gitahi on 14/09/15.
 */
public class UploadRequest {

    /**
     * The list of {@link MainRecord}s to be uploaded.
     */
    private List<MainRecord> mainRecords;

    /**
     * The id of the {@link Survey} to which the {@link #mainRecords} belong.
     */
    private Integer surveyId;

    /**
     * The version code of the app submitting the upload so that the server may opt to reject uploads from certain
     * (presumably older) versions of the mobile app.
     */
    private Integer appVersionCode;

    public UploadRequest() {
    }

    public UploadRequest(List<MainRecord> mainRecords, Integer surveyId) {
        this.mainRecords = mainRecords;
        this.surveyId = surveyId;
    }

    public List<MainRecord> getMainRecords() {
        return mainRecords;
    }

    public void setMainRecords(List<MainRecord> mainRecords) {
        this.mainRecords = mainRecords;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public Integer getAppVersionCode() {
        return appVersionCode;
    }

    public void setAppVersionCode(Integer appVersionCode) {
        this.appVersionCode = appVersionCode;
    }
}
