package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.dto.Form;

import java.util.List;

/**
 * Created by gitahi on 27/08/15.
 */
public class FormsResponse {

    private List<Form> forms;

    public FormsResponse() {
    }

    public FormsResponse(List<Form> forms) {
        this.forms = forms;
    }

    public List<Form> getForms() {
        return forms;
    }

    public void setForms(List<Form> forms) {
        this.forms = forms;
    }
}
