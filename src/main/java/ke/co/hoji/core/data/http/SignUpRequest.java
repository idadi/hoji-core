package ke.co.hoji.core.data.http;

/**
 * Created by gitahi on 14/01/16.
 */
public class SignUpRequest {

    private String name;
    private String username;
    private String password;
    private Integer appVersionCode;

    public SignUpRequest() {
    }

    public SignUpRequest(String name, String username, String password, Integer appVersionCode) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.appVersionCode = appVersionCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAppVersionCode() {
        return appVersionCode;
    }

    public void setAppVersionCode(Integer appVersionCode) {
        this.appVersionCode = appVersionCode;
    }
}
