package ke.co.hoji.core.data.dto;

import ke.co.hoji.core.data.ConfigObject;

import java.math.BigDecimal;

/**
 * Created by gitahi on 27/08/15.
 */
public class Rule extends ConfigObject {

    private BigDecimal ordinal;

    private String value;

    private Integer type;

    private Integer combiner;

    private int grouper;

    private Integer operatorDescriptorId;

    private Integer ownerId;

    private Integer targetId;

    public Rule() {
        super();
    }

    public Rule(Integer id, BigDecimal ordinal, String value, Integer type, int grouper) {
        super(id);
        this.ordinal = ordinal;
        this.value = value;
        this.type = type;
        this.grouper = grouper;
    }

    public Integer getOperatorDescriptorId() {
        return operatorDescriptorId;
    }

    public void setOperatorDescriptorId(Integer operatorDescriptorId) {
        this.operatorDescriptorId = operatorDescriptorId;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public Integer getTargetId() {
        return targetId;
    }

    public void setTargetId(Integer targetId) {
        this.targetId = targetId;
    }

    public BigDecimal getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(BigDecimal ordinal) {
        this.ordinal = ordinal;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCombiner() {
        return combiner;
    }

    public void setCombiner(Integer combiner) {
        this.combiner = combiner;
    }

    public int getGrouper() {
        return grouper;
    }

    public void setGrouper(int grouper) {
        this.grouper = grouper;
    }
}
