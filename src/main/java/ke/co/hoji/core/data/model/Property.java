package ke.co.hoji.core.data.model;

import ke.co.hoji.core.data.HojiObject;

/**
 * @author gitahi
 */
public class Property implements HojiObject {

    private String key;
    private String value;
    private Survey survey;

    public Property() {
    }

    public Property(String key) {
        this.key = key;
    }

    public Property(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public Property(String key, String value, Survey survey) {
        this.key = key;
        this.value = value;
        this.survey = survey;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    @Override
    public Object getUniqueId() {
        return getKey();
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Property)) {
            return false;
        }
        Property o = (Property) other;
        return (this.key == null ? o.key == null : this.key.equals(o.key)) &&
                (this.value == null ? o.value == null : this.value.equalsIgnoreCase(o.value)) &&
                (this.survey == null ? o.survey == null : this.survey.equals(o.survey));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (this.key == null ? 0 : this.key.hashCode());
        result = 31 * result + (this.value == null ? 0 : this.value.hashCode());
        result = 31 * result + (this.survey == null ? 0 : this.survey.hashCode());
        return result;
    }
}
