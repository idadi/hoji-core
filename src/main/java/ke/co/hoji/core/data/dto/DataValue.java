package ke.co.hoji.core.data.dto;

/**
 * Representation of a column from the database with an appropriate string representation of the value and data type.
 */
public class DataValue {
    private final String value;
    private final String type;

    public DataValue(String value, String type) {
        this.value = value;
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public String getType() {
        return type;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (this.value == null ? 0 : this.value.hashCode());
        result = 31 * result + (this.type == null ? 0 : this.type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof DataValue)) {
            return false;
        }
        DataValue o = (DataValue) other;
        return (this.value == null ? o.value == null : this.value.equals(o.value)) &&
                (this.type == null ? o.type == null : this.type.equalsIgnoreCase(o.type));
    }
}
