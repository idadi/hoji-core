package ke.co.hoji.core.data;

import java.math.BigDecimal;

/**
 * A convenience implementation of {@link Describable} objects that can be ordered.
 *
 * @author gitahi
 */
public class DescribableOrderable extends Describable implements Orderable<DescribableOrderable> {

    /**
     * The order position of this Form relative to other Forms.
     */
    private BigDecimal ordinal;

    public DescribableOrderable() {
        super();
    }

    public DescribableOrderable(Integer id) {
        super(id);
    }

    public DescribableOrderable(Integer id, String name, BigDecimal ordinal) {
        super(id, name);
        this.ordinal = ordinal;
    }

    public DescribableOrderable(Integer id, String name, String description, BigDecimal ordinal) {
        super(id, name, description);
        this.ordinal = ordinal;
    }

    @Override
    public BigDecimal getOrdinal() {
        return ordinal;
    }

    @Override
    public void setOrdinal(BigDecimal ordinal) {
        this.ordinal = ordinal;
    }

    @Override
    public int compareTo(DescribableOrderable describableOrderable) {
        return this.ordinal.compareTo(describableOrderable.ordinal);
    }
}
