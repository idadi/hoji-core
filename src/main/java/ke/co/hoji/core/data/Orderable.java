package ke.co.hoji.core.data;

import java.math.BigDecimal;

/**
 * An interface for orderables.
 *
 * Created by gitahi on 12/11/15.
 */
public interface Orderable<T> extends Comparable<T> {

    BigDecimal getOrdinal();

    void setOrdinal(BigDecimal ordinal);
}
