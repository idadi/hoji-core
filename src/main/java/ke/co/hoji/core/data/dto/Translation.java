package ke.co.hoji.core.data.dto;

import ke.co.hoji.core.data.ConfigObject;

/**
 * The DTO for @{@link ke.co.hoji.core.data.model.Translation}.
 */
public final class Translation extends ConfigObject {

    private String component;

    private String value;

    private String translatableName;

    private Integer translatableId;

    private Integer languageId;

    public Translation(Integer id, String component, String value) {
        super(id);
        this.component = component;
        this.value = value;
    }

    public String getTranslatableName() {
        return translatableName;
    }

    public void setTranslatableName(String translatableName) {
        this.translatableName = translatableName;
    }

    public Integer getTranslatableId() {
        return translatableId;
    }

    public void setTranslatableId(Integer translatableId) {
        this.translatableId = translatableId;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
