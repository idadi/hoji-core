package ke.co.hoji.core.data.dto;

import ke.co.hoji.core.data.DataObject;
import ke.co.hoji.core.data.http.SearchResponse;
import ke.co.hoji.core.data.model.Record;

/**
 * A lite version of a @{@link Record} for the purposes of returning lightweight @{@link SearchResponse}.
 * <p>
 * Created by gitahi on 11/16/16.
 */
public class LiteRecord extends DataObject {

    /**
     * The status of this @{@link LiteRecord} client side.
     */
    public static class Status {

        /**
         * NEW means this record does not exist client side and needs to be saved as new.
         */
        public static final int NEW = 0;

        /**
         * EXISTING means this record exists client side and needs only to be updated.
         */
        public static final int EXISTING = 1;

        /**
         * IMPORTED means this record has been successfully saved or updated.
         */
        public static final int IMPORTED = 2;
    }

    /**
     * The caption of the @{@link Record}.
     */
    private String caption;

    /**
     * The date when the @{@link Record} was created.
     */
    private Long dateCreated;

    /**
     * The date when the @{@link Record} was updated.
     */
    private Long dateUpdated;

    /**
     * The search terms associated with the record @{@link Record} (as stored, not as submitted).
     */
    private String searchTerms;

    /**
     * Indicates the @{@link Status} of this @{@link LiteRecord} client side.
     */
    private int status;

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Long dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getSearchTerms() {
        return searchTerms;
    }

    public void setSearchTerms(String searchTerms) {
        this.searchTerms = searchTerms;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
