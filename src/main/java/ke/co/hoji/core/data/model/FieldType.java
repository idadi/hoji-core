package ke.co.hoji.core.data.model;

import ke.co.hoji.core.data.Nameable;
import ke.co.hoji.core.data.Orderable;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.widget.Widget;

import java.math.BigDecimal;

/**
 * The type of a {@link Field}. This attributes determines the {@link ke.co.hoji.core.widget.Widget} to be loaded for
 * this field. It may also determine the data type of under which values collected for this field will be stored.
 *
 * @author gitahi
 */
public class FieldType extends Nameable implements Orderable<FieldType> {

    /**
     * Supported data types.
     */
    public static class DataType {

        public static final int NULL = 0;
        public static final int NUMBER = 1;
        public static final int DECIMAL = 2;
        public static final int STRING = 3;
    }

    /**
     * A logical category to which a FieldType might belong.
     */
    public static class Group {

        public static final int CATEGORICAL = 0;
        public static final int NUMERIC = 1;
        public static final int TEXT = 2;
        public static final int DATETIME = 3;
        public static final int OTHER = 4;
        public static final int ADVANCED = 5;

        public static String getGroupCode(int group) {
            String code;
            switch (group) {
                case 0:
                    code = "field.type.categorical";
                    break;
                case 1:
                    code = "field.type.numeric";
                    break;
                case 2:
                    code = "field.type.text";
                    break;
                case 3:
                    code = "field.type.datetime";
                    break;
                case 5:
                    code = "field.type.advanced";
                    break;
                default:
                    code = "field.type.other";
                    break;
            }
            return code;
        }
    }

    /**
     * Unique codes for each FieldType.
     */
    public static class Code {

        public static final String SINGLE_CHOICE = "SCH";
        public static final String SINGLE_LINE_TEXT = "STX";
        public static final String DATE = "DAT";
        public static final String TIME = "TIM";
        public static final String MATRIX = "MRX";
        public static final String LABEL = "LBL";
        public static final String WHOLE_NUMBER = "WNM";
        public static final String REFERENCE = "REF";
        public static final String IMAGE = "IMG";
        public static final String RECORD = "RCD";
        public static final String DECIMAL_NUMBER = "DNM";
        public static final String MULTIPLE_CHOICE = "MCH";
        public static final String MULTIPLE_LINES_TEXT = "MTX";
        public static final String SCALE = "SCL";
        public static final String ENCRYPTED_TEXT = "ETX";
        public static final String RANKING = "RNK";
        public static final String LOCATION = "LOC";
    }

    /**
     * The {@link Code} of this FieldType.
     */
    private String code;

    /**
     * The order position of this FieldType relative to other FieldTypes.
     */
    private BigDecimal ordinal;

    /**
     * The {@link DataType} of this FieldType.
     */
    private Integer dataType;

    /**
     * The {@link Group} to which this FieldType.
     */
    private Integer group;

    /**
     * The fully qualified class name of the {@link Widget} to be used to render {@link Field}s of this type when its
     * {@link FieldParent} is a {@link Form}.
     */
    private String widgetClass;

    /**
     * The fully qualified class name of the {@link Widget} to be used to render {@link Field}s of this type when its
     * {@link FieldParent} is a matrix {@link Field}.
     */
    private String matrixWidgetClass;

    /**
     * The fully qualified class name of the {@link Response} to be used as a response for {@link Field}s of this type.
     */
    private String responseClass;

    public FieldType(Integer id, String name, String code) {
        super(id, name);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public BigDecimal getOrdinal() {
        return ordinal;
    }

    @Override
    public void setOrdinal(BigDecimal ordinal) {
        this.ordinal = ordinal;
    }


    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public String getWidgetClass() {
        return widgetClass;
    }

    public void setWidgetClass(String widgetClass) {
        this.widgetClass = widgetClass;
    }

    public String getMatrixWidgetClass() {
        return matrixWidgetClass;
    }

    public void setMatrixWidgetClass(String matrixWidgetClass) {
        this.matrixWidgetClass = matrixWidgetClass;
    }

    public String getResponseClass() {
        return responseClass;
    }

    public void setResponseClass(String responseClass) {
        this.responseClass = responseClass;
    }

    /**
     * Get the fully qualified class name of the {@link Widget} to be used to render {@link Field}s of this type, given
     * their {@link FieldParent}. Some {@link FieldType}s provide 2 variations of Widgets to be convenient in the
     * {@link Form} (@{@link MainRecord}) scope and save space in the @{@link Field} (@{@link MatrixRecord}) scope.
     *
     * @param fieldParent the {@link FieldParent} of the field for which this is the type.
     * @return the fully qualified class name of the {@link Widget}.
     */
    public String getWidgetClassByFieldParent(FieldParent fieldParent) {
        String widgetClass = this.widgetClass;
        if (fieldParent != null) {
            if (fieldParent instanceof Field) {
                widgetClass = this.matrixWidgetClass;
            }
        }
        return widgetClass;
    }

    public boolean isNull() {
        return DataType.NULL == dataType;
    }

    public boolean isReference() {
        return Code.REFERENCE.equals(code);
    }

    public boolean isTextual() {
        return Code.SINGLE_LINE_TEXT.equals(code) || Code.MULTIPLE_LINES_TEXT.equals(code)
            || Code.ENCRYPTED_TEXT.equals(code);
    }

    public boolean isEncrypted() {
        return Code.ENCRYPTED_TEXT.equals(code);
    }

    public boolean isChoice() {
        return Code.SINGLE_CHOICE.equals(code)
            || Code.MULTIPLE_CHOICE.equals(code)
            || Code.RANKING.equals(code);
    }

    public boolean isParent() {
        return Code.MATRIX.equals(code);
    }

    public boolean isRecord() {
        return Code.RECORD.equals(code);
    }

    public boolean isDate() {
        return Code.DATE.equals(code);
    }

    public boolean isTime() {
        return Code.TIME.equals(code);
    }

    public Boolean isLabel() {
        return Code.LABEL.equals(code);
    }

    public Boolean isImage() {
        return Code.IMAGE.equals(code);
    }

    public Boolean isSingleChoice() {
        return Code.SINGLE_CHOICE.equals(code);
    }

    public Boolean isMultiChoice() {
        return Code.MULTIPLE_CHOICE.equals(code);
    }

    public Boolean isRanking() {
        return Code.RANKING.equals(code);
    }

    public Boolean isLocation() {
        return Code.LOCATION.equals(code);
    }

    public Boolean isNumber() {
        return Code.DECIMAL_NUMBER.equals(code) || Code.WHOLE_NUMBER.equals(code) || Code.SCALE.equals(code);
    }

    public Boolean isInteger() {
        return Code.WHOLE_NUMBER.equals(code) || Code.SCALE.equals(code);
    }

    public Boolean isDecimal() {
        return Code.DECIMAL_NUMBER.equals(code);
    }

    @Override
    public int compareTo(FieldType fieldType) {
        return this.ordinal.compareTo(fieldType.ordinal);
    }
}
