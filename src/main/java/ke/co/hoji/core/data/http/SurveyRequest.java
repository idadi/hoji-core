package ke.co.hoji.core.data.http;

/**
 * Created by gitahi on 27/08/15.
 */
public class SurveyRequest {

    private Integer surveyId;

    /**
     * The version code of the app submitting the upload so that the server may opt to reject uploads from certain
     * (presumably older) versions of the mobile app.
     */
    private Integer appVersionCode;

    public SurveyRequest() {
    }

    public SurveyRequest(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public Integer getAppVersionCode() {
        return appVersionCode;
    }

    public void setAppVersionCode(Integer appVersionCode) {
        this.appVersionCode = appVersionCode;
    }
}
