package ke.co.hoji.core.data.http;

/**
 * The result of uploading a single record.
 */
public final class UploadResult {

    /**
     * The code indicating a successful upload.
     */
    public static final int SUCCESS = 0;

    /**
     * The code indicating insufficient credits.
     */
    public static final int INSUFFICIENT_CREDITS = 1;

    /**
     * The code associated with this UploadResult. One of {@link #SUCCESS} or {@link #INSUFFICIENT_CREDITS}..
     */
    private final int code;

    /**
     * The server-side-assigned unique id for the record uploaded.
     */
    private final String id;

    /**
     * The date when the upload occurred, if it succeed and NULL otherwise.
     */
    private final Long dateUploaded;

    /**
     * Initializes a new UploadResult with the given code, id and date of upload.
     *
     * @param code         the code for this UploadResult
     * @param id           the id assigned to teh record uploaded.
     * @param dateUploaded the date of upload for this UploadResult
     */
    public UploadResult(int code, String id, Long dateUploaded) {
        this.code = code;
        this.id = id;
        this.dateUploaded = dateUploaded;
    }

    /**
     * Initializes a new UploadResult with the given code.
     *
     * @param code the code for this UploadResult
     */
    public UploadResult(int code) {
        this.code = code;
        this.id = null;
        this.dateUploaded = null;
    }

    public int getCode() {
        return code;
    }

    public String getId() {
        return id;
    }

    public Long getDateUploaded() {
        return dateUploaded;
    }
}
