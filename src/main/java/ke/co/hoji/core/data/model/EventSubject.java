package ke.co.hoji.core.data.model;

import java.util.LinkedHashMap;

/**
 * The subject of an application event that we may wish to log or otherwise react to.
 */
public interface EventSubject {

    /**
     * @return a map of the properties the describe this EventSubject.
     */
    LinkedHashMap<String, Object> descriptiveProperties();
}
