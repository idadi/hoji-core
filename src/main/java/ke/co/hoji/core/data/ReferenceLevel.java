package ke.co.hoji.core.data;

/**
 * @author gitahi
 */
public class ReferenceLevel extends Nameable implements Enablable {

    private Integer level;
    private String levelColumn;
    private Boolean displayable;
    private Integer inputType;
    private Boolean rulable;
    private Integer missingAction;

    /**
     * True if this RerefenceLevel is enabled and false otherwise.
     */
    protected boolean enabled;

    public ReferenceLevel() {
    }

    public ReferenceLevel(Integer id) {
        super(id);
    }

    public ReferenceLevel(Integer id, String name, boolean enabled) {
        super(id, name);
        this.enabled = enabled;
    }

    public ReferenceLevel(Integer id, String name, boolean enabled, Integer level, String levelColumn,
                          Boolean displayable, Integer inputType, Boolean rulable, Integer missingAction) {
        super(id, name);
        this.enabled = enabled;
        this.level = level;
        this.levelColumn = levelColumn;
        this.displayable = displayable;
        this.inputType = inputType;
        this.rulable = rulable;
        this.missingAction = missingAction;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getLevelColumn() {
        return levelColumn;
    }

    public void setLevelColumn(String levelColumn) {
        this.levelColumn = levelColumn;
    }

    public Boolean isDisplayable() {
        return displayable;
    }

    public void setDisplayable(Boolean displayable) {
        this.displayable = displayable;
    }

    public Boolean isEditable() {
        return inputType != TextInputType.NONE;
    }

    public Integer getInputType() {
        return inputType;
    }

    public void setInputType(Integer inputType) {
        this.inputType = inputType;
    }

    public Boolean isRulable() {
        return rulable;
    }

    public void setRulable(Boolean rulable) {
        this.rulable = rulable;
    }

    public Integer getMissingAction() {
        return missingAction;
    }

    public void setMissingAction(Integer missingAction) {
        this.missingAction = missingAction;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
