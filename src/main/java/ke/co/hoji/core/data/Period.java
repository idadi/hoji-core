package ke.co.hoji.core.data;

import org.joda.time.LocalDateTime;

import java.util.Date;

/**
 * Created by gitahi on 15/05/15.
 */
public class Period {

    public static final String WHENEVER = "Whenever";
    public static final String TODAY = "Today";
    public static final String YESTERDAY = "Yesterday";
    public static final String THIS_WEEK = "This week";
    public static final String LAST_WEEK = "Last week";
    public static final String THIS_MONTH = "This month";
    public static final String LAST_MONTH = "Last month";
    public static final String THIS_YEAR = "This year";
    public static final String LAST_YEAR = "Last year";
    public static final String BETWEEN = "Between";

    private final String name;

    public Period(String name) {
        this.name = name;
    }

    public Date getFrom() {
        LocalDateTime today = new LocalDateTime().withTime(0, 0, 0, 0);
        switch (name) {
            case TODAY:
                return today.toDate();
            case YESTERDAY:
                return today.minusDays(1).toDate();
            case THIS_WEEK:
                return today.dayOfWeek().withMinimumValue().toDate();
            case LAST_WEEK:
                return today.minusWeeks(1).dayOfWeek().withMinimumValue().toDate();
            case THIS_MONTH:
                return today.dayOfMonth().withMinimumValue().toDate();
            case LAST_MONTH:
                return today.minusMonths(1).dayOfMonth().withMinimumValue().toDate();
            case THIS_YEAR:
                return today.dayOfYear().withMinimumValue().toDate();
            case LAST_YEAR:
                return today.minusYears(1).dayOfYear().withMinimumValue().toDate();
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public Date getTo() {
        LocalDateTime today = new LocalDateTime().withTime(23, 59, 59, 999);
        switch (name) {
            case TODAY:
                return today.toDate();
            case YESTERDAY:
                return today.minusDays(1).toDate();
            case THIS_WEEK:
                return today.dayOfWeek().withMaximumValue().toDate();
            case LAST_WEEK:
                return today.minusWeeks(1).dayOfWeek().withMaximumValue().toDate();
            case THIS_MONTH:
                return today.dayOfMonth().withMaximumValue().toDate();
            case LAST_MONTH:
                return today.minusMonths(1).dayOfMonth().withMaximumValue().toDate();
            case THIS_YEAR:
                return today.dayOfYear().withMaximumValue().toDate();
            case LAST_YEAR:
                return today.minusYears(1).dayOfYear().withMaximumValue().toDate();
        }
        return null;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
