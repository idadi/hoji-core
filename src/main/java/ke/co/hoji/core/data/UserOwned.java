package ke.co.hoji.core.data;

/**
 * Any object that is bound to a user so that it belongs only to that user and users under his tenancy. Not all objects
 * associated with individual users need to implement this interface. Only objects at the top of the composition
 * hierarchy should bother.
 * <p/>
 * Created by gitahi on 11/12/15.
 */
public interface UserOwned {

    /**
     * Gets the username of the user that this object is bound to.
     *
     * @return the username.
     */
    Integer getUserId();

    /**
     * Sets the username of the user that this object is bound to.
     *
     * @param username of the user.
     */
    void setUserId(Integer username);
}
