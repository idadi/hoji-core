package ke.co.hoji.core.data.model;

import ke.co.hoji.core.data.ConfigObject;
import ke.co.hoji.core.data.Translatable;

/**
 * Represents a translation of a {@link Translatable} component to a given @{@link Language}.
 */
public final class Translation extends ConfigObject {

    /**
     * The component of to be translated.
     */
    private String component;
    /**
     * The actual text of this Translation in a given language.
     */
    private String value;

    public Translation(Integer id, String component, String value) {
        super(id);
        this.component = component;
        this.value = value;
    }

    /**
     * The @{@link Translatable} object for which this is the Translation.
     */
    private Translatable translatable;

    /**
     * The @{@link Language} of this Translation.
     */
    private Language language;

    public Translatable getTranslatable() {
        return translatable;
    }

    public void setTranslatable(Translatable translatable) {
        this.translatable = translatable;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
