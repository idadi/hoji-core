package ke.co.hoji.core.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A cache of {@link ConfigObject}s indexed by their UUIDs.
 *
 * @param <T> the type of ConfigObject cached here.
 *
 * @author gitahi
 */
public class ConfigObjectCache<T extends ConfigObject> {

    /**
     * The {@link Map} object used to store ConfigDataObjects.
     */
    private final Map<Integer, T> map = new ConcurrentHashMap<>();

    /**
     * Gets a ConfigObject of type T from the cache.
     *
     * @param id the id of the {@link ConfigObject} to get
     *
     * @return the cached {@link ConfigObject} if available, null otherwise
     */
    public T get(Integer id) {
        return map.get(id);
    }

    /**
     * Adds a ConfigObject of type T to the cache. If the key is null, the ConfigObject is not added and the cache
     * remains unchanged.
     *
     * @param configObject the ConfigObject to be added to the map
     */
    public void put(T configObject) {
        map.put(configObject.getId(), configObject);
    }

    /**
     * Adds a list of ConfigDataObjects of type T to the cache. If any key is null, the particular ConfigObject is not
     * added.
     *
     * @param configObjects the list of ConfigDataObjects to be added to the map
     */
    public void putAll(List<T> configObjects) {
        for (T t : configObjects) {
            put(t);
        }
    }

    /**
     * Returns the values in the cache in a {@link List}.
     *
     * @return the values in a List.
     */
    public List<T> values() {
        List<T> values = new ArrayList<>();
        values.addAll(map.values());
        return values;
    }

    /**
     * Remove a single item from the cache
     */
    public void remove(T configObject) {
        map.remove(configObject.getId());
    }

    /**
     * Clears the cache.
     */
    public void clear() {
        map.clear();
    }
}
