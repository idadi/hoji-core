package ke.co.hoji.core.data;

import java.io.Serializable;

/**
 * A marker interface for all data objects that are persisted to the database.
 *
 * @author gitahi
 */
public interface HojiObject extends Serializable {

    /**
     * Gets whatever value is used as the unique identifier for this HojiCobject.
     *
     * @return the unique value. May be null if unavailable.
     */
    Object getUniqueId();
}
