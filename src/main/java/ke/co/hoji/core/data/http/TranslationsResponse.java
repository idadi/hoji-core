package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.dto.Language;
import ke.co.hoji.core.data.dto.Translation;

import java.util.List;

/**
 * A (HTTP) request for returning the list of {@link Translation}s associated with the given @{@link Language}s.
 */
public class TranslationsResponse {

    private List<Translation> translations;

    public List<Translation> getTranslations() {
        return translations;
    }

    public void setTranslations(List<Translation> translations) {
        this.translations = translations;
    }
}
