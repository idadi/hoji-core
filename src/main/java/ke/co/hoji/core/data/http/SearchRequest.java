package ke.co.hoji.core.data.http;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Survey;

/**
 * A (HTTP) request from the client to the server to find matching records.
 * <p>
 * Created by gitahi on 11/16/16.
 */
public class SearchRequest {

    /**
     * The id of the @{@link Survey} whose record is to be imported.
     */
    private Integer surveyId;

    /**
     * The {@link Form#id} of the @{@link Form} whose records are to be searched.
     */
    private Integer formId;

    /**
     * The {@link Form#majorVersion} of the @{@link Form} whose records are to be searched.
     */
    private Integer formVersion;

    /**
     * Find records created from this date. No upper date bound if null.
     */
    private Long fromDate;

    /**
     * Find records created to this date. No lower date bound if null.
     */
    private Long toDate;

    /**
     * Find records created by the user with this ID. Null means any user.
     */
    private Integer userId;

    /**
     * Search terms to be matched against @{@link Record#searchTerms}.
     */
    private String searchTerms;

    /**
     * How sensitive the match for search terms should be, with 1 being the most sensitive and 3 the least sensitive.
     */
    private Integer matchSensitivity;

    public SearchRequest() {
    }

    public SearchRequest(Integer surveyId, Integer formId, Integer formVersion, Long fromDate, Long toDate,
                         Integer userId, String searchTerms, Integer matchSensitivity) {
        this.surveyId = surveyId;
        this.formId = formId;
        this.formVersion = formVersion;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.userId = userId;
        this.searchTerms = searchTerms;
        this.matchSensitivity = matchSensitivity;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    public Integer getFormVersion() {
        return formVersion;
    }

    public void setFormVersion(Integer formVersion) {
        this.formVersion = formVersion;
    }

    public Long getFromDate() {
        return fromDate;
    }

    public void setFromDate(Long fromDate) {
        this.fromDate = fromDate;
    }

    public Long getToDate() {
        return toDate;
    }

    public void setToDate(Long toDate) {
        this.toDate = toDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getSearchTerms() {
        return searchTerms;
    }

    public void setSearchTerms(String searchTerms) {
        this.searchTerms = searchTerms;
    }

    public Integer getMatchSensitivity() {
        return matchSensitivity;
    }

    public void setMatchSensitivity(Integer matchSensitivity) {
        this.matchSensitivity = matchSensitivity;
    }
}
