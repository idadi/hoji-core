package ke.co.hoji.core.data.http;

import java.util.List;

/**
 * Created by gitahi on 11/01/16.
 */
public class SurveysResponse {

    private List<SurveyResponse> surveyResponses;

    /**
     * The code of the tenant under which this user is operating. Used to refresh the apps own record of this
     * information in case it has since changed server side.
     */
    private String tenantCode;

    /**
     * The name of the tenant under which this user is operating. Used to refresh the apps own record of this
     * information in case it has since changed server side.
     */
    private String tenantName;

    public SurveysResponse() {
    }

    public SurveysResponse(List<SurveyResponse> surveyResponses) {
        this.surveyResponses = surveyResponses;
    }

    public List<SurveyResponse> getSurveyResponses() {
        return surveyResponses;
    }

    public void setSurveyResponses(List<SurveyResponse> surveyResponses) {
        this.surveyResponses = surveyResponses;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }
}
