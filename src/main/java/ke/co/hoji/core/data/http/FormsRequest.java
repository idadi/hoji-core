package ke.co.hoji.core.data.http;

/**
 * Created by gitahi on 27/08/15.
 */
public class FormsRequest {

    private Integer surveyId;

    public FormsRequest() {
    }

    public FormsRequest(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }
}
