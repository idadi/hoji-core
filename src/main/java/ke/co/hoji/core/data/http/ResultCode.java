package ke.co.hoji.core.data.http;

/**
 * The result code for {@link SearchResponse} and {@link ImportResponse}.
 * <p>
 * Created by geoffreywasilwa on 09/06/2017.
 */
public class ResultCode {

    /**
     * The import executed okay.
     */
    public static final int OKAY = 0;

    /**
     * The survey is out of date and needs to be updated.
     */
    public static final int SURVEY_OUT_OF_DATE = 1;

    /**
     * The survey is not enabled.
     */
    public static final int SURVEY_NOT_ENABLED = 2;

    /**
     * The survey is not published.
     */
    public static final int SURVEY_NOT_PUBLISHED = 3;
    /**
     * The form does not exist.
     */
    public static final int FORM_NOT_FOUND = 4;

}
