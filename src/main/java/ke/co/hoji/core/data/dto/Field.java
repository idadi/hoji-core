package ke.co.hoji.core.data.dto;

import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.DescribableOrderable;
import ke.co.hoji.core.data.Enablable;

import java.util.List;

/**
 * Created by gitahi on 27/08/15.
 */
public class Field extends DescribableOrderable implements Enablable {

    private Integer calculated;

    private String valueScript;

    private String tag;

    private String column;

    private String instructions;

    private Integer missingAction;

    private String minValue;

    private String maxValue;

    private String regexValue;

    private Integer uniqueness;

    private String defaultValue;

    private boolean captioning;

    private boolean searchable;

    private boolean filterable;

    private Integer outputType;

    private boolean responseInheriting;

    private boolean enabled;

    private Integer typeId;

    private ChoiceGroup choiceGroup;

    private List<Rule> rules;

    private Integer parentId;

    private Integer pipeSourceId;

    private Integer choiceFilterFieldId;

    private Integer referenceFieldId;

    private Integer mainRecordFieldId;

    private Integer matrixRecordFieldId;

    private List<Integer> recordFormIds;

    private Integer formId;

    private String missingValue;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public ChoiceGroup getChoiceGroup() {
        return choiceGroup;
    }

    public void setChoiceGroup(ChoiceGroup choiceGroup) {
        this.choiceGroup = choiceGroup;
    }

    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getPipeSourceId() {
        return pipeSourceId;
    }

    public void setPipeSourceId(Integer pipeSourceId) {
        this.pipeSourceId = pipeSourceId;
    }

    public Integer getChoiceFilterFieldId() {
        return choiceFilterFieldId;
    }

    public void setChoiceFilterFieldId(Integer choiceFilterFieldId) {
        this.choiceFilterFieldId = choiceFilterFieldId;
    }

    public Integer getReferenceFieldId() {
        return referenceFieldId;
    }

    public void setReferenceFieldId(Integer referenceFieldId) {
        this.referenceFieldId = referenceFieldId;
    }

    public Integer getMainRecordFieldId() {
        return mainRecordFieldId;
    }

    public void setMainRecordFieldId(Integer mainRecordFieldId) {
        this.mainRecordFieldId = mainRecordFieldId;
    }

    public Integer getMatrixRecordFieldId() {
        return matrixRecordFieldId;
    }

    public void setMatrixRecordFieldId(Integer matrixRecordFieldId) {
        this.matrixRecordFieldId = matrixRecordFieldId;
    }

    public List<Integer> getRecordFormIds() {
        return recordFormIds;
    }

    public void setRecordFormIds(List<Integer> recordFormIds) {
        this.recordFormIds = recordFormIds;
    }

    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    public Integer getCalculated() {
        return calculated;
    }

    public void setCalculated(Integer calculated) {
        this.calculated = calculated;
    }

    public String getValueScript() {
        return valueScript;
    }

    public void setValueScript(String valueScript) {
        this.valueScript = valueScript;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getMissingValue() {
        return missingValue;
    }

    public void setMissingValue(String missingValue) {
        this.missingValue = missingValue;
    }

    public Integer getMissingAction() {
        return missingAction;
    }

    public void setMissingAction(Integer missingAction) {
        this.missingAction = missingAction;
    }

    public String getMinValue() {
        return minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getRegexValue() {
        return regexValue;
    }

    public void setRegexValue(String regexValue) {
        this.regexValue = regexValue;
    }

    public Integer getUniqueness() {
        return uniqueness;
    }

    public void setUniqueness(Integer uniqueness) {
        this.uniqueness = uniqueness;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isCaptioning() {
        return captioning;
    }

    public void setCaptioning(boolean captioning) {
        this.captioning = captioning;
    }

    public boolean isSearchable() {
        return searchable;
    }

    public void setSearchable(boolean searchable) {
        this.searchable = searchable;
    }

    public boolean isFilterable() {
        return filterable;
    }

    public void setFilterable(boolean filterable) {
        this.filterable = filterable;
    }

    public Integer getOutputType() {
        return outputType;
    }

    public void setOutputType(Integer outputType) {
        this.outputType = outputType;
    }

    public boolean isResponseInheriting() {
        return responseInheriting;
    }

    public void setResponseInheriting(boolean responseInheriting) {
        this.responseInheriting = responseInheriting;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return description;
    }
}
