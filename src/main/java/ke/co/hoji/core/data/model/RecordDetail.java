package ke.co.hoji.core.data.model;

import java.io.Serializable;

/**
 * @author gitahi
 */
public class RecordDetail implements Serializable {

    private final Record record;

    public RecordDetail(Record record) {
        this.record = record;
    }

    public Record getRecord() {
        return record;
    }
}
