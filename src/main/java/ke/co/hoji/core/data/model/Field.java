package ke.co.hoji.core.data.model;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.DescribableOrderable;
import ke.co.hoji.core.data.Enablable;
import ke.co.hoji.core.data.Translatable;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.response.Response;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A field inside a {@link Form}. This is basically a representation of a question that may be asked of an interviewee
 * during a {@link Survey}.
 *
 * @author gitahi
 */
public class Field extends DescribableOrderable implements FieldParent, Translatable, EventSubject, Enablable {

    private static final String PLACEHOLDER_PATTERN = "\\$\\{(\\d+)\\}";
    /**
     * Specifies whether this field is calculated server-side only, both server and client side or not at all.
     * Default is {@link Calculated#CLIENT_SERVER}.
     */
    protected Integer calculated = Calculated.CLIENT_SERVER;
    /**
     * The executable script to use to set a value on the Widget for this Field. May be null.
     */
    protected String valueScript;

    /**
     * The tag for this field.
     */
    protected String tag;
    /**
     * The caption for this field.
     */
    protected String caption;
    /**
     * The name of the column or, in the case of a parent field, table that stores data for this Field.
     */
    protected String column;
    /**
     * The instructions associated with this field.
     */
    protected String instructions;
    /**
     * The action to take if this Field is missing a value.
     */
    protected Integer missingAction = 0;
    /**
     * The minimum value allowed for this field.
     */
    protected String minValue;
    /**
     * The minimum value allowed for this field.
     */
    protected String maxValue;
    /**
     * The regular expression for validating this field.
     */
    protected String regexValue;
    /**
     * The uniqueness setting for this Field. See #Uniqueness.
     */
    protected Integer uniqueness = 0;
    /**
     * The default value for this field.
     */
    protected String defaultValue;
    /**
     * True if this field's response is used to caption the record and false otherwise.
     */
    protected boolean captioning;
    /**
     * True if this field's response is used to search for a record
     */
    protected boolean searchable;
    /**
     * True if this field's response is used to filter data output for analysis
     */
    protected boolean filterable;
    /**
     * The {@link OutputType} for this Field.
     */
    protected Integer outputType;
    /**
     * True if thisField should inherit its Response from the previous Record and false otherwise.
     */
    protected boolean responseInheriting;
    /**
     * True if this Field is enabled and false otherwise.
     */
    protected boolean enabled;
    /**
     * The comma-separated list of values that represent a missing value e.g. 99,999,9999
     */
    private String missingValue;

    /**
     * The type of this field. Determines the UI widget to render.
     */
    private FieldType type;

    /**
     * The list of {@link Field#captioning} Fields.
     */
    private final List<Field> captioningFields = new ArrayList<>();

    /**
     * The list of {@link Field#searchable} Fields.
     */
    private final List<Field> searchableFields = new ArrayList<>();

    /**
     * The list of compound unique Fields.
     */
    private final List<Field> compoundUniqueFields = new ArrayList<>();

    /**
     * The {@link ChoiceGroup}s for this field.
     */
    private ChoiceGroup choiceGroup;

    /**
     * The {@link Form} to which this Field belongs.
     */
    private Form form;

    /**
     * The set of forward {@link Rule}s associated with this Field. Forward rules tell the field that is just about to
     * be unloaded (un-rendered) which field should be loaded next, based on the current field's response.
     */
    private List<Rule> forwardSkips;

    /**
     * The set of backward {@link Rule}s associated with this Field. Backward rules tell the field that is just about to
     * be loaded (rendered) whether it should actually go ahead and render based on responses from previous fields.
     */
    private List<Rule> backwardSkips;

    /**
     * The set of general {@link Rule}s associated with this Field. General rules are rules for general purpose use not
     * associated with navigation from one field to another.
     */
    private List<Rule> filterRules;

    /**
     * The list of Fields to be loaded as children by this field. Typically applicable for matrix fields or any other
     * field that may load other fields.
     */
    private List<Field> children;

    /**
     * The field for which this is a child. See {@link Field#children}.
     */
    private Field parent;

    /**
     * The field whose response will be used to filter this Field's choices, if applicable.
     */
    private Field choiceFilterField;

    /**
     * The Field in the form to which this belongs that represents a {@link Reference}.
     */
    private Field referenceField;

    /**
     * The Field in the form to which this belongs that represents a {@link MainRecord}.
     * Points to field in this {@link #form form} that links it to one or more {@link #recordForms record forms}.
     * For reference see Kenya National Survey project's Male Form.
     */
    private Field mainRecordField;

    /**
     * The Field in the form to which this belongs that represents a {@link MatrixRecord}.
     * Points to a matrix field in one of the linked {@link #recordForms record forms}.
     * For reference see Kenya National Survey project's Male Form.
     */
    private Field matrixRecordField;

    private List<Form> recordForms;

    /**
     * The active response for this field.
     */
    private transient Response response;

    public Field() {
        super();
    }

    public Field(Integer id) {
        super(id);
    }

    public Field(Integer id, String name, boolean enabled, String description, BigDecimal ordinal, Boolean captioning,
                 Boolean searchable, Boolean filterable, Integer outputType, Integer missingAction) {
        super(id, name, description, ordinal);
        this.enabled = enabled;
        this.captioning = captioning;
        this.searchable = searchable;
        this.filterable = filterable;
        this.outputType = outputType;
        this.missingAction = missingAction;
    }

    public Integer getCalculated() {
        return calculated;
    }

    public void setCalculated(Integer calculated) {
        this.calculated = calculated;
    }

    public String getValueScript() {
        return valueScript;
    }

    public void setValueScript(String valueScript) {
        this.valueScript = valueScript;
    }

    public String getTag() {
        return tag;
    }

    public String getTagLabel() {
        Integer value = Utils.parseInteger(tag);
        if (value == null) {
            return null;
        }
        return InputFormat.getByValue(value).getLabel();
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getMissingValue() {
        return missingValue;
    }

    public void setMissingValue(String missingValue) {
        this.missingValue = missingValue;
    }

    public Integer getMissingAction() {
        return missingAction;
    }

    public String getMissingActionLabel() {
        return FieldAction.getByValue(missingAction).getLabel();
    }

    public void setMissingAction(Integer missingAction) {
        this.missingAction = missingAction;
    }

    public String getMinValue() {
        return minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getRegexValue() {
        return regexValue;
    }

    public void setRegexValue(String regexValue) {
        this.regexValue = regexValue;
    }

    public Integer getUniqueness() {
        return uniqueness;
    }

    public void setUniqueness(Integer uniqueness) {
        this.uniqueness = uniqueness;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isCaptioning() {
        return captioning;
    }

    public void setCaptioning(boolean captioning) {
        this.captioning = captioning;
    }

    public boolean isSearchable() {
        return searchable;
    }

    public void setSearchable(boolean searchable) {
        this.searchable = searchable;
    }

    public boolean isFilterable() {
        return filterable;
    }

    public void setFilterable(boolean filterable) {
        this.filterable = filterable;
    }

    public Integer getOutputType() {
        return outputType;
    }

    public void setOutputType(Integer outputType) {
        this.outputType = outputType;
    }

    public boolean isResponseInheriting() {
        return responseInheriting;
    }

    public void setResponseInheriting(boolean responseInheriting) {
        this.responseInheriting = responseInheriting;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the caption for this field if caption is not null. Otherwise sets the caption to the name of this Field.
     */
    public String getCaption() {
        if (caption == null) {
            caption = getName();
        }
        return caption;
    }

    public FieldType getType() {
        return type;
    }

    public void setType(FieldType type) {
        this.type = type;
    }

    @Override
    public List<Field> getCaptioningFields() {
        return captioningFields;
    }

    @Override
    public List<Field> getSearchableFields() {
        return searchableFields;
    }

    @Override
    public List<Field> getCompoundUniqueFields() {
        return compoundUniqueFields;
    }

    @Override
    public boolean addCaptioningField(Field captioningField) {
        if (captioningField.getType().isNull()) {
            return false;
        }
        return captioningFields.add(captioningField);
    }

    @Override
    public boolean addSearchableField(Field searchableField) {
        if (searchableField.getType().isNull()) {
            return false;
        }
        return searchableFields.add(searchableField);
    }

    @Override
    public boolean addCompoundUniqueField(Field compoundUniqueField) {
        if (compoundUniqueField.getType().isNull()) {
            return false;
        }
        return compoundUniqueFields.add(compoundUniqueField);
    }

    public ChoiceGroup getChoiceGroup() {
        return choiceGroup;
    }

    public void setChoiceGroup(ChoiceGroup choiceGroup) {
        this.choiceGroup = choiceGroup;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public List<Rule> getForwardSkips() {
        return forwardSkips;
    }

    public void setForwardSkips(List<Rule> forwardSkips) {
        this.forwardSkips = forwardSkips;
    }

    public List<Rule> getBackwardSkips() {
        return backwardSkips;
    }

    public void setBackwardSkips(List<Rule> backwardSkips) {
        this.backwardSkips = backwardSkips;
    }

    public List<Rule> getFilterRules() {
        return filterRules;
    }

    public void setFilterRules(List<Rule> filterRules) {
        this.filterRules = filterRules;
    }

    public boolean hasForwardSkips() {
        return !forwardSkips.isEmpty();
    }

    public boolean hasBackwardSkips() {
        return !backwardSkips.isEmpty();
    }

    public boolean hasFilterRules() {
        return !filterRules.isEmpty();
    }

    @Override
    public List<Field> getChildren() {
        return children;
    }

    public void setChildren(List<Field> children) {
        this.children = children;
    }

    public Field getParent() {
        return parent;
    }

    public void setParent(Field parent) {
        this.parent = parent;
    }

    public Field getChoiceFilterField() {
        return choiceFilterField;
    }

    public void setChoiceFilterField(Field choiceFilterField) {
        this.choiceFilterField = choiceFilterField;
    }

    public Field getMatrixRecordField() {
        return matrixRecordField;
    }

    public void setMatrixRecordField(Field matrixRecordField) {
        this.matrixRecordField = matrixRecordField;
    }

    public Field getMainRecordField() {
        return mainRecordField;
    }

    public void setMainRecordField(Field mainRecordField) {
        this.mainRecordField = mainRecordField;
    }

    public Field getReferenceField() {
        return referenceField;
    }

    public void setReferenceField(Field referenceField) {
        this.referenceField = referenceField;
    }

    public List<Form> getRecordForms() {
        return recordForms;
    }

    public void setRecordForms(List<Form> recordForms) {
        this.recordForms = recordForms;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    @Override
    public String getShortName() {
        String shortName = null;
        if (name != null) {
            shortName = name;
        }
        if (description != null) {
            if (shortName != null) {
                shortName += (". " + description);
            } else {
                shortName = description;
            }
        }
        if (shortName != null && shortName.length() > SHORT_NAME_LENGTH) {
            return shortName.substring(0, SHORT_NAME_LENGTH) + " ...";
        }
        return shortName;
    }

    @Override
    public String toString() {
        return description;
    }

    /**
     * Checks whether this field the specified function in its calculation script. This method takes into account
     * whether the function has been commented or not. It does not check whether the function is well formed.
     *
     * @param function the name of the function.
     *
     * @return true if the script contains the function and false otherwise.
     */
    public boolean hasScriptFunction(String function) {
        if (StringUtils.isBlank(valueScript)) {
            return false;
        }
        String spaceLessString = valueScript.replaceAll("[\\n\\t ]", "");
        return spaceLessString.contains("function" + function + "(")
            && (
            !spaceLessString.contains("//function" + function + "(")
                && !spaceLessString.contains("*function" + function + "(")
        );
    }

    public boolean needsInterpolation() {
        return textContainsPlaceholders(this.name)
            || textContainsPlaceholders(this.description)
            || textContainsPlaceholders(this.instructions)
            || textContainsPlaceholders(this.defaultValue)
            || textContainsPlaceholders(this.maxValue)
            || textContainsPlaceholders(this.minValue);
    }

    private boolean hasSiblings() {
        if (this.form == null) {
            return false;
        }
        if (this.form.getFields() == null) {
            return false;
        }
        if (this.form.getFields().size() == 0) {
            return false;
        }
        return true;
    }

    public String getHeader() {
        if (StringUtils.isNotBlank(column)) {
            return column;
        }
        return getResolvedHeader();
    }

    /**
     * Parses field description and replaces any placeholders.
     * This method works on the assumption that this field is full formed with reference to a containing form and the form also contains references to fields.
     * @return a resolved field description
     */
    public String getResolvedHeader() {
        if (needsInterpolation() && hasSiblings()) {
            Matcher matcher = Pattern.compile(PLACEHOLDER_PATTERN).matcher(this.description);
            Map<Integer, Field> refs = new HashMap<>();
            for(Field field : this.form.getFields()) {
                refs.put(field.getId(), field);
            }
            String header = description;
            while(matcher.find()) {
                int fieldId = Integer.valueOf(matcher.group(1));
                Field ref = refs.get(fieldId);
                String replacement = "_________";
                if (ref != null) {
                    replacement = ref.getColumn() != null ? ref.getColumn() : ref.getName();
                }
                header = header.replace("${"+ fieldId +"}", String.format("[%s]", replacement));
            }
            return header;
        }
        return description;
    }

    public String getDataElementType(String renderingType) {
        String dataElementType = DataElement.Type.STRING;
        if (numberQualifiesAsNumber()) {
            dataElementType = DataElement.Type.NUMBER;
        }
        if (choiceQualifiesAsNumber(renderingType)) {
            dataElementType = DataElement.Type.NUMBER;
        }
        if (type.isDate()) {
            dataElementType = DataElement.Type.DATE;
        }
        if (type.isTime()) {
            dataElementType = DataElement.Type.TIME;
        }
        if (type.isLocation()) {
            dataElementType = DataElement.Type.NUMBER;
        }
        return dataElementType;
    }

    private boolean numberQualifiesAsNumber() {
        return type.isNumber() && (outputType == null || outputType == 0 || outputType.equals(OutputType.NUMBER));
    }

    private boolean choiceQualifiesAsNumber(String renderingType) {
        return type.isChoice() &&
            Integer.valueOf(OutputType.NUMBER).equals(outputType) &&
            form.getOutputStrategy().contains(renderingType);
    }

    public boolean qualifiesAsNumber(String renderingType) {
        if (type.isNumber()) {
            return outputType == null || outputType == 0 || outputType.equals(OutputType.NUMBER);
        }
        if (type.isChoice()) {
            return outputType != null && outputType.equals(OutputType.NUMBER) && form.getOutputStrategy().contains(renderingType);
        }
        return false;
    }

    public boolean isMatrix() {
        if (getType() != null) {
            return getType().isParent();
        }
        return false;
    }

    private boolean textContainsPlaceholders(String text) {
        if (text != null && Pattern.compile(PLACEHOLDER_PATTERN).matcher(text).find()) {
            return true;
        }
        return false;
    }

    @Override
    public LinkedHashMap<String, Object> descriptiveProperties() {
        LinkedHashMap descriptiveProperties = super.descriptiveProperties();
        descriptiveProperties.put("Type", type.getName());
        return descriptiveProperties;
    }

    @Override
    public String getTranslatableName() {
        return getClass().getSimpleName().toUpperCase();
    }

    @Override
    public List<String> getTranslatableComponents() {
        return new ArrayList<>(Arrays.asList(TranslatableComponent.DESCRIPTION, TranslatableComponent.INSTRUCTIONS));
    }

    /**
     * Maps {@link Field field's} uniqueness setting from code to label
     */
    public enum Uniqueness {

        NONE(0, "None"),
        SIMPLE(1, "Simple"),
        COMPOUND(2, "Compound");

        private final int value;
        private final String label;

        Uniqueness(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }

        public static Uniqueness getByValue(int value) {
            switch (value) {
                case 0:
                    return Uniqueness.NONE;
                case 1:
                    return Uniqueness.SIMPLE;
                case 2:
                    return Uniqueness.COMPOUND;
                default:
                    return null;
            }
        }
    }

    /**
     * Maps {@link Field field's} actions, such as missing action, from code to label
     */
    public enum FieldAction {
        DO_NOT_ALLOW(2, "Do not allow"),
        WARN_ALLOW(1, "Warn but allow"),
        ALLOW(0, "Always allow");

        private final int value;
        private final String label;

        FieldAction(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }

        public static FieldAction getByValue(int value) {
            switch (value) {
                case 0:
                    return FieldAction.ALLOW;
                case 1:
                    return FieldAction.WARN_ALLOW;
                case 2:
                    return FieldAction.DO_NOT_ALLOW;
                default:
                    return null;
            }
        }
    }

    /**
     * Supported input formats for a text and numeric fields.
     */
    public enum InputFormat {

        ALL_CAPS(2, "Upper case"),
        EMAIL(6, "Lower case"),
        TITLE(3, "Title case"),
        SENTENCE(4, "Sentence case"),
        NUMBER_UNSIGNED(8, "Unsigned number"),
        NUMBER_SIGNED(9, "Signed number"),
        DECIMAL_UNSIGNED(12, "Unsigned decimal"),
        DECIMAL_SIGNED(13, "Signed decimal"),
        DATETIME(16, "Formatted number");

        private final int value;
        private final String label;

        InputFormat(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }

        public static InputFormat getByValue(int value) {
            switch (value) {
                case 2:
                    return InputFormat.ALL_CAPS;
                case 3:
                    return InputFormat.TITLE;
                case 4:
                    return InputFormat.SENTENCE;
                case 6:
                    return InputFormat.EMAIL;
                case 8:
                    return InputFormat.NUMBER_UNSIGNED;
                case 9:
                    return InputFormat.NUMBER_SIGNED;
                case 12:
                    return InputFormat.DECIMAL_UNSIGNED;
                case 13:
                    return InputFormat.DECIMAL_SIGNED;
                case 16:
                    return InputFormat.DATETIME;
                default:
                    return InputFormat.TITLE;
            }
        }
    }

    /**
     * The components of this Field that are to be translated.
     */
    public final static class TranslatableComponent {

        public final static String DESCRIPTION = "DESCRIPTION";
        public final static String INSTRUCTIONS = "INSTRUCTIONS";
    }

    public static class ValidationAction {

        public static final int NONE = ValidationResult.OKAY;
        public static final int WARN = ValidationResult.WARN;
        public static final int STOP = ValidationResult.STOP;
    }

    /**
     * A value to be used for validating min and max constraints.
     */
    public static class ValidationValue {

        /**
         * The actual value to use for the comparison.
         */
        private final Comparable value;

        /**
         * The {@link ValidationAction} to take if the constraint is violated.
         */
        private final int action;

        public ValidationValue(Comparable value, int action) {
            this.value = value;
            this.action = action;
        }

        public Comparable getValue() {
            return value;
        }

        public int getAction() {
            return action;
        }
    }

    /**
     * Possible data output types. See
     * https://bitbucket.org/idadi/hoji-core/issues/615/server-add-support-for-custom-output-types
     * for a detailed description.
     */
    public static class OutputType {

        public static final int NUMBER = 1;
        public static final int STRING = 2;
    }

    /**
     * Options for {@link #calculated} property.
     */
    public static class Calculated {

        public static final int NOT_CALCULATED = 1; //default
        public static final int CLIENT_SERVER = 2;
        public static final int SERVER = 3;
    }

    /**
     * Functions that may be added to calculation scripts and that the application can call.
     */
    public static class ScriptFunctions {

        /**
         * Called to calculate the value of a calculated field.
         */
        public static final String CALCULATE = "calculate";

        /**
         * Called to validate the response to a field. This method is passed the language id of the current language.
         */
        public static final String VALIDATE = "validate";
    }
}
