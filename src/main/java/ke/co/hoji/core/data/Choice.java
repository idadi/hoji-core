package ke.co.hoji.core.data;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.response.RankingResponse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * A coded option that may be supplied as an answer to a {@link Field} either as a single selection or a multiple
 * selection.
 *
 * @author gitahi
 */
public class Choice extends Describable implements Translatable, Orderable, UserOwned {

    /**
     * The components of this Choice that are to be translated.
     */
    public final static class TranslatableComponent {

        public final static String NAME = "NAME";
        public final static String DESCRIPTION = "DESCRIPTION";
    }

    /**
     * The code for this Choice. May be provided as an alternative to the name (also known as label) for the
     * purposes of data analysis.
     */
    private String code;

    /**
     * The (possibly shared) "natural" order position of this Choice. Unlike ordinal this attribute may be shared by
     * several Choices.
     */
    private int scale;

    /**
     * Deselects sibling choices. If true, selecting this choice for a {@link Field} that allows multiple choices
     * deselects all other choices.
     */
    private boolean loner;

    /**
     * The ordinal position of this Choice in its current {@link ChoiceGroup}.
     */
    private BigDecimal ordinal;

    /**
     * This Choice's parent Choice. Choices are filtered based on the parent before rendering. This allows us to
     * create hierarchies such as Car Make - Car Model - Car Trim.
     */
    private Choice parent;

    /**
     * The id of the user who owns this Survey.
     */
    protected Integer userId;

    /**
     * A random id used for randomizing choice order if set as such in {@link ChoiceGroup#orderingStrategy}.
     */
    private transient Integer randomId;

    /**
     * True if this Choice has already been entered and is supposed to be unique for the current {@link Field}. Used
     * for preemptively indicating to the user what has already been entered.
     */
    private transient boolean alreadyEntered;

    /**
     * An integer used to assign ranks to choices in {@link RankingResponse}s.
     */
    private transient Integer rank;

    public Choice() {
        super();
    }

    public Choice(Integer id) {
        super(id);
    }

    public Choice(Integer id, String name, String code, int scale, boolean loner) {
        super(id, name);
        this.code = code;
        this.scale = scale;
        this.loner = loner;
    }

    public Choice(Integer id, String name, String description, String code, int scale, boolean loner) {
        super(id, name, description);
        this.code = code;
        this.scale = scale;
        this.loner = loner;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getScale() {
        return scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    public boolean isLoner() {
        return loner;
    }

    public void setLoner(boolean loner) {
        this.loner = loner;
    }

    @Override
    public BigDecimal getOrdinal() {
        return ordinal;
    }

    @Override
    public void setOrdinal(BigDecimal ordinal) {
        this.ordinal = ordinal;
    }

    public Choice getParent() {
        return parent;
    }

    public void setParent(Choice parent) {
        this.parent = parent;
    }

    @Override
    public Integer getUserId() {
        return userId;
    }

    @Override
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRandomId() {
        return randomId;
    }

    public void setRandomId(Integer randomId) {
        this.randomId = randomId;
    }

    public boolean isAlreadyEntered() {
        return alreadyEntered;
    }

    public void setAlreadyEntered(boolean alreadyEntered) {
        this.alreadyEntered = alreadyEntered;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    @Override
    public int compareTo(Object that) {
        BigDecimal thatOrdinal = BigDecimal.ZERO;
        if (that instanceof Choice) {
            thatOrdinal = ((Choice) that).getOrdinal();
        } else if (that instanceof BigDecimal) {
            thatOrdinal = (BigDecimal) that;
        }
        return ordinal.compareTo(thatOrdinal);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Choice)) {
            return false;
        }
        Choice o = (Choice) other;
        return (Objects.equals(this.id, o.id)) &&
            (this.name == null ? o.name == null : this.name.equalsIgnoreCase(o.name));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (this.id == null ? 0 : this.id);
        result = 31 * result + (this.name == null ? 0 : this.name.hashCode());
        return result;
    }

    //For creating copies of choices
    public Choice(Choice choice) {
        this(choice.id, choice.name, choice.description, choice.code, choice.scale, choice.loner);
        this.setUserId(choice.getUserId());
        if (choice.getParent() != null) {
            if (choice.parent.equals(choice)) {
                this.setParent(this);
            } else {
                this.setParent(new Choice(choice.getParent()));
            }
        }
    }

    @Override
    public String getTranslatableName() {
        return getClass().getSimpleName().toUpperCase();
    }

    @Override
    public List<String> getTranslatableComponents() {
        return new ArrayList<>(Arrays.asList(TranslatableComponent.NAME, TranslatableComponent.DESCRIPTION));
    }
}
