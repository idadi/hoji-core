package ke.co.hoji.core.data.http;

/**
 * Created by gitahi on 14/01/16.
 */
public class SignUpResponse {

    public static class StatusCode {

        public static final int SUCCEEDED = 0;
        public static final int EXISTS = 1;
        public static final int FAILED = 2;
    }

    private Integer userId;
    private String userCode;
    private int statusCode;

    public SignUpResponse() {
    }

    public SignUpResponse(int statusCode) {
        this.statusCode = statusCode;
    }

    public SignUpResponse(Integer userId, String userCode, int statusCode) {
        this.userId = userId;
        this.userCode = userCode;
        this.statusCode = statusCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
