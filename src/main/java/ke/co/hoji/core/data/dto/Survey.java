package ke.co.hoji.core.data.dto;

import ke.co.hoji.core.data.Enablable;
import ke.co.hoji.core.data.Nameable;
import ke.co.hoji.core.data.UserOwned;

/**
 * Created by gitahi on 26/08/15.
 */
public class Survey extends Nameable implements Enablable, UserOwned {

    private String code;

    private Integer status;

    private Integer userId;

    private boolean enabled;

    private int access;

    private boolean free;

    public Survey() {
    }

    public Survey(Integer id, String name, boolean enabled, String code) {
        super(id, name);
        this.enabled = enabled;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public Integer getUserId() {
        return userId;
    }

    @Override
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getAccess() {
        return access;
    }

    public void setAccess(int access) {
        this.access = access;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }
}
