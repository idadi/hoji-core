package ke.co.hoji.core.data.model;

import ke.co.hoji.core.data.Choice;

/**
 * @author gitahi
 */
public final class MainRecord extends RecordDetail {

    public static class Stage {

        public static final int DRAFT = 1;
        public static final int COMPLETE = 2;
        public static final int UPLOADED = 3;

        private final String name;
        private final int value;

        public Stage(String name, int value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public int getValue() {
            return value;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }

    private Integer stage;
    private Integer version;
    private Long dateCompleted;
    private Long dateUploaded;
    private Integer userId;
    private String deviceId;

    private Double startLatitude;
    private Double startLongitude;
    private Double startAccuracy;
    private Long startAge;
    private String startAddress;
    private Double endLatitude;
    private Double endLongitude;
    private Double endAccuracy;
    private Long endAge;
    private String endAddress;
    private String formVersion;
    private String appVersion;

    public MainRecord(Record record) {
        super(record);
        this.setStage(Stage.DRAFT);
        this.setFormVersion(((Form) record.getFieldParent()).getFullVersion());
    }

    public Integer getStage() {
        return stage;
    }

    public void setStage(Integer stage) {
        this.stage = stage;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(Long dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public Long getDateUploaded() {
        return dateUploaded;
    }

    public void setDateUploaded(Long dateUploaded) {
        this.dateUploaded = dateUploaded;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Double getStartLatitude() {
        return startLatitude;
    }

    public void setStartLatitude(Double startLatitude) {
        this.startLatitude = startLatitude;
    }

    public Double getStartLongitude() {
        return startLongitude;
    }

    public void setStartLongitude(Double startLongitude) {
        this.startLongitude = startLongitude;
    }

    public Double getStartAccuracy() {
        return startAccuracy;
    }

    public void setStartAccuracy(Double startAccuracy) {
        this.startAccuracy = startAccuracy;
    }

    public Long getStartAge() {
        return startAge;
    }

    public void setStartAge(Long startAge) {
        this.startAge = startAge;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public Double getEndLatitude() {
        return endLatitude;
    }

    public void setEndLatitude(Double endLatitude) {
        this.endLatitude = endLatitude;
    }

    public Double getEndLongitude() {
        return endLongitude;
    }

    public void setEndLongitude(Double endLongitude) {
        this.endLongitude = endLongitude;
    }

    public Double getEndAccuracy() {
        return endAccuracy;
    }

    public void setEndAccuracy(Double endAccuracy) {
        this.endAccuracy = endAccuracy;
    }

    public Long getEndAge() {
        return endAge;
    }

    public void setEndAge(Long endAge) {
        this.endAge = endAge;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public String getFormVersion() {
        return formVersion;
    }

    public void setFormVersion(String formVersion) {
        this.formVersion = formVersion;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public static int stage(Choice endStatus) {
        int scale = endStatus != null ? endStatus.getScale() : -1;
        switch (scale) {
            case Stage.DRAFT:
                return Stage.DRAFT;
            case Stage.COMPLETE:
                return Stage.COMPLETE;
            case Stage.UPLOADED:
                return Stage.UPLOADED;
            default:
                return Stage.DRAFT;
        }
    }
}
