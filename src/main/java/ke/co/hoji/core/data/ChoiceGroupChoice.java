package ke.co.hoji.core.data;

import java.math.BigDecimal;

/**
 * Created by gitahi on 03/11/15.
 */
public class ChoiceGroupChoice implements Orderable<ChoiceGroupChoice> {

    private ChoiceGroup choiceGroup;
    private Choice choice;
    private BigDecimal ordinal;

    public ChoiceGroup getChoiceGroup() {
        return choiceGroup;
    }

    public void setChoiceGroup(ChoiceGroup choiceGroup) {
        this.choiceGroup = choiceGroup;
    }

    public Choice getChoice() {
        return choice;
    }

    public void setChoice(Choice choice) {
        this.choice = choice;
    }

    public BigDecimal getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(BigDecimal ordinal) {
        this.ordinal = ordinal;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChoiceGroupChoice)) {
            return false;
        }
        ChoiceGroupChoice o = (ChoiceGroupChoice)obj;
        return this.choiceGroup.equals(o.choiceGroup) && this.choice.equals(o.choice);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + this.choice.hashCode();
        result = 31 * result + this.choiceGroup.hashCode();
        return result;
    }

    @Override
    public int compareTo(ChoiceGroupChoice choiceGroupChoice) {
        return this.ordinal.compareTo(choiceGroupChoice.ordinal);
    }
}
