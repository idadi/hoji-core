package ke.co.hoji.core.data.model;

import ke.co.hoji.core.data.DataObject;
import ke.co.hoji.core.data.NavigableList;

import java.util.ArrayList;
import java.util.List;

/**
 * Models a record in a table, such as an individual questionnaire or a single record in a matrix.
 *
 * @author gitahi
 */
public class Record extends DataObject {

    /**
     * The {@link FieldParent} that provides the fields that can potentially be answered in this record.
     */
    private final FieldParent fieldParent;
    /**
     * A String that a human may use to identify this record.
     */
    protected String caption;
    /**
     * A String that may be used to find this record.
     */
    protected String searchTerms;
    /**
     * A compound key based on the compound unique {@link Field fields}s designated in this Record's
     * {@link FieldParent}.
     */
    protected String uniqueValue;
    /**
     * A record is main if it is the top level record, as opposed to a nested record such as a matrix record
     */
    protected boolean main;
    /**
     * A record is a test record if it was submitted from a device in test mode.
     */
    protected boolean test;
    /**
     * The date when this record was created in Unix time.
     */
    protected Long dateCreated;
    /**
     * The date when this record was last updated in Unix time.
     */
    protected Long dateUpdated;

    /**
     * The {@link LiveField} for this record that hasn't yet been saved and is about to be saved.
     */
    private LiveField activeLiveField;

    /**
     * The last {@link Field} in this record.
     */
    private Field lastField;

    /**
     * Fields in this OldRecord that have actually been answered.
     */
    private List<LiveField> liveFields;

    private RecordDetail detail;

    public Record(FieldParent fieldParent, boolean main, boolean test) {
        this(fieldParent, main, test, null);
    }

    public Record(FieldParent fieldParent, boolean main, boolean test, String caption) {
        this.main = main;
        this.test = test;
        this.fieldParent = fieldParent;
        this.caption = caption;
    }

    public FieldParent getFieldParent() {
        return fieldParent;
    }

    public List<Field> getFields() {
        return fieldParent.getChildren();
    }

    public LiveField getActiveLiveField() {
        return activeLiveField;
    }

    public void setActiveLiveField(LiveField activeLiveField) {
        this.activeLiveField = activeLiveField;
    }

    public Field getLastField() {
        return lastField;
    }

    public void setLastField(Field lastField) {
        this.lastField = lastField;
    }

    public List<LiveField> getLiveFields() {
        if (liveFields == null) {
            liveFields = new ArrayList();
        }
        return liveFields;
    }

    public void setLiveFields(List<LiveField> liveFields) {
        this.liveFields = liveFields;
    }

    /**
     * @return the first {@link LiveField} in the ordered list of answered fields
     */
    public LiveField first() {
        return new NavigableList<>(getLiveFields()).first();
    }

    /**
     * @return the last {@link LiveField} in the ordered list of answered fields
     */
    public LiveField last() {
        return new NavigableList<>(getLiveFields()).last();
    }

    /**
     * Finds the {@link LiveField} associated with a given {@link Field}.
     *
     * @param field the Field for which to find the corresponding LiveField.
     * @return the {@link LiveField} associated with the {@link Field} passed.
     */
    public LiveField find(Field field) {
        for (LiveField liveField : getLiveFields()) {
            if (liveField.getField().equals(field)) {
                return liveField;
            }
        }
        return null;
    }

    /**
     * Finds the {@link LiveField} associated with the {@link Field} with the given id.
     *
     * @param fieldId the id of the Field for which to find the corresponding LiveField.
     * @return the {@link LiveField} associated with the {@link Field} passed.
     */
    public LiveField find(Integer fieldId) {
        Field field = new Field(fieldId);
        return find(field);
    }

    /**
     * @param liveField the {@link LiveField} you want to know whether it is contained in the list of answered fields.
     * @return true if the list of answered fields contains the {@link LiveField} passed
     */
    public boolean contains(LiveField liveField) {
        return getLiveFields().contains(liveField);
    }

    /**
     * Adds a {@link LiveField} to the list of answered Fields in this Record. If the added LiveField is equal to {@link
     * Record#activeLiveField}, then {@link Record#activeLiveField} is set to null since once saved a LiveField is no
     * longer active.
     *
     * @param liveField the LiveField to addLiveField
     * @return true if the passed LiveField is added and false otherwise.
     */
    public boolean addLiveField(LiveField liveField) {
        boolean added = false;
        if (!contains(liveField)) {
            added = getLiveFields().add(liveField);
            if (added) {
                setActiveLiveField(null);
                if (!this.isMain()) {
                    Record main = ((MatrixRecord) this.getDetail()).getMainRecord();
                    LiveField parent = main.getActiveLiveField();
                    main.addLiveField(parent);
                }
            }
        }
        return added;
    }

    public RecordDetail getDetail() {
        return detail;
    }

    public void setDetail(RecordDetail detail) {
        this.detail = detail;
    }

    /**
     * Returns the {@link LiveField} instance from a List of {@link LiveField}s based on the {@link Field} passed.
     *
     * @param field      the {@link Field} by which to findMainRecords.
     * @param liveFields the {@link LiveField}s within which to findMainRecords
     * @return the {@link LiveField} found.
     */
    public LiveField getLiveField(Field field, List<LiveField> liveFields) {
        LiveField liveField = null;
        for (LiveField lf : liveFields) {
            if (lf.getField().equals(field)) {
                liveField = lf;
                break;
            }
        }
        return liveField;
    }

    public LiveField getLiveField(Field field) {
        return getLiveField(field, getLiveFields());
    }

    @Override
    public String toString() {
        if (this.caption != null && !caption.isEmpty()) {
            return caption;
        }
        if (this.getUuid() != null) {
            return this.getUuid();
        }
        return "";
    }

    public int computeProgress() {
        int progress = 0;
        if (main) {
            MainRecord mainRecord = (MainRecord) detail;
            if (mainRecord != null) {
                if (mainRecord.getStage() != MainRecord.Stage.DRAFT) {
                    progress = 100;
                } else {
                    Field last;
                    if (liveFields != null && !liveFields.isEmpty()) {
                        last = new NavigableList<>(liveFields).last().getField();
                    } else {
                        last = lastField;
                    }
                    if (last != null) {
                        int numerator = fieldParent.getChildren().indexOf(last) + 1;
                        int denominator = fieldParent.getChildren().size();
                        progress = (numerator * 100) / denominator;
                    }
                }
            }
        }
        return progress;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getSearchTerms() {
        return searchTerms;
    }

    public void setSearchTerms(String searchTerms) {
        this.searchTerms = searchTerms;
    }

    public String getUniqueValue() {
        return uniqueValue;
    }

    public void setUniqueValue(String uniqueValue) {
        this.uniqueValue = uniqueValue;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Long dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
}
