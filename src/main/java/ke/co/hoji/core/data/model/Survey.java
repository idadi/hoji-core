package ke.co.hoji.core.data.model;

import ke.co.hoji.core.data.Enablable;
import ke.co.hoji.core.data.Nameable;
import ke.co.hoji.core.data.Translatable;
import ke.co.hoji.core.data.UserOwned;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * A group of {@link Form}s that constitutes the totality of all questions to be asked in a single data collection
 * drive.
 *
 * @author gitahi
 */
public class Survey extends Nameable implements Translatable, EventSubject, Enablable, UserOwned {

    public static final int UNPUBLISHED = 0;
    public static final int PUBLISHED = 1;
    public static final int MODIFIED = 2;
    /**
     * The translatable name of this Translatable. Different from class name to reflect the user-facing nomenclature.
     */
    private static final String TRANSLATABLE_NAME = "PROJECT";
    /**
     * The unique code of the Survey.
     */
    protected String code;
    /**
     * One of {@link #UNPUBLISHED}, {@link #PUBLISHED} or {@link #MODIFIED} status.
     */
    protected Integer status;
    /**
     * The id of the user who owns this Survey.
     */
    protected Integer userId;
    /**
     * True if this Survey is enabled and false otherwise.
     */
    protected boolean enabled;
    /**
     * The @{@link Access} setting for this Survey.
     */
    protected int access;
    /**
     * True if this Survey is free of charge and false otherwise.
     */
    protected boolean free;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public Integer getUserId() {
        return userId;
    }

    @Override
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getAccess() {
        return access;
    }

    public void setAccess(int access) {
        this.access = access;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    /**
     * @return a String representation of the Survey {@link #status}.
     */
    protected String statusString() {
        String surveyStatusString = "UNKNOWN";
        if (status == Survey.PUBLISHED) {
            surveyStatusString = "Published";
        } else if (status == Survey.UNPUBLISHED) {
            surveyStatusString = "Unpublished";
        } else if (status == Survey.MODIFIED) {
            surveyStatusString = "Modified";
        }
        return surveyStatusString;
    }

    /**
     * The components of this Survey that are to be translated.
     */
    public final static class TranslatableComponent {

        public final static String NAME = "NAME";
    }

    /**
     * The id of the Sample Project/Survey.
     */
    public static final int SAMPLE_SURVEY_ID = 302;

    public Survey() {
    }

    public Survey(Integer id, String name, boolean enabled, String code) {
        super(id, name);
        this.enabled = enabled;
        this.code = code;
    }

    /**
     * The set of {@link Form}s in this Survey.
     */
    private List<Form> forms;

    public List<Form> getForms() {
        if (forms != null) {
            Collections.sort(forms);
        }
        return forms;
    }

    public void setForms(List<Form> forms) {
        this.forms = forms;
    }

    /**
     * @return true if this is a sample survey (i.e. for demo purposes) and false otherwise.
     */
    public boolean isSample() {
        return id.equals(SAMPLE_SURVEY_ID);
    }

    @Override
    public LinkedHashMap<String, Object> descriptiveProperties() {
        LinkedHashMap descriptiveProperties = super.descriptiveProperties();
        descriptiveProperties.put("Status", statusString());
        return descriptiveProperties;
    }

    @Override
    public String getTranslatableName() {
        return TRANSLATABLE_NAME;
    }

    @Override
    public List<String> getTranslatableComponents() {
        return new ArrayList<>(Arrays.asList(TranslatableComponent.NAME));
    }

    /**
     * Defines how access to a Survey is controlled.
     */
    public static class Access {

        /**
         * Access is restricted to authorized users only.
         */
        public static final int PRIVATE = 0;

        /**
         * The public have unrestricted access for data entry.
         */
        public static final int PUBLIC_ENTRY = 1;

        /**
         * The public have unrestricted access to view the data.
         */
        public static final int PUBLIC_VIEW = 2;

        /**
         * The public have unrestricted access for data entry and to view the data.
         */
        public static final int PUBLIC_ENTRY_AND_VIEW = 3;
    }
}
