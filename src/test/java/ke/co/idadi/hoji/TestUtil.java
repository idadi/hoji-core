package ke.co.idadi.hoji;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.data.model.Survey;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by gitahi on 26/07/15.
 */
public class TestUtil {

    public static Record createRecordWithoutLiveFields(Integer noOfFields) {
        return createRecord(false, noOfFields);
    }

    public static Record createRecordWithoutLiveFields() {
        return createRecord(false, null);
    }

    public static Record createRecordWithLiveFields(Integer noOfFields) {
        return createRecord(true, noOfFields);
    }

    public static Record createRecordWithLiveFields() {
        return createRecord(true, null);
    }

    private static Record createRecord(boolean liveFields, Integer noOfFields) {
        Survey survey = createSurvey(noOfFields);
        Form form = survey.getForms().get(0);
        Record record = new Record(form, true, false);
        record.setCaption("Test Record");
        if (liveFields) {
            for (Field field : record.getFieldParent().getChildren()) {
                if (field.getId() > 15) {
                    break;
                }
                record.addLiveField(new LiveField(field));
            }
        }
        return record;
    }

    /**
     * @return a new {@link Survey}.
     */
    public static Survey createSurvey(Integer noOfFields) {
        Survey survey = new Survey(1, "Test Project", true, "TS");
        survey.setForms(createForms(survey, noOfFields));
        return survey;
    }

    public static List<Form> createForms(Survey survey, Integer noOfFields) {
        List<Form> forms = new ArrayList<>();
        {
            Form form = new Form(1, "Household", true, BigDecimal.ONE, 0, 0);
            form.setSurvey(survey);
            form.setFields(createFields(form, noOfFields));
            forms.add(form);
        }
        return forms;
    }

    public static List<Field> createFields(Form form, Integer noOfFields) {
        List<Field> fields = new ArrayList<>();
        if (noOfFields == null) {
            noOfFields = 20;
        }
        for (int i = 1; i <= noOfFields; i++) {
            Field field = new Field(i);
            field.setName("Q" + i);
            field.setDescription("Question " + i);
            field.setOrdinal(BigDecimal.valueOf(i));
            field.setForm(form);
            field.setCaptioning(Boolean.FALSE);
            field.setForwardSkips(new ArrayList<Rule>());
            field.setBackwardSkips(new ArrayList<Rule>());
            field.setFilterRules(new ArrayList<Rule>());
            fields.add(field);
        }
        return fields;
    }

    public static Date createDate(int year, int month, int day) {
        return new DateTime(year, month, day, 12, 0, 0, 0).toDate();
    }
}
