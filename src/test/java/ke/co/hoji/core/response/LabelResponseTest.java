package ke.co.hoji.core.response;

import ke.co.hoji.core.data.model.LiveField;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Created by gitahi on 19/08/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class LabelResponseTest {

    @Mock
    private LiveField liveField;

    /**
     * @verifies always return true.
     * @see LabelResponse#isAnswered()
     */
    @Test
    public void isAnswered_shouldAlwaysReturnTrue() throws Exception {
        {
            Response<String, String> response = new LabelResponse(null, liveField, "some value", true);
            Assert.assertThat(response.isAnswered(), Is.is(true));
        }
        {
            Response<String, String> response = new LabelResponse(null, liveField, null, true);
            Assert.assertThat(response.isAnswered(), Is.is(true));
        }
    }
}
