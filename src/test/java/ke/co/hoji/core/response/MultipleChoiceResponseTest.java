package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.Choice.TranslatableComponent;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.service.model.TranslationService;
import org.hamcrest.Matchers;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * Created by gitahi on 06/08/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class MultipleChoiceResponseTest {

    @Mock
    private LiveField liveField;

    /**
     * @verifies set the actual value to the {@link <Map<Choice, Boolean>} represented by the String value if actual is
     * false.
     * @see MultipleChoiceResponse#MultipleChoiceResponse(ke.co.hoji.core.HojiContext, LiveField, Object, Boolean)
     */
    @Test
    public void MultipleChoiceResponse_shouldSetTheActualValueToTheLinkMapChoiceBooleanRepresentedByTheStringValueIfActualIsFalse() throws Exception {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1));
        choices.add(new Choice(2));
        choices.add(new Choice(3));
        choices.add(new Choice(4));
        choices.add(new Choice(5));
        ChoiceGroup choiceGroup = new ChoiceGroup(choices);

        HojiContext hojiContext = Mockito.mock(HojiContext.class);

        Field field = Mockito.mock(Field.class);
        Mockito.when(liveField.getField()).thenReturn(field);
        Mockito.when(field.getChoiceGroup()).thenReturn(choiceGroup);

        LinkedHashMap<Choice, Boolean> expected = new LinkedHashMap<>();
        expected.put(choices.get(0), Boolean.TRUE);
        expected.put(choices.get(1), Boolean.FALSE);
        expected.put(choices.get(2), Boolean.TRUE);
        expected.put(choices.get(3), Boolean.FALSE);
        expected.put(choices.get(4), Boolean.TRUE);

        String input = "1#1|2#0|3#1|4#0|5#1";

        MultipleChoiceResponse multipleChoiceResponse = new MultipleChoiceResponse(hojiContext, liveField, input, false);
        Assert.assertThat(multipleChoiceResponse.getActualValue(), Is.is(expected));
    }

    /**
     * @verifies return the pipe separated string of choice ids and boolean values of the actual {@link <Map<Choice,
     * Boolean>}.
     * @see MultipleChoiceResponse#toStorageValue()
     */
    @Test
    public void toStorageValue_shouldReturnThePipeSeparatedStringOfChoiceIdsAndBooleanValuesOfTheActualLinkMapChoiceBoolean() throws Exception {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1));
        choices.add(new Choice(2));
        choices.add(new Choice(3));
        choices.add(new Choice(4));
        choices.add(new Choice(5));

        LinkedHashMap<Choice, Boolean> input = new LinkedHashMap<>();
        input.put(choices.get(0), Boolean.TRUE);
        input.put(choices.get(1), Boolean.FALSE);
        input.put(choices.get(2), Boolean.TRUE);
        input.put(choices.get(3), Boolean.FALSE);
        input.put(choices.get(4), Boolean.TRUE);

        String expected = "1#1|2#0|3#1|4#0|5#1";

        MultipleChoiceResponse multipleChoiceResponse = new MultipleChoiceResponse(null, liveField, input, true);
        Assert.assertThat(expected, Is.is(multipleChoiceResponse.toStorageValue()));
    }

    /**
     * @verifies return the coma separated names of all the Choices selected.
     * @see MultipleChoiceResponse#displayString()
     */
    @Test
    public void displayString_shouldReturnTheComaSeparatedNamesOfAllTheChoicesSelected() throws Exception {
        List<Choice> choices = new ArrayList<>();
        {
            Choice choice = new Choice(1);
            choice.setName("First");
            choices.add(choice);
        }
        {
            Choice choice = new Choice(2);
            choice.setName("Second");
            choices.add(choice);
        }
        {
            Choice choice = new Choice(3);
            choice.setName("Third");
            choices.add(choice);
        }

        LinkedHashMap<Choice, Boolean> input = new LinkedHashMap<>();
        input.put(choices.get(0), Boolean.TRUE);
        input.put(choices.get(1), Boolean.FALSE);
        input.put(choices.get(2), Boolean.TRUE);

        String expected = "First, Third";
        HojiContext hojiContext = Mockito.mock(HojiContext.class);
        ModelServiceManager modelServiceManager = Mockito.mock(ModelServiceManager.class);
        TranslationService translationService = Mockito.mock(TranslationService.class);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getTranslationService()).thenReturn(translationService);
        for (Choice choice : input.keySet()) {
            if (input.get(choice)) {
                when(translationService.translate(eq(choice), anyInt(), eq(TranslatableComponent.NAME), eq(choice.getName()))).thenReturn(choice.getName());
            }
        }

        MultipleChoiceResponse multipleChoiceResponse = new MultipleChoiceResponse(hojiContext, liveField, input, true);
        Assert.assertThat(expected, Is.is(multipleChoiceResponse.displayString()));
    }

    /**
     * @verifies return empty string if no Choices are selected.
     * @see MultipleChoiceResponse#displayString()
     */
    @Test
    public void displayString_shouldReturnEmptyStringIfNoChoicesAreSelected() throws Exception {
        List<Choice> choices = new ArrayList<>();
        {
            Choice choice = new Choice(1);
            choice.setName("First");
            choices.add(choice);
        }
        {
            Choice choice = new Choice(2);
            choice.setName("Second");
            choices.add(choice);
        }
        {
            Choice choice = new Choice(3);
            choice.setName("Third");
            choices.add(choice);
        }

        LinkedHashMap<Choice, Boolean> input = new LinkedHashMap<>();
        input.put(choices.get(0), Boolean.FALSE);
        input.put(choices.get(1), Boolean.FALSE);
        input.put(choices.get(2), Boolean.FALSE);

        String expected = "";

        HojiContext hojiContext = Mockito.mock(HojiContext.class);
        ModelServiceManager modelServiceManager = Mockito.mock(ModelServiceManager.class);
        TranslationService translationService = Mockito.mock(TranslationService.class);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getTranslationService()).thenReturn(translationService);
        for (Choice choice : input.keySet()) {
            if (input.get(choice)) {
                when(translationService.translate(eq(choice), anyInt(), eq(TranslatableComponent.NAME), eq(choice.getName()))).thenReturn(choice.getName());
            }
        }
        MultipleChoiceResponse multipleChoiceResponse = new MultipleChoiceResponse(hojiContext, liveField, input, true);
        Assert.assertThat(expected, Is.is(multipleChoiceResponse.displayString()));
    }

    /**
     * @verifies return false if actual value is null.
     * @see MultipleChoiceResponse#isAnswered()
     */
    @Test
    public void isAnswered_shouldReturnFalseIfActualValueIsNull() throws Exception {
        MultipleChoiceResponse multipleChoiceResponse = new MultipleChoiceResponse(null, liveField, null, true);
        Assert.assertThat(multipleChoiceResponse.isAnswered(), Is.is(false));
    }

    /**
     * @verifies return true if at least one choice is selected.
     * @see MultipleChoiceResponse#isAnswered()
     */
    @Test
    public void isAnswered_shouldReturnTrueIfAtLeastOneChoiceIsSelected() throws Exception {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1));
        choices.add(new Choice(2));
        choices.add(new Choice(3));
        choices.add(new Choice(4));
        choices.add(new Choice(5));

        LinkedHashMap<Choice, Boolean> input = new LinkedHashMap<>();
        input.put(choices.get(0), Boolean.FALSE);
        input.put(choices.get(1), Boolean.FALSE);
        input.put(choices.get(2), Boolean.FALSE);
        input.put(choices.get(3), Boolean.FALSE);
        input.put(choices.get(4), Boolean.TRUE);

        MultipleChoiceResponse multipleChoiceResponse = new MultipleChoiceResponse(null, liveField, input, true);
        Assert.assertThat(multipleChoiceResponse.isAnswered(), Is.is(true));
    }

    /**
     * @verifies return false if no choice is selected.
     * @see MultipleChoiceResponse#isAnswered()
     */
    @Test
    public void isAnswered_shouldReturnFalseIfNoChoiceIsSelected() throws Exception {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1));
        choices.add(new Choice(2));
        choices.add(new Choice(3));
        choices.add(new Choice(4));
        choices.add(new Choice(5));

        LinkedHashMap<Choice, Boolean> input = new LinkedHashMap<>();
        input.put(choices.get(0), Boolean.FALSE);
        input.put(choices.get(1), Boolean.FALSE);
        input.put(choices.get(2), Boolean.FALSE);
        input.put(choices.get(3), Boolean.FALSE);
        input.put(choices.get(4), Boolean.FALSE);

        MultipleChoiceResponse multipleChoiceResponse = new MultipleChoiceResponse(null, liveField, input, true);
        Assert.assertThat(multipleChoiceResponse.isAnswered(), Is.is(false));
    }

    /**
     * @verifies return the 0 or 1 boolean values of the actual {@link <Map<Choice,Boolean>} stringed together.
     * @see MultipleChoiceResponse#ruleString()
     */
    @Test
    public void ruleString_shouldReturnThe0Or1BooleanValuesOfTheActualLinkMapChoiceBooleanStringedTogether() throws Exception {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1));
        choices.add(new Choice(2));
        choices.add(new Choice(3));
        choices.add(new Choice(4));
        choices.add(new Choice(5));

        LinkedHashMap<Choice, Boolean> input = new LinkedHashMap<>();
        input.put(choices.get(0), Boolean.TRUE);
        input.put(choices.get(1), Boolean.FALSE);
        input.put(choices.get(2), Boolean.TRUE);
        input.put(choices.get(3), Boolean.FALSE);
        input.put(choices.get(4), Boolean.TRUE);

        String expected = "10101";

        MultipleChoiceResponse multipleChoiceResponse = new MultipleChoiceResponse(null, liveField, input, true);
        Assert.assertThat(expected, Is.is(multipleChoiceResponse.ruleString()));
    }

    /**
     * @verifies return ruleValue converted to Long.
     * @see MultipleChoiceResponse#comparableFromRuleString(String)
     */
    @Test
    public void comparableFromRuleString_shouldReturnRuleValueConvertedToLong() throws Exception {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1));
        choices.add(new Choice(2));
        choices.add(new Choice(3));
        choices.add(new Choice(4));
        choices.add(new Choice(5));
        ChoiceGroup choiceGroup = new ChoiceGroup(choices);

        Field field = Mockito.mock(Field.class);
        Mockito.when(liveField.getField()).thenReturn(field);
        Mockito.when(field.getChoiceGroup()).thenReturn(choiceGroup);

        LinkedHashMap<Choice, Boolean> input = new LinkedHashMap<>();
        input.put(choices.get(0), Boolean.TRUE);
        input.put(choices.get(1), Boolean.FALSE);
        input.put(choices.get(2), Boolean.TRUE);
        input.put(choices.get(3), Boolean.FALSE);
        input.put(choices.get(4), Boolean.TRUE);

        Long expected = 21L;
        String ruleValue = "1#1|2#0|3#1|4#0|5#1";

        HojiContext hojiContext = Mockito.mock(HojiContext.class);
        ModelServiceManager modelServiceManager = Mockito.mock(ModelServiceManager.class);

        MultipleChoiceResponse multipleChoiceResponse = new MultipleChoiceResponse(hojiContext, liveField, input, true);
        Assert.assertThat(expected, Is.is(multipleChoiceResponse.comparableFromRuleString(ruleValue)));
    }

    /**
     * @verifies return {@link MultipleChoiceResponse#ruleString()} converted to Long.
     * @see MultipleChoiceResponse#comparableFromActualValue()
     */
    @Test
    public void comparableFromActualValue_shouldReturnLinkRuleStringConvertedToLong() throws Exception {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1));
        choices.add(new Choice(2));
        choices.add(new Choice(3));
        choices.add(new Choice(4));
        choices.add(new Choice(5));

        LinkedHashMap<Choice, Boolean> input = new LinkedHashMap<>();
        input.put(choices.get(0), Boolean.TRUE);
        input.put(choices.get(1), Boolean.FALSE);
        input.put(choices.get(2), Boolean.TRUE);
        input.put(choices.get(3), Boolean.FALSE);
        input.put(choices.get(4), Boolean.TRUE);

        Long expected = 21L;

        MultipleChoiceResponse multipleChoiceResponse = new MultipleChoiceResponse(null, liveField, input, true);
        Assert.assertThat(expected, Is.is(multipleChoiceResponse.comparableFromActualValue()));
    }

    @Test
    public void getValue_shouldReturn_1_Or_0_AsValue() {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1, "Basketball", "B", 0, false));
        choices.add(new Choice(2, "Rugby", "R", 0, false));
        choices.add(new Choice(3, "Football", "F", 0, false));
        ChoiceGroup choiceGroup = new ChoiceGroup(choices);

        Form form = Mockito.mock(Form.class);
        Field field = Mockito.mock(Field.class);
        Mockito.when(liveField.getField()).thenReturn(field);
        Mockito.when(field.getForm()).thenReturn(form);
        String fieldHeader = "What's your favorite sports?";
        Mockito.when(field.getHeader()).thenReturn(fieldHeader);

        LinkedHashMap<Choice, Boolean> input = new LinkedHashMap<>();
        input.put(choices.get(0), Boolean.TRUE);
        input.put(choices.get(1), Boolean.FALSE);
        input.put(choices.get(2), Boolean.TRUE);

        List<DataElement> dataElements = new MultipleChoiceResponse(null, liveField, input, true).getValue(Form.OutputStrategy.TABLE);

        Assert.assertThat(dataElements,
                Matchers.<DataElement>hasItems(
                        allOf(
                                hasProperty("label", is(fieldHeader + " (" + choices.get(0).getName() + ")")),
                                hasProperty("value", is("1"))
                        ),
                        allOf(
                                hasProperty("label", is(fieldHeader + " (" + choices.get(1).getName() + ")")),
                                hasProperty("value", is("0"))
                        ),
                        allOf(
                                hasProperty("label", is(fieldHeader + " (" + choices.get(2).getName() + ")")),
                                hasProperty("value", is("1"))
                        )
                )
        );
    }
}
