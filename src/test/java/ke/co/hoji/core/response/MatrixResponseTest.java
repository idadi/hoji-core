package ke.co.hoji.core.response;

import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.MatrixRecord;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gitahi on 06/08/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class MatrixResponseTest {

    @Mock
    private LiveField liveField;

    /**
     * @verifies return a comma separated list of the string representation of the constituent matrix records.
     * @see MatrixResponse#displayString()
     */
    @Test
    public void displayString_shouldReturnACommaSeparatedListOfTheStringRepresentationOfTheConstituentMatrixRecords() throws Exception {
        List<MatrixRecord> matrixRecords = new ArrayList<>();
        {
            MatrixRecord matrixRecord = Mockito.mock(MatrixRecord.class);
            Mockito.when(matrixRecord.toString()).thenReturn("A");
            matrixRecords.add(matrixRecord);
        }
        {
            MatrixRecord matrixRecord = Mockito.mock(MatrixRecord.class);
            Mockito.when(matrixRecord.toString()).thenReturn("B");
            matrixRecords.add(matrixRecord);
        }
        {
            MatrixRecord matrixRecord = Mockito.mock(MatrixRecord.class);
            Mockito.when(matrixRecord.toString()).thenReturn("C");
            matrixRecords.add(matrixRecord);
        }
        MatrixResponse matrixResponse = new MatrixResponse(null, liveField, matrixRecords, true);
        Assert.assertThat("A, B, C", Is.is(matrixResponse.displayString()));
    }

    /**
     * @verifies return true if actual value is not null and is not empty.
     * @see MatrixResponse#isAnswered()
     */
    @Test
    public void isAnswered_shouldReturnTrueIfActualValueIsNotNullAndIsNotEmpty() throws Exception {
        List<MatrixRecord> matrixRecords = new ArrayList<>();
        {
            MatrixRecord matrixRecord = Mockito.mock(MatrixRecord.class);
            matrixRecords.add(matrixRecord);
        }
        {
            MatrixRecord matrixRecord = Mockito.mock(MatrixRecord.class);
            matrixRecords.add(matrixRecord);
        }
        {
            MatrixRecord matrixRecord = Mockito.mock(MatrixRecord.class);
            matrixRecords.add(matrixRecord);
        }
        MatrixResponse matrixResponse = new MatrixResponse(null, liveField, matrixRecords, true);
        Assert.assertThat(matrixResponse.isAnswered(), Is.is(true));
    }

    /**
     * @verifies return false if actual value is not null and is empty.
     * @see MatrixResponse#isAnswered()
     */
    @Test
    public void isAnswered_shouldReturnFalseIfActualValueIsNotNullAndIsEmpty() throws Exception {
        List<MatrixRecord> matrixRecords = new ArrayList<>();
        MatrixResponse matrixResponse = new MatrixResponse(null, liveField, matrixRecords, true);
        Assert.assertThat(matrixResponse.isAnswered(), Is.is(false));
    }

    /**
     * @verifies return false if actual value is null.
     * @see MatrixResponse#isAnswered()
     */
    @Test
    public void isAnswered_shouldReturnFalseIfActualValueIsNull() throws Exception {
        MatrixResponse matrixResponse = new MatrixResponse(null, liveField, null, true);
        Assert.assertThat(matrixResponse.isAnswered(), Is.is(false));
    }
}
