package ke.co.hoji.core.response;

import ke.co.hoji.core.data.model.LiveField;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Created by gitahi on 10/08/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class TextResponseTest {

    @Mock
    private LiveField liveField;
    private String stringValue = "value";

    /**
     * @verifies return the parameter passed.
     * @see TextResponse#comparableFromRuleString(String)
     */
    @Test
    public void comparableFromRuleString_shouldReturnTheParameterPassed() throws Exception {
        Response response = new TextResponse(null, liveField, stringValue, true);
        Comparable comparable = response.comparableFromRuleString("test");
        Assert.assertThat(comparable, Is.is((Comparable) "test"));
    }
}
