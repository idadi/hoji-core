package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.emptyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by gitahi on 02/03/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TimeResponseTest {

    @Mock
    private LiveField liveField;

    /**
     * @verifies set the actual value to the time represented by the long value if actual is false.
     * @see TimeResponse#TimeResponse(ke.co.hoji.core.HojiContext, LiveField, Object, Boolean)
     */
    @Test
    public void TimeResponse_shouldSetTheActualValueToTheTimeRepresentedByTheLongValueIfActualIsFalse() throws Exception {
        Date input = new Date();
        Long value = input.getTime();
        TimeResponse timeResponse = new TimeResponse(null, liveField, value, false);
        Assert.assertThat(input, Is.is(timeResponse.getActualValue()));
    }

    /**
     * @verifies return the actual time value as a long.
     * @see TimeResponse#toStorageValue()
     */
    @Test
    public void toStorageValue_shouldReturnTheActualTimeValueAsALong() throws Exception {
        Date input = new Date();
        Long value = input.getTime();
        TimeResponse timeResponse = new TimeResponse(null, liveField, input, true);
        Assert.assertThat(value, Is.is(timeResponse.toStorageValue()));
    }

    /**
     * @verifies return the actual time formatted to the current locale.
     * @see TimeResponse#displayString()
     */
    @Test
    public void displayString_shouldReturnTheActualTimeFormattedToTheCurrentLocale() throws Exception {
        Date input = new Date();
        TimeResponse timeResponse = new TimeResponse(null, liveField, input, true);
        Assert.assertThat(Utils.formatTime(input), Is.is(timeResponse.displayString()));
    }

    /**
     * @verifies {@link TimeResponse#displayString(boolean)} returns actual value for answered question
     */
    @Test
    public void getValue_shouldReturnActualValueForAnsweredQuestion() {
        Date dateValue = new Date(1528737366794L);
        List<DataElement> values =
                getResponse("23:23:00", UUID.randomUUID().toString(), dateValue).getValue(Form.OutputStrategy.PIVOT);

        Assert.assertThat(values.get(0).getValue(), is("20:16:06"));
    }

    /**
     * @verifies {@link TimeResponse#displayString(boolean)} returns missing string for response matching specified
     * missing value
     */
    @Test
    public void displayString_shouldReturnMissingStringForResponseMatchingSpecifiedMissingValue() {
        Response response = getResponse("00:00:00", UUID.randomUUID().toString(), new Date(250722000000L));

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link TimeResponse#displayString(boolean)} returns missing string for specified missing values
     * that have leading spaces
     */
    @Test
    public void displayString_shouldReturnMissingStringForSpecifiedMissingValuesThatHaveLeadingSpaces() {
        Response response = getResponse(" 00:00:00", UUID.randomUUID().toString(), new Date(250722000000L));

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link TimeResponse#displayString(boolean)} return missing string for specified missing values
     * that have trailing spaces
     */
    @Test
    public void displayString_shouldReturnMissingStringForSpecifiedMissingValuesThatHaveTrailingSpaces() {
        Response response = getResponse("00:00:00 ", UUID.randomUUID().toString(), new Date(250722000000L));

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link TimeResponse#displayString(boolean)} returns missing string for unanswered question
     */
    @Test
    public void displayString_shouldReturnMissingStringForUnansweredQuestion() {
        Response response = getResponse("00:00:00", UUID.randomUUID().toString(), null);

        Assert.assertThat(response.displayString(true), is("Missing"));
    }

    /**
     * @verifies {@link TimeResponse#displayString(boolean)} returns empty string for skipped question
     */
    @Test
    public void displayString_shouldReturnEmptyStringForSkippedQuestion() {
        Response response = getResponse("00:00:00", null, null);

        Assert.assertThat(response.displayString(true), emptyString());
    }

    /**
     * @verifies {@link TimeResponse#getValue(String)} returns missing string for unanswered question
     */
    @Test
    public void getValue_shouldReturnMissingStringForUnansweredQuestion() {
        List<DataElement> values =
                getResponse("23:23:00", UUID.randomUUID().toString(), null).getValue(Form.OutputStrategy.PIVOT);

        Assert.assertThat(values.get(0).getValue(), is("Missing"));
    }

    /**
     * @verifies {@link TimeResponse#getValue(String)} returns empty string for skipped question
     */
    @Test
    public void getValue_shouldReturnEmptyStringForSkippedQuestion() {
        List<DataElement> values =
                getResponse("23:23:00", null, null).getValue(Form.OutputStrategy.PIVOT);

        Assert.assertThat(values.get(0).getValue(), emptyString());
    }

    private Response getResponse(String missingValue, String liveFieldUuid, Object value) {
        Field time = mock(Field.class);
        when(time.getHeader()).thenReturn("Time");
        when(time.getMissingValue()).thenReturn(missingValue);
        LiveField timeLf = mock(LiveField.class);
        when(timeLf.getUuid()).thenReturn(liveFieldUuid);
        when(timeLf.getField()).thenReturn(time);
        HojiContext hojiContext = mock(HojiContext.class);
        return new TimeResponse(hojiContext, timeLf, value, true);
    }
}
