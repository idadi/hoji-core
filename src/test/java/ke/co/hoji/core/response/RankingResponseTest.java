package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.Choice.TranslatableComponent;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.service.model.TranslationService;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * Created by gitahi on 17/02/18.
 */
@RunWith(MockitoJUnitRunner.class)
public class RankingResponseTest {

    @Mock
    private LiveField liveField;

    /**
     * @verifies set the actual value to the {@link <Map<Choice, Integer>} represented by the String value if actual is
     * false.
     * @see RankingResponse#RankingResponse(HojiContext, LiveField, Object, Boolean)
     */
    @Test
    public void RankingResponse_shouldSetTheActualValueToTheLinkMapChoiceIntegerRepresentedByTheStringValueIfActualIsFalse() throws Exception {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1));
        choices.add(new Choice(2));
        choices.add(new Choice(3));
        choices.add(new Choice(4));
        choices.add(new Choice(5));
        ChoiceGroup choiceGroup = new ChoiceGroup(choices);

        HojiContext hojiContext = Mockito.mock(HojiContext.class);

        Field field = Mockito.mock(Field.class);
        Mockito.when(liveField.getField()).thenReturn(field);
        Mockito.when(field.getChoiceGroup()).thenReturn(choiceGroup);

        LinkedHashMap<Choice, Integer> expected = new LinkedHashMap<>();
        expected.put(choices.get(0), 1);
        expected.put(choices.get(1), 0);
        expected.put(choices.get(2), 3);
        expected.put(choices.get(3), 0);
        expected.put(choices.get(4), 2);

        String input = "1#1|2#0|3#3|4#0|5#2";

        RankingResponse multipleChoiceResponse = new RankingResponse(hojiContext, liveField, input, false);
        Assert.assertThat(multipleChoiceResponse.getActualValue(), Is.is(expected));
    }

    /**
     * @verifies return the pipe separated string of choice ids and rank values of the actual {@link <Map<Choice,
     * Boolean>}.
     * @see RankingResponse#toStorageValue()
     */
    @Test
    public void toStorageValue_shouldReturnThePipeSeparatedStringOfChoiceIdsAndRankValuesOfTheActualLinkMapChoiceBoolean() throws Exception {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1));
        choices.add(new Choice(2));
        choices.add(new Choice(3));
        choices.add(new Choice(4));
        choices.add(new Choice(5));

        LinkedHashMap<Choice, Integer> input = new LinkedHashMap<>();
        input.put(choices.get(0), 1);
        input.put(choices.get(1), 0);
        input.put(choices.get(2), 3);
        input.put(choices.get(3), 0);
        input.put(choices.get(4), 2);

        String expected = "1#1|2#0|3#3|4#0|5#2";

        RankingResponse multipleChoiceResponse = new RankingResponse(null, liveField, input, true);
        Assert.assertThat(expected, Is.is(multipleChoiceResponse.toStorageValue()));
    }

    /**
     * @verifies return the coma separated names of all the Choices selected.
     * @see RankingResponse#displayString()
     */
    @Test
    public void displayString_shouldReturnTheOrderedComaSeparatedNamesOfAllRankedChoices() throws Exception {
        List<Choice> choices = new ArrayList<>();
        {
            Choice choice = new Choice(1);
            choice.setName("First");
            choices.add(choice);
        }
        {
            Choice choice = new Choice(2);
            choice.setName("Second");
            choices.add(choice);
        }
        {
            Choice choice = new Choice(3);
            choice.setName("Third");
            choices.add(choice);
        }
        {
            Choice choice = new Choice(4);
            choice.setName("Fourth");
            choices.add(choice);
        }
        {
            Choice choice = new Choice(5);
            choice.setName("Fifth");
            choices.add(choice);
        }

        LinkedHashMap<Choice, Integer> input = new LinkedHashMap<>();
        input.put(choices.get(0), 2);
        input.put(choices.get(1), 0);
        input.put(choices.get(2), 3);
        input.put(choices.get(3), 0);
        input.put(choices.get(4), 1);

        String expected = "Fifth, First, Third";
        HojiContext hojiContext = Mockito.mock(HojiContext.class);
        ModelServiceManager modelServiceManager = Mockito.mock(ModelServiceManager.class);
        TranslationService translationService = Mockito.mock(TranslationService.class);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getTranslationService()).thenReturn(translationService);
        for (Choice choice : input.keySet()) {
            if (input.get(choice) != 0) {
                when(translationService.translate(eq(choice), anyInt(), eq(TranslatableComponent.NAME), eq(choice.getName()))).thenReturn(choice.getName());
            }
        }

        RankingResponse multipleChoiceResponse = new RankingResponse(hojiContext, liveField, input, true);
        Assert.assertThat(expected, Is.is(multipleChoiceResponse.displayString()));
    }

    /**
     * @verifies return empty string if no Choices are selected.
     * @see RankingResponse#displayString()
     */
    @Test
    public void displayString_shouldReturnEmptyStringIfNoChoicesAreSelected() throws Exception {
        List<Choice> choices = new ArrayList<>();
        {
            Choice choice = new Choice(1);
            choice.setName("First");
            choices.add(choice);
        }
        {
            Choice choice = new Choice(2);
            choice.setName("Second");
            choices.add(choice);
        }
        {
            Choice choice = new Choice(3);
            choice.setName("Third");
            choices.add(choice);
        }
        {
            Choice choice = new Choice(4);
            choice.setName("Fourth");
            choices.add(choice);
        }
        {
            Choice choice = new Choice(5);
            choice.setName("Fifth");
            choices.add(choice);
        }

        LinkedHashMap<Choice, Integer> input = new LinkedHashMap<>();
        input.put(choices.get(0), 0);
        input.put(choices.get(1), 0);
        input.put(choices.get(2), 0);
        input.put(choices.get(3), 0);
        input.put(choices.get(4), 0);

        String expected = "";
        HojiContext hojiContext = Mockito.mock(HojiContext.class);
        ModelServiceManager modelServiceManager = Mockito.mock(ModelServiceManager.class);
        TranslationService translationService = Mockito.mock(TranslationService.class);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getTranslationService()).thenReturn(translationService);
        for (Choice choice : input.keySet()) {
            if (input.get(choice) != 0) {
                when(translationService.translate(eq(choice), anyInt(), eq(TranslatableComponent.NAME), eq(choice.getName()))).thenReturn(choice.getName());
            }
        }

        RankingResponse multipleChoiceResponse = new RankingResponse(hojiContext, liveField, input, true);
        Assert.assertThat(expected, Is.is(multipleChoiceResponse.displayString()));
    }

    /**
     * @verifies return false if actual value is null.
     * @see RankingResponse#isAnswered()
     */
    @Test
    public void isAnswered_shouldReturnFalseIfActualValueIsNull() throws Exception {
        RankingResponse multipleChoiceResponse = new RankingResponse(null, liveField, null, true);
        Assert.assertThat(multipleChoiceResponse.isAnswered(), Is.is(false));
    }

    /**
     * @verifies return true if at least one choice is selected.
     * @see RankingResponse#isAnswered()
     */
    @Test
    public void isAnswered_shouldReturnTrueIfAtLeastOneChoiceIsRanked() throws Exception {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1));
        choices.add(new Choice(2));
        choices.add(new Choice(3));
        choices.add(new Choice(4));
        choices.add(new Choice(5));

        LinkedHashMap<Choice, Integer> input = new LinkedHashMap<>();
        input.put(choices.get(0), 0);
        input.put(choices.get(1), 0);
        input.put(choices.get(2), 0);
        input.put(choices.get(3), 0);
        input.put(choices.get(4), 1);

        RankingResponse multipleChoiceResponse = new RankingResponse(null, liveField, input, true);
        Assert.assertThat(multipleChoiceResponse.isAnswered(), Is.is(true));
    }

    /**
     * @verifies return false if no choice is selected.
     * @see RankingResponse#isAnswered()
     */
    @Test
    public void isAnswered_shouldReturnFalseIfNoChoiceIsRanked() throws Exception {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1));
        choices.add(new Choice(2));
        choices.add(new Choice(3));
        choices.add(new Choice(4));
        choices.add(new Choice(5));

        LinkedHashMap<Choice, Integer> input = new LinkedHashMap<>();
        input.put(choices.get(0), 0);
        input.put(choices.get(1), 0);
        input.put(choices.get(2), 0);
        input.put(choices.get(3), 0);
        input.put(choices.get(4), 0);

        RankingResponse multipleChoiceResponse = new RankingResponse(null, liveField, input, true);
        Assert.assertThat(multipleChoiceResponse.isAnswered(), Is.is(false));
    }

    /**
     * @verifies return the 0 or 1 boolean values of the actual {@link <Map<Choice,Boolean>} stringed together.
     * @see RankingResponse#ruleString()
     */
    @Test
    public void ruleString_shouldReturnThe0Or1BooleanValuesOfTheActualLinkMapChoiceBooleanStringedTogetherWith1WhenRankedAndFalseOtherwise() throws Exception {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1));
        choices.add(new Choice(2));
        choices.add(new Choice(3));
        choices.add(new Choice(4));
        choices.add(new Choice(5));

        LinkedHashMap<Choice, Integer> input = new LinkedHashMap<>();
        input.put(choices.get(0), 3);
        input.put(choices.get(1), 0);
        input.put(choices.get(2), 1);
        input.put(choices.get(3), 0);
        input.put(choices.get(4), 2);

        String expected = "10101";

        RankingResponse multipleChoiceResponse = new RankingResponse(null, liveField, input, true);
        Assert.assertThat(expected, Is.is(multipleChoiceResponse.ruleString()));
    }

    /**
     * @verifies return ruleValue converted to Long.
     * @see RankingResponse#comparableFromRuleString(String)
     */
    @Test
    public void comparableFromRuleString_shouldReturnRuleValueConvertedToLong() throws Exception {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1));
        choices.add(new Choice(2));
        choices.add(new Choice(3));
        choices.add(new Choice(4));
        choices.add(new Choice(5));
        ChoiceGroup choiceGroup = new ChoiceGroup(choices);

        Field field = Mockito.mock(Field.class);
        Mockito.when(liveField.getField()).thenReturn(field);
        Mockito.when(field.getChoiceGroup()).thenReturn(choiceGroup);

        LinkedHashMap<Choice, Boolean> input = new LinkedHashMap<>();
        input.put(choices.get(0), Boolean.TRUE);
        input.put(choices.get(1), Boolean.FALSE);
        input.put(choices.get(2), Boolean.TRUE);
        input.put(choices.get(3), Boolean.FALSE);
        input.put(choices.get(4), Boolean.TRUE);

        Long expected = 21L;
        String ruleValue = "1#1|2#0|3#1|4#0|5#1";

        HojiContext hojiContext = Mockito.mock(HojiContext.class);

        RankingResponse multipleChoiceResponse = new RankingResponse(hojiContext, liveField, input, true);
        Assert.assertThat(expected, Is.is(multipleChoiceResponse.comparableFromRuleString(ruleValue)));
    }

    /**
     * @verifies return {@link RankingResponse#ruleString()} converted to Long.
     * @see RankingResponse#comparableFromActualValue()
     */
    @Test
    public void comparableFromActualValue_shouldReturnLinkRuleStringConvertedToLong() throws Exception {
        List<Choice> choices = new ArrayList<>();
        choices.add(new Choice(1));
        choices.add(new Choice(2));
        choices.add(new Choice(3));
        choices.add(new Choice(4));
        choices.add(new Choice(5));

        LinkedHashMap<Choice, Integer> input = new LinkedHashMap<>();
        input.put(choices.get(0), 3);
        input.put(choices.get(1), 0);
        input.put(choices.get(2), 1);
        input.put(choices.get(3), 0);
        input.put(choices.get(4), 2);

        Long expected = 21L;

        RankingResponse multipleChoiceResponse = new RankingResponse(null, liveField, input, true);
        Assert.assertThat(expected, Is.is(multipleChoiceResponse.comparableFromActualValue()));
    }
}
