package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Reference;
import ke.co.hoji.core.data.ReferenceLevel;
import ke.co.hoji.core.service.model.ReferenceService;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

/**
 * Created by gitahi on 06/08/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class ReferenceResponseTest {

    @Mock
    private LiveField liveField;

    @Mock
    private HojiContext hojiContext;

    @Mock
    private ModelServiceManager modelServiceManager;

    @Mock
    private ReferenceService referenceService;

    /**
     * @verifies set the actual value to the {@link Reference} represented by the Long value if actual is false.
     * @see ReferenceResponse#ReferenceResponse(ke.co.hoji.core.HojiContext, LiveField, Object, Boolean)
     */
    @Test
    public void ReferenceResponse_shouldSetTheActualValueToTheLinkReferenceRepresentedByTheLongValueIfActualIsFalse() throws Exception {
        Reference reference = new Reference(1, null);
        Mockito.when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        Mockito.when(modelServiceManager.getReferenceService()).thenReturn(referenceService);
        Mockito.when(referenceService.search(reference)).thenReturn(reference);
        Long value = Long.valueOf(reference.getId());
        ReferenceResponse referenceResponse = new ReferenceResponse(hojiContext, liveField, value, false);
        Assert.assertThat(reference, Is.is(referenceResponse.getActualValue()));
    }

    /**
     * @verifies return the Long of the actual Record.
     * @see ReferenceResponse#toStorageValue()
     */
    @Test
    public void toStorageValue_shouldReturnTheLongOfTheActualRecord() throws Exception {
        Reference reference = new Reference(1, null);
        ReferenceResponse referenceResponse = new ReferenceResponse(null, liveField, reference, true);
        Assert.assertThat(Long.valueOf(reference.getId()), Is.is(referenceResponse.toStorageValue()));
    }

    /**
     * @verifies return the value of the first rulable level, if any.
     * @see ReferenceResponse#ruleString()
     */
    @Test
    public void ruleString_shouldReturnTheValueOfTheFirstRulableLevelIfAny() throws Exception {
        final String VALUE = "value";
        Reference reference = new Reference(1, new ArrayList<ReferenceLevel>());
        ReferenceLevel referenceLevel = new ReferenceLevel();
        referenceLevel.setRulable(true);
        reference.getReferenceLevels().add(referenceLevel);
        reference.addValue(referenceLevel.getLevel(), VALUE);
        ReferenceResponse referenceResponse = new ReferenceResponse(null, liveField, reference, true);
        Assert.assertThat(VALUE, Is.is(referenceResponse.ruleString()));
    }

    /**
     * @verifies return empty string if no rulable level is found.
     * @see ReferenceResponse#ruleString()
     */
    @Test
    public void ruleString_shouldReturnEmptyStringIfNoRulableLevelIsFound() throws Exception {
        Reference reference = new Reference(1, new ArrayList<ReferenceLevel>());
        ReferenceResponse referenceResponse = new ReferenceResponse(null, liveField, reference, true);
        Assert.assertThat("", Is.is(referenceResponse.ruleString()));
    }
}
