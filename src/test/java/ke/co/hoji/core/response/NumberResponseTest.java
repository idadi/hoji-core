package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.LiveField;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.emptyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by gitahi on 10/08/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class NumberResponseTest {

    @Mock
    private LiveField liveField;
    private Long longValue = 1L;

    /**
     * @verifies return ruleValue converted to Long.
     * @see NumberResponse#comparableFromRuleString(String)
     */
    @Test
    public void comparableFromRuleString_shouldReturnRuleValueConvertedToLong() throws Exception {
        Response numberResponse = new NumberResponse(null, liveField, longValue, true);
        Assert.assertThat(numberResponse.comparableFromRuleString("10"), Is.is((Comparable) 10L));
    }

    /**
     * @verifies {@link NumberResponse#getFormat()} returns correct number format
     */
    @Test
    public void getFormat_shouldReturnCommaSeparatedFormatForThousandValue() {
        FieldType type = new FieldType(1, "Number", FieldType.Code.WHOLE_NUMBER);
        Field age = new Field(1);
        age.setType(type);
        LiveField ageLiveField = mock(LiveField.class);
        when(ageLiveField.getField()).thenReturn(age);
        HojiContext hojiContext = mock(HojiContext.class);
        Response response = new NumberResponse(hojiContext, ageLiveField, 9999, true);

        String format = response.getFormat();

        Assert.assertThat(format, is("9,999"));
    }

    /**
     * @verifies {@link NumberResponse#getFormat()} returns empty format string for unanswered question
     */
    @Test
    public void getFormat_shouldReturnEmptyStringForUnansweredQuestion() {
        Response response = getResponse("99", UUID.randomUUID().toString(), null);

        String format = response.getFormat();

        Assert.assertThat(format, is(""));
    }

    /**
     * @verifies {@link NumberResponse#getFormat()} returns empty format string for response matching specified
     * missing value
     */
    @Test
    public void getFormat_shouldReturnEmptyStringForResponseMatchingSpecifiedMissingValue() {
        Response response = getResponse("999", UUID.randomUUID().toString(), 999);

        String format = response.getFormat();

        Assert.assertThat(format, is(""));
    }

    /**
     * @verifies {@link NumberResponse#displayString(boolean)} returns missing string for response matching any specified
     * missing values
     */
    @Test
    public void displayString_shouldReturnMissingForResponseMatchingAnySpecifiedMissingValues() {
        Response response = getResponse("999,000", UUID.randomUUID().toString(), 999L);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));

        response = getResponse("999,000", UUID.randomUUID().toString(), 0L);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link NumberResponse#displayString(boolean)} returns missing string for specified missing values that
     * have leading spaces
     */
    @Test
    public void displayString_shouldReturnMissingStringForSpecifiedMissingValuesThatHaveLeadingSpaces() {
        Response response = getResponse(" 999, 000", UUID.randomUUID().toString(), 999L);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));

        response = getResponse(" 999, 000", UUID.randomUUID().toString(), 0L);;

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link NumberResponse#displayString(boolean)} returns missing string for specified missing values
     * that have trailing spaces
     */
    @Test
    public void displayString_shouldReturnMissingStringForSpecifiedMissingValuesThatHaveTrailingSpaces() {
        Response response = getResponse("999 ,000 ", UUID.randomUUID().toString(), 999L);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));

        response = getResponse("999 ,000 ", UUID.randomUUID().toString(), 0L);;

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link NumberResponse#displayString(boolean)} returns missing string for unanswered response
     */
    @Test
    public void displayString_shouldReturnMissingStringForUnansweredResponse() {
        Response response = getResponse("999", UUID.randomUUID().toString(), null);

        Assert.assertThat(response.displayString(true), is("Missing"));
    }

    /**
     * @verifies {@link NumberResponse#displayString(boolean)} returns empty string for skipped response
     */
    @Test
    public void displayString_shouldReturnEmptyStringForSkippedResponse() {
        Response response = getResponse("999", null, null);

        Assert.assertThat(response.displayString(true), emptyString());
    }

    private Response getResponse(String missingValue, String liveFieldUuid, Object value) {
        Field age = mock(Field.class);
        when(age.getMissingValue()).thenReturn(missingValue);
        LiveField ageLf = mock(LiveField.class);
        when(ageLf.getUuid()).thenReturn(liveFieldUuid);
        when(ageLf.getField()).thenReturn(age);
        HojiContext hojiContext = mock(HojiContext.class);
        return new NumberResponse(hojiContext, ageLf, value, true);
    }
}
