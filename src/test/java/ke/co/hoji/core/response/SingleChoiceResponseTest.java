package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.Choice.TranslatableComponent;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.service.model.TranslationService;
import org.hamcrest.Matchers;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * Created by gitahi on 06/08/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class SingleChoiceResponseTest {

    @Mock
    private LiveField liveField;

    private final Integer ID = 1;

    private final String NAME = "name";

    /**
     * @verifies set the actual value to the {@link Choice} represented by the String (choice code) value if actual is
     * false.
     * @see SingleChoiceResponse#SingleChoiceResponse(ke.co.hoji.core.HojiContext, LiveField,
     * Object, Boolean)
     */
    @Test
    public void SingleChoiceResponse_shouldSetTheActualValueToTheLinkChoiceRepresentedByTheStringChoiceIdValueIfActualIsFalse() throws Exception {
        Choice input = new Choice();
        input.setId(ID);

        List<Choice> choices = new ArrayList<>();
        {
            Choice choice = new Choice();
            choice.setId(ID);
            choices.add(choice);
        }
        {
            Choice choice = new Choice();
            choice.setId(2);
            choices.add(choice);
        }
        ChoiceGroup choiceGroup = new ChoiceGroup(choices);

        HojiContext hojiContext = Mockito.mock(HojiContext.class);

        Field field = Mockito.mock(Field.class);
        when(liveField.getField()).thenReturn(field);
        when(field.getChoiceGroup()).thenReturn(choiceGroup);

        Long value = input.getId().longValue();
        SingleChoiceResponse singleChoiceResponse = new SingleChoiceResponse(hojiContext, liveField, value, false);
        Assert.assertThat(input, Is.is(singleChoiceResponse.getActualValue()));
    }

    /**
     * @verifies return the code of the actual Choice.
     * @see SingleChoiceResponse#toStorageValue()
     */
    @Test
    public void toStorageValue_shouldReturnTheIdOfTheActualChoice() throws Exception {
        Choice input = new Choice();
        input.setId(ID);
        Long value = input.getId().longValue();
        SingleChoiceResponse singleChoiceResponse = new SingleChoiceResponse(null, liveField, input, true);
        Assert.assertThat(value, Is.is(singleChoiceResponse.toStorageValue()));
    }

    /**
     * @verifies return the name of the Choice, if choice is not null.
     * @see SingleChoiceResponse#displayString()
     */
    @Test
    public void displayString_shouldReturnTheNameOfTheChoiceIfChoiceIsNotNull() throws Exception {
        Choice input = new Choice();
        input.setName(NAME);

        HojiContext hojiContext = Mockito.mock(HojiContext.class);
        ModelServiceManager modelServiceManager = Mockito.mock(ModelServiceManager.class);
        TranslationService translationService = Mockito.mock(TranslationService.class);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getTranslationService()).thenReturn(translationService);
        when(translationService.translate(eq(input), anyInt(), eq(TranslatableComponent.NAME), eq(input.getName()))).thenReturn(input.getName());

        SingleChoiceResponse singleChoiceResponse = new SingleChoiceResponse(hojiContext, liveField, input, true);
        Assert.assertThat(input.getName(), Is.is(singleChoiceResponse.displayString()));
    }

    /**
     * @verifies return the code of the Choice, if choice is not null.
     * @see SingleChoiceResponse#ruleString()
     */
    @Test
    public void ruleString_shouldReturnTheIdOfTheChoiceIfChoiceIsNotNull() throws Exception {
        Choice input = new Choice();
        input.setId(ID);
        SingleChoiceResponse singleChoiceResponse = new SingleChoiceResponse(null, liveField, input, true);
        Assert.assertThat(input.getId(), Is.is(Utils.parseInteger(singleChoiceResponse.ruleString())));
    }

    /**
     * @verifies return the Choice whose code is equal to the rule value passed
     * @see SingleChoiceResponse#comparableFromRuleString(String)
     */
    @Test
    public void comparableFromRuleString_shouldReturnTheChoiceWhoseIdIsEqualToTheRuleValuePassed() throws Exception {
        Choice input = new Choice();
        input.setId(ID);

        List<Choice> choices = new ArrayList<>();
        {
            Choice choice = new Choice();
            choice.setId(ID);
            choices.add(choice);
        }
        {
            Choice choice = new Choice();
            choice.setId(2);
            choices.add(choice);
        }
        ChoiceGroup choiceGroup = new ChoiceGroup(choices);

        Field field = Mockito.mock(Field.class);
        when(field.getChoiceGroup()).thenReturn(choiceGroup);
        when(liveField.getField()).thenReturn(field);
        when(field.getChoiceGroup()).thenReturn(choiceGroup);

        Long value = Long.valueOf(input.getId());
        SingleChoiceResponse singleChoiceResponse = new SingleChoiceResponse(null, liveField, value, false);
        Assert.assertThat(singleChoiceResponse.comparableFromRuleString(String.valueOf(ID)), Is.is((Comparable) input));
    }
}
