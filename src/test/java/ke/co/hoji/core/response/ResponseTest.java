package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.LiveField;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by gitahi on 04/08/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class ResponseTest {

    @Mock
    private LiveField liveField;
    private String stringValue = "value";
    private Integer integerValue = 1;

    /**
     * @verifies return S cast to A if S is the same type as A.
     * @see Response#toActualValue(Object)
     */
    @Test
    public void toActualValue_shouldReturnSCastToAIfSIsTheSameTypeAsA() throws Exception {
        Response<String, String> response = new Response(null, liveField, stringValue, true);
        String actual = response.toActualValue(stringValue);
        Assert.assertThat(actual, Is.is(stringValue));
    }

    /**
     * @verifies throw ClassCastException if A and S are not the same.
     * @see Response#toActualValue(Object)
     */
    @Test(expected = ClassCastException.class)
    public void toActualValue_shouldThrowClassCastExceptionIfAAndSAreNotTheSame() throws Exception {
        Response<Integer, String> response = new Response(null, liveField, stringValue, true);
        Integer actual = response.toActualValue(stringValue);
    }

    /**
     * @verifies return A case to S if A is the same type as S.
     * @see Response#toStorageValue()
     */
    @Test
    public void toStorageValue_shouldReturnACaseToSIfAIsTheSameTypeAsS() throws Exception {
        Response<String, String> response = new Response(null, liveField, stringValue, true);
        String storage = response.toStorageValue();
        Assert.assertThat(storage, Is.is(stringValue));
    }

    /**
     * @verifies throw ClassCastException if A and S are not the same.
     * @see Response#toStorageValue()
     */
    @Test(expected = ClassCastException.class)
    public void toStorageValue_shouldThrowClassCastExceptionIfAAndSAreNotTheSame() throws Exception {
        Response<Integer, String> response = new Response(null, liveField, integerValue, true);
        String storage = response.toStorageValue();
    }

    /**
     * @verifies return {@link Response#actualValue#toString()} if possible and empty String otherwise.
     * @see Response#displayString()
     */
    @Test
    public void displayString_shouldReturnLinkResponseactualValuetoStringIfPossibleAndEmptyStringOtherwise() throws Exception {
        Response<String, String> response = new Response(null, liveField, stringValue, true);
        String display = response.displayString();
        Assert.assertThat(display, Is.is(stringValue.toString()));
    }

    /**
     * @verifies return display string.
     * @see Response#ruleString()
     */
    @Test
    public void ruleString_shouldReturnDisplayString() throws Exception {
        Response<String, String> response = new Response(null, liveField, stringValue, true);
        String rule = response.ruleString();
        Assert.assertThat(rule, Is.is(response.displayString()));
    }

    /**
     * @verifies return null;
     * @see Response#comparableFromRuleString(String)
     */
    @Test
    public void comparableFromRuleString_shouldReturnNull() throws Exception {
        Response<String, String> response = new Response(null, liveField, stringValue, true);
        Comparable comparable = response.comparableFromRuleString("test");
        Assert.assertThat(comparable, Is.is(CoreMatchers.nullValue()));
    }

    /**
     * @verifies return true if actual value is not null.
     * @see Response#isAnswered()
     */
    @Test
    public void isAnswered_shouldReturnTrueIfActualValueIsNotNull() throws Exception {
        Response<String, String> response = new Response(null, liveField, stringValue, true);
        Assert.assertThat(response.isAnswered(), Is.is(true));
    }

    /**
     * @verifies return false if actual value is null.
     * @see Response#isAnswered()
     */
    @Test
    public void isAnswered_shouldReturnFalseIfActualValueIsNull() throws Exception {
        Response<String, String> response = new Response(null, liveField, null, true);
        Assert.assertThat(response.isAnswered(), Is.is(false));
    }

    /**
     * @verifies {@link Response#displayString(boolean)} handles missing values correctly
     */
    @Test
    public void displayString_shouldReturnMissingIfLiveFieldValueIsSameAsDefinedInMissingValue() {
        Field comment = mock(Field.class);
        when(comment.getMissingValue()).thenReturn("None,meh");
        LiveField commentLf = mock(LiveField.class);
        when(commentLf.getField()).thenReturn(comment);
        HojiContext hojiContext = mock(HojiContext.class);

        Response<String, String> response = new Response(hojiContext, commentLf, "meh", true);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));

        response = new Response(hojiContext, commentLf, "None", true);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link Response#displayString(boolean)} handles missing values correctly when defined missing values
     * have leading spaces
     */
    @Test
    public void displayString_shouldReturnMissingWhenDefinedInMissingValuesHaveLeadingSpaces() {
        Field comment = mock(Field.class);
        when(comment.getMissingValue()).thenReturn(" None, meh");
        LiveField commentLf = mock(LiveField.class);
        when(commentLf.getField()).thenReturn(comment);
        HojiContext hojiContext = mock(HojiContext.class);

        Response<String, String> response = new Response(hojiContext, commentLf, "meh", true);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));

        response = new Response(hojiContext, commentLf, "None", true);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link Response#displayString(boolean)} handles missing values correctly when defined missing values
     * have trailing spaces
     */
    @Test
    public void displayString_shouldReturnMissingWhenDefinedInMissingValuesHaveTrailingSpaces() {
        Field comment = mock(Field.class);
        when(comment.getMissingValue()).thenReturn("None ,meh ");
        LiveField commentLf = mock(LiveField.class);
        when(commentLf.getField()).thenReturn(comment);
        HojiContext hojiContext = mock(HojiContext.class);

        Response<String, String> response = new Response(hojiContext, commentLf, "meh", true);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));

        response = new Response(hojiContext, commentLf, "None", true);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link Response#displayString(boolean)} returns empty string for null values
     */
    @Test
    public void displayString_shouldReturnEmptyStringIfLiveFieldValueIsNull() {
        Field comment = mock(Field.class);
        when(comment.getMissingValue()).thenReturn("None,meh");
        LiveField commentLf = mock(LiveField.class);
        when(commentLf.getField()).thenReturn(comment);
        HojiContext hojiContext = mock(HojiContext.class);

        Response<String, String> response = new Response(hojiContext, commentLf, null, true);

        Assert.assertThat(response.displayString(true), Matchers.emptyString());
    }
}
