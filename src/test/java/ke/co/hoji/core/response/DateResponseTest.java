package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.emptyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by gitahi on 04/08/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class DateResponseTest {

    @Mock
    private LiveField liveField;

    /**
     * @verifies set the actual value to the date represented by the long value if actual is false.
     * @see DateResponse#DateResponse(ke.co.hoji.core.HojiContext, LiveField, Object, Boolean)
     */
    @Test
    public void DateResponse_shouldSetTheActualValueToTheDateRepresentedByTheLongValueIfActualIsFalse() throws Exception {
        Date input = new Date();
        Long value = input.getTime();
        DateResponse dateResponse = new DateResponse(null, liveField, value, false);
        Assert.assertThat(input, Is.is(dateResponse.getActualValue()));
    }

    /**
     * @verifies return the actual date value as a long.
     * @see DateResponse#toStorageValue()
     */
    @Test
    public void toStorageValue_shouldReturnTheActualDateValueAsALong() throws Exception {
        Date input = new Date();
        Long value = input.getTime();
        DateResponse dateResponse = new DateResponse(null, liveField, input, true);
        Assert.assertThat(value, Is.is(dateResponse.toStorageValue()));
    }

    /**
     * @verifies return the actual date formatted to the current locale.
     * @see DateResponse#displayString()
     */
    @Test
    public void displayString_shouldReturnTheActualDateFormattedToTheCurrentLocale() throws Exception {
        Date input = new Date();
        DateResponse dateResponse = new DateResponse(null, liveField, input, true);
        Assert.assertThat(Utils.formatDate(input), Is.is(dateResponse.displayString()));
    }

    /**
     * @verifies {@link DateResponse#displayString(boolean)} returns actual value for answered question
     */
    @Test
    public void getValue_shouldReturnActualValueForAnsweredQuestion() {
        Date dateValue = new Date(1528737366794L);
        List<DataElement> values =
                getResponse("1977-12-12",  UUID.randomUUID().toString(), dateValue).getValue(Form.OutputStrategy.PIVOT);

        Assert.assertThat(values.get(0).getValue(), is("2018-06-11"));
    }

    /**
     * @verifies {@link DateResponse#displayString(boolean)} returns missing string for responses matching specified
     * missing values
     */
    @Test
    public void displayString_shouldReturnEmptyStringForResponsesMatchingSpecifiedMissingValue() {
        Response response = getResponse("1977-12-12",  UUID.randomUUID().toString(), new Date(250722561490L));

        Assert.assertThat(response.displayString(true), Is.is("Missing"));

        response = getResponse("1977",  UUID.randomUUID().toString(), new Date(250722561490L));

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link DateResponse#displayString(boolean)} returns missing string when specified missing values
     * that have leading spaces
     */
    @Test
    public void displayString_shouldReturnMissingWhenSpecifiedMissingValuesThatHaveLeadingSpaces() {
        Response response = getResponse(" 1977-12-12",  UUID.randomUUID().toString(), new Date(250722000000L));

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link DateResponse#displayString(boolean)} returns missing string when specified missing values that
     * have trailing spaces
     */
    @Test
    public void displayString_shouldReturnMissingWhenSpecifiedMissingValuesThatHaveTrailingSpaces() {
        Response response = getResponse("1977-12-12 ", UUID.randomUUID().toString(), new Date(250722000000L));

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link DateResponse#displayString(boolean)} returns missing string for unanswered question
     */
    @Test
    public void displayString_shouldReturnMissingStringForUnansweredQuestion() {
        Response response = getResponse("1977-12-12", UUID.randomUUID().toString(), null);

        Assert.assertThat(response.displayString(true), is("Missing"));
    }

    /**
     * @verifies {@link DateResponse#displayString(boolean)} returns empty string for skipped question
     */
    @Test
    public void displayString_shouldReturnEmptyStringForSkippedQuestion() {
        Response response = getResponse("1977-12-12", null, null);

        Assert.assertThat(response.displayString(true), emptyString());
    }

    private Response getResponse(String missingValue, String liveFieldUuid, Object value) {
        Field dateOfBirth = mock(Field.class);
        when(dateOfBirth.getHeader()).thenReturn("Date of Birth");
        when(dateOfBirth.getMissingValue()).thenReturn(missingValue);
        LiveField dateOfBirthLf = mock(LiveField.class);
        when(dateOfBirthLf.getUuid()).thenReturn(liveFieldUuid);
        when(dateOfBirthLf.getField()).thenReturn(dateOfBirth);
        HojiContext hojiContext = mock(HojiContext.class);
        return new DateResponse(hojiContext, dateOfBirthLf, value, true);
    }
}
