package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.emptyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by gitahi on 10/08/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class DecimalResponseTest {

    @Mock
    private LiveField liveField;
    private Double doubleValue = 1.0;

    /**
     * @verifies return rule value converted to Double.
     * @see DecimalResponse#comparableFromRuleString(String)
     */
    @Test
    public void comparableFromRuleString_shouldReturnRuleValueConvertedToDouble() throws Exception {
        Response decimalResponse = new DecimalResponse(null, liveField, doubleValue, true);
        Assert.assertThat(decimalResponse.comparableFromRuleString("10.1"), Is.is((Comparable) 10.1));
    }

    /**
     * @verifies {@link DecimalResponse#getValue(String)} returns actual value for answered question
     */
    @Test
    public void getValue_shouldReturnActualValueForAnsweredQuestion() {
        Response response = getResponse("99,99.00,9999", null, 999D);

        List<DataElement> dataElements = response.getValue(Form.OutputStrategy.PIVOT);

        Assert.assertThat(dataElements.get(0).getValue(), is("999.0"));
    }

    /**
     * @verifies {@link DecimalResponse#getValue(String)} returns missing value when {@link LiveField}'s value
     * matches that specified in {@link Field}'s missing value
     */
    @Test
    public void getValue_shouldReturnMissingWhenValueMatchesSpecifiedInMissingValue() {
        Response response = getResponse("999", null, 999D);

        List<DataElement> dataElements = response.getValue(Form.OutputStrategy.PIVOT);

        Assert.assertThat(dataElements.get(0).getValue(), is("Missing"));
    }

    /**
     * @verifies {@link DecimalResponse#getValue(String)} returns missing string for response matching any specified
     * missing value
     */
    @Test
    public void getValue_shouldReturnMissingStringForResponseMatchingAnySpecifiedMissingValues() {
        Response response = getResponse("99,999,999.0", null, 999D);

        List<DataElement> dataElements = response.getValue(Form.OutputStrategy.PIVOT);

        Assert.assertThat(dataElements.get(0).getValue(), is("Missing"));
    }

    /**
     * @verifies {@link DecimalResponse#getFormat()} returns correct format for value
     */
    @Test
    public void getFormat_shouldReturnCommaSeparatedFormatForThousandValue() {
        FieldType type = new FieldType(1, "Number", FieldType.Code.DECIMAL_NUMBER);
        Field age = new Field(1);
        age.setType(type);
        LiveField ageLiveField = mock(LiveField.class);
        when(ageLiveField.getField()).thenReturn(age);
        HojiContext hojiContext = mock(HojiContext.class);
        Response response = new DecimalResponse(hojiContext, ageLiveField, 9999.99D, true);

        String format = response.getFormat();

        Assert.assertThat(format, is("9,999.99"));
    }

    /**
     * @verifies {@link DecimalResponse#getFormat()} returns empty format string for response matching specified missing
     * value
     */
    @Test
    public void getFormat_shouldReturnEmptyStringForResponseMatchingSpecifiedMissingValue() {
        Response response = getResponse("999", UUID.randomUUID().toString(), 999D);

        String format = response.getFormat();

        Assert.assertThat(format, is(""));
    }

    /**
     * @verifies {@link DecimalResponse#getFormat()} returns empty format string for response matching any specified
     * missing values
     */
    @Test
    public void getFormat_shouldReturnEmptyStringForResponseMatchingAnySpecifiedMissingValues() {
        Response response = getResponse("99,999.9", UUID.randomUUID().toString(), 999.9D);

        String format = response.getFormat();

        Assert.assertThat(format, is(""));
    }

    /**
     * @verifies {@link DecimalResponse#getFormat()} returns empty format string for unanswered question
     */
    @Test
    public void getFormat_shouldReturnEmptyFormatStringForUnansweredQuestion() {
        Response response = getResponse("99,999.9", UUID.randomUUID().toString(), null);

        String format = response.getFormat();

        Assert.assertThat(format, is(""));
    }

    /**
     * @verifies {@link DecimalResponse#displayString(boolean)} returns missing string for response matching any
     * specified missing values
     */
    @Test
    public void displayString_shouldReturnMissingStringForResponseMatchingAnySpecifiedMissingValues() {
        Response response = getResponse("999,000", UUID.randomUUID().toString(), 999D);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));

        response = getResponse("999,000", UUID.randomUUID().toString(), 0D);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link DecimalResponse#displayString(boolean)} returns missing string when specified missing values
     * that have leading spaces
     */
    @Test
    public void displayString_shouldReturnMissingStringWhenSpecifiedMissingValuesThatHaveLeadingSpaces() {
        Response response = getResponse(" 999, 000", UUID.randomUUID().toString(), 999D);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));

        response = getResponse(" 999, 000", UUID.randomUUID().toString(), 0D);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link DecimalResponse#displayString(boolean)} returns missing string when specified missing values
     * that have trailing spaces
     */
    @Test
    public void displayString_shouldReturnMissingStringWhenSpecifiedMissingValuesThatHaveTrailingSpaces() {
        Response response = getResponse("999 ,000 ", UUID.randomUUID().toString(), 999D);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));

        response = getResponse("999 ,000 ", UUID.randomUUID().toString(), 0D);

        Assert.assertThat(response.displayString(true), Is.is("Missing"));
    }

    /**
     * @verifies {@link DecimalResponse#displayString(boolean)} returns missing string for unanswered question
     */
    @Test
    public void displayString_shouldReturnMissingStringForUnansweredQuestion() {
        Response response = getResponse("999", UUID.randomUUID().toString(), null);

        Assert.assertThat(response.displayString(true), is("Missing"));
    }

    /**
     * @verifies {@link DecimalResponse#displayString(boolean)} returns empty string for skipped question
     */
    @Test
    public void displayString_shouldReturnEmptyStringForSkippedQuestion() {
        Response response = getResponse("999", null, null);

        Assert.assertThat(response.displayString(true), emptyString());
    }

    private Response getResponse(String missingValue, String liveFieldUuid, Object value) {
        Field weight = mock(Field.class);
        when(weight.getHeader()).thenReturn("Weight");
        when(weight.getMissingValue()).thenReturn(missingValue);
        LiveField weightLiveField = mock(LiveField.class);
        when(weightLiveField.getUuid()).thenReturn(liveFieldUuid);
        when(weightLiveField.getField()).thenReturn(weight);
        HojiContext hojiContext = mock(HojiContext.class);
        return new DecimalResponse(hojiContext, weightLiveField, value, true);
    }
}
