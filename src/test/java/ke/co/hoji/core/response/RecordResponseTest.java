package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Form.OutputStrategy;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.service.model.RecordService;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static ke.co.hoji.core.data.dto.DataElement.Type.NUMBER;
import static ke.co.hoji.core.data.dto.DataElement.Type.STRING;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by gitahi on 04/08/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class RecordResponseTest {

    @Mock
    private LiveField liveField;

    @Mock
    private HojiContext hojiContext;

    @Mock
    private ModelServiceManager modelServiceManager;

    @Mock
    private RecordService recordService;

    /**
     * @verifies set the actual value to the {@link Record} represented by the uuid value if actual is false.
     * @see RecordResponse#RecordResponse(ke.co.hoji.core.HojiContext, LiveField, Object, Boolean)
     */
    @Test
    public void RecordResponse_shouldSetTheActualValueToTheLinkRecordRepresentedByTheUuidValueIfActualIsFalse() throws Exception {
        Record record = new Record(null, true, false);
        record.setUuid("385168ce-3ab4-11e5-98b1-530f548684be");
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getRecordService()).thenReturn(recordService);
        when(recordService.findRecordByUuid(record.getUuid(), liveField.getField())).thenReturn(record);
        String value = record.getUuid();
        RecordResponse recordResponse = new RecordResponse(hojiContext, liveField, value, false);
        Assert.assertThat(record, is(recordResponse.getActualValue()));
    }

    /**
     * @verifies return the uuid of the actual Record.
     * @see RecordResponse#toStorageValue()
     */
    @Test
    public void toStorageValue_shouldReturnTheUuidOfTheActualRecord() throws Exception {
        Record record = new Record(null, true, false);
        RecordResponse recordResponse = new RecordResponse(null, liveField, record, true);
        Assert.assertThat(record.getUuid(), is(recordResponse.toStorageValue()));
    }

    /**
     * verifies data values are obtained from searchable fields only
     */
    @Test
    public void getValue_shouldReturnSearchableFieldValuesAsDataElements() {
        Form regForm = new Form(1);
        Field name = buildField(1, "Name", true);
        name.setType(new FieldType(1, "String", FieldType.Code.SINGLE_LINE_TEXT));
        Field age = buildField(2, "Age", false);
        regForm.setFields(asList(name, age));
        Form followUpForm = new Form(2);
        Record record = buildRegFormRecord(regForm, name, age);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getRecordService()).thenReturn(recordService);
        Field linkField = buildField(1, "Link Field", false);
        linkField.setForm(followUpForm);
        linkField.setRecordForms(Collections.singletonList(regForm));
        LiveField linkLF = mock(LiveField.class);
        when(linkLF.getField()).thenReturn(linkField);
        when(recordService.findRecordByUuid(record.getUuid(), linkField)).thenReturn(record);
        String value = record.getUuid();
        RecordResponse recordResponse = new RecordResponse(hojiContext, linkLF, value, false);

        List<DataElement> dataElements = recordResponse.getValue("");
        DataElement expected = new DataElement("Name", "Test", STRING, "");
        Assert.assertThat(dataElements, hasItem(expected));
    }

    /**
     * verifies that uuid of linked record is obtained when request is for download
     */
    @Test
    public void getValue_shouldReturnRecordUuidWhenDownloading() {
        Form regForm = new Form(1);
        Field name = buildField(1, "Name", true);
        name.setType(new FieldType(1, "String", FieldType.Code.SINGLE_LINE_TEXT));
        Field age = buildField(2, "Age", false);
        regForm.setFields(asList(name, age));
        Form followUpForm = new Form(2);
        Record record = buildRegFormRecord(regForm, name, age);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getRecordService()).thenReturn(recordService);
        Field linkField = buildField(1, "Link Field", false);
        FieldType type = new FieldType(1, "Text", FieldType.Code.SINGLE_LINE_TEXT);
        linkField.setType(type);
        linkField.setForm(followUpForm);
        linkField.setRecordForms(Collections.singletonList(regForm));
        LiveField linkLF = mock(LiveField.class);
        when(linkLF.getField()).thenReturn(linkField);
        when(recordService.findRecordByUuid(record.getUuid(), linkField)).thenReturn(record);
        String value = record.getUuid();
        RecordResponse recordResponse = new RecordResponse(hojiContext, linkLF, value, false);

        List<DataElement> dataElements = recordResponse.getValue(OutputStrategy.DOWNLOAD);
        DataElement expected = new DataElement("Link Field", "385168ce-3ab4-11e5-98b1-530f548684be", STRING, "");
        Assert.assertThat(dataElements, hasItem(expected));
    }

    /**
     * verifies that get value returns missing DataElement for null linked record
     */
    @Test
    public void getValue_shouldReturnMissingDataElementForNullLinkedRecord() {
        Form regForm = new Form(1);
        Field name = buildField(1, "Name", true);
        name.setType(new FieldType(1, "String", FieldType.Code.SINGLE_LINE_TEXT));
        Field age = buildField(2, "Age", false);
        regForm.setFields(asList(name, age));
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getRecordService()).thenReturn(recordService);
        Field linkField = buildField(3, "Link Field", false);
        linkField.setForm(new Form(2));
        linkField.setRecordForms(Collections.singletonList(regForm));
        LiveField linkLF = mock(LiveField.class);
        when(linkLF.getField()).thenReturn(linkField);
        RecordResponse recordResponse = new RecordResponse(hojiContext, linkLF, "385168ce-3ab4-11e5-98b1-530f548684be", false);

        List<DataElement> dataElements = recordResponse.getValue("");
        DataElement expected = new DataElement(name.getHeader(), null, STRING, "");
        Assert.assertThat(dataElements, hasItem(expected));
    }

    /**
     * verifies that {@link RecordResponse#getValue(String)} returns missing {@link DataElement} when {@link Field}'s
     * {@link LiveField} cannot be found in collection
     */
    @Test
    public void getValue_shouldReturnMissingDataElementForNullLiveField() {
        Form regForm = new Form(1);
        Field name = buildField(1, "Name", true);
        name.setType(new FieldType(1, "String", FieldType.Code.SINGLE_LINE_TEXT));
        Field age = buildField(2, "Age", false);
        regForm.setFields(asList(name, age));
        Record record = new Record(regForm, true, false);
        record.setUuid("385168ce-3ab4-11e5-98b1-530f548684be");
        Field linkField = buildField(3,"Link Field", false);
        linkField.setForm(new Form(2));
        linkField.setRecordForms(Collections.singletonList(regForm));
        LiveField linkLF = mock(LiveField.class);
        when(linkLF.getField()).thenReturn(linkField);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getRecordService()).thenReturn(recordService);
        when(recordService.findRecordByUuid(record.getUuid(), linkField)).thenReturn(record);
        RecordResponse recordResponse = new RecordResponse(hojiContext, linkLF, record.getUuid(), false);

        List<DataElement> dataElements = recordResponse.getValue("");
        DataElement expected = new DataElement(name.getHeader(), null, STRING, "");
        Assert.assertThat(dataElements, hasItem(expected));
    }

    /**
     * verifies that {@link RecordResponse#getValue(String)} returns unique {@link DataElement} when {@link Field}s
     * from different linked {@link Form}s have the same value for {@link Field#getDescription()}
     */
    @Test @Ignore("not supporting this use case")
    public void getValue_shouldReturnUniqueDataElementsWhenFieldsHaveSameDescription() {
        Form maleRegForm = new Form(1);
        Field mName = buildField(1, "Name", true);
        FieldType stringType = new FieldType(1, "String", FieldType.Code.SINGLE_LINE_TEXT);
        mName.setType(stringType);
        Field mAge = buildField(2, "Age", false);
        maleRegForm.setFields(asList(mName, mAge));
        Form femaleRegForm = new Form(2);
        Field fName = buildField(3, "Name", true);
        fName.setType(stringType);
        Field fAge = buildField(4, "Age", false);
        femaleRegForm.setFields(asList(fName, fAge));
        Record record = buildRegFormRecord(maleRegForm, mName, mAge);
        Field linkField = buildField(5,"Link Field", false);
        linkField.setForm(new Form(3));
        linkField.setRecordForms(asList(maleRegForm, femaleRegForm));
        LiveField linkLF = mock(LiveField.class);
        when(linkLF.getField()).thenReturn(linkField);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getRecordService()).thenReturn(recordService);
        when(recordService.findRecordByUuid(record.getUuid(), linkField)).thenReturn(record);
        RecordResponse recordResponse = new RecordResponse(hojiContext, linkLF, record.getUuid(), false);

        List<DataElement> dataElements = recordResponse.getValue("");
        DataElement expected = new DataElement(mName.getHeader(), "Test", STRING, "");
        Assert.assertThat(dataElements.size(), is(1));
        Assert.assertThat(dataElements, hasItem(expected));
    }

    /**
     * verifies that {@link RecordResponse#getValue(String)} returns unique {@link DataElement} when {@link Field}s
     * from different linked {@link Form}s have the same value for {@link Field#getColumn()}}
     */
    @Test @Ignore("not supporting this use case")
    public void getValue_shouldReturnUniqueDataElementsWhenFieldsHaveSameColumn() {
        Form maleRegForm = new Form(1);
        Field mName = buildField(1, "Name", true);
        mName.setColumn("_name");
        FieldType stringType = new FieldType(1, "String", FieldType.Code.SINGLE_LINE_TEXT);
        mName.setType(stringType);
        Field mAge = buildField(2, "Age", false);
        maleRegForm.setFields(asList(mName, mAge));
        Form femaleRegForm = new Form(2);
        Field fName = buildField(3, "Name", true);
        fName.setColumn("_name");
        fName.setType(stringType);
        Field fAge = buildField(4, "Age", false);
        femaleRegForm.setFields(asList(fName, fAge));
        Record record = buildRegFormRecord(maleRegForm, mName, mAge);
        Field linkField = buildField(5,"Link Field", false);
        linkField.setForm(new Form(3));
        linkField.setRecordForms(asList(maleRegForm, femaleRegForm));
        LiveField linkLF = mock(LiveField.class);
        when(linkLF.getField()).thenReturn(linkField);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getRecordService()).thenReturn(recordService);
        when(recordService.findRecordByUuid(record.getUuid(), linkField)).thenReturn(record);
        RecordResponse recordResponse = new RecordResponse(hojiContext, linkLF, record.getUuid(), false);

        List<DataElement> dataElements = recordResponse.getValue("");
        DataElement expected = new DataElement(mName.getHeader(), "Test", STRING, "");
        Assert.assertThat(dataElements.size(), is(1));
        Assert.assertThat(dataElements, hasItem(expected));
    }

    /**
     * verifies that {@link RecordResponse#getValue(String)} returns unique {@link DataElement} when {@link Field}s
     * from different linked {@link Form}s have different value for {@link Field#getColumn()}}
     */
    @Test
    public void getValue_shouldReturnUniqueDataElementsWhenFieldsHaveDifferentColumn() {
        Form maleRegForm = new Form(1);
        Field mName = buildField(1, "Name", true);
        mName.setColumn("m_name");
        FieldType stringType = new FieldType(1, "String", FieldType.Code.SINGLE_LINE_TEXT);
        mName.setType(stringType);
        Field mAge = buildField(2, "Age", false);
        maleRegForm.setFields(asList(mName, mAge));
        Form femaleRegForm = new Form(2);
        Field fName = buildField(3, "Name", true);
        fName.setColumn("f_name");
        fName.setType(stringType);
        Field fAge = buildField(4, "Age", false);
        femaleRegForm.setFields(asList(fName, fAge));
        Record record = buildRegFormRecord(maleRegForm, mName, mAge);
        Field linkField = buildField(5,"Link Field", false);
        linkField.setForm(new Form(3));
        linkField.setRecordForms(asList(maleRegForm, femaleRegForm));
        LiveField linkLF = mock(LiveField.class);
        when(linkLF.getField()).thenReturn(linkField);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getRecordService()).thenReturn(recordService);
        when(recordService.findRecordByUuid(record.getUuid(), linkField)).thenReturn(record);
        RecordResponse recordResponse = new RecordResponse(hojiContext, linkLF, record.getUuid(), false);

        List<DataElement> dataElements = recordResponse.getValue("");
        DataElement expectedMale = new DataElement(mName.getHeader(), "Test", STRING, "");
        DataElement expectedFemale = new DataElement(fName.getHeader(), null, STRING, "");
        Assert.assertThat(dataElements.size(), is(2));
        Assert.assertThat(dataElements, hasItems(expectedMale, expectedFemale));
    }

    /**
     * verifies that {@link RecordResponse#getValue(String)} returns {@link DataElement} with correct
     * {@link ke.co.hoji.core.data.dto.DataElement.Type}
     */
    @Test
    public void getValue_shouldReturnCorrectDataElementType() {
        Form regForm = new Form(1);
        Field name = buildField(1, "Name", true);
        name.setType(new FieldType(1, "String", FieldType.Code.SINGLE_LINE_TEXT));
        Field age = buildField(2, "Age", true);
        age.setType(new FieldType(2, "Number", FieldType.Code.WHOLE_NUMBER));
        regForm.setFields(asList(name, age));
        Record record = new Record(regForm, true, false);
        record.setUuid("385168ce-3ab4-11e5-98b1-530f548684be");
        Field linkField = buildField(3,"Link Field", false);
        linkField.setForm(new Form(2));
        linkField.setRecordForms(Collections.singletonList(regForm));
        LiveField linkLF = mock(LiveField.class);
        when(linkLF.getField()).thenReturn(linkField);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getRecordService()).thenReturn(recordService);
        when(recordService.findRecordByUuid(record.getUuid(), linkField)).thenReturn(record);
        RecordResponse recordResponse = new RecordResponse(hojiContext, linkLF, record.getUuid(), false);

        List<DataElement> dataElements = recordResponse.getValue("");
        Assert.assertThat(dataElements,
                hasItems(
                        Matchers.<DataElement>hasProperty("type", is(STRING)),
                        Matchers.<DataElement>hasProperty("type", is(NUMBER))
                )
        );
    }

    /**
     * verifies that {@link RecordResponse#getValue(String)} only includes the following {@link FieldType}s: textual,
     * number and single choice
     */
    @Test
    public void getValue_shouldOnlyIncludeTextualNumberAndSingleChoiceFieldOfLinkedForm() {
        Form regForm = new Form(1);
        Field name = buildField(1, "Name", true);
        name.setType(new FieldType(1, "String", FieldType.Code.SINGLE_LINE_TEXT));
        Field gender = buildField(2, "Gender", true);
        gender.setType(new FieldType(2, "Single Choice", FieldType.Code.SINGLE_CHOICE));
        Field telNo = buildField(3, "Tel No.", true);
        telNo.setType(new FieldType(3, "Number", FieldType.Code.WHOLE_NUMBER));
        Field favoriteColors = buildField(4, "Favorite Colors", true);
        favoriteColors.setType(new FieldType(4, "Multiple Choice", FieldType.Code.MULTIPLE_CHOICE));
        Field birthDate = buildField(5, "Birth date", true);
        birthDate.setType(new FieldType(5, "Date", FieldType.Code.DATE));
        Field regTime = buildField(6, "Reg time", true);
        regTime.setType(new FieldType(6, "Time", FieldType.Code.TIME));
        regForm.setFields(asList(name, gender, telNo, favoriteColors, birthDate, regTime));
        Record record = new Record(regForm, true, false);
        record.setUuid("385168ce-3ab4-11e5-98b1-530f548684be");
        Field linkField = buildField(3,"Link Field", false);
        linkField.setForm(new Form(2));
        linkField.setRecordForms(Collections.singletonList(regForm));
        LiveField linkLF = mock(LiveField.class);
        when(linkLF.getField()).thenReturn(linkField);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getRecordService()).thenReturn(recordService);
        when(recordService.findRecordByUuid(record.getUuid(), linkField)).thenReturn(record);
        RecordResponse recordResponse = new RecordResponse(hojiContext, linkLF, record.getUuid(), false);

        List<DataElement> dataElements = recordResponse.getValue("");
        DataElement nameDataElement = new DataElement(name.getHeader(), null, name.getDataElementType(""), "");
        DataElement genderDataElement = new DataElement(name.getHeader(), null, name.getDataElementType(""), "");
        DataElement telNoDataElement = new DataElement(name.getHeader(), null, name.getDataElementType(""), "");
        Assert.assertThat(dataElements.size(), is(3));
        Assert.assertThat(dataElements, hasItems(nameDataElement, genderDataElement, telNoDataElement));
    }

    private Field buildField(int id, String description, boolean searchable) {
        Field field = new Field(id);
        field.setDescription(description);
        field.setEnabled(true);
        field.setSearchable(searchable);
        return field;
    }

    private Record buildRegFormRecord(Form regForm, Field name, Field age) {
        Response nameResponse = mock(Response.class);
        when(nameResponse.getValue("")).thenReturn(
                Collections.singletonList(new DataElement(name.getHeader(), "Test", STRING, ""))
        );
        LiveField nameLF = mock(LiveField.class);
        when(nameLF.getResponse()).thenReturn(nameResponse);
        when(nameLF.getField()).thenReturn(name);
        LiveField ageLF = mock(LiveField.class);
        when(ageLF.getField()).thenReturn(age);
        Record record = new Record(regForm, true, false);
        record.setLiveFields(asList(nameLF, ageLF));
        record.setUuid("385168ce-3ab4-11e5-98b1-530f548684be");
        return record;
    }
}
