package ke.co.hoji.core.response;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.response.helper.Base64Encoder;
import org.apache.commons.codec.binary.Base64;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ImageResponseTests {

    @Mock
    private HojiContext hojiContext;

    private String imageString =
            "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkYPhfDwAChwGA60e6kgAAAABJRU5ErkJggg==";

    @Test
    public void constructor_shouldHandleBase64String() {
        ImageResponse response = new ImageResponse(hojiContext, new LiveField(new Field()), imageString, false);

        assertThat(response.toStorageValue(), equalTo(imageString));
    }

    @Test
    @Ignore
    /*TODO: Fix test.*/
    public void constructor_shouldHandleImagePath() {
        File imageLocation = new File(getClass().getClassLoader().getResource(".").getFile() + "test.png");
        try (FileOutputStream fos = new FileOutputStream(imageLocation)) {
            fos.write(new Base64EncoderImpl().decodeToByteArray(imageString));
        }  catch (IOException e) {
            // ignore
        }

        ImageResponse response = new ImageResponse(hojiContext, null, imageLocation.getPath(), false);

        assertThat(response.toStorageValue(), equalTo(imageString));

        imageLocation.delete();
    }

    static class Base64EncoderImpl implements Base64Encoder {

        private Base64 base64 = new Base64();

        @Override
        public String encodeToBase64String(byte[] byteArray) {
            return base64.encodeAsString(byteArray);
        }

        @Override
        public byte[] decodeToByteArray(String base64String) {
            return base64.decode(base64String);
        }
    }

}