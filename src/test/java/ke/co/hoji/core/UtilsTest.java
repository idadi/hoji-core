package ke.co.hoji.core;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;

/**
 * Created by geoffreywasilwa on 15/03/2017.
 */
public class UtilsTest {

    @Test
    public void toCleanIdentifier_shouldRemoveHtmlTags() throws Exception {
        String idWithHtmlTags = "Question<em>1</em>";
        String expectedId = "question1";

        String actualId = Utils.toCleanIdentifier(idWithHtmlTags);
        Assert.assertThat(actualId, is(expectedId));
    }

    @Test
    public void toCleanIdentifier_shouldRemoveNonAlphaNumericCharacters() {
        String idWithNonAlphaNumeric = "Question#1";
        String expectedId = "question1";

        String actualId = Utils.toCleanIdentifier(idWithNonAlphaNumeric);
        Assert.assertThat(actualId, is(expectedId));
    }

    @Test
    public void toCleanIdentifier_shouldConcatenateIdentifierWithUnderscore() {
        String idWithSpaces = "Question 1";
        String expectedId = "question_1";

        String actualId = Utils.toCleanIdentifier(idWithSpaces);
        Assert.assertThat(actualId, is(expectedId));
    }

    @Test
    public void toCleanIdentifier_shouldRepresentIdentifierInASingleLine() {
        String idWithMultipleLines = "Question\n1";
        String expectedId = "question1";

        String actualId = Utils.toCleanIdentifier(idWithMultipleLines);
        Assert.assertThat(actualId, is(expectedId));

        idWithMultipleLines = "Question\r1";
        expectedId = "question1";

        actualId = Utils.toCleanIdentifier(idWithMultipleLines);
        Assert.assertThat(actualId, is(expectedId));

        idWithMultipleLines = "Question\r\n1";
        expectedId = "question1";

        actualId = Utils.toCleanIdentifier(idWithMultipleLines);
        Assert.assertThat(actualId, is(expectedId));
    }

    @Test
    public void toCleanIdentifier_shouldRemoveTabs() {
        String idWithTab = "Question\t1";
        String expectedId = "question1";

        String actualId = Utils.toCleanIdentifier(idWithTab);
        Assert.assertThat(actualId, is(expectedId));
    }

    @Test
    public void removeSpaces_shouldRemoveSpaces() {
        String withMultipleSpaces = "Question  1";
        String withLeadingSpaces = " Question 1";
        String withTrailingSpaces = "Question 1 ";
        String expected = "Question 1";

        String actual1 = Utils.removeSpaces(withMultipleSpaces);
        String actual2 = Utils.removeSpaces(withLeadingSpaces);
        String actual3 = Utils.removeSpaces(withTrailingSpaces);
        Assert.assertThat(actual1, is(expected));
        Assert.assertThat(actual2, is(expected));
        Assert.assertThat(actual3, is(expected));
    }

}