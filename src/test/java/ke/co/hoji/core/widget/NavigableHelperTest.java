package ke.co.hoji.core.widget;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.OperatorDescriptor;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.data.model.ValidationResult;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.rule.RuleEvaluator;
import ke.co.hoji.core.service.model.RecordService;
import ke.co.hoji.core.widget.NavigableHelper.Direction;
import ke.co.idadi.hoji.TestUtil;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by gitahi on 30/07/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class NavigableHelperTest {

    @Mock
    private RecordService recordService;

    @Mock
    private WidgetManager widgetManager;

    @Mock
    private Navigable navigable;

    @Mock
    private Widget activeWidget;

    private RuleEvaluator ruleEvaluator = new RuleEvaluator();

    @Before
    public void setUp() {
        when(navigable.isMain()).thenReturn(true);
        when(navigable.validateWidget()).thenReturn(ValidationResult.okay());
        when(navigable.getResponse()).thenReturn(null);
    }

    /**
     * @verifies set the current field in the navigator to the first field in the record if necessary.
     * @see NavigableHelper#first()
     */
    @Test
    public void first_shouldSetTheCurrentFieldInTheNavigatorToTheFirstFieldInTheRecordIfAny() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();

        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);

        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);

        navigableHelper.first();
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(0)));

        navigableHelper.next();
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(1)));

        navigableHelper.first();
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(0)));

        navigableHelper.first();
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(0)));
    }

    /**
     * @verifies set the current field in the navigator to the previous field in the record if necessary.
     * @see NavigableHelper#previous()
     */
    @Test
    public void previous_shouldSetTheCurrentFieldInTheNavigatorToThePreviousFieldInTheRecordIfAny() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();

        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);

        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);

        navigableHelper.first();
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(0)));

        navigableHelper.next();
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(1)));

        navigableHelper.previous();
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(0)));

        navigableHelper.previous();
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(0)));
    }

    /**
     * @verifies set the current field in the navigator to the last previously visited and saved field in the record, if
     * any.
     * @see NavigableHelper#last()
     */
    @Test
    public void last_shouldSetTheCurrentFieldInTheNavigatorToTheLastPreviouslyVisitedAndSavedFieldInTheRecordIfAny() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();

        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);

        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);

        navigableHelper.first();
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(0)));

        navigableHelper.next();
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(1)));

        navigableHelper.next();
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(2)));

        navigableHelper.last();
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(1)));
    }

    /**
     * @verifies set the current field in the navigator to the field passed.
     * @see NavigableHelper#navigateTo(Field)
     */
    @Test
    public void navigateTo_shouldSetTheCurrentFieldInTheNavigatorToTheFieldPassed() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();

        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);

        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);

        navigableHelper.first();
        navigableHelper.next();
        navigableHelper.next();
        navigableHelper.next();

        navigableHelper.navigateTo(record.getFieldParent().getChildren().get(0));
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(0)));

        navigableHelper.navigateTo(record.getFieldParent().getChildren().get(1));
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(1)));
    }

    /**
     * @verifies reset the active widget. @see {@link Widget#reset();}
     * @see NavigableHelper#resetWidget()
     */
    @Test
    public void resetWidget_shouldResetTheActiveWidgetSeeLinkWidgetreset() throws Exception {
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.setActiveWidget(activeWidget);
        navigableHelper.resetWidget();
        verify(activeWidget, times(1)).reset();
    }

    /**
     * @verifies set {@link WidgetEvent.EventCause} to user on the current widget.
     * @see NavigableHelper#resetWidget()
     */
    @Test
    public void resetWidget_shouldSetLinkWidgetEventEventCauseToUserOnTheCurrentWidget() throws Exception {
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.setActiveWidget(activeWidget);
        navigableHelper.resetWidget();
        verify(activeWidget, times(1)).setEventCause(WidgetEvent.EventCause.PROGRAM);
        verify(activeWidget, times(1)).setEventCause(WidgetEvent.EventCause.USER);
    }

    /**
     * @verifies set navigable dirty to true if widget event cause is {@link WidgetEvent.EventCause#USER}
     * @see NavigableHelper#responseChanging(WidgetEvent)
     */
    @Test
    public void responseChanging_shouldSetNavigableDirtyToTrueIfWidgetEventCauseIsLinkWidgetEventEventCauseUSER() throws Exception {
        WidgetEvent widgetEvent = new WidgetEvent(activeWidget, WidgetEvent.EventCause.USER);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.setDirty(false);
        navigableHelper.responseChanging(widgetEvent);
        Assert.assertThat(navigableHelper.isDirty(), is(Boolean.TRUE));
    }

    /**
     * @verifies not set navigable dirty to true if widget event cause is {@link WidgetEvent.EventCause#USER}
     * @see NavigableHelper#responseChanging(WidgetEvent)
     */
    @Test
    public void responseChanging_shouldNotSetNavigableDirtyToTrueIfWidgetEventCauseIsLinkWidgetEventEventCausePROGRAM() throws Exception {
        WidgetEvent widgetEvent = new WidgetEvent(activeWidget, WidgetEvent.EventCause.PROGRAM);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.setDirty(false);
        navigableHelper.responseChanging(widgetEvent);
        Assert.assertThat(navigableHelper.isDirty(), is(Boolean.FALSE));
    }

    /**
     * @verifies set navigable dirty to true if widget event cause is {@link WidgetEvent.EventCause#USER}
     * @see NavigableHelper#responseChanged(WidgetEvent)
     */
    @Test
    public void responseChanged_shouldSetNavigableDirtyToTrueIfWidgetEventCauseIsLinkWidgetEventEventCauseUSER() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        when(activeWidget.getLiveField()).thenReturn(new LiveField(record.getFieldParent().getChildren().get(0)));
        WidgetEvent widgetEvent = new WidgetEvent(activeWidget, WidgetEvent.EventCause.USER);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        navigableHelper.setDirty(false);
        navigableHelper.first();
        navigableHelper.responseChanged(widgetEvent);
        Assert.assertThat(navigableHelper.isDirty(), is(Boolean.TRUE));
    }

    /**
     * @verifies not set navigable dirty to true if widget event cause is {@link WidgetEvent.EventCause#PROGRAM}
     * @see NavigableHelper#responseChanged(WidgetEvent)
     */
    @Test
    public void responseChanged_shouldNotSetNavigableDirtyToTrueIfWidgetEventCauseIsLinkWidgetEventEventCausePROGRAM() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        WidgetEvent widgetEvent = new WidgetEvent(activeWidget, WidgetEvent.EventCause.PROGRAM);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        navigableHelper.setDirty(false);
        navigableHelper.first();
        navigableHelper.responseChanged(widgetEvent);
        Assert.assertThat(navigableHelper.isDirty(), is(Boolean.FALSE));
    }

    /**
     * @verifies call {@link NavigableHelper#next();} if widget event is direction agnostic
     * @see NavigableHelper#responseChanged(WidgetEvent)
     */
    @Test
    public void responseChanged_shouldCallLinkNavigableHelpernextIfWidgetEventIsDirectionAgnostic() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        when(navigable.autoNavigate()).thenReturn(true);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.direction = NavigableHelper.Direction.BACKWARD;
        WidgetEvent widgetEvent = new WidgetEvent(activeWidget, WidgetEvent.EventCause.USER, true);
        navigableHelper.first();
        navigableHelper.responseChanged(widgetEvent);
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(1)));
    }

    /**
     * @verifies call {@link NavigableHelper#next();} if direction is forward
     * @see NavigableHelper#responseChanged(WidgetEvent)
     */
    @Test
    public void responseChanged_shouldCallLinkNavigableHelpernextIfDirectionIsForward() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        when(navigable.autoNavigate()).thenReturn(true);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        WidgetEvent widgetEvent = new WidgetEvent(activeWidget, WidgetEvent.EventCause.USER, false);
        navigableHelper.first();
        navigableHelper.direction = NavigableHelper.Direction.FORWARD;
        navigableHelper.responseChanged(widgetEvent);
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(1)));
    }

    /**
     * @verifies call {@link NavigableHelper#next();} if direction is backward and widget even is not direction
     * agnostic
     * @see NavigableHelper#responseChanged(WidgetEvent)
     */
    @Test
    public void responseChanged_shouldCallLinkNavigableHelpernextIfDirectionIsForwardAndWidgetIFooFirstField() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        when(activeWidget.getLiveField()).thenReturn(new LiveField(record.getFieldParent().getChildren().get(0)));
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        WidgetEvent widgetEvent = new WidgetEvent(activeWidget, WidgetEvent.EventCause.USER, false);
        navigableHelper.first();
        navigableHelper.direction = NavigableHelper.Direction.BACKWARD;
        navigableHelper.responseChanged(widgetEvent);
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(1)));
    }

    /**
     * @verifies not call {@link NavigableHelper#next();} if direction is backward and widget even is not direction
     * agnostic
     * @see NavigableHelper#responseChanged(WidgetEvent)
     */
    @Test
    public void responseChanged_shouldNotCallLinkNavigableHelpernextIfDirectionIsBackwardAndWidgetEvenIsNotDirectionAgnosticAndWidgetIsNotForFirstField() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        when(activeWidget.getLiveField()).thenReturn(new LiveField(record.getFieldParent().getChildren().get(1)));
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        WidgetEvent widgetEvent = new WidgetEvent(activeWidget, WidgetEvent.EventCause.USER, false);
        navigableHelper.first();
        navigableHelper.direction = NavigableHelper.Direction.BACKWARD;
        navigableHelper.responseChanged(widgetEvent);
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(0)));
    }

    /**
     * @verifies set direction to backward.
     * @see NavigableHelper#first()
     */
    @Test
    public void first_shouldSetDirectionToForward() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.direction = NavigableHelper.Direction.FORWARD;
        navigableHelper.first();
        Assert.assertThat(navigableHelper.direction, is(Direction.BACKWARD));
    }

    /**
     * @verifies set direction to backward.
     * @see NavigableHelper#previous()
     */
    @Test
    public void previous_shouldSetDirectionToBackward() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.direction = NavigableHelper.Direction.FORWARD;
        navigableHelper.previous();
        Assert.assertThat(navigableHelper.direction, is(NavigableHelper.Direction.BACKWARD));
    }

    /**
     * @verifies set direction to backward.
     * @see NavigableHelper#last()
     */
    @Test
    public void last_shouldSetDirectionToBackward() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.direction = NavigableHelper.Direction.BACKWARD;
        navigableHelper.last();
        Assert.assertThat(navigableHelper.direction, is(NavigableHelper.Direction.FORWARD));
    }

    /**
     * @verifies if navigable has a parent navigable, set dirty should be called on it too.
     * @see NavigableHelper#setDirty(boolean)
     */
    @Test
    public void setDirty_shouldIfNavigableHasAParentNavigableSetDirtyShouldBeCalledOnItToo() throws Exception {
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        Navigable parent = mock(Navigable.class);
        when(navigable.getParentNavigable()).thenReturn(parent);
        navigableHelper.setDirty(true);
        verify(parent, times(1)).setDirty(true);
    }

    /**
     * @verifies read the response of the active widget.
     * @see NavigableHelper#readResponse()
     */
    @Test
    public void readResponse_shouldReadTheResponseOfTheActiveWidget() throws Exception {
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.setActiveWidget(activeWidget);
        navigableHelper.readResponse();
        verify(activeWidget, times(1)).readResponse();
    }

    /**
     * @verifies validate the active widget.
     * @see NavigableHelper#validate()
     */
    @Test
    public void validate_shouldValidateTheActiveWidget() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.setActiveWidget(activeWidget);
        navigableHelper.validate();
        verify(activeWidget, times(1)).validate(record);
    }

    /**
     * @verifies do nothing if any interruption flag is set to abort.
     * @see NavigableHelper#next(int, int, int)
     */
    @Test
    public void next_shouldDoNothingIfAnyInterruptionFlagIsSetToAbort() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.first();

        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.ABORT);
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(0)));
        verify(recordService, times(0)).save(record);

        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.ABORT, Navigator.Interruption.CHECK);
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(0)));
        verify(recordService, times(0)).save(record);

        navigableHelper.next(Navigator.Interruption.ABORT, Navigator.Interruption.CHECK, Navigator.Interruption.CHECK);
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(0)));
        verify(recordService, times(0)).save(record);

        navigableHelper.next(Navigator.Interruption.ABORT, Navigator.Interruption.ABORT, Navigator.Interruption.ABORT);
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(0)));
        verify(recordService, times(0)).save(record);
    }

    /**
     * @verifies set the current field in the navigator to the next field if any interruption flag is set to execute.
     * @see NavigableHelper#next(int, int, int)
     */
    @Test
    public void next_shouldSetTheCurrentFieldInTheNavigatorToTheNextFieldIfAnyInterruptionFlagIsSetToExecute() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.first();

        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.EXECUTE);
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(1)));

        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.CHECK);
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(2)));

        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.EXECUTE, Navigator.Interruption.CHECK);
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(3)));
    }

    /**
     * @verifies save the record if any interruption flag is set to execute.
     * @see NavigableHelper#next(int, int, int)
     */
    @Test
    public void next_shouldSaveTheRecordIfAnyInterruptionFlagIsSetToExecute() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.first();

        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.EXECUTE);
        verify(recordService, times(1)).save(record);

        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.CHECK);
        verify(recordService, times(2)).save(record);

        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.EXECUTE, Navigator.Interruption.CHECK);
        verify(recordService, times(3)).save(record);
    }

    /**
     * @verifies set the current field in the navigator to the next field all interruption flags are check, widget
     * validates okay, response is not record end and there is no violation.
     * @see NavigableHelper#next(int, int, int)
     */
    @Test
    public void next_shouldSetTheCurrentFieldInTheNavigatorToTheNextFieldAllInterruptionFlagsAreCheckWidgetValidatesOkayResponseIsNotRecordEndAndThereIsNoViolation() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.first();

        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.CHECK);
        Assert.assertThat(navigator.getLiveField().getField(), is(record.getFieldParent().getChildren().get(1)));
    }

    /**
     * @verifies save the record interruption flags are check, widget validates okay, response is not record end and
     * there is no violation.
     * @see NavigableHelper#next(int, int, int)
     */
    @Test
    public void next_shouldSaveTheRecordInterruptionFlagsAreCheckWidgetValidatesOkayResponseIsNotRecordEndAndThereIsNoViolation() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.first();

        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.CHECK);
        verify(recordService, times(1)).save(record);
    }

    /**
     * @verifies process validation if widget does not validate okay and validation interruption is check.
     * @see NavigableHelper#next(int, int, int)
     */
    @Test
    public void next_shouldProcessValidationIfWidgetDoesNotValidateOkayAndValidationInterruptionIsCheck() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.first();
        when(navigable.validateWidget()).thenReturn(ValidationResult.stop(null));
        navigableHelper.next(Navigator.Interruption.EXECUTE, Navigator.Interruption.EXECUTE, Navigator.Interruption.CHECK);
        verify(navigable, times(1)).processValidation();
    }

    /**
     * @verifies not process validation if widget does not validate okay but validation interruption is not check.
     * @see NavigableHelper#next(int, int, int)
     */
    @Test
    public void next_shouldNotProcessValidationIfWidgetDoesNotValidateOkayButValidationInterruptionIsNotCheck() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.first();
        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.ABORT);
        verify(navigable, times(0)).processValidation();
    }

    /**
     * @verifies process skip violation next field violates skip.
     * @see NavigableHelper#next(int, int, int)
     */
    @Test
    public void next_shouldProcessSkipViolationNextFieldViolatesSkip() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        HojiContext hojiContext = mock(HojiContext.class);

        Field first = record.getFieldParent().getChildren().get(0);
        Field third = record.getFieldParent().getChildren().get(2);

        FieldType fieldType = new FieldType(1, "", null);
        fieldType.setResponseClass("ke.co.hoji.core.response.TextResponse");
        first.setType(fieldType);

        Rule forwardSkip = new Rule(1, BigDecimal.ONE, "Violate skip rule", Rule.Type.FORWARD_SKIP, 0);
        forwardSkip.setOwner(first);
        forwardSkip.setTarget(third);
        forwardSkip.setOperatorDescriptor(new OperatorDescriptor(1, "Equals", "ke.co.hoji.core.rule.Equals"));
        first.getForwardSkips().add(forwardSkip);

        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.first();

        when(navigable.getResponse()).thenReturn(Response.create(hojiContext, navigator.getLiveField(), "Do not violate skip rule", true));

        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.CHECK);
        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.CHECK);
        navigableHelper.previous();
        navigableHelper.previous();

        when(navigable.getResponse()).thenReturn(Response.create(hojiContext, navigator.getLiveField(), "Violate skip rule", true));
        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.CHECK);

        verify(navigable, times(1)).processSkipViolation();
    }

    /**
     * @verifies not process skip violation next field violates skip but validation interruption is not check.
     * @see NavigableHelper#next(int, int, int)
     */
    @Test
    public void next_shouldNotProcessSkipViolationNextFieldViolatesSkipButValidationInterruptionIsNotCheck() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields();
        HojiContext hojiContext = mock(HojiContext.class);

        Field first = record.getFieldParent().getChildren().get(0);
        Field third = record.getFieldParent().getChildren().get(2);

        FieldType fieldType = new FieldType(1, "", null);
        fieldType.setResponseClass("ke.co.hoji.core.response.TextResponse");
        first.setType(fieldType);

        Rule forwardSkip = new Rule(1, BigDecimal.ONE, "Violate skip rule", Rule.Type.FORWARD_SKIP, 0);
        forwardSkip.setOwner(first);
        forwardSkip.setTarget(third);
        forwardSkip.setOperatorDescriptor(new OperatorDescriptor(1, "Equals", "ke.co.hoji.core.rule.Equals"));
        first.getForwardSkips().add(forwardSkip);

        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.first();

        when(navigable.getResponse()).thenReturn(Response.create(hojiContext, navigator.getLiveField(),
                "Do not violate skip rule", true));

        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.CHECK);
        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.CHECK);
        navigableHelper.previous();
        navigableHelper.previous();

        when(navigable.getResponse()).thenReturn(Response.create(hojiContext, navigator.getLiveField(),
                "Do not violate skip rule", true));
        navigableHelper.next(Navigator.Interruption.CHECK, Navigator.Interruption.CHECK, Navigator.Interruption.CHECK);

        verify(navigable, times(0)).processSkipViolation();
    }

    /**
     * @verifies notify end if there is no next field.
     * @see NavigableHelper#next(int, int, int)
     */
    @Test
    public void next_shouldNotifyEndIfThereIsNoNextField() throws Exception {
        Record record = TestUtil.createRecordWithoutLiveFields(2);
        Navigator navigator = new Navigator(widgetManager, ruleEvaluator, recordService, record);
        when(navigable.getNavigator()).thenReturn(navigator);
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.first();
        navigableHelper.next();
        navigableHelper.next();
        verify(navigable, times(1)).processRecordEnd(record, navigator);
    }

    /**
     * @verifies set dirty to true.
     * @see NavigableHelper#resetWidget()
     */
    @Test
    public void resetWidget_shouldSetDirtyToTrue() throws Exception {
        NavigableHelper navigableHelper = new NavigableHelper(navigable, widgetManager);
        navigableHelper.setActiveWidget(activeWidget);
        navigableHelper.setDirty(false);
        navigableHelper.resetWidget();
        Assert.assertThat(navigableHelper.isDirty(), Is.is(true));
    }
}
