package ke.co.hoji.core.calculation;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.response.SingleChoiceResponse;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;

import static ke.co.hoji.core.calculation.CalculationEvaluator.UTILS_SCRIPT_KEY;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CalculationEvaluatorTest {

    private ScriptEngine scriptEngine = new ScriptEngineManager().getEngineByName("nashorn");

    @Mock
    private CalculationUtils calculationUtils;

    @Test
    public void evaluateScript_shouldInvokeNamedFunctionIfItExists() {
        String script = "function calculate() { return (1 + 1); }";
        Object actualResult = new CalculationEvaluator(scriptEngine).evaluateScript(script, Field.ScriptFunctions.CALCULATE,
            calculationUtils, new Field());

        Object expectedResult = 2;
        assertThat(actualResult, is(expectedResult));
    }

    @Test(expected = CalculationException.class)
    public void evaluateScript_shouldThrowExceptionIfNamedFunctionDoesNotExist() {
        String script = "function validate() { return (1 + 1); }";
        new CalculationEvaluator(scriptEngine).evaluateScript(script, Field.ScriptFunctions.CALCULATE,
            calculationUtils, new Field());
    }

    @Test(expected = CalculationException.class)
    public void evaluateScript_shouldThrowCalculationException() {
        String script = "function calculate() { utils.throwException(); }";

        new CalculationEvaluator(scriptEngine).evaluateScript(script, Field.ScriptFunctions.CALCULATE,
            calculationUtils, new Field());
    }

    @Test
    public void evaluateScript_shouldAccessCalculationUtilsFromScript() {
        String textValue = "Testing 1, 2, 3...";
        when(calculationUtils.getValue("key")).thenReturn(textValue);
        String script = "function calculate() { return " + UTILS_SCRIPT_KEY + ".getValue('key'); }";

        Object actualResult = new CalculationEvaluator(scriptEngine).evaluateScript(script, Field.ScriptFunctions.CALCULATE,
            calculationUtils, new Field());

        assertThat(actualResult, Matchers.<Object>is(textValue));

    }

    @Test
    public void evaluateScript_shouldAccessPreviousResponseFromScript() {
        Field favColor = new Field(1, "Fav color", true, null, BigDecimal.ONE, false, false, false, null, null);
        FieldType singleChoice = new FieldType(1, "Single Choice", FieldType.Code.SINGLE_CHOICE);
        singleChoice.setResponseClass(SingleChoiceResponse.class.getName());
        favColor.setType(singleChoice);
        Choice blue = new Choice(1, "Blue", "B", 0, false);
        ChoiceGroup colors = new ChoiceGroup(1, "Colors");
        colors.setChoices(Collections.singletonList(blue));
        favColor.setChoiceGroup(colors);
        LiveField prevLf = new LiveField(favColor);
        Response response = Response.create(null, prevLf, blue, true);
        prevLf.setResponse(response);
        CalculationUtils calculationUtils = new CalculationUtils(1, null, Collections.singletonList(prevLf), null,
            new Date());
        String script = "function calculate() { return " + UTILS_SCRIPT_KEY + ".getValue(" + favColor.getId() + ")" +
            ".getCode(); }";

        Object actualResult = new CalculationEvaluator(scriptEngine).evaluateScript(script, Field.ScriptFunctions.CALCULATE,
            calculationUtils, new Field());

        assertThat(actualResult, Matchers.<Object>is(blue.getCode()));
    }
}