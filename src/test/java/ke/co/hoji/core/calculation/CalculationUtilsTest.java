package ke.co.hoji.core.calculation;

import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.service.model.PropertyService;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.tz.UTCProvider;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(MockitoJUnitRunner.class)
public class CalculationUtilsTest {

    private final int SURVEY_ID = 1;

    @Mock
    private ModelServiceManager modelServiceManager;

    @Mock
    private PropertyService propertyService;

    private CalculationUtils calculationUtils;

    @Before
    public void setUp() throws Exception {
        DateTimeZone.setProvider(new UTCProvider()); // To prevent ZoneInfoMap not found IOException.
        calculationUtils = new CalculationUtils(SURVEY_ID, modelServiceManager, null, null,null);
        Mockito.when(modelServiceManager.getPropertyService()).thenReturn(propertyService);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getValue() {
    }

    @Test
    public void getChoiceById() {
    }

    @Test
    public void getChoiceByName() {
    }

    @Test
    public void getTextProperty() {
        String key = "KEY";
        String value = "VALUE";
        Mockito.when(propertyService.getValue(key, SURVEY_ID)).thenReturn(value);
        String property = calculationUtils.getTextProperty(key);
        Assert.assertTrue(property.equals(value));
    }

    @Test
    public void getNumberProperty() {
        String key = "KEY";
        Long lng = 1986L;
        String value = lng.toString();
        Mockito.when(propertyService.getValue(key, SURVEY_ID)).thenReturn(value);
        Long property = calculationUtils.getNumberProperty(key);
        Assert.assertEquals(property, lng);
    }

    @Test
    public void getDecimalProperty() {
        String key = "KEY";
        Double dbl = 1986.86;
        String value = dbl.toString();
        Mockito.when(propertyService.getValue(key, SURVEY_ID)).thenReturn(value);
        Double property = calculationUtils.getDecimalProperty(key);
        Assert.assertEquals(property, dbl);
    }

    @Test
    public void getDateProperty() {
        String key = "KEY";
        Date date = new LocalDate().toDate();
        String value = new SimpleDateFormat("yyyy-MM-dd").format(date);
        Mockito.when(propertyService.getValue(key, SURVEY_ID)).thenReturn(value);
        Date property = calculationUtils.getDateProperty(key);
        Assert.assertEquals(property, date);
    }

    @Test
    public void sumStrict() {
    }

    @Test
    public void sumLenient() {
    }

    @Test
    public void doubleToLong() {
        Double dbl = 1986.00;
        Long lng = calculationUtils.doubleToLong(dbl);
        Assert.assertEquals((long) lng, 1986L);
    }

    @Test
    public void createDate() {
    }

    @Test
    public void dateDifference() {
    }

    @Test
    public void compareDatesWithoutTime() {
    }

    @Test
    public void addPeriod() {
    }

    @Test
    public void createValidationResult() {
    }

    @Test
    public void formatIsoDate() {
    }

    @Test
    public void formatIsoTime() {
    }
}