package ke.co.hoji.core.rule;

import ke.co.hoji.core.response.Response;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created by gitahi on 07/08/15.
 */
public class EqualsTest {

    /**
     * @verifies return true if actual value of response compares to comparable value computed from first value with
     * result equal to zero.
     * @see Equals#evaluate(ke.co.hoji.core.response.Response, java.util.List)
     */
    @Test
    public void evaluate_shouldReturnTrueIfActualValueOfResponseComparesToComparableValueComputedFromFirstValueWithResultEqualToZero() throws Exception {
        String against = "1";
        Response response = Mockito.mock(Response.class);
        Comparable comparable = Mockito.mock(Comparable.class);
        Mockito.when(response.comparableFromActualValue()).thenReturn(comparable);
        Mockito.when(response.comparableFromRuleString(against)).thenReturn(comparable);
        Mockito.when(comparable.compareTo(comparable)).thenReturn(0);
        Assert.assertThat(new ComparableHelper().evaluate(response, against, ComparableHelper.Sign.EQUALS), Is.is(true));
    }

    /**
     * @verifies return false if actual value of response compares to comparable value computed from first value with
     * result not equal to zero.
     * @see Equals#evaluate(ke.co.hoji.core.response.Response, java.util.List)
     */
    @Test
    public void evaluate_shouldReturnFalseIfActualValueOfResponseComparesToComparableValueComputedFromFirstValueWithResultNotEqualToZero() throws Exception {
        String against = "1";
        Response response = Mockito.mock(Response.class);
        Comparable comparable = Mockito.mock(Comparable.class);
        Mockito.when(response.getActualValue()).thenReturn(comparable);
        Mockito.when(response.comparableFromRuleString(against)).thenReturn(comparable);
        Mockito.when(comparable.compareTo(comparable)).thenReturn(-1);
        Assert.assertThat(new ComparableHelper().evaluate(response, against, ComparableHelper.Sign.EQUALS), Is.is(false));
    }
}
