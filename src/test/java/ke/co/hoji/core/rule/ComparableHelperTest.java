package ke.co.hoji.core.rule;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.response.Response;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Created by gitahi on 10/08/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class ComparableHelperTest {

    @Mock
    private LiveField liveField;
    /**
     * @verifies return true if both the actual value of response and the against value are null.
     * @see ComparableHelper#evaluate(ke.co.hoji.core.response.Response, String, String)
     */
    @Test
    public void evaluate_shouldReturnTrueIfBothTheActualValueOfResponseAndTheAgainstValueAreNull() throws Exception {
        Field field = RuleTestUtils.createFieldWithResponse("ke.co.hoji.core.response.NumberResponse", null);
        Assert.assertThat(new ComparableHelper().evaluate(field.getResponse(), null, ComparableHelper.Sign.EQUALS), Is.is(true));
    }

    /**
     * @verifies return false if the actual value of response is null and the against value is not.
     * @see ComparableHelper#evaluate(ke.co.hoji.core.response.Response, String, String)
     */
    @Test
    public void evaluate_shouldReturnFalseIfTheActualValueOfResponseIsNullAndTheAgainstValueIsNot() throws Exception {
        Field field = RuleTestUtils.createFieldWithResponse("ke.co.hoji.core.response.NumberResponse", null);
        Assert.assertThat(new ComparableHelper().evaluate(field.getResponse(), "1", ComparableHelper.Sign.EQUALS), Is.is(false));
    }

    /**
     * @verifies return false if the actual value of response is not null and the against value is.
     * @see ComparableHelper#evaluate(ke.co.hoji.core.response.Response, String, String)
     */
    @Test
    public void evaluate_shouldReturnFalseIfTheActualValueOfResponseIsNotNullAndTheAgainstValueIs() throws Exception {
        Field field = RuleTestUtils.createFieldWithResponse("ke.co.hoji.core.response.NumberResponse", 1);
        Assert.assertThat(new ComparableHelper().evaluate(field.getResponse(), null, ComparableHelper.Sign.EQUALS), Is.is(false));
    }

    /**
     * @verifies compute the comparable value from the against value if both response actual value and against value are
     * not null
     * @see ComparableHelper#evaluate(ke.co.hoji.core.response.Response, String, String)
     */
    @Test
    public void evaluate_shouldComputeTheComparableValueFromTheAgainstValueIfBothResponseActualValueAndAgainstValueAreNotNull() throws Exception {
        String against = "1";
        Response response = Mockito.mock(Response.class);
        Comparable comparable = Mockito.mock(Comparable.class);
        Mockito.when(response.comparableFromActualValue()).thenReturn(comparable);
        Mockito.when(response.comparableFromRuleString(against)).thenReturn(comparable);
        new ComparableHelper().evaluate(response, against, ComparableHelper.Sign.EQUALS);
        Mockito.verify(response, Mockito.times(1)).comparableFromRuleString(against);
    }

    /**
     * @verifies compare actual response to computed against value if response actual value and against value are not
     * null
     * @see ComparableHelper#evaluate(ke.co.hoji.core.response.Response, String, String)
     */
    @Test
    public void evaluate_shouldCompareActualResponseToComputedAgainstValueIfResponseActualValueAndAgainstValueAreNotNull() throws Exception {
        String against = "1";
        Response response = Mockito.mock(Response.class);
        Comparable comparable = Mockito.mock(Comparable.class);
        Mockito.when(response.comparableFromActualValue()).thenReturn(comparable);
        Mockito.when(response.comparableFromRuleString(against)).thenReturn(comparable);
        new ComparableHelper().evaluate(response, against, ComparableHelper.Sign.EQUALS);
        Mockito.verify(comparable, Mockito.times(1)).compareTo(comparable);
    }

    /**
     * @verifies return true if both the response and the against value are null.
     * @see ComparableHelper#evaluate(Response, String, String)
     */
    @Test
    public void evaluate_shouldReturnTrueIfBothTheResponseAndTheAgainstValueAreNull() throws Exception {
        Field field = RuleTestUtils.createFieldWithoutResponse();
        Assert.assertThat(new ComparableHelper().evaluate(field.getResponse(), null, ComparableHelper.Sign.EQUALS), Is.is(true));
    }

    /**
     * @verifies return false if response is null and the against value is not.
     * @see ComparableHelper#evaluate(Response, String, String)
     */
    @Test
    public void evaluate_shouldReturnFalseIfResponseIsNullAndTheAgainstValueIsNot() throws Exception {
        Field field = RuleTestUtils.createFieldWithoutResponse();
        Assert.assertThat(new ComparableHelper().evaluate(field.getResponse(), "1", ComparableHelper.Sign.EQUALS), Is.is(false));
    }
}
