package ke.co.hoji.core.rule;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.response.Response;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gitahi on 01/12/15.
 */
public class BitwiseAndTest {

    /**
     * @verifies return false if any of the operands are null.
     * @see BitwiseAnd#evaluate(ke.co.hoji.core.response.Response, java.util.List)
     */
    @Test
    public void evaluate_shouldReturnFalseIfAnyOfTheOperandsAreNull() throws Exception {
        String against = "1#0|2#0|3#1";
        Response response = Mockito.mock(Response.class);
        Comparable comparable = "1#0|2#0|3#1";
        Mockito.when(response.getActualValue()).thenReturn(comparable);
        Mockito.when(response.comparableFromRuleString(against)).thenReturn(comparable);
        List<String> values = new ArrayList<>();
        Assert.assertThat(new BitwiseAnd().evaluate(response, values), Is.is(false));
        values.add(against);
        Assert.assertThat(new BitwiseAnd().evaluate(response, values), Is.is(false));
    }

    /**
     * @verifies return false if any of the operands are not integers.
     * @see BitwiseAnd#evaluate(ke.co.hoji.core.response.Response, java.util.List)
     */
    @Test
    public void evaluate_shouldReturnFalseIfAnyOfTheOperandsAreNotIntegers() throws Exception {
        String against = "1#0|2#0|3#1";
        Response response = Mockito.mock(Response.class);
        Comparable comparable = "001";
        Mockito.when(response.getActualValue()).thenReturn(comparable);
        Mockito.when(response.comparableFromRuleString(against)).thenReturn(comparable);
        List<String> values = new ArrayList<>();
        values.add(against);
        Assert.assertThat(new BitwiseAnd().evaluate(response, values), Is.is(false));
    }

    /**
     * @verifies return true if the logical and result is equal to the rule value.
     * @see BitwiseAnd#evaluate(ke.co.hoji.core.response.Response, java.util.List)
     */
    @Test
    public void evaluate_shouldReturnTrueIfTheLogicalAndResultIsEqualToTheRuleValue() throws Exception {
        {
            String against = "1#0|2#0|3#1";
            Response response = Mockito.mock(Response.class);
            Comparable comparable = Utils.parseBin("001");
            Mockito.when(response.comparableFromActualValue()).thenReturn(comparable);
            Mockito.when(response.comparableFromRuleString(against)).thenReturn(comparable);
            List<String> values = new ArrayList<>();
            values.add(against);
            Assert.assertThat(new BitwiseAnd().evaluate(response, values), Is.is(true));
        }
        {
            String against = "1#0|2#0|3#1";
            Response response = Mockito.mock(Response.class);
            Comparable comparable = Utils.parseBin("101");
            Mockito.when(response.comparableFromActualValue()).thenReturn(comparable);
            Mockito.when(response.comparableFromRuleString(against)).thenReturn(comparable);
            List<String> values = new ArrayList<>();
            values.add(against);
            Assert.assertThat(new BitwiseAnd().evaluate(response, values), Is.is(true));
        }
        {
            String against = "1#0|2#0|3#1";
            Response response = Mockito.mock(Response.class);
            Comparable comparable = Utils.parseBin("111");
            Mockito.when(response.comparableFromActualValue()).thenReturn(comparable);
            Mockito.when(response.comparableFromRuleString(against)).thenReturn(comparable);
            List<String> values = new ArrayList<>();
            values.add(against);
            Assert.assertThat(new BitwiseAnd().evaluate(response, values), Is.is(true));
        }
        {
            String against = "1#0|2#0|3#1";
            Response response = Mockito.mock(Response.class);
            Comparable comparable = Utils.parseBin("110");
            Mockito.when(response.comparableFromActualValue()).thenReturn(comparable);
            Mockito.when(response.comparableFromRuleString(against)).thenReturn(comparable);
            List<String> values = new ArrayList<>();
            values.add(against);
            Assert.assertThat(new BitwiseAnd().evaluate(response, values), Is.is(true));
        }
    }
}
