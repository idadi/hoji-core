package ke.co.hoji.core.rule;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.response.Response;

/**
 * Created by gitahi on 07/08/15.
 */
class RuleTestUtils {

    static Field createFieldWithResponse(String responseClass, Object value) {
        Field field = new Field();
        FieldType fieldType = new FieldType(1, "", "");
        fieldType.setResponseClass(responseClass);
        field.setType(fieldType);
        LiveField liveField = new LiveField(field);
        field.setResponse(Response.create(null, liveField, value, true));
        return field;
    }

    static Field createFieldWithoutResponse() {
        Field field = new Field();
        return field;
    }
}
