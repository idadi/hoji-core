package ke.co.hoji.core.rule;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.response.Response;
import ke.co.idadi.hoji.TestUtil;
import org.hamcrest.core.Is;
import org.hamcrest.core.IsNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

/**
 * Created by gitahi on 13/08/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class RuleOperatorTest {

    /**
     * @verifies call evaluate with null response if material field is not already a live field in record
     * @see RuleOperator#evaluate(Rule, Record, Field)
     */
    @Test
    public void evaluate_shouldCallEvaluateWithNullResponseIfMaterialFieldIsNotAlreadyALiveFieldInRecord() throws Exception {
        RuleOperator ruleOperator = new RuleOperator() {
            @Override
            protected boolean evaluate(Response response, List<String> values) {
                Assert.assertThat(response, Is.is(IsNull.nullValue()));
                return false;
            }
        };
        ruleOperator.evaluate(new Rule("", Rule.Type.FORWARD_SKIP, new Field()), TestUtil.createRecordWithoutLiveFields(), null);
    }
}
