package ke.co.hoji.core.data;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ChoiceTests {

    @Test
    public void constructor_shouldMakeCopyOfChoice() {
        Choice original = new Choice(1, "Test", "T", 0, false);
        original.setParent(original);

        Choice copy = new Choice(original);

        assertFalse(original == copy);
        assertFalse(original == copy.getParent());
        assertTrue(copy == copy.getParent());
        assertEquals(original, copy);
        assertEquals(copy, copy.getParent());
    }

}