package ke.co.hoji.core.data.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class FieldTest {

    /**
     * @verifies that {@link Field#getResolvedHeader()} returns {@link Field#getDescription()} when no references are specified
     */
    @Test
    public void getResolvedHeader_shouldReturnFieldDescriptionWhenNoPlaceholders() {
        Field name = new Field();
        name.setDescription("What is your name");

        Assert.assertThat(name.getResolvedHeader(), is(name.getDescription()));
    }

    /**
     * @verifies that {@link Field#getResolvedHeader()} resolves placeholder with referenced field variable name (column).
     */
    @Test
    public void getResolvedHeader_shouldResolveFieldDescriptionWithReferencedFieldVariableName() {
        Field reference = mock(Field.class);
        when(reference.getId()).thenReturn(1);
        when(reference.getColumn()).thenReturn("name of respondent");
        Field age = new Field();
        age.setId(2);
        age.setDescription(String.format("What is the age of ${%s}", reference.getId()));
        Form form = new Form();
        age.setForm(form);
        form.setFields(Arrays.asList(reference, age));

        Assert.assertThat(age.getResolvedHeader(), is("What is the age of [name of respondent]"));
    }

    /**
     * @verifies that {@link Field#getResolvedHeader()} resolves placeholder with referenced field name when variable name is not specified
     */
    @Test
    public void getResolveHeader_shouldResolveFieldDescriptionWithReferencedFieldName() {
        Field reference = mock(Field.class);
        when(reference.getId()).thenReturn(1);
        when(reference.getName()).thenReturn("1");
        Field age = new Field();
        age.setId(2);
        age.setDescription(String.format("What is the age of ${%s}", reference.getId()));
        Form form = new Form();
        age.setForm(form);
        form.setFields(Arrays.asList(reference, age));

        Assert.assertThat(age.getResolvedHeader(), is("What is the age of [1]"));
    }

    /**
     * @verifies that {@link Field#getResolvedHeader()} multiple placeholders are resolved
     */
    @Test
    public void getResolveHeader_shouldResolveMultiplePlaceholders() {
        Field reference1 = mock(Field.class);
        when(reference1.getId()).thenReturn(1);
        when(reference1.getName()).thenReturn("1");
        Field reference2 = mock(Field.class);
        when(reference2.getId()).thenReturn(2);
        when(reference2.getColumn()).thenReturn("B");
        Field reference3 = mock(Field.class);
        when(reference3.getId()).thenReturn(3);
        when(reference3.getName()).thenReturn("3");
        Field total = new Field();
        total.setId(4);
        total.setDescription(String.format("What is the sum of ${%s}, ${%s} and ${%s}", reference1.getId(), reference2.getId(), reference3.getId()));
        Form form = new Form();
        total.setForm(form);
        form.setFields(Arrays.asList(reference1, reference2, reference3, total));

        Assert.assertThat(total.getResolvedHeader(), is("What is the sum of [1], [B] and [3]"));
    }    

}